//
//  PQActivityPostCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 2/21/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Post;
@class EventModel;
@class PQActivityPostCell;

NS_ASSUME_NONNULL_BEGIN

@protocol PQActivityPostCellDelegate <NSObject>

- (void)didTapUserArea:(PQActivityPostCell *)cell;
- (void)didTapCell:(PQActivityPostCell *)cell;

@end

@interface PQActivityPostCell : UICollectionViewCell

@property (nullable, strong, nonatomic) EventModel *event;
@property (nullable, weak, nonatomic) id<PQActivityPostCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
