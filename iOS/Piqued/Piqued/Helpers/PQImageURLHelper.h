//
//  PQImageURLHelper.h
//  Piqued
//
//  Created by Matt Mo on 5/7/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    PQImageSizeSmall = 0,
    PQImageSizeMedium, 
    PQImageSizeLarge
} PQImageSize;

@interface PQImageURLHelper : NSObject

+ (NSString *)adjustImageURLString: (NSString*) originalURL toSize: (PQImageSize)size;

@end
