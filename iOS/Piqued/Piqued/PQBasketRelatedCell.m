//
//  PQBasketRelatedCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQBasketRelatedCell.h"
#import "PQBasketRelatedButtonCell.h"

@interface PQBasketRelatedCell() <UICollectionViewDelegate, UICollectionViewDataSource>


@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation PQBasketRelatedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    NSString * nibName = NSStringFromClass([PQBasketRelatedButtonCell class]);
    [self.collectionView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellWithReuseIdentifier:nibName];
    
    [self.collectionView setContentInset:UIEdgeInsetsMake(0, 10, 0, 10)];
}

+ (int) estimatedHeight
{
    return 105;
}

- (void) reloadData
{
    if ([self.basketDelegate getYelpLink] != NULL) {
        [self.collectionView reloadData];
    }
    
    if ([self.basketDelegate getFSLink] != NULL) {
        [self.collectionView reloadData];
    }
}

#pragma mark UICollectionView Delegate Methods

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return numCount;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([self hasLinkOfType:(BasketRelatedType) section]) {
        return 1;
    } else {
        return 0;
    }
}

- (BOOL) hasLinkOfType:(BasketRelatedType) type
{
    switch (type) {
        case Yelp:
            return [self.basketDelegate getYelpLink] != NULL;
        case Zagat:
            return NO;
        case FourSquare:
            return [self.basketDelegate getFSLink] != NULL;
        case TripAdvisor:
            return NO;
        default:
            return NO;
    }
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PQBasketRelatedButtonCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PQBasketRelatedButtonCell class]) forIndexPath:indexPath];
 
    switch ((BasketRelatedType) indexPath.section) {
        case Yelp:
            [cell setType:Yelp];
            [cell setLinkUrl:[self.basketDelegate getYelpLink]];
            break;
        case Zagat:
            [cell setType:Zagat];
            break;
        case FourSquare:
            [cell setType:FourSquare];
            [cell setLinkUrl:[self.basketDelegate getFSLink]];
            break;
        case TripAdvisor:
            [cell setType:TripAdvisor];
            break;
        default:
            break;
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self hasLinkOfType:(BasketRelatedType) indexPath.section]) {
        return CGSizeMake(50, 50);
    } else {
        return CGSizeZero;
    }
}


@end
