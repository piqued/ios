//
//  PQUserListViewController.h
//  Piqued
//
//  Created by Matt Mo on 9/10/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PQUserListViewController : UIViewController

+ (NSString *)segueIdentifier;

@property (nonatomic) PQFollowType userListType;
@property (nonatomic, copy) User *user;

@end
