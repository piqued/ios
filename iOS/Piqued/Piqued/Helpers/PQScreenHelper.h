//
//  PQScreenHelper.h
//  Piqued
//
//  Created by Kenny M. Liou on 6/13/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQScreenHelper : NSObject

+ (CGFloat) getScreenWidth;
+ (CGFloat) getScreenHeight;
@end
