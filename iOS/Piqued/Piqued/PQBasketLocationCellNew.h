//
//  PQBasketLocationCellNew.h
//  Piqued
//
//  Created by Kenny M. Liou on 8/6/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQBasketBaseCell.h"

@interface PQBasketLocationCellNew : PQBasketBaseCell

+ (int) estimatedHeight;

@end
