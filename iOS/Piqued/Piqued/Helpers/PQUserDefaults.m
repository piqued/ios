//
//  PiquedUserDefault.m
//  Piqued
//
//  Created by Kenny M. Liou on 10/18/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQUserDefaults.h"
#import "Constants.h"
#import "PQEnvironment.h"

@implementation PQUserDefaults

+ (NSUserDefaults *) getAppUserDefaults
{
    static NSUserDefaults * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NSUserDefaults alloc] initWithSuiteName:PQEnvironment.appGroupName];
    });
    
    return instance;
}

+ (BOOL) isUserLoggedIn
{
    User * user = [PQUserDefaults getUser];
    
    return user != NULL;
}

+ (BOOL) isUserFinishedWelcomeScreenMain
{
    NSInteger viewed = [self getIntegerWithKey:kWelcomeScreenViewed];
    
    return viewed != 0;
}

+ (void) setUserFinishedWelcomeScreen:(BOOL) value
{
    [self setKey:kWelcomeScreenViewed withInteger:value? 1: 0];
}

+ (void) setKey:(NSString *) key withString:(NSString *) string
{
    [[self getAppUserDefaults] setObject:string forKey:key];
    [[self getAppUserDefaults] synchronize];
}

+ (void) setKey:(NSString *) key withInteger:(NSInteger) integer
{
    [[self getAppUserDefaults] setInteger:integer forKey:key];
    [[self getAppUserDefaults] synchronize];
}

+ (NSString *) getStringWithKey:(NSString *) key
{
    return [[self getAppUserDefaults] stringForKey:key];
}

+ (NSInteger) getIntegerWithKey:(NSString *) key
{
    return [[self getAppUserDefaults] integerForKey:key];
}

+ (void) setUser:(User *)user
{
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:user];
    
    NSUserDefaults * ud = [self getAppUserDefaults];
    
    [ud removeObjectForKey:kUserInfo];
    [ud setObject:encodedObject forKey:kUserInfo];
    [ud synchronize];
}

+ (NSString *)getAPNSDeviceToken
{
    return [self.getAppUserDefaults objectForKey:kPQUserDefaultsKeyAPNSDeviceToken];
}

+ (void)setAPNSDeviceToken:(NSString *)deviceToken
{
    NSUserDefaults * defaults = [self getAppUserDefaults];
    [defaults setObject:deviceToken forKey:kPQUserDefaultsKeyAPNSDeviceToken];
    [defaults synchronize];
}

+ (User *) getUser
{
    NSData * encodedObject = [[self getAppUserDefaults] objectForKey:kUserInfo];
    User * user = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    
    return user;
}

+ (User *)updateUser:(User *)updatedUser
{
    User *user = [PQUserDefaults getUser];
    [updatedUser mergeValueForKey:NSStringFromSelector(@selector(secret)) fromModel:user];
    [updatedUser mergeValueForKey:NSStringFromSelector(@selector(token)) fromModel:user];
    
    [PQUserDefaults setUser:updatedUser];
    return updatedUser;
}

+ (void) setKey:(NSString *)key withEncodedArrayObject:(NSObject *) array
{
    NSUserDefaults * ud = [self getAppUserDefaults];
    [ud setObject:array forKey:key];
    [ud synchronize];
}

+ (NSData *) getEncodedArrayObjectWithKey:(NSString *)key
{
    return [[self getAppUserDefaults] objectForKey:key];
}

+ (BOOL) bookmarkListChanged
{
    NSNumber * changed = [[self getAppUserDefaults] objectForKey:kBookmarkListModified];
    
    return changed.boolValue;
}

+ (void) setBookmarkListChanged:(BOOL)changed
{
    [[self getAppUserDefaults] setObject:[NSNumber numberWithBool:changed] forKey:kBookmarkListModified];
    
    if (changed) {
        if ([NSThread isMainThread]) {
            [PQUserDefaults sendNotificationWithKey:kBookmarkListModified];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [PQUserDefaults sendNotificationWithKey:kBookmarkListModified];
            });
        }

    }
}

+ (void) setInstagramRequestSeen
{
    [[self getAppUserDefaults] setBool:YES forKey:PQDefaultsInstagramRequestSeen];
    [[self getAppUserDefaults] synchronize];
}

+ (BOOL) hasInstagramRequestBeenSeen
{
    return [[self getAppUserDefaults] boolForKey:PQDefaultsInstagramRequestSeen];
}

+ (void) sendNotificationWithKey:(NSString *) key
{
    [[NSNotificationCenter defaultCenter] postNotificationName:key object:NULL];
}

+ (NSArray *) getArrayForKey:(NSString *)key
{
    return [[self getAppUserDefaults] objectForKey:key];
}

+ (void) setArray:(NSArray *) array forKey:(NSString *)key
{
    [[self getAppUserDefaults] setObject:array forKey:key];
}

+ (void) removeObjectForKey:(nonnull NSString *)key
{
    [[self getAppUserDefaults] removeObjectForKey:key];
}

@end
