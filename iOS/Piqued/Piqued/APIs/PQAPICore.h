//
//  PQAPICore.h
//  Piqued
//
//  Created by Kenny M. Liou on 8/31/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface PQAPICore : NSObject

+ (instancetype) sharedInstance;

- (void) onNetworkStart;
- (void) onNetworkEnd;

- (void) performPost:(NSURL *)url data:(NSDictionary *)data callback:(void (^)(NSError * error, NSDictionary * responseData))callback;

/**
 Perform Delete

 @param url      url to perform delete
 @param data     any data that is required to be passed to server
 @param callback callback of delete
 */
- (void) performDelete:(NSURL *)url callback:(void (^)(NSError *error, NSDictionary * responseData))callback;

- (void) loginWithData:(NSDictionary *) loginInfo andUrl:(NSString *) url withCallback:(void (^)(User * user, NSError * error))callback;
- (void) createAccount:(NSDictionary *) createAccountInfo andUrl:(NSString *) url withCallback:(void (^)(User * user, NSError * error))callback;

@end
