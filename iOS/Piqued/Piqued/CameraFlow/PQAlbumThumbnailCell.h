//
//  PQAlbumThumbnailCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 11/9/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PQAlbumThumbnailCell : UICollectionViewCell

- (void) setImage:(UIImage *)thumbnailImage;

@end
