//
//  PQKeyChain.m
//  Piqued
//
//  Created by Matt Mo on 1/6/18.
//  Copyright © 2018 First Brew. All rights reserved.
//

#import "PQKeyChain.h"
#import <sys/utsname.h> // import it in your header or implementation file.

static NSString * const kPQKeyChainKeyDeviceModel = @"piqued.io.keychain.deviceModel";
static NSString * const kPQKeyChainKeyDeviceId = @"piqued.io.keychain.uniqueDeviceId";

@import UICKeyChainStore;

@interface PQKeyChain()

@property (nonatomic, assign) BOOL isFirstInstall;
@property (nonatomic, assign) BOOL hasDeviceChanged;

@end

@implementation PQKeyChain

+ (instancetype) sharedKeyChain
{
    static PQKeyChain *keyChain;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        keyChain = [[self alloc] init];
    });
    
    return keyChain;
}

/*
 * Verify Device Id
 * Device Id should be unique for lifetime of device
 *
 * Even if user uninstalls app, and reinstalls app, device id
 * should stay unique
 *
 * If no device id exists, this is the first install.
 * If device id exists, but device model differs from current model,
 * user backup restored.
 * If device id exists and device model matches, nothing has changed.
 */
- (void)verifyDeviceId
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *currentDeviceModel = [NSString stringWithCString:systemInfo.machine
                                                      encoding:NSUTF8StringEncoding];
    NSString *storedDeviceId = [PQKeyChain getDeviceId];
    NSString *storedDeviceModel = [PQKeyChain getDeviceModel];
    
    if (storedDeviceId.length == 0) {
        self.isFirstInstall = YES;
    } else if (![storedDeviceModel isEqualToString:currentDeviceModel]) {
        self.hasDeviceChanged = YES;
    }
    
    if (self.isFirstInstall || self.hasDeviceChanged) {
        [PQKeyChain setDeviceModel];
        [PQKeyChain setDeviceId:[NSUUID.UUID UUIDString]];
    }
}

+ (void)setDeviceModel
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *deviceModel = [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
    [UICKeyChainStore setString:deviceModel forKey:kPQKeyChainKeyDeviceModel];
}

+ (NSString *)getDeviceModel
{
    return [UICKeyChainStore stringForKey:kPQKeyChainKeyDeviceModel] ?: @"";
}

+ (void)setDeviceId:(NSString *)deviceId
{
    [UICKeyChainStore setString:deviceId forKey:kPQKeyChainKeyDeviceId];
}

+ (NSString *)getDeviceId
{
    return [UICKeyChainStore stringForKey:kPQKeyChainKeyDeviceId] ?: @"";
}

@end
