//
//  PQCommentViewController.h
//  Piqued
//
//  Created by Kenny M. Liou on 6/29/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PQCommentViewController : UIViewController

@property (strong, nonatomic) Post * post;
@property (strong, nonatomic) NSArray * comments;

@end
