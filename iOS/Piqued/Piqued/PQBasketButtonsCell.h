//
//  PQBasketButtonsCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 12/10/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQBasketBaseCell.h"

@interface PQBasketButtonsCell : PQBasketBaseCell

+ (int) estimatedHeight;

@end
