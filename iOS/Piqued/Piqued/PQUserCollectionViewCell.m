//
//  PQUserCollectionViewCell.m
//  Piqued
//
//  Created by Matt Mo on 9/11/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQUserCollectionViewCell.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PQUserDefaults.h"

@interface PQUserCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UIButton *followButton;

@property (strong, nonatomic) UITapGestureRecognizer *tapGestureRecognizer;

@end

@implementation PQUserCollectionViewCell

+ (NSString *)nibName
{
    return NSStringFromClass([self class]);
}

+ (UINib *)nib
{
    return [UINib nibWithNibName:[self nibName] bundle:nil];
}

+ (NSString *)reuseIdentifier
{
    return [NSString stringWithFormat:@"%@%@", [self nibName], @"ReuseIdentifier"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openProfile)];
        [self.contentView addGestureRecognizer:self.tapGestureRecognizer];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setUser:(User *)user
{
    _user = user;
    
    [self.userLabel setText:user.userName];
    
    NSString * imageUrl = user.profileImage;
    if (imageUrl && imageUrl.length > 0) {
        [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:kBlankProfileImage]];
    } else {
        [self.profileImageView setImage:[UIImage imageNamed:kBlankProfileImage]];
    }
    
    User *currentUser = [PQUserDefaults getUser];
    if (currentUser.userId == user.userId) {
        self.followButton.hidden = YES;
    }
    
    [self.followButton setSelected:user.isFollowed];
}

- (IBAction)followButtonPressed:(UIButton *)sender
{
    sender.selected = !sender.selected;
    [[PQNetworking sharedInstance] follow:sender.isSelected user:self.user];
    [self.delegate didPressFollowButton:sender];
}

- (void)openProfile
{
    [self.delegate openProfile:self.user.userId];
}

- (void)prepareForReuse
{
    self.profileImageView.image = nil;
    self.userLabel.text = @"";
}

@end
