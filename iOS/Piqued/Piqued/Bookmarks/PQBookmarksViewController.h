//
//  PQBookmarksViewController.h
//  Piqued
//
//  Created by Kenny M. Liou on 11/7/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQMainTabBarController.h"

@interface PQBookmarksViewController : UIViewController <PQTabBarControllerTabProtocol>

@end
