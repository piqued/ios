//
//  PQFourSquareTableDataSource.h
//  Piqued
//
//  Created by Matt Mo on 10/14/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQFourSquareTableDataSource : NSObject<UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) UISearchBar *searchBar;
@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, strong) NSString *locationName;
@property (nonatomic, strong) NSArray *data;

/*
 Initialization
 */
- (instancetype)initWithTableView:(UITableView *)tableView;
- (void)setupTableView:(UITableView *)tableView;
- (void)loadWithInitialResults:(NSArray *)data location:(CLLocation *)location;

/*
 Table View methods
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

/*
 Update results
 */
- (void)updateSearchResultsWithPlaceName:(NSString *)placeName;
- (void)updateSearchResultsWithLocation:(CLLocation *)location;


/*
 Array Accessor
 */
- (id)objectAtIndexedSubscript:(NSUInteger)index; //getter
- (void)setObject:(id)obj atIndexedSubscript:(NSUInteger)index; //setter

@end
