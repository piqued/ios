//
//  PQImageURLHelper.m
//  Piqued
//
//  Created by Matt Mo on 5/7/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQImageURLHelper.h"

@implementation PQImageURLHelper

+ (NSString *)adjustImageURLString: (NSString*) originalURL toSize: (PQImageSize)size
{
    if ([originalURL containsString:@"imgur"]) {
        switch (size) {
            case PQImageSizeSmall:
            {
                return [originalURL stringByReplacingOccurrencesOfString:@".jpg" withString:@"s.jpg"];
            }
            case PQImageSizeMedium:
            {
                return [originalURL stringByReplacingOccurrencesOfString:@".jpg" withString:@"m.jpg"];
            }
            case PQImageSizeLarge:
            {
                return [originalURL stringByReplacingOccurrencesOfString:@".jpg" withString:@"l.jpg"];
            }
                
            default:
                break;
        }
    }
    
    return originalURL;
}

@end
