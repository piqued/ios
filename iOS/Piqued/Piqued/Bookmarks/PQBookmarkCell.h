//
//  PQBookmarkCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 11/7/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"

@interface PQBookmarkCell : UITableViewCell

- (void) setPost:(Post *)post;

@end
