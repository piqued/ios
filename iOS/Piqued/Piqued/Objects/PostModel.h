//
//  PostModel.h
//  Piqued
//
//  Created by Matt Mo on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "BaseModel.h"

@interface PostModel : BaseModel

@property (strong, nonatomic, readonly) NSNumber *postId;
@property (strong, nonatomic, readonly) NSString *imageUrl;
@property (strong, nonatomic, readonly) NSString *title;
@property (strong, nonatomic, readonly) NSString *desc;
@property (strong, nonatomic, readonly) NSString *username;
@property (strong, nonatomic, readonly) NSString *userimage;
@property (strong, nonatomic, readonly) NSString *address;
@property (strong, nonatomic, readonly) NSString *foursquareVenueID;
@property (nonatomic, readonly) NSUInteger createdAtTimeStamp;
@property (nonatomic, readonly) CLLocationDegrees lat;
@property (nonatomic, readonly) CLLocationDegrees lng;
@property (nonatomic) BOOL bookmarked;

// Helper properties
@property (strong, nonatomic, readonly) NSDate *timeStamp;

@end
