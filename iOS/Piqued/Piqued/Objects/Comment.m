//
//  Comment.m
//  Piqued
//
//  Created by Kenny M. Liou on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "Comment.h"

@interface Comment()

@property (strong, nonatomic) NSNumber * commentId;
@property (nonatomic) double commenterId;
@property (strong, nonatomic) NSString * message;
@property (strong, nonatomic) NSDate * timeStamp;
@property (strong, nonatomic) NSString * userProfileImage;
@property (strong, nonatomic) NSString * userName;

@end
@implementation Comment

+ (instancetype) createCommentWithDictionary:(NSDictionary *)dic
{
    Comment * comment = [[Comment alloc] init];
    NSDictionary * userInfo = [dic objectForKey:@"user"];
    
    NSNumber * commentId = (NSNumber *) [dic objectForKey:@"id"];
    NSNumber * commenterId = (NSNumber *) [userInfo objectForKey:@"id"];
    NSString * userName = [userInfo objectForKey:@"name"];
    NSString * userProfileImage = [userInfo objectForKey:@"profileImage"];
    
    if ([userProfileImage isEqual:[NSNull null]]) {
        userProfileImage = nil;
    }
    
    int timeStamp = [(NSNumber *)[dic objectForKey:@"createdAt"] intValue];
    
    NSString * message = (NSString *) [dic objectForKey:@"message"];
    if ([message isEqual:[NSNull null]]) {
        message = nil;
    }
    
    [comment setCommentId:commentId];
    [comment setMessage:message];
    [comment setUserName:userName];
    [comment setUserProfileImage:userProfileImage];
    [comment setTimeStamp:[NSDate dateWithTimeIntervalSince1970:timeStamp]];
    [comment setCommenterId:commenterId.doubleValue];
    
    return comment;
}

@end
