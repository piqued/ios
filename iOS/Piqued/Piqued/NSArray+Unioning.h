//
//  NSArray+Unioning.h
//  Piqued
//
//  Created by Matt Mo on 9/3/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Unioning)

- (NSArray *)unionWithArray:(NSArray *)otherArray;

@end
