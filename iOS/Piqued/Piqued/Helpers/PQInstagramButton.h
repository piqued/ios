//
//  PQInstagramButton.h
//  Piqued
//
//  Created by Matt Mo on 5/14/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQInstagramManager.h"

@protocol PQInstagramLoginButtonDelegate;

@interface PQInstagramButton : UIButton

@property (weak, nonatomic) id<PQInstagramLoginButtonDelegate> delegate;

+ (instancetype)button;

@end

@protocol PQInstagramLoginButtonDelegate <NSObject>

- (void)loginButton:(PQInstagramButton *)loginButton didCompleteWithResult:(BOOL)success error:(NSError *)error;

@end
