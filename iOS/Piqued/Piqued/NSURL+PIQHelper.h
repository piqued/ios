//
//  NSURL+PIQHelper.h
//  Piqued
//
//  Created by admin on 3/13/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (PIQHelper)

NS_ASSUME_NONNULL_BEGIN

+ (nullable instancetype)URLWithURLEncodingString:(NSString *)nonURLEncodedString;

NS_ASSUME_NONNULL_END
@end
