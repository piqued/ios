//
//  PQEditProfileTableViewCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 8/20/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQEditProfileTableViewCell.h"
#import "PQMapSelectionHelper.h"

@interface PQEditProfileTableViewCell () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *cellIcon;
@property (weak, nonatomic) IBOutlet UITextField *valueText;

@property (weak, nonatomic) IBOutlet UIButton *passwordButton;
@property (weak, nonatomic) IBOutlet UIButton *mapButton;

@property (nonatomic) EditProfileType type;

@end

@implementation PQEditProfileTableViewCell

+ (int) estimatedHeight
{
    return 44;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.valueText addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void) setCellType:(EditProfileType) type
{
    UIImage * image;
    
    if (type == EditProfile_DefaultMap && ![PQMapSelectionHelper hasOtherMaps]) {
        UIImage * disabledIcon = [UIImage imageNamed:[self getIconName:type]];
        image = [disabledIcon imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    } else {
        image = [UIImage imageNamed:[self getIconName:type]];
    }
    
    [self.cellIcon setImage:image];
    
    [self setupValue:type];
    [self setKeyboardType:type];
    [self setType:type];
}

- (void) setInfoValue:(NSString *) value
{
    [self.valueText setText:value];
}

- (void) setKeyboardType:(EditProfileType) type
{
    switch (type) {
        case EditProfile_Name:
        case EditProfile_Location:
        case EditProfile_Bio:
            self.valueText.keyboardType = UIKeyboardTypeDefault;
            break;
        case EditProfile_Email: {
            self.valueText.keyboardType = UIKeyboardTypeEmailAddress;
        }
        case EditProfile_Password:
        case EditProfile_Phone:
        case EditProfile_DefaultMap:
            self.valueText.keyboardType = UIKeyboardTypeDefault;
            break;
    }
}

- (NSString *) getIconName:(EditProfileType) type
{
    switch (type) {
        case EditProfile_Name:
            return @"profileName";
        case EditProfile_Location:
            return @"profileLocation";
        case EditProfile_Bio:
            return @"profileBio";
        case EditProfile_Email:
            return @"profileEmail";
        case EditProfile_Password:
            return @"profilePassword";
        case EditProfile_Phone:
            return @"profilePhone";
        case EditProfile_DefaultMap:
            return @"profileMaps";
    }
}

- (void) setupValue:(EditProfileType) type
{
    User * user = [self.delegate getUser];
    NSString * placeHolder;
    NSString * value;
    
    [self.valueText setHidden:NO];
    [self.passwordButton setHidden:YES];
    [self.mapButton setHidden:YES];
    
    switch (type) {
        case EditProfile_Name:
            placeHolder = @"Name";
            value = user.userName;
            break;
        case EditProfile_Location:
            placeHolder = @"Location";
            value = @""; // we don't have location yet
            break;
        case EditProfile_Bio:
            placeHolder = @"Personal bio/statement";
            value = @"";
            break;
        case EditProfile_Email:
            placeHolder = @"Email";
            value = user.email;
            break;
        case EditProfile_Password:
            [self.passwordButton setHidden:NO];
            [self.valueText setHidden:YES];
            break;
        case EditProfile_Phone:
            placeHolder = @"add a phone number";
            break;
        case EditProfile_DefaultMap:
            [self.mapButton setHidden:NO];
            [self.valueText setHidden:YES];
            break;
    }
    
    if (type == EditProfile_DefaultMap) {
        if (![PQMapSelectionHelper hasOtherMaps]) {
            [self.mapButton setEnabled:NO];
        }
    }
    
    [self.valueText setPlaceholder:placeHolder];
    [self.valueText setText:value];
}

#pragma mark TextField method

- (void) updateModification
{
    switch (self.type) {
        case EditProfile_Name:
            [self.delegate setNewName:self.valueText.text];
            break;
        case EditProfile_Email:
            [self.delegate setNewEmail:self.valueText.text];
            break;
        default:
            break;
    }

}

- (IBAction)editEnded:(UITextField *)sender {
    
    [self updateModification];
}

- (IBAction)showEditPassword:(id)sender {
    [self.delegate showEditPassword];
}

- (IBAction)editDefaultMap:(UIButton *)sender {
    [self.delegate showEditMap];
}

- (void) textFieldDidChange:(UITextField *) textField {
    
    [self updateModification];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.valueText.keyboardType = UIKeyboardTypeDefault;
}

@end
