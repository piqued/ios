//
//  PQHashTagManager.m
//  Piqued
//
//  Created by Kenny M. Liou on 6/11/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQHashTagManager.h"
#import "PQHashTagRecord.h"

@interface PQHashTagManager()

# pragma mark properties
@property (nonatomic, strong) NSMutableArray * hashTagPool;

@end

@implementation PQHashTagManager

# pragma mark Static Methods
+ (instancetype) sharedInstance {
    static PQHashTagManager * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        [instance reloadData];
    });
    
    return instance;
}

- (instancetype) init
{
    self = [super init];
    if (self) {
//        _hashTagPool = [NSMutableDictionary dictionary];
    }
    
    return self;
}

- (NSMutableArray *) hashTagPool
{
    if (_hashTagPool == nil) {
        _hashTagPool = [NSMutableArray array];
        
        NSString* fileRoot = [[NSBundle mainBundle]
                              pathForResource:@"hashTags" ofType:@"txt"];
        // read everything from text
        NSString* fileContents =
        [NSString stringWithContentsOfFile:fileRoot encoding:NSUTF8StringEncoding error:nil];
        
        // first, separate by new line
        NSArray* allLinedStrings = [fileContents componentsSeparatedByCharactersInSet: [NSCharacterSet newlineCharacterSet]];
        
        for (NSString * hashTag in allLinedStrings) {
            [_hashTagPool addObject:[[PQHashTagRecord alloc] initWithHashTag:hashTag]];
        }
    }
    
    return _hashTagPool;
}

# pragma mark Public Methods

- (NSArray *) getHashTag:(NSString *) keyword
{
    if (keyword == nil || keyword.length < 1) {
        return [self.hashTagPool subarrayWithRange:NSMakeRange(0, 20)];
    } else {
        NSMutableArray * result = [NSMutableArray array];
        
        
        for (PQHashTagRecord * record in self.hashTagPool) {
            
            if ([record.hashTag hasPrefix:keyword]) {
                [result addObject:record];
            }
            
            if ([result count] > 15) {
                break;
            }
        }
        
        return result;
    }
}

# pragma mark Helpers

- (void) reloadData
{
    // TODO: load hash tags from disk to memory
//    @synchronized (self) {
//        if ([NSThread isMainThread]) {
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                [self resetAllData];
//            });
//        } else {
//            [self resetAllData];
//        }
//    }
}

- (void) resetAllData
{
//    [self.viewedHashTag removeAllObjects];
//    [self.usedHashTag removeAllObjects];
//    [self.hashTagPool removeAllObjects];
}

- (void) appWillResume
{
    // TODO: make sure we reload data
}

- (void) appWillpause
{
    // TODO: save current data from memory to disk
}
@end
