//
//  PQExplorerContainerViewController.m
//  Piqued
//
//  Created by Matt Mo on 10/27/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQExplorerContainerViewController.h"
#import "PQExplorerViewController.h"
#import "PQAnalyticHelper.h"

@interface PQExplorerContainerViewController ()

@property (strong, nonatomic) NSString *currentSegueIdentifier;
@property (strong, nonatomic) PQGridViewController * gridViewController;
@property (strong, nonatomic) PQMapViewController * mapViewController;

@end

@implementation PQExplorerContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.currentSegueIdentifier == nil) {
        self.currentSegueIdentifier = [PQGridViewController segueIdentifier];
        [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self viewIsShowing:YES];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self viewIsShowing:NO];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:[PQGridViewController segueIdentifier]]) {
        if (self.gridViewController == nil) {
            self.gridViewController = segue.destinationViewController;
            self.explorerController.gridViewController = self.gridViewController;
            self.gridViewController.delegate = self.explorerController;
            [self addController:self.gridViewController];
        } else {
            [self.view sendSubviewToBack:self.mapViewController.view];
            [self.view bringSubviewToFront:self.gridViewController.view];
        }
        [self.gridViewController viewSwitched];
        [self.mapViewController showMiniBasket:NO];
    } else if ([segue.identifier isEqualToString:[PQMapViewController segueIdentifier]]) {
        if (self.mapViewController == nil) {
            self.mapViewController = segue.destinationViewController;
            self.explorerController.mapViewController = self.mapViewController;
            self.mapViewController.delegate = self.explorerController;
            [self addController:self.mapViewController];
        } else {
            [self.view sendSubviewToBack:self.gridViewController.view];
            [self.view bringSubviewToFront:self.mapViewController.view];
        }
    }

}

#pragma mark - Public methods

+ (NSString *)segueIdentifier {
    return @"ContainerView";
}

- (void) viewIsShowing:(BOOL) isShowing
{
    PQAnalyticHelper * helper = [PQAnalyticHelper sharedInstance];
    
    if ([self.currentSegueIdentifier isEqualToString:[PQMapViewController segueIdentifier]]) {
        if (isShowing) {
            [helper viewWillAppear:kHomeMap];
        } else {
            [helper viewWillDisappear:kHomeMap];
        }
    } else if ([self.currentSegueIdentifier isEqualToString:[PQGridViewController segueIdentifier]]) {
        if (isShowing) {
            [helper viewWillAppear:kHomeList];
        } else {
            [helper viewWillDisappear:kHomeList];
        }
    }
}

- (void)changeToViewWithSegueIdentifier:(NSString *)segueId
{
    [self viewIsShowing:NO];
    self.currentSegueIdentifier = segueId;
    [self viewIsShowing:YES];
    
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:self];
}

#pragma mark - Helpers

- (void) addController:(UIViewController *)subController
{
    if (subController != nil) {
        [self addChildViewController:subController];
        subController.view.frame = self.view.bounds;
        [self.view addSubview:subController.view];
        [self.view bringSubviewToFront:subController.view];
        [self didMoveToParentViewController:self];
    }
}

- (void) viewSwitched
{
    
}

@end
