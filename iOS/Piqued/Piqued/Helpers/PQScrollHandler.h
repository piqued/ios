//
//  PQScrollHandler.h
//  Piqued
//
//  Created by Kenny M. Liou on 5/21/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    UP,
    DOWN
} PQScrollDirection;

@protocol PQScrollHandlerDelegate <NSObject>

- (void) onScroll:(PQScrollDirection) direction;

@end

@interface PQScrollHandler : NSObject

@property (weak, nonatomic) id<PQScrollHandlerDelegate> delegate;

- (void) onViewScrolled:(UIScrollView *)scrollView;

@end
