//
//  PQEditProfileHeader.h
//  Piqued
//
//  Created by Kenny M. Liou on 8/20/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#ifndef PQEditProfileHeader_h
#define PQEditProfileHeader_h

#import "User.h"

@protocol PQEditProfileHeaderDelegate <NSObject>

- (User *) getUser;
- (void) getUserImage:(void (^)(UIImage * userImage))callbackBlock;

- (void) setNewName:(NSString *) newName;
- (void) setNewEmail:(NSString *) newEmail;

- (void) showEditPassword;
- (void) showEditMap;
- (void) selectImage;

@end

#endif /* PQEditProfileHeader_h */
