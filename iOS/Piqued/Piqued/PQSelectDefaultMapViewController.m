//
//  PQSelectDefaultMapViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 9/3/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQSelectDefaultMapViewController.h"
#import "PQMapSelectionHelper.h"

@interface PQSelectDefaultMapViewController () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;


@end

@implementation PQSelectDefaultMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    PQSupportedMaps defaultMap = [PQMapSelectionHelper getDefaultMap];
    NSArray<NSNumber *> * supportedMaps = [PQMapSelectionHelper getCurrentlyInstalledMaps];
    
    int selectedMap = 0; // default is 0
    
    for (int i = 0; i < [supportedMaps count]; i++) {
        if (defaultMap == supportedMaps[i].intValue) {
            selectedMap = i;
            break;
        }
    }
    
    [self.pickerView selectRow:selectedMap inComponent:0 animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[PQMapSelectionHelper getCurrentlyInstalledMaps] count];
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSArray * allmapName = [PQMapSelectionHelper getCurrentlyInstalledMaps];
    
    NSNumber * mapName = allmapName[row];
    
    return [PQMapSelectionHelper getMapName:mapName.intValue];
}

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSArray * allmapName = [PQMapSelectionHelper getCurrentlyInstalledMaps];
    
    NSNumber * selectedMap = allmapName[row];
    
    [PQMapSelectionHelper setDefaultMap:selectedMap.intValue];
}

@end
