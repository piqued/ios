//
//  PQCameraCollectionViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 2/27/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQCameraCollectionViewController.h"
#import "PQCameraPreviewCell.h"

static NSString * const kCellName = @"PQCameraPreviewCell";
static NSString * const kEmptyCellName = @"PQCameraEmptyPreviewCell";

@interface PQCameraCollectionViewController()

@property (strong, nonatomic) PQCameraPreviewCell * previewCell;
@property (nonatomic) BOOL startPreviewImmediately;

@end

@implementation PQCameraCollectionViewController

- (instancetype) init
{
    self = [super init];
    if (self) {
        _startPreviewImmediately = NO;
    }
    
    return self;
}

+ (NSString *) getCellName
{
    return kCellName;
}

+ (NSString *) getEmptyCellName
{
    return kEmptyCellName;
}


- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 2;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionViewCell * result;
    PQCameraPreviewCell * cell;
    
    switch (indexPath.row) {
        case 0:
            result = [collectionView dequeueReusableCellWithReuseIdentifier:kEmptyCellName forIndexPath:indexPath];
            break;
        case 1:
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellName forIndexPath:indexPath];
            [cell setHandler:self.handler];
            [self setPreviewCell:cell];
            if (self.startPreviewImmediately) {
                [self setStartPreviewImmediately:NO];
                [self.previewCell startCamera];
            }
            result = cell;
            break;
    }
    
    return result;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake(0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return collectionView.frame.size;
}

- (void) start
{
    if (self.previewCell) {
        [self.previewCell startCamera];
    } else {
        [self setStartPreviewImmediately:YES];
    }
}

- (void) stop
{
    [self.previewCell stopCamera];
}

- (void) takePhoto
{
    [self.previewCell takePhoto];
}

@end
