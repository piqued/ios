//
//  PQClusterAnnotationView.m
//  Piqued
//
//  Created by Matt Mo on 12/19/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQClusterAnnotationView.h"
#import <SDWebImage/SDWebImageManager.h>
#import "UIColor+PIQHex.h"
#import "PQImageCacheDelegate.h"

CGPoint TBRectCenter(CGRect rect)
{
    return CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
}

CGRect TBCenterRect(CGRect rect, CGPoint center)
{
    CGRect r = CGRectMake(center.x - rect.size.width/2.0,
                          center.y - rect.size.height/2.0,
                          rect.size.width,
                          rect.size.height);
    return r;
}

static CGFloat const TBScaleFactorAlpha = 0.3;
static CGFloat const TBScaleFactorBeta = 0.4;

CGFloat TBScaledValueForValue(CGFloat value)
{
    return 1.0 / (1.0 + expf(-1 * TBScaleFactorAlpha * powf(value, TBScaleFactorBeta)));
}

@interface PQClusterAnnotationView ()
@property (strong, nonatomic) UILabel *countLabel;
@property (strong, nonatomic) UIImageView *thumbnailImageView;
@end

@implementation PQClusterAnnotationView

- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor piq_colorFromHexString:kOrangeColor];
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 70, 70);
        self.layer.cornerRadius = self.frame.size.height / 2.;
        
        [self setupLabel];
        [self setCount:1];
    }
    return self;
}

- (void)setupLabel
{
    if (!_countLabel) {
        _countLabel = [[UILabel alloc] initWithFrame:self.frame];
        _countLabel.backgroundColor = [UIColor piq_colorFromHexString:kOrangeColor];
        _countLabel.clipsToBounds = YES;
        _countLabel.textColor = [UIColor whiteColor];
        _countLabel.textAlignment = NSTextAlignmentCenter;
        _countLabel.adjustsFontSizeToFitWidth = YES;
        _countLabel.numberOfLines = 1;
        _countLabel.font = [UIFont boldSystemFontOfSize:12];
        _countLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        [self addSubview:_countLabel];
    }
}

- (void)setCount:(NSUInteger)count
{
    _count = count;
    
    if (count > 1) {
        CGFloat totalWidth = self.bounds.size.width;
        CGFloat totalHeight = self.bounds.size.height;
        
        CGFloat ratio = 3;
        
        CGRect labelFrame = CGRectMake(totalWidth * ((ratio - 1)/ratio) , totalHeight * ((ratio - 1)/ratio), totalWidth/ratio, totalHeight/ratio);
        self.countLabel.layer.cornerRadius = totalWidth/ratio/2;
        self.countLabel.frame = labelFrame;
        self.countLabel.text = [@(_count) stringValue];
        self.countLabel.hidden = NO;
        
        [self setNeedsDisplay];
    } else {
        self.countLabel.hidden = YES;
    }
}

- (UIImageView *)thumbnailImageView
{
    if (_thumbnailImageView) {
        return _thumbnailImageView;
    }
    
    _thumbnailImageView = [[UIImageView alloc] init];
    _thumbnailImageView.userInteractionEnabled = YES;
    self.thumbnailImageView = _thumbnailImageView;
    [self insertSubview:_thumbnailImageView atIndex:0];
    return _thumbnailImageView;
}

- (void)setImageURL:(NSURL *)imageURL
{
    __block NSURL *thumbnailURL = imageURL;
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:imageURL
                          options:0
                         progress:nil
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
     {
         if (image && finished && [imageURL isEqual:thumbnailURL]) {
             self.thumbnailImageView.image = [self getSymbolForImage:image];
             CGRect frame = CGRectZero;
             frame.size = CGSizeMake(70, 70);
             self.thumbnailImageView.frame = frame;
             self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, frame.size.width, frame.size.height);
         }
     }];
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    BOOL pointInside = NO;
    
    if (CGRectContainsPoint(self.thumbnailImageView.frame, point)) {
        pointInside = YES;
    }
    
    return pointInside;
}

- (UIImage*)getSymbolForImage:(UIImage*)image {
    double maxRadius = (MIN(image.size.width, 70) < 70) ? 70: 70;
    CALayer* imgLayer = [CALayer layer];
    imgLayer.frame = CGRectMake(0,0, maxRadius, maxRadius);
    imgLayer.contents = (id)image.CGImage;
    imgLayer.masksToBounds = YES;
    imgLayer.cornerRadius = maxRadius / 2;
    imgLayer.borderWidth = 2.0f;
    imgLayer.borderColor = [UIColor whiteColor].CGColor;
    CGSize imgSize = CGSizeMake(maxRadius, maxRadius);
    UIGraphicsBeginImageContext(imgSize);
    [imgLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage* newImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(newImg.size, NO, 0.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, newImg.size.width, newImg.size.height);
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    CGContextDrawImage(ctx, area, newImg.CGImage);
    UIImage *pinImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return pinImage;
}

- (void)prepareForReuse
{
    self.thumbnailImageView.image = nil;
    self.count = 0;
}

//- (void)drawRect:(CGRect)rect
//{
//    CGContextRef context = UIGraphicsGetCurrentContext();
//
//    CGContextSetAllowsAntialiasing(context, true);
//    
//    UIColor *outerCircleStrokeColor = [UIColor colorWithWhite:0 alpha:0.25];
//    UIColor *innerCircleStrokeColor = [UIColor whiteColor];
//    UIColor *innerCircleFillColor = [UIColor colorWithRed:(255.0 / 255.0) green:(95 / 255.0) blue:(42 / 255.0) alpha:1.0];
//    
//    CGRect circleFrame = CGRectInset(rect, 4, 4);
//    
//    [outerCircleStrokeColor setStroke];
//    CGContextSetLineWidth(context, 5.0);
//    CGContextStrokeEllipseInRect(context, circleFrame);
//    
//    [innerCircleStrokeColor setStroke];
//    CGContextSetLineWidth(context, 4);
//    CGContextStrokeEllipseInRect(context, circleFrame);
//    
//    [innerCircleFillColor setFill];
//    CGContextFillEllipseInRect(context, circleFrame);
//}

@end
