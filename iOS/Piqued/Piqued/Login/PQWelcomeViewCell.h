//
//  PQWelcomeViewCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 9/5/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICollectionViewCell+NibHelpers.h"

@interface PQWelcomeViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView * imageView;

@end
