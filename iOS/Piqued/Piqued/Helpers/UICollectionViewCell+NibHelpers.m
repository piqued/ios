//
//  UICollectionViewCell+NibHelpers.m
//  Piqued
//
//  Created by Matt Mo on 7/21/18.
//  Copyright © 2018 First Brew. All rights reserved.
//

#import "UICollectionViewCell+NibHelpers.h"

@implementation UICollectionViewCell (NibHelpers)

+ (UINib *)piq_nib {
    return [UINib nibWithNibName:self.piq_nibName bundle:nil];
}

+ (NSString *)piq_nibName {
    return NSStringFromClass(self.class);
}

+ (NSString *)piq_reuseIdentifier {
    return NSStringFromClass(self.class);
}

@end
