//
//  PQMapSelectionHelper.m
//  Piqued
//
//  Created by Kenny M. Liou on 9/3/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQMapSelectionHelper.h"
#import "PQUserDefaults.h"

#define kMapDefault         @"pq_default_map"
#define kAppleMapScheme     @"http://maps.apple.com/"
#define kGoogleMapScheme    @"comgooglemaps://"
#define kWazeMapScheme      @"waze://"
#define kHereMapScheme      @"here-place://"
#define kAppleMapName       @"Apple Map"
#define kGoogleMapName      @"Google Map"
#define kWazeMapName        @"Waze"
#define kHereMapName        @"Here"

@implementation PQMapSelectionHelper

+ (PQSupportedMaps) getDefaultMap
{
    
    PQSupportedMaps defaultMap = (PQSupportedMaps) [PQUserDefaults getIntegerWithKey:kMapDefault];
    
    if (defaultMap != pq_map_UNKNOWN && ![self canOpenMap:defaultMap]) {
        defaultMap = pq_map_UNKNOWN;
        [PQUserDefaults setKey:kMapDefault withInteger:defaultMap];
    }
    
    return defaultMap;
}

+ (NSString *) getMapURLWithPost:(Post *)post
{
    NSString * urlScheme = @"";
    
    PQSupportedMaps defaultMap = [self getDefaultMap];
    
    switch (defaultMap) {
        case pq_map_UNKNOWN:
        case pq_map_END:
            PQLog(@"WARNING: user hasn't setup their default map or their default map is removed and you need to check it!! Other wise the map url scheme is always going to be empty string!!");
            return urlScheme; // WTF? you should at lease check your default map first
        case pq_map_APPLE:
            urlScheme = [self getAppleURLWithPost:post];
            break;
        case pq_map_GOOGLE:
            urlScheme = [self getGoogleURLWithPost:post];
            break;
        case pq_map_WAZE:
            urlScheme = [self getWazeURLWithPost:post];
            break;
        case pq_map_HERE:
            urlScheme = [self getHereURLWithPost:post];
            break;
    }
    
    return urlScheme;
}

+ (NSString *) getAppleURLWithPost:(Post *)post
{
    return [NSString stringWithFormat:@"%@?q=%f,%f&z=10&t=m", kAppleMapScheme, post.lat, post.lng];
}

+ (NSString *) getGoogleURLWithPost:(Post *)post
{
    return [NSString stringWithFormat:@"%@?q=%f,%f&zoom=14", kGoogleMapScheme, post.lat, post.lng];
}

+ (NSString *) getWazeURLWithPost:(Post *)post
{
    return [NSString stringWithFormat:@"%@?ll=%f,%f&navigate=no", kWazeMapScheme, post.lat, post.lng];
}

+ (NSString *) getHereURLWithPost:(Post *)post
{
    return [NSString stringWithFormat:@"%@%f,%f", kWazeMapScheme, post.lat, post.lng];
}

+ (BOOL) hasOtherMaps
{
    for (int i = pq_map_APPLE + 1; i < pq_map_END; i++) {
        if ([self canOpenMap:(PQSupportedMaps)i]) {
            return YES;
        }
    }
    
    return NO;
}

+ (NSArray<NSNumber *> *) getCurrentlyInstalledMaps;
{
    NSMutableArray<NSNumber *> * mapsArray = [NSMutableArray array];
    for (int i = pq_map_UNKNOWN + 1; i < pq_map_END; i++) {
        if ([self canOpenMap:(PQSupportedMaps)i]) {
            
            [mapsArray addObject:[NSNumber numberWithInt:i]];
        }
    }
    
    return mapsArray;
}

+ (NSString *) getMapName:(NSInteger)mapType
{
    NSString * mapName = @"Unknown";
    int _type = (int) mapType;
    
    switch (_type) {
        case pq_map_GOOGLE:
            mapName = kGoogleMapName;
            break;
        case pq_map_APPLE:
            mapName = kAppleMapName;
            break;
        case pq_map_WAZE:
            mapName = kWazeMapName;
            break;
        case pq_map_HERE:
            mapName = kHereMapName;
            break;
        case pq_map_END:
        case pq_map_UNKNOWN:
            // do nothing
            break;
    }
    
    return mapName;
}

+ (void) setDefaultMap:(PQSupportedMaps)map
{
    [PQUserDefaults setKey:kMapDefault withInteger:map];
}


+ (BOOL) canOpenMap:(PQSupportedMaps) mapType
{
    switch(mapType) {
        case pq_map_UNKNOWN:
        case pq_map_END:
            return false;
        case pq_map_APPLE:
            return true; // no shit
        case pq_map_GOOGLE:
            return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:kGoogleMapScheme]];
        case pq_map_WAZE:
            return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:kWazeMapScheme]];
        case pq_map_HERE:
            return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:kHereMapScheme]];
    }
}

@end
