//
//  PQNetworking.h
//  Piqued
//
//  Created by Matt Mo on 7/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post.h"
#import "User.h"
#import <AFNetworking/AFNetworking.h>
#import <Bolts/Bolts.h>

enum {
    PQFollowTypeFollower = 0,
    PQFollowTypeFollowing,
};
typedef NSUInteger PQFollowType;
/*
 * For PQNetworking, we use BFTask to handle callbacks, and avoid callbackhell
 * READ:
 *      - http://blog.dimroc.com/2014/12/17/bolt-framework-promisekit-or-reactive-cocoa-for-async/
 *      - http://iosmanbh.blogspot.com/2016/01/ios-bolts-with-afnetworking-batch.html
 */

@interface PQNetworking : NSObject

@property (nonatomic, strong) AFHTTPSessionManager *manager;

- (instancetype)init NS_UNAVAILABLE NS_DESIGNATED_INITIALIZER;
+ (instancetype)sharedInstance;

#warning TODO - add custom HTTP calls for GET, POST, etc

/*
 * Fetch User Events data
 */
- (BFTask *)fetchEvents;

/**
 Fetch a list of users the @user is either following or followed by
 
 @param type    determines whether or not the list is followers or following
 @param user    provides an id for endpoint
 
 @return BFTask
         .result    should contain a list of users
         .error     should contain HTTP or JSON parsing error 
 */
- (BFTask *)fetchFollowTypeUsers:(PQFollowType)type ForUser:(User *)user;

/**
 Follow or unfollow a @user

 @param follow  YES means follow. NO means unfollow.
 @param user    User to follow

 @return BFTask
         .result    should contain server response
         .error     should contain HTTP error code
 */
- (BFTask *)follow:(BOOL)follow user:(User *)user;

- (BFTask *)uploadImageToPresign:(UIImage *)image;
- (BFTask *)uploadPost:(NSDictionary *)postContents;

#pragma mark - Profile
- (BFTask *)editProfile:(NSDictionary *)profileContents;

#pragma mark - Push Notifications

/**
 Upload APNS device token to server

 @param deviceToken String received from Apple for server usage
 @return BFTask
         .result    Successful upload
         .error     Error result of attempt to upload to server
 */
- (BFTask *)registerDeviceToken:(NSString *)deviceToken;

#pragma mark - API Check
- (BFTask *)checkAPIVersion;

@end
