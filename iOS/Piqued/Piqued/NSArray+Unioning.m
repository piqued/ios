//
//  NSArray+Unioning.m
//  Piqued
//
//  Created by Matt Mo on 9/3/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "NSArray+Unioning.h"

@implementation NSArray (Unioning)

- (NSArray<BaseModel *>*)unionWithArray:(NSArray<BaseModel *>*)otherArray
{
    NSSet * before = [NSSet setWithArray:self];
    NSSet * after = [NSSet setWithArray:otherArray];
    
    NSMutableSet *toAdd = [NSMutableSet setWithSet:after];
    [toAdd minusSet:before];
    
    NSArray *aggregatedPosts = [self arrayByAddingObjectsFromArray:[toAdd allObjects]];
    
    return aggregatedPosts;
}

@end
