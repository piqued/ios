//
//  PQImageCacheDelegate.m
//  Piqued
//
//  Created by Matt Mo on 2/12/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import "PQImageCacheDelegate.h"
#import "PQUtilities.h"

@implementation PQImageCacheDelegate

+ (instancetype)sharedInstance
{
    static PQImageCacheDelegate * imageManagerDelegate = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        imageManagerDelegate = [[self alloc] init];
    });
    
    return imageManagerDelegate;
}

+ (NSUInteger)resolutionForType:(PQImageCacheSizeType)sizeType
{
    switch (sizeType) {
        case PQImageCacheSizeTypeThumbnail:
            return 480;
        case PQImageCacheSizeTypeNormal:
            return 1080;
    }
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.sizeType = PQImageCacheSizeTypeNormal;
    }
    return self;
}

- (UIImage *)imageManager:(SDWebImageManager *)imageManager transformDownloadedImage:(UIImage *)image withURL:(NSURL *)imageURL
{
    UIImage *compressedImage = scaleAndRotateImage(image, [PQImageCacheDelegate resolutionForType:PQImageCacheSizeTypeNormal]);
    return compressedImage;
}

@end
