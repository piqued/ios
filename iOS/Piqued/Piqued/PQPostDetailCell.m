//
//  PQPostDetailCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 12/5/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQPostDetailCell.h"
#import "PQLocationManager.h"
#import "UIColor+PIQHex.h"
#import "PQMath.h"

@interface PQPostDetailCell()

@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *comments;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UIButton *bookmarkButton;

@property (nonatomic) BOOL bookmarkChanged;

@end

@implementation PQPostDetailCell

+ (CGFloat) estimatedHeight
{
    return 60;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void) layoutSubviews
{
    [self updateUIData];
}

- (void) reloadData
{
    [self updateUIData];
}

- (void) updateUIData
{
    Post * post = [self.basketDelegate getPost];
    
    double distance = [PQMath getMiles:post];
    
    NSString * distanceString;
    if (distance >= 20) {
        distanceString = [NSString stringWithFormat:@"%.lf mi away", distance];
    } else {
        distanceString = [NSString stringWithFormat: @"%.1lf mi away", distance];
    }
    
    [self.distanceLabel setText:distanceString];
    [self.comments setText:post.desc];
    
    if (post.title != NULL && ![post.title isEqualToString:@""]) {
        [self.address setText:post.title];
    } else {
        [self.address setText:post.address];
    }
    
    [self.bookmarkButton setSelected:post.bookmarked];
}

- (IBAction)onBookmarked:(id)sender {
    
    Post * post = [self.basketDelegate getPost];
    
    [post setBookmarked:!self.bookmarkButton.selected];
    [self.bookmarkButton setSelected:!self.bookmarkButton.selected];
}

@end
