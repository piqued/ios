//
//  PQPostGridCollectionViewCell.h
//  Piqued
//
//  Created by Matt Mo on 9/3/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Post;

@protocol PQPostGridCollectionViewCellDelegate <NSObject>

@required
- (void)openBasketViewWithID:(NSNumber *)postID;

@end

@interface PQPostGridCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id<PQPostGridCollectionViewCellDelegate> delegate;
@property (nonatomic, weak) Post *post;

+ (NSString *)nibName;
+ (NSString *)reuseIdentifier;

@end
