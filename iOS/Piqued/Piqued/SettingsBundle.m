//
//  SettingsBundle.m
//  Piqued
//
//  Created by Matt Mo on 1/14/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import "SettingsBundle.h"

static NSString *const kSettingsBundleHostKey = @"kSettingsBundleHostKey";
@interface SettingsBundle()

@end

@implementation SettingsBundle

+ (NSString *)hostString
{
    NSString *hostString = [[NSUserDefaults standardUserDefaults] stringForKey:kSettingsBundleHostKey];
    return (hostString.length > 0) ? hostString : nil;
}

+ (NSURL *)hostURL
{
    NSString *hostString = [[NSUserDefaults standardUserDefaults] stringForKey:kSettingsBundleHostKey];
    NSURL *url = (hostString.length > 0) ? [NSURL URLWithString:hostString] : nil;
    return url;
}

@end
