//
//  AppDelegate.m
//  Piqued
//
//  Created by Kenny M. Liou on 10/18/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "AppDelegate.h"
#import "PQUserDefaults.h"
#import "PQKeyChain.h"
#import "PQPostManager.h"
#import "PQDeviceOrientationManager.h"
#import "PQAnalyticHelper.h"
#import "PQEnvironment.h"
#import "PQImageCacheDelegate.h"

// Third party
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Google/Analytics.h>
#import <OneSignal/OneSignal.h>
#import <GooglePlaces/GooglePlaces.h>

// Push notifications
#import "PQNotifications.h"

// TODO: REMOVE INSTAGRAM
#import "PQInstagramManager.h"

@interface AppDelegate ()
@property (nonatomic, copy) void (^dispatchHandler)(GAIDispatchResult result);
@property (strong, nonatomic) PQImageCacheDelegate *imageCacheDelegate;
@property (strong, nonatomic) GMSPlacesClient *placesClient;

@property UIWindow *alertWindow;
@property BOOL isDebugWindowOpen;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Check device id
    [PQKeyChain.sharedKeyChain verifyDeviceId];
    
    // Facebook
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    // Analytics
    [[PQAnalyticHelper sharedInstance] initialize];
    [[PQAnalyticHelper sharedInstance] enterForeGround];
    
    // Crashlytics
    [Fabric with:@[[Crashlytics class]]];
    
    // Push Notifications
    [UNUserNotificationCenter currentNotificationCenter].delegate = [PQNotifications sharedManager];
    [self bootUpOneSignal:launchOptions];
    
    // Image Cache Delegate
    [PQDeviceOrientationManager sharedInstance];
    self.imageCacheDelegate = [PQImageCacheDelegate sharedInstance];
    [SDWebImageManager sharedManager].delegate = self.imageCacheDelegate;

    // Maps
    [GMSPlacesClient provideAPIKey:@"AIzaSyB87zgJH1OJYAdQD3osGQdq2qdF5jMxdtU"];
    self.placesClient = GMSPlacesClient.sharedClient;
    
    // Debug
    if ([PQEnvironment buildConfiguration] != PQBuildConfigurationAppStore) {
        [self setupDebugInfo];
    }
    
    [self startApp];
    
    [self handleAPICheck];
    
    return YES;
}

- (void)bootUpOneSignal:(NSDictionary *)launchOptions
{
    NSString *oneSignalAppID = @"";
    switch (PQEnvironment.buildConfiguration) {
        case PQBuildConfigurationDebug:
        case PQBuildConfigurationRelease: {
            oneSignalAppID = @"36235abd-979f-42eb-9839-b62846f7e674";
            break;
        }
        case PQBuildConfigurationAppStore: {
            oneSignalAppID = @"8fe64e5c-f5ec-4824-bb20-1a48ae71f063";
            break;
        }
    }

    [OneSignal initWithLaunchOptions:launchOptions
                               appId:oneSignalAppID
            handleNotificationAction:nil
                            settings:@{kOSSettingsKeyAutoPrompt: @false}];
    
    OneSignal.inFocusDisplayType = OSNotificationDisplayTypeNotification;
    
    // Recommend moving the below line to prompt for push after informing the user about
    //   how your app will use them.
//    [OneSignal promptForPushNotificationsWithUserResponse:^(BOOL accepted) {
//        PQLog(@"User accepted notifications: %d", accepted);
//    }];
    
    // Call syncHashedEmail anywhere in your iOS app if you have the user's email.
    // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
    // [OneSignal syncHashedEmail:userEmail];
}

- (void)setupDebugInfo
{
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] init];
    longPress.numberOfTouchesRequired = piq_deviceIsSimulator() ? 2 : 3;
    longPress.minimumPressDuration = 2;
    [longPress addTarget:self action:@selector(launchDebug:)];
    [self.window addGestureRecognizer:longPress];
}

- (void)launchDebug: (UILongPressGestureRecognizer *)gesture
{
    if (self.isDebugWindowOpen) return;
    
    self.isDebugWindowOpen = YES;
    
    NSMutableString *debugInfo = [PQEnvironment.description mutableCopy];
    [debugInfo appendFormat:@"\n%@", [PQUserDefaults getUser].description];
    
    self.alertWindow = [[UIWindow alloc] initWithFrame: [UIScreen mainScreen].bounds];
    self.alertWindow.rootViewController = [UIViewController new];
    self.alertWindow.windowLevel = UIWindowLevelAlert + 1;
    
    UIAlertController *debugAlert = [UIAlertController alertControllerWithTitle:@"Debug Info" message:debugInfo preferredStyle:UIAlertControllerStyleAlert];
    
    __weak AppDelegate *weakSelf = self;
    UIAlertAction *close = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        __block AppDelegate *strongSelf = weakSelf;
        [strongSelf.window makeKeyAndVisible];
        strongSelf.alertWindow = nil;
        strongSelf.isDebugWindowOpen = NO;
    }];
    
    [debugAlert addAction:close];
    
    [self.alertWindow makeKeyAndVisible];
    [self.alertWindow.rootViewController presentViewController:debugAlert  animated: YES completion: nil];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[PQPostManager sharedInstance] saveCurrentData];
    [self sendHitsInBackground];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [[PQAnalyticHelper sharedInstance] enterForeGround];
    
    [self handleAPICheck];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[PQAnalyticHelper sharedInstance] tearDown];
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    if ([url.scheme containsString:@"fb522265951246058"]) {
        NSString * sourceApplication = options[UIApplicationOpenURLOptionsSourceApplicationKey];
        NSString * annotation = options[UIApplicationOpenURLOptionsOpenInPlaceKey];
        return [[FBSDKApplicationDelegate sharedInstance] application:app openURL:url sourceApplication: sourceApplication annotation:annotation];
    } else if ([url.scheme isEqualToString:@"instagramc9a42d19e8e24f9da7340a3c2f03aada"]) {
        NSString * sourceApplication = options[UIApplicationOpenURLOptionsSourceApplicationKey];
        NSString * annotation = options[UIApplicationOpenURLOptionsOpenInPlaceKey];
        return [[PQInstagramManager sharedInstance] application:app openURL:url sourceApplication:sourceApplication annotation:annotation];
    }
    return NO;
}

- (void) startApp {
    if ([PQUserDefaults isUserLoggedIn]) {
        if ([PQUserDefaults isUserFinishedWelcomeScreenMain]) {
            UINavigationController *navVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateInitialViewController];
            self.window.rootViewController = navVC;
            
        } else {
            UINavigationController* navigation = [[UIStoryboard storyboardWithName:@"Login" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"WelcomeScreen"];
            self.window.rootViewController = navigation;
        }
    } else {
        UINavigationController* navigation = [[UIStoryboard storyboardWithName:@"Login" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"LoginFlow"];
        self.window.rootViewController = navigation;
    }
}

- (void) handleAPICheck
{
    [[PQNetworking.sharedInstance checkAPIVersion] continueWithBlock:^id _Nullable(BFTask * _Nonnull t) {
        PQLog(@"API Version check response: %@", t.result);
        if (t.error) {
            PQLog(@"API Version check failed");
            return nil;
        }
        
        BOOL requiresUpdate = t.result[@"requires_update"];
        NSString *message = t.result[@"message"];
        if (requiresUpdate) {
            dispatch_main_async_safe(^{
                self.alertWindow = [[UIWindow alloc] initWithFrame: [UIScreen mainScreen].bounds];
                self.alertWindow.rootViewController = [UIViewController new];
                self.alertWindow.windowLevel = UIWindowLevelAlert + 1;
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Update Piqued" message:message preferredStyle:UIAlertControllerStyleAlert];
                
                __weak AppDelegate *weakSelf = self;
                UIAlertAction *close = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    __block AppDelegate *strongSelf = weakSelf;
                    [strongSelf.window makeKeyAndVisible];
                    strongSelf.alertWindow = nil;
                    strongSelf.isDebugWindowOpen = NO;
                    
                    NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/app/piqued-find-new-places-with-your-social-circles/id1270601626?mt=8"];
                    [UIApplication.sharedApplication openURL:url];
                }];
                [alert addAction:close];
                
                [self.alertWindow makeKeyAndVisible];
                [self.alertWindow.rootViewController presentViewController:alert  animated: YES completion: nil];
            });
        }
        
        return nil;
    }];
}

#pragma mark - User Notifications
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    if (PQUserDefaults.getUser != nil) {
        [PQNotifications handleRemoteNotificationsDeviceToken:deviceToken];
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    PQLog(@"Failed to register: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [PQNotifications piquedApplication:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
}

#pragma mark Google Analytics
// This method sends any queued hits when the app enters the background.
- (void)sendHitsInBackground {
    __block BOOL taskExpired = NO;
    
    __block UIBackgroundTaskIdentifier taskId =
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        taskExpired = YES;
    }];
    
    if (taskId == UIBackgroundTaskInvalid) {
        return;
    }
    
    __weak AppDelegate *weakSelf = self;
    self.dispatchHandler = ^(GAIDispatchResult result) {
        // Send hits until no hits are left, a dispatch error occurs, or
        // the background task expires.
        if (result == kGAIDispatchGood && !taskExpired) {
            [[GAI sharedInstance] dispatchWithCompletionHandler:weakSelf.dispatchHandler];
        } else {
            [[UIApplication sharedApplication] endBackgroundTask:taskId];
        }
    };
    
    [[GAI sharedInstance] dispatchWithCompletionHandler:self.dispatchHandler];
}

@end
