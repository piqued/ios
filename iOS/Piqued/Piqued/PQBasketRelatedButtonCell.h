//
//  PQBasketRelatedButtonCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    Yelp,
    Zagat,
    FourSquare,
    TripAdvisor,
    numCount
} BasketRelatedType;

@interface PQBasketRelatedButtonCell : UICollectionViewCell

@property (nonatomic) BasketRelatedType type;
@property (strong, nonatomic) NSURL * linkUrl;

@end
