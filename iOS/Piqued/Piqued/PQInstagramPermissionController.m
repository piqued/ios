//
//  PQInstagramPermissionViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 4/17/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQInstagramPermissionController.h"
#import "UIColor+PIQHex.h"

@interface PQInstagramPermissionController ()

@end

@implementation PQInstagramPermissionController

+ (void) showPermissionMessage:(UIViewController *) controller withOkayBlock:(void (^ __nullable)(UIAlertAction *action))okayBlock
{
    UIColor * color = [UIColor piq_colorFromHexString:kOrangeColor];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    NSMutableAttributedString * message = [[NSMutableAttributedString alloc] initWithString:@"It looks like you’ve liked several images on Instagram, would you like to add them?"];
    [message addAttribute:NSForegroundColorAttributeName
                  value:color
                  range:NSMakeRange(0, message.length)];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Okay"
                         style:UIAlertActionStyleDefault
                         handler:okayBlock];
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:nil];
    
    [alert setValue:message forKey:@"attributedMessage"];
    [alert.view setTintColor:color];
    [alert addAction:cancel];
    [alert addAction:ok];
    
    [controller presentViewController:alert animated:YES completion:^{
        // does nothing
    }];
}

@end
