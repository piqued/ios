//
//  PQImageCacheDelegate.h
//  Piqued
//
//  Created by Matt Mo on 2/12/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SDWebImage/SDWebImageManager.h>

typedef enum {
    PQImageCacheSizeTypeThumbnail,
    PQImageCacheSizeTypeNormal
} PQImageCacheSizeType;

@interface PQImageCacheDelegate : NSObject <SDWebImageManagerDelegate>

+ (instancetype)sharedInstance;

@property (assign) PQImageCacheSizeType sizeType;

@end
