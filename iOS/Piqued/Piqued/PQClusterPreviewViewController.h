//
//  PQClusterPreviewViewController.h
//  Piqued
//
//  Created by Matt Mo on 1/15/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQPostManager.h"

@interface PQClusterPreviewViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, PQPostManagerDelegate>

+ (NSString *)segueIdentifier;
- (void) setPosts:(NSArray *) posts;

@end
