//
//  PQMapAnnotation.m
//  Piqued
//
//  Created by Matt Mo on 5/20/15.
//  Copyright (c) 2015 First Brew. All rights reserved.
//

#import "PQMapAnnotation.h"

@implementation PQMapAnnotation

- (instancetype) initWithCoordinates:(CLLocationCoordinate2D) coords postID:(NSNumber *)postID imageURL:(NSString *)imageURL numNeighbors:(long) numNeighbors {
    self = [super init];
    if (self) {
        _coordinate = coords;
        _postID = postID;
        _numNeighbors = numNeighbors;
        if (imageURL && imageURL != (id)[NSNull null]) {
            _imageURL = imageURL;
        }
    }
    return self;
}

- (BOOL) isEqual:(id)object {
    if (self == object) {
        return YES;
    } else if (![object isKindOfClass:[self class]]) {
        return NO;
    } else {
        return [self isEqualAnnotation:object];
    }
}

- (BOOL) isEqualAnnotation:(PQMapAnnotation *)annotation {
    return  self.coordinate.latitude == annotation.coordinate.latitude &&
            self.coordinate.longitude == annotation.coordinate.longitude &&
            [self.postID isEqualToNumber:annotation.postID] &&
            self.numNeighbors == annotation.numNeighbors;
}

#define NSUINT_BIT (CHAR_BIT * sizeof(NSUInteger))
#define NSUINTROTATE(val, howmuch) ((((NSUInteger)val) << howmuch) | (((NSUInteger)val) >> (NSUINT_BIT - howmuch)))

- (NSUInteger) hash {
    return NSUINTROTATE(_coordinate.latitude, NSUINT_BIT / 3) ^ NSUINTROTATE(_coordinate.longitude, NSUINT_BIT * 2/ 3) ^ [_postID hash];
}

- (NSString *)description {
    NSMutableString * description = [NSMutableString stringWithString:@"PQMapAnnotation: { "];
    NSString * postId = [NSString stringWithFormat:@"id: %@, ", self.postID];
    [description appendString:postId];
    NSString * imageUrl = [NSString stringWithFormat:@"imageUrl: %@, ", self.imageURL];
    [description appendString:imageUrl];
    NSString * latitude = [NSString stringWithFormat:@"latitude: %f, ", self.coordinate.latitude];
    [description appendString:latitude];
    NSString * longitude = [NSString stringWithFormat:@"longitude: %f ", self.coordinate.longitude];
    [description appendString:longitude];
    NSString * numNeighbors = [NSString stringWithFormat:@"neighbors: %ld ", self.numNeighbors];
    [description appendString:numNeighbors];
    [description appendString:@"}"];
    return description;
}

@end
