//
//  PQThreadHelper.h
//  Piqued
//
//  Created by Kenny M. Liou on 9/1/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQThreadHelper : NSObject

+ (void) performOnMainThread:(void(^)(void))callback;

@end
