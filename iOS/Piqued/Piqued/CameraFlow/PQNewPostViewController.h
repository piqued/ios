//
//  PQNewPostViewController.h
//  Piqued
//
//  Created by Kenny M. Liou on 11/14/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@import CoreLocation;

typedef enum {
    PQUseFoursquareLocationUndetermined = 0,
    PQUseFoursquareLocationNo,
    PQUseFoursquareLocationYes
} PQUseFoursquareLocation;

@interface PQNewPostViewController : UIViewController

// Set this at initialization, otherwise do not mutate
@property (strong, nonatomic) NSDictionary *postInfo;

@end
