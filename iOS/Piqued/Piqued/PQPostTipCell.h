//
//  PQPostTipCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 4/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PQBasketBaseCell.h"

@interface PQPostTipCell : PQBasketBaseCell

+ (int) estimatedHeight;

- (void) setCellIndex:(int) index;

@end
