//
//  PQBasketBaseCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 12/5/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQBasketBaseCell.h"

@implementation PQBasketBaseCell

-(void) reloadData
{
    // Child cell should do something
}

@end
