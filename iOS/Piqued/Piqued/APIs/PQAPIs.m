//
//  PQAPIs.m
//  Piqued
//
//  Created by Kenny M. Liou on 10/26/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQAPIs.h"

#import "PQUserDefaults.h"
#import "PQLocationManager.h"
#import "Constants.h"
#import "NSHTTPURLResponse+PIQHelper.h"
#import "PQPostManager.h"
#import "Comment.h"
#import <InstagramKit/InstagramKit.h>
#import "EventModel.h"
#import "LoginModel.h"
#import "PQAPICore.h"
#import "PQEnvironment.h"

#define kLoginURL               @"/users/login.json"
#define kCreateUserURL          @"/users.json"
#define kPostsURL               @"/posts"
#define kOathLoginURL           @"/users/oauth.json"
#define kUpdateUserDataURL      @"/profile.json"
#define kCurrentUserHistoryURL  @"/posts.json"
#define kUserHistoryURL         @"/users/%.f/posts.json"
#define kCreatePostsURL         @"/posts.json"
#define kGetPostsURL            @"/posts"
#define kProfileURL             @"/users/%.f.json"
#define kPostURL                @"/posts/%.d.json"
#define kGetCommentURL          @"/posts/%.d/comments.json"
#define kBookmarkPostURLPrefix        @"/posts/"
#define kBookmarkPostURLSuffix        @"/bookmark.json"
#define kBookmarkedPostsPath           @"/bookmarks.json"
#define kEventsURL              @"/events.json"
#define kReactToPostURL         @"/%.d/reactions.json"

@implementation PQAPIs

typedef struct {
    double latMin;
    double latMax;
    double lngMin;
    double lngMax;
} RegionBounds;

#pragma mark - Login API calls

+ (NSString *)baseURL
{
    return PQEnvironment.serverBaseURLString;
}

+ (void) loginWithData:(NSDictionary *) loginInfo andUrl:(NSString *) url withCallback:(void (^)(User * user, NSError * error))callbackBlock
{
    [[PQAPICore sharedInstance] loginWithData:loginInfo andUrl:url withCallback:callbackBlock];
}

+ (void) loginWithSiteName:(NSString *) siteName withSiteToken:(NSString *) token withCallback:(void (^)(User * user, NSError * error))callbackBlock
{
    
    NSDictionary *loginInfo = @{@"user" : @{@"site_name":siteName, @"site_token":token}};
    
    [PQAPIs loginWithData:loginInfo andUrl:[PQEnvironment.serverBaseURLString stringByAppendingString:kOathLoginURL] withCallback:callbackBlock];
}

+ (void) loginWithName:(NSString *) userName andPassword:(NSString *) password withCallback:(void (^)(User * user, NSError * error))callbackBlock
{
    
    NSDictionary *loginInfo = @{@"user" : @{@"email":userName, @"password":password}};
    
    [PQAPIs loginWithData:loginInfo andUrl:[PQEnvironment.serverBaseURLString stringByAppendingString:kLoginURL] withCallback:callbackBlock];
}

+ (void) createAccount:(NSString *) userName withEmail:(NSString *)email andPassword:(NSString *) password andConfirmPassword:(NSString *) cPassword withCallback:(void (^)(User *user, NSError *error))callbackBlock
{
    NSDictionary *createAccountInfo = @{@"user":@{
                                                @"name" : userName,
                                                @"email":email,
                                                @"password":password,
                                                @"password_confirmation": cPassword
                                                }
                                        };

    [[PQAPICore sharedInstance] createAccount:createAccountInfo andUrl:[PQEnvironment.serverBaseURLString stringByAppendingString:kCreateUserURL] withCallback:^(User *user, NSError *error) {
        callbackBlock(user, error);
    }];
    
//    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString[PQEnvironment.serverBaseURLString stringByAppendingString:kCreateUserURL]]];
//    [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
//    
//    // we are making a post
//    request.HTTPMethod = @"POST";
//    
//    // this request contains data type json
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    
//    NSError * error = NULL;
//    
//    // serialize data
//    NSDictionary *loginInfo = @{@"user" : @{
//                                        @"name" : userName,
//                                        @"email":email,
//                                        @"password":password,
//                                        @"password_confirmation": cPassword
//                                        }
//                                };
//    
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:loginInfo options:0 error:&error];
//    
//    if (jsonData && !error) {
//        
//        NSURLSession * session = [NSURLSession sharedSession];
//        
//        [[session uploadTaskWithRequest:request fromData:jsonData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
//            NSError * parseError = NULL;
//            
//            NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
//            if (responseError) {
//                PQLog(@"Error: %@", responseError);
//            } else {
//                // we have data!!
//                
//                NSDictionary * userInfo = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
//                
//                if (!parseError) {
//                    
//                    LoginModel * user = [LoginModel modelWithDictionary:userInfo];
//                    
//                    [PQUserDefaults setUser:user];
//                    
//                    [[PQPostManager sharedInstance] reloadAllData:^(bool success) {
//                        if (success) {
//                            [self fetchCurrentUserWithCallback:^(User *user) {
//                                dispatch_async(dispatch_get_main_queue(), ^{
//                                    callbackBlock(YES, user);
//                                });
//                            }];
//                        }
//                    }];
//                    
//                    return;
//                } else {
//                    
//                    PQLog(@"failed on parsing return data");
//                }
//            }
//            
//            PQLog(@"failed to login!");
//            dispatch_async(dispatch_get_main_queue(), ^{
//                callbackBlock(NO, NULL);
//            });
//        }] resume];
//        
//    } else {
//        PQLog(@"Unable to serialize the data %@: %@", loginInfo, error);
//    }
}

+ (NSString *) prepareUserCreds:(NSString *) hostUrl
{
    User * user = [PQUserDefaults getUser];
    
    NSString * creds = [NSString stringWithFormat:@"%@?creds[token]=%@&creds[secret]=%@", hostUrl, user.token, user.secret];
    
    return creds;
}

+ (NSString *) prepareInstagramCreds:(NSString *) hostUrl
{
    NSString * token = [[InstagramEngine sharedEngine] accessToken];
    
    //    NSString * user = [NSString stringWithFormat:@"%@&user[site_name]=%@&user[site_token]=%@", hostUrl, @"instagram", @"1990728836.1fb234f.64f50924c73544698cf18a7ab1159f17"];
    NSString * user = [NSString stringWithFormat:@"%@&user[site_name]=%@&user[site_token]=%@", hostUrl, @"instagram", token ?: @""];
    
    return user;
}

#pragma mark - User Data API calls

+ (void)fetchEventsWithCallback:(void (^) (NSArray *json))callback
{
    NSString * host = [PQEnvironment.serverBaseURLString stringByAppendingString:kEventsURL];
    
    NSString * url = [PQAPIs prepareUserCreds:host];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];
    
    request.HTTPMethod = @"GET";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSError * parseError = NULL;
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            PQLog(@"Error: %@", responseError);
        } else {
            // we have data!!
            
            NSArray * historyData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
            if (!parseError) {
                NSError *mantleError = nil;
                NSArray<EventModel *> *array = [MTLJSONAdapter modelsOfClass:[EventModel class] fromJSONArray:historyData error:&mantleError];
                if (callback) {
                    callback(array);
                }
                return;
            } else {
                
                PQLog(@"failed on parsing return data");
                PQLog(@"%@", [parseError localizedDescription]);
            }
        }
    }] resume];
}

+ (void)fetchCurrentUserWithCallback:(void (^) (User * user)) callback
{
    User *user = [PQUserDefaults getUser];
    [PQAPIs fetchUserInfo:user.userId withCallback:^(User *updatedUser) {
        if (updatedUser != nil) {
            User *user = [PQUserDefaults updateUser:updatedUser];
        }
        callback(user);
    }];
}

+ (void)fetchUserInfo:(double) userId withCallback:(void (^) (User * user)) callback
{
    NSString * host = [NSString stringWithFormat:[PQEnvironment.serverBaseURLString stringByAppendingString:kProfileURL], userId];
    
    NSString * url = [PQAPIs prepareUserCreds:host];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];
    
    request.HTTPMethod = @"GET";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSError * parseError = NULL;
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            PQLog(@"Error: %@", responseError);
        } else {
            // we have data!!
            
            NSDictionary * historyData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
            
            if (!parseError) {
                User * user = [User modelWithDictionary:historyData];

                User *currentUser = [PQUserDefaults getUser];
                if (currentUser.userId == user.userId) {
                    user = [PQUserDefaults updateUser:user];
                }
                    
                dispatch_async(dispatch_get_main_queue(), ^{
                    callback(user);
                });
                return;
            } else {
                
                PQLog(@"failed on parsing return data");
                PQLog(@"%@", [parseError localizedDescription]);
            }
        }
        
        callback(nil);
    }] resume];
}

+ (void) fetchUserPostHistory:(double)userId withPage:(int) page andSize:(int)size andCallback:(void (^)(BOOL result, long count, NSArray * posts))callbackBlock
{
    NSString *url = [PQAPIs prepareUserCreds:[NSString stringWithFormat:[PQEnvironment.serverBaseURLString stringByAppendingString:kUserHistoryURL], userId]];
    if (page > 0) url = [NSString stringWithFormat:@"%@&page=%i", url, page];
    if (size > 0) url = [NSString stringWithFormat:@"%@&size=%i", url, size];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];
    
    request.HTTPMethod = @"GET";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    PQLog(@"request.url: %@", request.URL);
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSError * parseError = NULL;
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            PQLog(@"Error: %@", responseError);
        } else {
            // we have data!!
            
            NSDictionary * historyData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
            
            if (!parseError) {
                // TODO: API changed!! FIX ME!!
                NSNumber * postCounts = historyData[@"total_count"];
                NSMutableArray * posts = [NSMutableArray array];
                if (postCounts > 0) {
                    for (NSDictionary * post in historyData[@"posts"]) {
                        [posts addObject:[Post modelWithDictionary:post]];
                    }
                }
                
                callbackBlock(YES, [postCounts intValue], posts);
                return;
            } else {
                
                PQLog(@"failed on parsing return data");
                PQLog(@"%@", [parseError localizedDescription]);
            }
        }
        
        PQLog(@"Error: %@",[error localizedDescription]);
        
        callbackBlock(NO, 0, NULL);
    }] resume];
}

+ (void) fetchCurrentUserPostHistoryWithPage:(int) page andSize:(int)size andCallback:(void (^)(BOOL result, long count, NSArray * posts))callbackBlock
{
    NSString *url = [PQAPIs prepareUserCreds:[PQEnvironment.serverBaseURLString stringByAppendingString:kCurrentUserHistoryURL]];
    if (page > 0) url = [NSString stringWithFormat:@"%@&page=%i", url, page];
    if (size > 0) url = [NSString stringWithFormat:@"%@&size=%i", url, size];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];
    
    request.HTTPMethod = @"GET";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    PQLog(@"request.url: %@", request.URL);
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSError * parseError = NULL;
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            PQLog(@"Error: %@", responseError);
        } else {
            // we have data!!
            
            NSDictionary * historyData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
            
            if (!parseError) {
                // TODO: API changed!! FIX ME!!
                NSNumber * postCounts = historyData[@"total_count"];
                NSMutableArray * posts = [NSMutableArray array];
                if (postCounts > 0) {
                    for (NSDictionary * post in historyData[@"posts"]) {
                        [posts addObject:[Post modelWithDictionary:post]];
                    }
                }
                
                callbackBlock(YES, [postCounts intValue], posts);
                return;
            } else {
                
                PQLog(@"failed on parsing return data");
                PQLog(@"%@", [parseError localizedDescription]);
            }
        }
        
        PQLog(@"Error: %@",[error localizedDescription]);
        
        callbackBlock(NO, 0, NULL);
    }] resume];
}

+ (void) editUserPassword:(NSString *) password andConfirmed:(NSString *) cPassword withCallback:(void (^)(BOOL))callbackBlock
{
    NSDictionary *passwordUpdate = @{@"password":password,
                                     @"password_confirmation":cPassword};
    
    [PQAPIs editUserData:passwordUpdate withCallback:callbackBlock];
}

+ (void) editUserInfo:(NSString *) newUserName withImageData:(UIImage *)image withEmail:(NSString *)newEmail withCallback:(void (^)(BOOL suess))callbackBlock
{
    NSMutableDictionary * userInfo = [NSMutableDictionary dictionary];
    
    BOOL hasData = NO;
    
    if (newUserName) {
        [userInfo setObject:newUserName forKey:@"name"];
        hasData = YES;
    }
    
    if (image) {
        
        NSString * image64 = [UIImageJPEGRepresentation(image, 0.75) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        
        [userInfo setObject:image64 forKey:@"image_data"];
        hasData = YES;
    }
    
    if (newEmail) {
        [userInfo setObject:newEmail forKey:@"email"];
        hasData = YES;
    }
    
    if (hasData) {
        [PQAPIs editUserData:userInfo withCallback:callbackBlock];
    } else {
        callbackBlock(NO);
    }
}

+ (void) editUserData:(NSDictionary *) newUserData withCallback:(void (^)(BOOL))callbackBlock
{
    NSMutableString * hostURLString = [NSMutableString stringWithString:[PQEnvironment.serverBaseURLString stringByAppendingString:kUpdateUserDataURL]];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:hostURLString]];
    [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"PATCH"];
    
    // setup data
    User * user = [PQUserDefaults getUser];
    
    NSDictionary * credentialsJSON = @{
                                       @"token": user.token,
                                       @"secret": user.secret
                                       };
    NSDictionary * requestJSON = @{
                                   @"creds": credentialsJSON,
                                   @"user": newUserData
                                   };
    
    NSString * serializedJSON = [self dictionaryToJSON:requestJSON];
    NSData * data = [serializedJSON dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    [[session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        });
        
        NSError * parseError = NULL;
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            PQLog(@"Error: %@", responseError);
        } else {
            
            NSDictionary * dataReturned = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
            
            NSMutableDictionary * userInfo = [NSMutableDictionary dictionaryWithDictionary:dataReturned];
            
            if (!parseError) {
                
                User * currentUser = [PQUserDefaults getUser];
                
                [userInfo setObject:currentUser.secret forKey:@"secret"];
                [userInfo setObject:currentUser.token forKey:@"token_id"];
                
                User * newUser = [User modelWithDictionary:userInfo];
                            
                [PQUserDefaults setUser:newUser];
                
                [[PQPostManager sharedInstance] reloadAllData:^(bool success) {
                    if (success) {
                        [PQAPIs fetchCurrentUserWithCallback:^(User *user) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                callbackBlock(YES);
                            });
                        }];
                    }
                }];
                
                return;
            } else {
                
                PQLog(@"failed on parsing return data");
            }
        }
        
        PQLog(@"failed to edit user data: %@", serializedJSON);
        callbackBlock(false);
    }] resume];

}

#pragma mark - Posts Data API calls

//TODO: Make a better way to enter a nil parameter for a struct
+ (void)retrievePostsForOuterRegion:(MKCoordinateRegion)outerRegion notInnerRegion:(MKCoordinateRegion)innerRegion withOptions:(NSDictionary *)options withCallback:(void (^)(NSDictionary *postInfo, NSError * error))callbackBlock {
    
    RegionBounds outerBounds = [self getBoundsOfRegion:outerRegion];
    
    NSMutableString * restUrlMutable = [NSMutableString string];
    [restUrlMutable appendString:PQEnvironment.serverBaseURLString];
    [restUrlMutable appendString:@"/posts/region.json"];
    
    NSMutableString * restUrl = [NSMutableString stringWithString:[self prepareUserCreds:restUrlMutable]];
    
    if (options != nil) {
        NSNumber * pageNumber = [options objectForKey:@"page"];
        if (pageNumber != nil) {
            [restUrl appendString:[NSString stringWithFormat:@"&page=%d", pageNumber.intValue]];
        }
        NSNumber * sizeNumber = [options objectForKey:@"size"];
        if (sizeNumber != nil) {
            [restUrl appendString:[NSString stringWithFormat:@"&size=%d", sizeNumber.intValue]];
        }
    }
    
    [restUrl appendString:[NSString stringWithFormat:@"&region[outer_bounds][lat_min]=%f", outerBounds.latMin]];
    [restUrl appendString:[NSString stringWithFormat:@"&region[outer_bounds][lat_max]=%f", outerBounds.latMax]];
    [restUrl appendString:[NSString stringWithFormat:@"&region[outer_bounds][lng_min]=%f", outerBounds.lngMin]];
    [restUrl appendString:[NSString stringWithFormat:@"&region[outer_bounds][lng_max]=%f", outerBounds.lngMax]];
    
    if (innerRegion.span.latitudeDelta > 0.0 && innerRegion.span.longitudeDelta > 0.0) {
        RegionBounds innerBounds = [self getBoundsOfRegion:innerRegion];
        [restUrl appendString:[NSString stringWithFormat:@"&region[inner_bounds][lat_min]=%f", innerBounds.latMin]];
        [restUrl appendString:[NSString stringWithFormat:@"&region[inner_bounds][lat_max]=%f", innerBounds.latMax]];
        [restUrl appendString:[NSString stringWithFormat:@"&region[inner_bounds][lng_min]=%f", innerBounds.lngMin]];
        [restUrl appendString:[NSString stringWithFormat:@"&region[inner_bounds][lng_max]=%f", innerBounds.lngMax]];
    }
    
    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:restUrl]];
    
    PQLog(@"Rest URL: %@\n", restUrl);
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    // 3: Give the user feedback, while downloading the data source by enabling network activity indicator.
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        });
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            // Check for error with NSURLSession
            PQLog(@"Error: %@", responseError);
        } else {
            // Response is OK
            NSError * jsonParseError = NULL;
            NSData * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParseError];
            
            if (jsonParseError) {
                PQLog(@"Json seralization error: %@", jsonParseError);
                responseError = jsonParseError;
            } else {
                callbackBlock((NSDictionary *)json, nil);
                return;
            }
        }
        callbackBlock(nil, responseError);
    }] resume];
}

+ (void)retrieveBookmarkedPosts:(NSDictionary *)options
                       callback:(void (^)(NSArray *bookmarkedPosts, NSError *error))callbackBlock {
    // Make network call to get bookmarked posts
    // API call returns array
    
    NSMutableString * hostURLString = [NSMutableString string];
    [hostURLString appendString:PQEnvironment.serverBaseURLString];
    [hostURLString appendString:kBookmarkedPostsPath];
    
    NSMutableString * requestURLString = [[self prepareUserCreds:hostURLString] mutableCopy];
    
    if (options != nil) {
        NSNumber * pageNumber = [options objectForKey:@"page"];
        if (pageNumber != nil) {
            [requestURLString appendString:[NSString stringWithFormat:@"&page=%d", pageNumber.intValue]];
        }
        NSNumber * sizeNumber = [options objectForKey:@"size"];
        if (sizeNumber != nil) {
            [requestURLString appendString:[NSString stringWithFormat:@"&size=%d", sizeNumber.intValue]];
        }
    }
    
    PQLog(@"url: %@", requestURLString);
    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:requestURLString]];
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        });
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            // Error in response or making request
            PQLog(@"Network error: %@", responseError);
        } else {
            // Response is OK
            NSError * jsonParseError = NULL;
            NSData * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParseError];
            
            if (jsonParseError) {
                PQLog(@"Json parser error: %@", jsonParseError);
                responseError = jsonParseError;
            } else {
                NSMutableArray * posts = [[NSMutableArray alloc] init];
                
                for (id obj in ((NSArray *)json)) {
                    NSDictionary * dic = obj;
                    
                    Post *post = [Post modelWithDictionary:dic];
                    [post setBookmarked:YES];
                    [posts addObject:post];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    callbackBlock(posts, nil);
                });
                return;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callbackBlock(nil, responseError);
        });
        
    }] resume];
}

+ (void)retrieveInstagramPostsForOuterRegion:(MKCoordinateRegion)outerRegion notInnerRegion:(MKCoordinateRegion)innerRegion withOptions:(NSDictionary *)options withCallback:(void (^)(NSDictionary *, NSError *))callbackBlock
{
    if (![[InstagramEngine sharedEngine] accessToken]) {
        PQLog(@"Instagram access token is not available!");
        return;
    }
    
    RegionBounds outerBounds = [self getBoundsOfRegion:outerRegion];
    
    NSMutableString * restUrlMutable = [NSMutableString string];
    [restUrlMutable appendString:PQEnvironment.serverBaseURLString];
    [restUrlMutable appendString:@"/posts/import.json"];
    
    NSMutableString * restUrl = [[self prepareUserCreds:restUrlMutable] mutableCopy];
    restUrl = [[self prepareInstagramCreds:restUrl] mutableCopy];
    
    if (options != nil) {
        NSString * pageNumber = [options objectForKey:@"next_max_like_id"];
        if ((id)pageNumber != [NSNull null] && ![pageNumber isEqualToString:@""]) {
            [restUrl appendString:[NSString stringWithFormat:@"&next_max_like_id=%@", pageNumber ?: @""]];
        }
    }
    
    [restUrl appendString:[NSString stringWithFormat:@"&region[outer_bounds][lat_min]=%f", outerBounds.latMin]];
    [restUrl appendString:[NSString stringWithFormat:@"&region[outer_bounds][lat_max]=%f", outerBounds.latMax]];
    [restUrl appendString:[NSString stringWithFormat:@"&region[outer_bounds][lng_min]=%f", outerBounds.lngMin]];
    [restUrl appendString:[NSString stringWithFormat:@"&region[outer_bounds][lng_max]=%f", outerBounds.lngMax]];
    
    if (innerRegion.span.latitudeDelta > 0.0 && innerRegion.span.longitudeDelta > 0.0) {
        RegionBounds innerBounds = [self getBoundsOfRegion:innerRegion];
        [restUrl appendString:[NSString stringWithFormat:@"&region[inner_bounds][lat_min]=%f", innerBounds.latMin]];
        [restUrl appendString:[NSString stringWithFormat:@"&region[inner_bounds][lat_max]=%f", innerBounds.latMax]];
        [restUrl appendString:[NSString stringWithFormat:@"&region[inner_bounds][lng_min]=%f", innerBounds.lngMin]];
        [restUrl appendString:[NSString stringWithFormat:@"&region[inner_bounds][lng_max]=%f", innerBounds.lngMax]];
    }
    
    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:restUrl]];
    
    PQLog(@"Rest URL: %@\n", restUrl);
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    // 3: Give the user feedback, while downloading the data source by enabling network activity indicator.
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        });
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            // Check for error with NSURLSession
            PQLog(@"Error: %@", responseError);
        } else {
            // Response is OK
            NSError * jsonParseError = NULL;
            NSData * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParseError];
            
            if (jsonParseError) {
                PQLog(@"Json seralization error: %@", jsonParseError);
                responseError = jsonParseError;
            } else {
                callbackBlock((NSDictionary *)json, nil);
                return;
            }
        }
        callbackBlock(nil, responseError);
    }] resume];
}


+ (void)apiCallToCreatePost:(NSDictionary *)postContents withCallback:(void (^)(NSError * error))callback {
    UIImage * image = [postContents objectForKey:@"image"];
    UIImage * thumbnail = [postContents objectForKey:@"thumbnail"];
    NSString * description = [postContents objectForKey:@"description"];
    NSString * title = [postContents objectForKey:@"title"];
    CLLocation * location = [postContents objectForKey:@"location"];
    NSString * address = [postContents objectForKey:@"address"];
    NSString * foursquareID = [postContents objectForKey:@"foursquare_venue_id"];
    
    User * user = [PQUserDefaults getUser];
    NSString * token = user.token;
    NSString * secret = user.secret;
    
    // JSON Call
    NSMutableString * restUrl = [NSMutableString string];
    [restUrl appendString:[PQEnvironment.serverBaseURLString stringByAppendingString:kCreatePostsURL]];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:restUrl]];
    request.cachePolicy = NSURLRequestReloadIgnoringCacheData;
    
    // Specify that it will be a POST request
    request.HTTPMethod = @"POST";
    
    // Set header field
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    NSString * image64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    NSData *thumbnailData = UIImageJPEGRepresentation(thumbnail, 0.5);
    NSString * thumbnail64 = [thumbnailData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    //http body property
    NSDictionary * postJSON = @{
                                @"lat": [NSNumber numberWithDouble:location.coordinate.latitude],
                                @"lng": [NSNumber numberWithDouble:location.coordinate.longitude],
                                @"address": address,
                                @"title": title,
                                @"description": description,
                                @"images": @{
                                        @"image_sm_data": thumbnail64,
                                        @"image_lg_data": image64,
                                        },
                                @"foursquare_venue_id": foursquareID
                                };
    NSDictionary * credentialsJSON = @{
                                       @"token": token,
                                       @"secret": secret
                                       };
    NSDictionary *requestJSON = @{
                                  @"post": postJSON,
                                  @"creds": credentialsJSON
                                  };
    
    NSError * error;
    NSData * json;
    NSString *jsonString = nil;
    
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:requestJSON])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:requestJSON options:NSJSONWritingPrettyPrinted error:&error];
        
        // If no errors, let's view the JSON
        if (json != nil && error == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
        }
    }
    
    NSData * requestBodyData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    [[session uploadTaskWithRequest:request fromData:requestBodyData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            PQLog(@"Error: %@", responseError);
            callback(responseError);
        } else {
            // Response is OK
            NSString * responseData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            PQLog(@"Response data: %@", responseData);
            callback(nil);
        }
        
    }] resume];
}

#pragma mark - Editing Post Data

+ (void)apiCallToBookmarkPost:(BOOL)isBookmarked withPostID:(NSNumber *) postID withCallback:(void (^)(BOOL successfulCall, BOOL isBookmarked))callback
{
    // Request URL
    NSMutableString * restURL = [NSMutableString string];
    [restURL appendString:[PQEnvironment.serverBaseURLString  stringByAppendingString:kBookmarkPostURLPrefix]];
    [restURL appendString:[postID stringValue]];
    [restURL appendString:kBookmarkPostURLSuffix];
    
    // Request JSON
    restURL = [NSMutableString stringWithString:[self prepareUserCreds:restURL]];
    
    User * user = [PQUserDefaults getUser];
    
    NSDictionary * credentialsJSON = @{
                                       @"token": user.token,
                                       @"secret": user.secret
                                       };
    NSDictionary * bookmarksJSON = @{
                                     @"bookmarked": isBookmarked? @"true":@"false"
                                     };
    NSDictionary * requestJSON = @{
                                   @"creds": credentialsJSON,
                                   @"bookmark": bookmarksJSON
                                   };
    NSString * serializedJSON = [self dictionaryToJSON:requestJSON];
    NSData * bookmarkData = [serializedJSON dataUsingEncoding:NSUTF8StringEncoding];
    
    // Request Object
    NSMutableURLRequest * bookmarkRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:restURL]];
    [bookmarkRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [bookmarkRequest setHTTPMethod:@"POST"];
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    [[session uploadTaskWithRequest:bookmarkRequest fromData:bookmarkData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSError * parseError = nil;
        NSDictionary * serverResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
        NSNumber * success = serverResponse[@"success"];
        NSString * status = serverResponse[@"status"];
        if (error || !success) {
            PQLog(@"Error: %@", error);
            dispatch_async(dispatch_get_main_queue(), ^{
                callback(NO, NO);
            });
        } else {
            if ([status isEqualToString:@"removed"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    callback(YES, NO);
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    callback(YES, YES);
                });
            }
        }
    }] resume];
}

#pragma mark - Helper methods

+ (RegionBounds) getBoundsOfRegion: (MKCoordinateRegion)region {
    RegionBounds bounds;
    
    double latitudeSpan = region.span.latitudeDelta;
    double longitudeSpan = region.span.longitudeDelta;
    bounds.latMin = region.center.latitude - latitudeSpan;
    bounds.latMax = region.center.latitude + latitudeSpan;
    bounds.lngMin = region.center.longitude - longitudeSpan;
    bounds.lngMax = region.center.longitude + longitudeSpan;
    
    return bounds;
}

+ (NSString *) dictionaryToJSON:(NSDictionary *) jsonDict {
    NSString * jsonString = nil;
    NSData * json = nil;
    NSError * error = nil;
    
    // Dictionary convertable to JSON ?
    if ([NSJSONSerialization isValidJSONObject:jsonDict])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:&error];
        
        // If no errors, let's view the JSON
        if (json != nil && error == nil)
        {
            jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            
            //PQLog(@"JSON: %@", jsonString);
        } else {
            PQLog(@"ERROR: %@", error);
        }
    }
    return jsonString;
}

// FUCK YOU APPLE!!!!!!!!!
// You are not allowed to have body in GET
+ (void)getPostByID:(NSNumber *)postID withCallback:(void(^)(Post * post)) callback
{
    // Make network call to get post based on post id
    
    // construct url
    NSMutableString * hostURLString = [NSMutableString string];
    [hostURLString appendString:[NSString stringWithFormat:[PQEnvironment.serverBaseURLString stringByAppendingString:kPostURL], postID.intValue]];
    
    // prepare user creds
    NSString * requestURLString = [self prepareUserCreds:hostURLString];
    // create request
    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:requestURLString]];
    // create session
    NSURLSession * session = [NSURLSession sharedSession];
    
    // execute session
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        });
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            // Error in response or making request
            PQLog(@"Network error: %@", responseError);
        } else {
            // Response is OK
            NSError * jsonParseError = NULL;
            NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParseError];
            
            if (jsonParseError) {
                PQLog(@"Json parser error: %@", jsonParseError);
                responseError = jsonParseError;
            } else {
                
                Post * post = [Post modelWithDictionary:json];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    callback(post);
                });
                return;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(nil);
        });
        
    }] resume];
}

+ (void) addComment:(NSString *)comment toPost:(Post *)post withCallback:(void(^)(BOOL finished)) callback
{
    NSMutableString * hostURLString = [NSMutableString string];
    // create command
    [hostURLString appendString:[NSString stringWithFormat:[PQEnvironment.serverBaseURLString stringByAppendingString: kGetCommentURL], post.postId.intValue]];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:hostURLString]];
    [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    
    // create session
    NSURLSession * session = [NSURLSession sharedSession];
    
    User * user = [PQUserDefaults getUser];
    
    NSDictionary * credentialsJSON = @{
                                       @"token": user.token,
                                       @"secret": user.secret
                                       };
    NSDictionary * messageJSON = @{@"message": comment};
    NSDictionary * requestJSON = @{
                                   @"creds": credentialsJSON,
                                   @"comment": messageJSON
                                   };
    
    NSString * serializedJSON = [self dictionaryToJSON:requestJSON];
    NSData * data = [serializedJSON dataUsingEncoding:NSUTF8StringEncoding];
    
    [[session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        });
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            // Error in response or making request
            PQLog(@"Network error: %@", responseError);
        } else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                callback(YES);
            });
            return;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(NO);
        });
    }] resume];
}

+ (void) getComments:(Post *) post withCallback:(void(^) (Post * post, NSArray * comments)) callback
{
    NSMutableString * hostURLString = [NSMutableString string];
    // create command
    [hostURLString appendString:[NSString stringWithFormat:[PQEnvironment.serverBaseURLString stringByAppendingString: kGetCommentURL], post.postId.intValue]];
    // append user creds
    NSString * requestURLString = [self prepareUserCreds:hostURLString];
    // create request
    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:requestURLString]];
    // create session
    NSURLSession * session = [NSURLSession sharedSession];
    
    // execute session
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        });
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            // Error in response or making request
            PQLog(@"Network error: %@", responseError);
        } else {
            // Response is OK
            NSError * jsonParseError = NULL;
            NSArray * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParseError];
            
            if (jsonParseError) {
                PQLog(@"Json parser error: %@", jsonParseError);
                responseError = jsonParseError;
            } else {
                
                //TODO: read comments out
                NSMutableArray * comments = [NSMutableArray array];
                
                for (NSDictionary * dic in json) {
                    [comments insertObject:[Comment createCommentWithDictionary:dic] atIndex:0];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    callback(post, comments);
                });
                
                return;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(post, nil);
        });
        
    }] resume];
}

+ (void) setPost:(Post *) post liked:(BOOL) liked withCallback:(void(^) (BOOL success, Post * post)) callback
{
    [self reactToPost:post react:liked? 1: 0 withCallback:callback];
}

/*
 * React to a post
 *
 * 0: undo any reaction
 * 1: like
 *
 */
+ (void) reactToPost:(Post *) post react:(int) reaction withCallback:(void(^) (BOOL success, Post * post)) callback
{
    NSString * hostURLString = [NSString stringWithFormat:@"%@%@", [PQEnvironment.serverBaseURLString stringByAppendingString:kPostsURL], [NSString stringWithFormat:kReactToPostURL, post.postId.intValue]];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:hostURLString]];
    [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    
    // create session
    NSURLSession * session = [NSURLSession sharedSession];
    
    User * user = [PQUserDefaults getUser];
    
    NSDictionary * credentialsJSON = @{
                                       @"token": user.token,
                                       @"secret": user.secret
                                       };
    NSDictionary * reactionJSON = @{@"reaction": [NSNumber numberWithInt:reaction]};
    NSDictionary * requestJSON = @{
                                   @"creds": credentialsJSON,
                                   @"reaction": reactionJSON
                                   };
    
    NSString * serializedJSON = [self dictionaryToJSON:requestJSON];
    NSData * data = [serializedJSON dataUsingEncoding:NSUTF8StringEncoding];
    
    [[session uploadTaskWithRequest:request fromData:data completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        });
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];

        if (responseError) {
            // Error in response or making request
            PQLog(@"Network error: %@", responseError);
        } else {
            
            NSError * jsonParseError = NULL;
            NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParseError];
            
            if (jsonParseError) {
                
                PQLog(@"Failed to parse response: %@", jsonParseError);
            } else {
                
                NSNumber * result = [json objectForKey:@"success"];
                NSString * status = [json objectForKey:@"status"];
                
                if (result.intValue == 1 && ((reaction == 0 && [status isEqualToString:@"removed"]) || (reaction != 0 && [status isEqualToString:@"added"]))) {
                    
                    [self getPostByID:post.postId withCallback:^(Post *post) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            callback(YES, post);
                        });
                    }];
                    
                    return;
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(NO, nil);
        });
    }] resume];
}

+ (void) deletePost:(Post *) post withCallback:(void(^) (BOOL success)) callback
{
    User * user = [PQUserDefaults getUser];
    
    if (user.userId != post.userId) {
        callback(NO); // non post owner cannot delete a post
    } else {
        
        NSString * host = [NSString stringWithFormat:[PQEnvironment.serverBaseURLString stringByAppendingString:kPostURL], post.postId.intValue];
        
        NSString * url = [self prepareUserCreds:host];
        
        [[PQAPICore sharedInstance] performDelete:[NSURL URLWithString:url] callback:^(NSError *error, NSDictionary *responseData) {
            if (error || [responseData objectForKey:@"errors"] != nil) {
                PQLog(@"Error: Failed to delete post %d %@", post.postId.intValue, error);
                callback(NO);
            } else {
                callback(YES);
            }
        }];
    }
}

@end
