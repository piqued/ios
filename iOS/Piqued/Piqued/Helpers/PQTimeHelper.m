//
//  PQTimeHelper.m
//  Piqued
//
//  Created by Kenny M. Liou on 6/12/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQTimeHelper.h"

@implementation PQTimeHelper

+ (NSString *) timeFromNow:(NSDate *) timeStamp
{
    NSDate * now = [NSDate date];
    
    // Get the system calendar
    NSCalendar *sysCalendar = [NSCalendar currentCalendar];
    
    // Get conversion to months, days, hours, minutes
    NSCalendarUnit unitFlags = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitDay | NSCalendarUnitWeekOfYear | NSCalendarUnitMonth | NSCalendarUnitYear;
    
    NSDateComponents *breakdownInfo = [sysCalendar components:unitFlags fromDate:timeStamp  toDate:now  options:0];
    
    if ([breakdownInfo year] > 0) {
        return [self printYear:[breakdownInfo year]];
    } else if ([breakdownInfo month] > 0) {
        return [self printMonth:[breakdownInfo month]];
    } else if ([breakdownInfo weekOfYear] > 0) {
        return [self printWeek:[breakdownInfo weekOfYear]];
    } else if ([breakdownInfo day]) {
        return [self printDay:[breakdownInfo day]];
    } else if ([breakdownInfo hour] > 0) {
        return [self printHour:[breakdownInfo hour]];
    } else if ([breakdownInfo minute] > 0) {
        return [self printMinute:[breakdownInfo minute]];
    } else if ([breakdownInfo second] > 0) {
        return [self printSecond:[breakdownInfo second]];
    } else {
        return @"";
    }
}

+ (NSString *) printYear:(NSInteger) year
{
    return [self printString:(int) year withUnit:@"year"];
}

+ (NSString *) printMonth:(NSInteger) month
{
    return [self printString:(int) month withUnit:@"month"];
}

+ (NSString *) printWeek:(NSInteger) week
{
    return [self printString:(int) week withUnit:@"week"];
}

+ (NSString *) printDay:(NSInteger) day
{
    return [self printString:(int) day withUnit:@"day"];
}

+ (NSString *) printHour:(NSInteger) hour
{
    return [self printString:(int) hour withUnit:@"hour"];
}

+ (NSString *) printMinute:(NSInteger) min
{
    return [self printString:(int) min withUnit:@"min"];
}

+ (NSString *) printSecond:(NSInteger) sec
{
    return [self printString:(int) sec withUnit:@"second"];
}

+ (NSString *) printString:(int) count withUnit:(NSString *)unit
{
    if (count > 1) {
        return [NSString stringWithFormat:@"%d %@s", count, unit];
    } else {
        return [NSString stringWithFormat:@"%d %@", count, unit];
    }
}

@end
