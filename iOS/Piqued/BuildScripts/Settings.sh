#!/bin/bash -
#===============================================================================
#
#          FILE:  Settings.sh
# 
#         USAGE:  ./Settings.sh 
# 
#   DESCRIPTION:  Fill in values for the Settings.bundle plist file.
#                 This script is run in the phase "Run Edit Settings Bundle Script"
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR: Matt Mo, matt@piqued.io
#       COMPANY: 
#       CREATED: 01/03/2017
#      REVISION:  ---
#===============================================================================

# Treat unset variables as an error
set -o nounset

# Exit on error.
set -e

PLISTBUDDY="/usr/libexec/PlistBuddy"

SETTINGS_BUNDLE_PATH="$CODESIGNING_FOLDER_PATH/Settings.bundle/Root.plist"
INFO_PLIST_PATH="${CODESIGNING_FOLDER_PATH}/Info.plist"

# Get build version
BUNDLE_SHORT_VERSION=`$PLISTBUDDY -c "Print :CFBundleShortVersionString" ${INFO_PLIST_PATH}`
BUNDLE_VERSION=`$PLISTBUDDY -c "Print :CFBundleVersion" ${INFO_PLIST_PATH}`
APP_VERSION="$BUNDLE_SHORT_VERSION.$BUNDLE_VERSION"

if [[ "$CONFIGURATION" = *AppStore* ]]; then
  # Clear out existing entries
  $PLISTBUDDY -c "Delete :PreferenceSpecifiers" "${SETTINGS_BUNDLE_PATH}"

  # Reconstruct :PreferenceSpecifiers
  $PLISTBUDDY -c "Add :PreferenceSpecifiers array" "${SETTINGS_BUNDLE_PATH}"
  $PLISTBUDDY -c "Add :PreferenceSpecifiers:0 dict" "${SETTINGS_BUNDLE_PATH}"

  # Add group title for info
  $PLISTBUDDY -c "Add :PreferenceSpecifiers:0:Type string 'PSGroupSpecifier'" "${SETTINGS_BUNDLE_PATH}"
  $PLISTBUDDY -c "Add :PreferenceSpecifiers:0:Title string 'Version Information'" "${SETTINGS_BUNDLE_PATH}"

  # Add App version info
  $PLISTBUDDY -c "Add :PreferenceSpecifiers:1:Type string 'PSTitleValueSpecifier'" "${SETTINGS_BUNDLE_PATH}"
  $PLISTBUDDY -c "Add :PreferenceSpecifiers:1:Title string 'Release'" "${SETTINGS_BUNDLE_PATH}"
  $PLISTBUDDY -c "Add :PreferenceSpecifiers:1:Key string 'settings_app_version'" "${SETTINGS_BUNDLE_PATH}"
  $PLISTBUDDY -c "Add :PreferenceSpecifiers:1:DefaultValue string '${APP_VERSION}'" "${SETTINGS_BUNDLE_PATH}"
else
  # Set build version
  $PLISTBUDDY -c "Set :PreferenceSpecifiers:1:DefaultValue ${APP_VERSION}" "$SETTINGS_BUNDLE_PATH"

  # Set git info
  GIT_HASH="$(git rev-parse --short HEAD)"
  GIT_BRANCH="$(git rev-parse --abbrev-ref HEAD)"
  $PLISTBUDDY -c "Set :PreferenceSpecifiers:2:DefaultValue ${GIT_BRANCH}" -c "Set :PreferenceSpecifiers:3:DefaultValue ${GIT_HASH}" "$SETTINGS_BUNDLE_PATH"

  # Get build timestamp and set
  TIMESTAMP=`date "+%Y-%m-%d %H:%M"`
  $PLISTBUDDY -c "Set :PreferenceSpecifiers:4:DefaultValue ${TIMESTAMP}" "$SETTINGS_BUNDLE_PATH"

  # Print results
  $PLISTBUDDY -c "Print" "$SETTINGS_BUNDLE_PATH"
fi