//
//  PQBasketViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 11/28/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQBasketViewController.h"
#import "UIColor+PIQHex.h"
#import "PQAPIs.h"
#import "Post.h"
#import "AppDelegate.h"
#import "PQPostDetailCell.h"
#import "PQPostManager.h"
#import "PQBasketButtonsCell.h"
#import "PQPostCommentCell.h"
#import "PQCommentShowMore.h"
#import "PQBasketLocationCellNew.h"
#import "PQBasketRelatedCell.h"
#import "PQTipTitleCell.h"
#import "PQPostTipCell.h"
#import "PQPostMapCell.h"
#import "PQEmptyTipCell.h"
#import "PQUserDefaults.h"
#import "PQAnalyticHelper.h"
#import "PQFourSquareAPI.h"
#import "PQImageURLHelper.h"
#import "PQProfileViewController.h"
#import "PQCommentViewController.h"
#import "PQMainTabBarController.h"
#import "PopupMessage.h"
#import "PQYelpAPI.h"
#import "PQZagatAPI.h"
#import "PQThreadHelper.h"
#import "PQSupportTeamHelper.h"

#import <SDWebImage/UIImageView+WebCache.h>

@import MessageUI;

@interface PQBasketViewController () <UITableViewDelegate, UITableViewDataSource, PQBasketCellDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageHeightConstrain;

@property (strong, nonatomic) Post * post;
@property (strong, nonatomic) NSArray * comments;
@property (strong, nonatomic) NSString * postLocationInfo;
@property (strong, nonatomic) NSURL * fsLink;
@property (strong, nonatomic) NSURL * yelpLink;
@property (strong, nonatomic) NSURL * zagatLink;
@property (strong, nonatomic) UIView * loadingOverlay;

@property BOOL refetching;

@property (nonatomic) BOOL getFSData;
@end

@implementation PQBasketViewController

+ (NSString *) segueIdentifier
{
    return @"BasketViewSegue";
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.navigationController.navigationBar setTintColor:[UIColor piq_colorFromHexString:kOrangeColor]];
    
    
    [[PQPostManager sharedInstance] getPostAsync:self.postID callback:^(Post *post) {
        [self setPost:post];
        [self onGetPost];
    }];
    
    [self registerClassWithTable:NSStringFromClass([PQPostDetailCell class])];
    [self registerClassWithTable:NSStringFromClass([PQBasketButtonsCell class])];
    [self registerClassWithTable:NSStringFromClass([PQPostCommentCell class])];
    [self registerClassWithTable:NSStringFromClass([PQCommentShowMore class])];
    [self registerClassWithTable:NSStringFromClass([PQBasketLocationCellNew class])];
    [self registerClassWithTable:NSStringFromClass([PQBasketRelatedCell class])];
    [self registerClassWithTable:NSStringFromClass([PQTipTitleCell class])];
    [self registerClassWithTable:NSStringFromClass([PQPostTipCell class])];
    [self registerClassWithTable:NSStringFromClass([PQPostMapCell class])];
    [self registerClassWithTable:NSStringFromClass([PQEmptyTipCell class])];
    
    [self.tableView setContentInset:UIEdgeInsetsMake(300, 0, 0, 0)];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
}

- (UIView *) loadingOverlay
{
    if (!_loadingOverlay) {
        
        NSArray * subViewArray = [[NSBundle mainBundle] loadNibNamed:@"SimpleLoadingView" owner:self options:nil];
        
        _loadingOverlay = (UIView *) [subViewArray objectAtIndex:0];
        
        [_loadingOverlay setHidden:YES];
        
        [[UIApplication sharedApplication].keyWindow addSubview:_loadingOverlay];
    }
    
    return _loadingOverlay;
}

- (void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    [self.loadingOverlay setFrame:[UIApplication sharedApplication].keyWindow.bounds];
}

- (void) onGetPost
{
    if ([self.post isKindOfClass:[PQInstagramPost class]]) {
        [(PQInstagramPost *)self.post updateFsVenueIdWithCallback:^(Post * updatedPost) {
            [PQFourSquareAPI fetchVenueDataAndUpdatePost:updatedPost withCallback:^(Post *post, NSString * formattedAddressInfo, NSString * fsurl, NSError *error) {
                if (error) {
                    // Oh crap...
                } else {
                    // Notify the UI to update
                    [self setPostLocationInfo:formattedAddressInfo];
                    [self setFsLink:fsurl ? [NSURL URLWithString:fsurl]: NULL];
                    [self setGetFSData:YES];
                    [self performRelatedContentSearch];
                }
            }];
        }];
    } else {
        [PQFourSquareAPI fetchVenueDataAndUpdatePost:self.post withCallback:^(Post *post, NSString * formattedAddressInfo, NSString * fsurl, NSError *error) {
            
            if (error) {
                // Oh crap...
            } else {
                // Notify the UI to update
                [self setPostLocationInfo:formattedAddressInfo];
                [self setFsLink:fsurl ? [NSURL URLWithString:fsurl]: NULL];
                [self setGetFSData:YES];
                [self performRelatedContentSearch];
            }
        }];
    }
    
    [PQAPIs getComments:self.post withCallback:^(Post *post, NSArray *comments) {
        
        [self setComments:comments];
        [self.tableView reloadData];
        
        if (self.openCommentImmediately) {
            [self setOpenCommentImmediately:NO];
            [self showCommentView];
        }
    }];
    
    NSString * shrunkURL = [PQImageURLHelper adjustImageURLString:self.post.imageUrlLarge toSize:PQImageSizeLarge];
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:shrunkURL]
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                 if (image) {
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         self.imageView.image = image;
                                     });
                                 } else {
                                     if (self.refetching) {
                                         return;
                                     }
                                     
                                     [self refetchPost:self.post.postId];
                                 }
    }];
}

- (void)refetchPost:(NSNumber *)postID
{
    self.refetching = YES;
    [[PQPostManager sharedInstance] getPostAsync:postID andRefresh:YES callback:^(Post *post) {
        [self setPost:post];
        [self onGetPost];
        self.refetching = NO;
    }];
}

- (void) performRelatedContentSearch
{
    [PQYelpAPI getYelpURL:self.post withLocationHint:self.postLocationInfo withCallback:^(NSURL * _Nullable yelpLink) {
        
        [self setYelpLink:yelpLink];
        [self.tableView reloadData];
    }];
    [PQZagatAPI getZagatURL:self.post withCallback:^(NSURL * _Nullable zagatLink) {
        
        [self setZagatLink:zagatLink];
        [self.tableView reloadData];
    }];
}

- (void) registerClassWithTable:(NSString *) nibName
{
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [[PQAnalyticHelper sharedInstance] viewWillAppear:kBasket];
    [self.tableView setContentOffset:CGPointMake(0, -300)];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.loadingOverlay removeFromSuperview];
    [self setLoadingOverlay:NULL];
    
    [[PQAnalyticHelper sharedInstance] viewWillDisappear:kBasket];
    
    [[PQPostManager sharedInstance] setPostBookmarked:self.post isBookmarked:self.post.bookmarked];
    
    if (self.hideNavBarWhenBack) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
}

#pragma mark Helper Methods

- (void) showCommentView
{
    PQCommentViewController *commentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PQCommentViewController"];

    [commentViewController setPost:self.post];
    [commentViewController setComments:self.comments];
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    if (self.parentViewController.parentViewController != NULL && [self.parentViewController.parentViewController isKindOfClass:[PQMainTabBarController class]]) {
        [self.parentViewController.parentViewController.navigationController pushViewController:commentViewController animated:YES];
    } else if ([self.parentViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController * controller = (UINavigationController *) self.parentViewController;
        
        [controller pushViewController:commentViewController animated:YES];
    }
}

#pragma mark UICollectionView data source and delegate methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return BasketCellCount;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch ((PQBasketCellType) section) {
        case BasketDetail:
            return 1;
            break;
        case BasketButtons:
            return 1;
        case BasketComment:
            return MIN(2, 1 + [self.comments count]);
        case BasketShowMore:
            return [self.comments count] > 0 ? 1: 0;
        case BasketMap:
            return 1;
        case BasketLocation:
            return 1;
        case BasketRelatedContent:
            if (self.fsLink != NULL || self.yelpLink != NULL || self.zagatLink != NULL) {
                return 1;
            } else {
                return 0;
            }
        case BasketTipTitle:
            return 1;
        case BasketTips:
            return MAX(1, [self.post.tips count]);
        default:
            return 0;
            break;
    }
}

- (Class) getCellClass:(NSInteger) section
{
    switch ((PQBasketCellType) section) {
        case BasketDetail:
            return [PQPostDetailCell class];
        case BasketButtons:
            return [PQBasketButtonsCell class];
        case BasketComment:
            return [PQPostCommentCell class];
        case BasketShowMore:
            return [PQCommentShowMore class];
        case BasketMap:
            return [PQPostMapCell class];
        case BasketLocation:
            return [PQBasketLocationCellNew class];
        case BasketRelatedContent:
            return [PQBasketRelatedCell class];
        case BasketTipTitle:
            return [PQTipTitleCell class];
        case BasketTips:
            if ([self.post.tips count] > 0) {
                return [PQPostTipCell class];
            } else {
                return [PQEmptyTipCell class];
            }
        default:
            return NULL;
            break;
    }
}

- (NSString *) getCellName:(NSInteger) section
{
    return NSStringFromClass([self getCellClass:section]);
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger row = indexPath.row;
    
    NSString * cellName = [self getCellName:indexPath.section];
    
    PQBasketBaseCell * baseCell = [tableView dequeueReusableCellWithIdentifier:cellName];
    
    [baseCell setBasketDelegate:self];
    
    if ([baseCell isKindOfClass:[PQPostTipCell class]]) {
        PQPostTipCell * tipCell = (PQPostTipCell *) baseCell;
        
        [tipCell setCellIndex:(int)row];
    }
    
    if ([baseCell isKindOfClass:[PQPostCommentCell class]]) {
        PQPostCommentCell * commentCell = (PQPostCommentCell *) baseCell;
        
        if ([self.comments count] > 0) {
            [commentCell setType:PostMany];
        } else {
            [commentCell setType:PostSingle];
        }
        
        if (row > 0) {
            [commentCell setCommentIndex:(int)[self.comments count] - 1];
            [commentCell setSourceType:SourceComment];
        } else {
            [commentCell setCommentIndex:-1];
            [commentCell setSourceType:SourcePost];
        }
    }
    
    if ([baseCell isKindOfClass:[PQCommentShowMore class]]) {
        if ([self.comments count] < 1) {
            [baseCell setHidden:YES];
        }
    }
    
    if ([baseCell isKindOfClass:[PQBasketButtonsCell class]]) {
        [baseCell.contentView.superview setClipsToBounds:NO];
    }
    
    if ([[self.post foursquareVenueID] length] < 1) {
        [baseCell setHidden:YES];
    }
    
    if (self.getFSData) {
        [baseCell reloadData];
    }
    
    return baseCell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ((PQBasketCellType) indexPath.section) {
        case BasketDetail:
            return [PQPostDetailCell estimatedHeight];
        case BasketButtons:
            return [PQBasketButtonsCell estimatedHeight];
        case BasketComment:
            return [PQPostCommentCell estimatedHeight];
        case BasketShowMore:
            if ([self.comments count] == 0) {
                return 0;
            } else {
                return [PQCommentShowMore estimatedHeight];
            }
        case BasketMap:
            return [PQPostMapCell estimatedHeight];
        case BasketLocation:
            return [PQBasketLocationCellNew estimatedHeight];
        case BasketRelatedContent:
            return [PQBasketRelatedCell estimatedHeight];
        case BasketTipTitle:
            return [PQTipTitleCell estimatedHeight];
        case BasketTips:
            if ([self.post.tips count] > 0) {
                return [PQPostTipCell estimatedHeight];
            } else {
                return [PQEmptyTipCell estimatedHeight];
            }
        default:
            return 0;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int newHeight = scrollView.contentOffset.y * -1; // offset is negative
    
    [self.imageHeightConstrain setConstant:newHeight];
}

#pragma mark PQBasketCellDelegate methods

- (void) presentViewController:(UIViewController *)controller
{
    [self presentViewController:controller animated:YES completion:nil];
}

- (Post *) getPost
{
    return self.post;
}

- (void) openProfile:(double)userId
{
    PQLog(@"Profile opened");
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"TabProfile" bundle:nil];
    PQProfileViewController *profileVC = [storyBoard instantiateViewControllerWithIdentifier:[PQProfileViewController storyboardIdentifier]];
    profileVC.userId = userId;
    
    UINavigationController *navigationVC = [[UINavigationController alloc] initWithRootViewController:profileVC];
    [self presentViewController:navigationVC animated:YES completion:nil];
}

- (void) showComment
{
    [self showCommentView];
}

- (void)shareButtonPressed
{
    CGSize screenshotSize = self.view.window.bounds.size;
    screenshotSize.height = MIN(450, (int)screenshotSize.height * (2. / 3.));
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(screenshotSize, NO, [UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext(screenshotSize);
    }
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage *compressedImage = [UIImage imageWithData:UIImageJPEGRepresentation(image, 0.3)];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[self.post.address, compressedImage] applicationActivities:nil];
    [self presentViewController:activityVC];
}

- (Comment *) getComment:(int)index
{
    Comment *comment = nil;
    if (index >= 0 && index < self.comments.count) {
        comment = [self.comments objectAtIndex:index];
    }
    return comment;
}

- (NSString *) getPostLocationInfo
{
    if ([self.postLocationInfo length] > 0) {
        return self.postLocationInfo;
    } else {
        return @"USA";
    }
}

- (NSURL *) getFSLink
{
    return self.fsLink;
}

- (NSURL *) getYelpLink
{
    return self.yelpLink;
}

- (void) setLike:(BOOL) like
{
    [[PQPostManager sharedInstance] setPostLiked:self.post isLiked:like];
}

- (void) goBack
{
    [self.loadingOverlay removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClick:(id)sender {
    
    // show more
    User * myself = [PQUserDefaults getUser];
    
    if (self.post.userId == myself.userId) {
        // show delete
        [PopupMessage showActionSheetWithOneAction:@"Delete Post" andCallback:^(UIAlertAction *action) {
            
            [self.loadingOverlay setHidden:NO];
            
            [[PQPostManager sharedInstance] deletePost:self.post withCallback:^(BOOL success) {
                
                [PQThreadHelper performOnMainThread:^{
                    
                    [self.loadingOverlay setHidden:YES];
                    
                    if (success) {    
                        [PopupMessage displayTitle:@"Post UPDATE" withMessage:@"Your post is deleted!" withOkAction:^(UIAlertAction *action) {
                            
                                [self goBack];
                        } controller:self];
                        
                    } else {
                        [PopupMessage displayTitle:@"Post Update Error" withMessage:@"There seems to be some error, please try again" controller:self];
                    }
                }];
            }];
        } controller:self];
    } else {
        // show report
        [PopupMessage showActionSheetWithOneAction:@"Report Post" andCallback:^(UIAlertAction *action) {
            
            [PQSupportTeamHelper reportPost:self.post withViewController:self withDelegate:self];
        } controller:self];
    }
}

#pragma mark MailCompose Controller method
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(nullable NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:^{
        if (error || result == MFMailComposeResultFailed) {
            [PopupMessage displayTitle:@"Error" withMessage:@"There seems to be something wrong, please try again" controller:self];
        } else if (result == MFMailComposeResultSent) {
            
            [PopupMessage displayTitle:@"Thank you!" withMessage:@"We appreciate your feedback! We will look into this issue ASAP!" controller:self];
        }
    }];
}


@end
