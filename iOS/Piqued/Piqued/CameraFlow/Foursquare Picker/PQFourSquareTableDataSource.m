//
//  PQFourSquareTableDataSource.m
//  Piqued
//
//  Created by Matt Mo on 10/14/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import "PQFourSquareTableDataSource.h"
#import "PQFourSquarePlaceTableViewCell.h"
#import "PQFourSquareAPI.h"
#import "PQFourSquareAPIConstants.h"
#import "PQSearchHandler.h"

NSString * const kTableIdentifier = @"PQFourSquarePlaceTableViewCell";

@interface PQFourSquareTableDataSource()

@property (atomic, assign) BOOL useLocation;
@property (nonatomic, strong) PQSearchHandler *searchHandler;

@end

@implementation PQFourSquareTableDataSource

- (void)setSearchBar:(UISearchBar *)searchBar
{
    _searchBar = searchBar;
}

- (PQSearchHandler *)searchHandler
{
    if (_searchHandler == nil) {
        _searchHandler = [[PQSearchHandler alloc] init];
        @weakify(self);
        _searchHandler.searchCallback = ^(NSString *query) {
            @strongify(self);
            NSMutableDictionary *options = [NSMutableDictionary dictionary];
            if (self.useLocation) {
                options[@"ll"] = [NSString stringWithFormat:@"%f,%f", self.location.coordinate.latitude, self.location.coordinate.longitude];
            } else {
                options[@"near"] = self.locationName;
            }
            if (query.length > 0) {
                options[@"query"] = query;
            }
            
            [PQFourSquareAPI requestFSExploreResults:options withCallback:^(NSArray *venues, NSError *error) {
                self.data = venues;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableView reloadData];
                });
            }];
        };
    }
    return _searchHandler;
}

- (instancetype)initWithTableView:(UITableView *)tableView
{
    if (self = [super init]) {
        self.tableView = tableView;
        [self registerIdentifier:kTableIdentifier];
    }
    return self;
}

- (void)setupTableView:(UITableView *)tableView
{
    self.tableView = tableView;
    [self registerIdentifier:kTableIdentifier];
}

- (void)updateSearchResultsWithPlaceName:(NSString *)placeName
{
    self.useLocation = NO;
    self.locationName = placeName;
    [self.searchHandler scheduleSearch:self.searchBar.text withMinimumLength:0];
}

- (void)updateSearchResultsWithLocation:(CLLocation *)location
{
    self.useLocation = YES;
    self.location = location;
    [self.searchHandler scheduleSearch:self.searchBar.text withMinimumLength:0];
}

- (void)loadWithInitialResults:(NSArray *)data location:(CLLocation *)location
{
    _data = data;
    _location = location;
    _useLocation = YES;
}

#pragma mark - Table View Support

- (void)registerIdentifier:(NSString *)tableIdentifier
{
    [self.tableView registerNib:[UINib nibWithNibName:kTableIdentifier bundle:NSBundle.mainBundle]
         forCellReuseIdentifier:kTableIdentifier];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PQFourSquarePlaceTableViewCell * locationCell = [tableView dequeueReusableCellWithIdentifier:kTableIdentifier];
    
    NSDictionary * venueData = self.data[indexPath.row];
    
    [locationCell.venueName setText:venueData[kPQFourSquareVenueKeyName]];
    
    [locationCell.address setText:[PQFourSquareAPI getAddressString:venueData[kPQFourSquareVenueKeyLocation]]];
    
    return locationCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSAssert(false, @"didSelectRowAtIndexPath is not implemented for PQFourSquareTableDataSource");
}

#pragma mark - Search

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self.searchHandler scheduleSearch:searchText withMinimumLength:3];
}

#pragma mark - Data Source Access

- (id)objectAtIndexedSubscript:(NSUInteger)index
{
    return self.data[index];
}

- (void)setObject:(id)obj atIndexedSubscript:(NSUInteger)index
{
    return;
}

@end
