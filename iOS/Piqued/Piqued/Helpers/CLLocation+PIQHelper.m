//
//  CLLocation+PIQHelper.m
//  Piqued
//
//  Created by Matt Mo on 3/25/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import "CLLocation+PIQHelper.h"
@import MapKit;

@implementation CLLocation (PIQHelper)

- (CLLocation *)piq_randomLocation:(double)meters
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        srand48(time(0));
    });
    
    // Get scale, because the number of meters per degree is different at different latitudes
    double mapPointsPerMeter = MKMapPointsPerMeterAtLatitude(self.coordinate.latitude);
    
    // Offset = (random number between -0.75 and 0.75) * (mapPoints per meter * meters)
    // We use 0.75 because it's difficult to get exact meters per points (meter varies per latitude degree)
    double randomXOffset = (drand48() - 0.5) * 1.5 * (mapPointsPerMeter * meters);
    double randomYOffset = (drand48() - 0.5) * 1.5 * (mapPointsPerMeter * meters);
    
    MKMapPoint randomMapPoint = MKMapPointForCoordinate(self.coordinate);
    randomMapPoint.x = randomMapPoint.x + randomXOffset;
    randomMapPoint.y = randomMapPoint.y + randomYOffset;

    CLLocationCoordinate2D randomCoordinate = MKCoordinateForMapPoint(randomMapPoint);
    
    CLLocation * location = [[CLLocation alloc] initWithLatitude:randomCoordinate.latitude
                                                       longitude:randomCoordinate.longitude];
    
    return location;
}

@end
