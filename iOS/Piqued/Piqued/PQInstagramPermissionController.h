//
//  PQInstagramPermissionViewController.h
//  Piqued
//
//  Created by Kenny M. Liou on 4/17/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PQInstagramPermissionController : NSObject


+ (void) showPermissionMessage:(UIViewController *) controller withOkayBlock:(void (^ __nullable)(UIAlertAction *action))okayBlock;

@end
