//
//  PQUserManager.m
//  Piqued
//
//  Created by Kenny M. Liou on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQUserManager.h"
#import "PQAPIs.h"

@interface PQUserManager()

+ (instancetype) sharedInstance;

@property (strong, nonatomic) NSMutableDictionary * userDictionary;

@end

@implementation PQUserManager

+ (instancetype) sharedInstance {
    static PQUserManager * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

+ (void)getUserWithID:(NSNumber *) userID withCallback:(void (^)(User * user)) callback
{
    [PQAPIs fetchUserInfo:userID.doubleValue withCallback:^(User *user) {
        [[PQUserManager sharedInstance] addUser:user];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(user);
        });
    }];
}

- (NSMutableDictionary *) userDictionary
{
    if (_userDictionary == NULL) {
        _userDictionary = [NSMutableDictionary dictionary];
        
    }
    
    return _userDictionary;
}

- (User *) getUserWithID:(NSNumber *) userID
{
    return [self.userDictionary objectForKey:userID];
}

- (void) addUser:(User *) user
{
    if (user) {
        [self.userDictionary setObject:user forKey:[NSNumber numberWithDouble:user.userId]];
    }
}

@end
