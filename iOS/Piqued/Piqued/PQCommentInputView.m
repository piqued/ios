//
//  PQCommentInputView.m
//  Piqued
//
//  Created by Kenny M. Liou on 7/8/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQCommentInputView.h"
#import "PQScreenHelper.h"
#import "UIColor+PIQHex.h"

@interface PQCommentInputView()

@property (weak, nonatomic) IBOutlet UIView *overlay;
@property (weak, nonatomic) IBOutlet UITextView *inputTextView;

@property (weak, nonatomic) IBOutlet UILabel *hintMessage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerBottomConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerHeightConstrain;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIView *loadingContainer;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndivator;

@end

@implementation PQCommentInputView

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onOverlayTapped)];
    [tapGesture setNumberOfTapsRequired:1];
    
    [self.overlay addGestureRecognizer:tapGesture];
}

- (void) setCurrentKeyboardHeight:(CGFloat)currentKeyboardHeight
{
    _currentKeyboardHeight = currentKeyboardHeight;
    
    [self.containerBottomConstrain setConstant:currentKeyboardHeight];
    
    [UIView animateWithDuration:0.1 animations:^{
        [self layoutIfNeeded];
    }];
}

#pragma mark Public methods

- (NSString *) getComment
{
    return self.inputTextView.text;
}

- (void) resetView
{
    [self.inputTextView setText:@""];
    [self.loadingContainer setHidden:YES];
    [self.loadingIndivator stopAnimating];
}

#pragma mark Delegate Methods
- (void) onOverlayTapped
{
    if ([self.delegate respondsToSelector:@selector(onOverlayTapped)]) {
        [self.delegate onOverlayTapped];
    }
}

#pragma mark TextViewDelegate Methods

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    CGFloat halfScreenHeight = [PQScreenHelper getScreenHeight] / 2;
    
    CGFloat remainHeight = halfScreenHeight - self.currentKeyboardHeight;
    
    if (remainHeight > textView.contentSize.height) {
        
        if (textView.contentSize.height > 40) {
            [self.containerHeightConstrain setConstant:textView.contentSize.height];
        } else {
            [self.containerHeightConstrain setConstant:41];
        }
    }
    
    return YES;
}

- (void) textViewDidChange:(UITextView *)textView
{
    if (textView.text.length > 0) {
        [self.hintMessage setHidden:YES];
        [self.sendButton setEnabled:YES];
        [self.sendButton setTitleColor:[UIColor piq_colorFromHexString:kOrangeColor] forState:UIControlStateNormal];
    } else {
        [self.hintMessage setHidden:NO];
        [self.sendButton setEnabled:NO];
        [self.sendButton setTitleColor:[UIColor piq_colorFromHexString:@"CCCCCC"] forState:UIControlStateNormal];
    }
}

- (IBAction)onSendComment:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(addComment:)]) {
        [self.inputTextView resignFirstResponder];
        [self.loadingContainer setHidden:NO];
        [self.loadingIndivator startAnimating];
        [self.delegate addComment:[self getComment]];
    }
}

- (void) viewIsVisiable:(BOOL) visiable
{
    if (visiable) {
        [self.inputTextView becomeFirstResponder];
    } else {
        [self.inputTextView resignFirstResponder];
    }
}

@end
