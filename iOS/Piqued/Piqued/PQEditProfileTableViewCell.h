//
//  PQEditProfileTableViewCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 8/20/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQEditProfileHeader.h"

typedef enum {
    EditProfile_Name,
    EditProfile_Location,
    EditProfile_Bio,
    EditProfile_Email,
    EditProfile_Password,
    EditProfile_Phone,
    EditProfile_DefaultMap
    
} EditProfileType;

@interface PQEditProfileTableViewCell : UITableViewCell

@property (weak, nonatomic) id<PQEditProfileHeaderDelegate> delegate;

+ (int) estimatedHeight;

- (void) setCellType:(EditProfileType) type;
- (void) setInfoValue:(NSString *) value;

@end
