//
//  PQMiniBasketViewController.h
//  Piqued
//
//  Created by Matt Mo on 1/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PQMiniBasketViewController : UIViewController

+ (NSString *)segueIdentifier;
- (void) setPostId:(double)postId;

@end
