//
//  PQAlertControllerHelper.h
//  Piqued
//
//  Created by Kenny M. Liou on 9/3/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQAlertControllerHelper : NSObject

+ (UIAlertAction *) createActionWithTitle:(NSString *) title andIntegerValue:(NSInteger)integer withHandler:(void (^ __nullable)(UIAlertAction *action))handler;

@end
