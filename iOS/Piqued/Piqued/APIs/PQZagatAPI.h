//
//  PQZagatAPI.h
//  Piqued
//
//  Created by Kenny M. Liou on 7/10/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post.h"

@interface PQZagatAPI : NSObject

+ (void) getZagatURL:(Post * _Nonnull)post withCallback:(void(^ _Nonnull) (NSURL * _Nullable zagatLink)) callback;

@end
