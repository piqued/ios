//
//  PQMapPinAnnotation.m
//  Piqued
//
//  Created by Kenny M. Liou on 11/12/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQMapPinAnnotation.h"

@interface PQMapPinAnnotation()

@end

@implementation PQMapPinAnnotation

- (instancetype) initWithCoordinates:(CLLocationCoordinate2D) coords {
    self = [super init];
    if (self) {
        _coordinate = coords;
    }
    
    return self;
}

@end
