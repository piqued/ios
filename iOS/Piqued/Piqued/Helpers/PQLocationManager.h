//
//  PQLocationManager.h
//  Piqued
//
//  Created by Kenny M. Liou on 10/26/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
@import CoreLocation;

@protocol PQLocationManagerDelegate

- (void)locationManagerDidUpdateLocation:(NSArray *)locations;
- (void)locationManagerDidChangeAuthorizationStatus:(CLAuthorizationStatus)status;

@optional
- (void)locationManagerDidFailWithError:(NSError *)error;

@end

@interface PQLocationManager : NSObject <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager * locationManager;
@property (strong, nonatomic) CLLocation * location;

@property (weak, nonatomic) id delegate;

+ (instancetype) sharedInstance;

@end