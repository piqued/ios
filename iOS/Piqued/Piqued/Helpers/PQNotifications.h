//
//  PQNotifications.h
//  Piqued
//
//  Created by Matt Mo on 7/2/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UserNotifications;

NS_ASSUME_NONNULL_BEGIN
/**
 PQNotifications
 
 Wrapper class for handling Push Notifications
 */
@interface PQNotifications : NSObject<UNUserNotificationCenterDelegate>

@property (weak, nonatomic) UIViewController *viewController;

+ (void)registerForPushNotifications;

+ (instancetype)sharedManager;

// AppDelegate Remote Notification Handler
+ (void)handleRemoteNotificationsDeviceToken:(NSData *)deviceToken;
+ (void)piquedApplication:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;

// Badge Updates
+ (void)updateBadgeOnLoad;
+ (void)markAllNotificationsRead;

NS_ASSUME_NONNULL_END
@end
