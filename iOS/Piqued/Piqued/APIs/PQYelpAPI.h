//
//  PQYelpAPI.h
//  Piqued
//
//  Created by Kenny M. Liou on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post.h"

NS_ASSUME_NONNULL_BEGIN

@interface PQYelpAPI : NSObject

+ (void) getYelpURL:(Post *)post withLocationHint:(NSString *)hint withCallback:(void (^)(NSURL *_Nullable yelpLink))callback;

+ (void) getYelpURL:(NSString *)postTitle location:(CLLocation *)location withLocationHint:(NSString *)hint withCallback:(void(^)(NSURL * _Nullable yelpLink)) callback;

@end

NS_ASSUME_NONNULL_END
