//
//  FirstViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 10/18/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQExplorerViewController.h"
#import "PQExplorerContainerViewController.h"
#import "PQMiniBasketViewController.h"
#import "PQBasketViewController.h"
#import "PQNewPostViewController.h"
#import "PQInstagramPermissionController.h"
#import "PQScrollHandler.h"

#import "PQNotifications.h"

@interface PQExplorerViewController () <PQScrollHandlerDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *viewModeSegmentControl;
@property (weak, nonatomic) PQExplorerContainerViewController * containerViewController;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIScrollView *filterView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonConstraint;
@property (assign) CGFloat cameraButtonBottomLimitConstraintConstant;
@property (assign) CGFloat cameraButtonTopLimitConstraintConstant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonTrailing;
//@property (weak, nonatomic) IBOutlet UIBarButtonItem *searchButton;

@property (strong, nonatomic) PQMiniBasketViewController *miniBasketViewController;
@property (strong, nonatomic) PQScrollHandler * handler;

@end

typedef enum {
    ExplorerGridView = 0,
    ExplorerMapView
} ExplorerSegmentControlType;

@implementation PQExplorerViewController
{
    CGFloat _cameraButtonToBottomConstraintConstant;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
#warning TODO - remove when implementing search
//    [self.searchButton setTintColor:[UIColor clearColor]];

    [self setupFilterView];
    
    [self setHandler:[[PQScrollHandler alloc] init]];
    [self.handler setDelegate:self];
    
    self.cameraButtonBottomLimitConstraintConstant = self.buttonConstraint.constant;
    CGFloat topConstraint = 0;
    if (@available(iOS 11.0, *)) {
        topConstraint = self.buttonConstraint.constant + self.tabBarController.tabBar.frame.size.height - self.tabBarController.tabBar.safeAreaInsets.bottom;
    } else {
        // Fallback on earlier versions
        topConstraint = self.buttonConstraint.constant + self.tabBarController.tabBar.frame.size.height;
    }
    self.cameraButtonTopLimitConstraintConstant = topConstraint;
    self.buttonConstraint.constant = self.cameraButtonTopLimitConstraintConstant;
    
    [PQNotifications sharedManager].viewController = self;
    [PQNotifications registerForPushNotifications];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    // deal with different device size
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    if (iOSDeviceScreenSize.width == 320) { // iPhone 5
        self.buttonTrailing.constant = 8;
    } else if (iOSDeviceScreenSize.width == 375) { // iPhone 6
        self.buttonTrailing.constant = 16;
    } else { // iPhone 6 plus
        self.buttonTrailing.constant = 21;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PQTabBarControllerTabProtocol

- (void)tabBarControllerDidSwitchToViewController:(UITabBarController *)tabBarController
{
    
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:[PQMiniBasketViewController segueIdentifier]]) {
        self.miniBasketViewController = segue.destinationViewController;
    } else if ([segue.identifier isEqualToString:[PQExplorerContainerViewController segueIdentifier]]) {
        self.containerViewController = segue.destinationViewController;
        [self.containerViewController setExplorerController:self];
    } else {
        PQLog(@"ERROR: unused identifier is '%@'", segue.identifier);
    }
}

#pragma mark - PQMapViewControllerDelegate

- (void)prepareForMiniBasket:(BOOL)showBasket {
    [self.cameraButton setHidden:showBasket];
}

#pragma mark - Filter

- (void)setupFilterView {
    [self.filterView setBackgroundColor:[UIColor piq_colorFromHexString:@"#FFFFFFB2"]];
    [self.filterView setShowsHorizontalScrollIndicator:NO];
    [self.filterView setShowsVerticalScrollIndicator:NO];
    [self.filterView setHidden:YES];
    
    [self addFilters];
}

- (void)addFilters {
    NSArray * filterItems = @[@"all", @"following", @"food", @"events", @"shopping", @"coffee", @"nightlife", @"fun", @"dessert", @"liked"];
    
    int offsetX = 0;
    CGFloat buttonHeight = self.filterView.frame.size.height;
    UIColor *buttonTitleNormalColor = [UIColor piq_colorFromHexString:kOrangeColor];
    UIColor *buttonTitleSelectedColor = [UIColor piq_colorFromHexString:kSelectedColor];
    
    for (int i = 0; i < filterItems.count; i++) {
        NSAttributedString *attrTitleNormal = [self getAttributedStringForFilterButtonNormal:filterItems[i]];
        NSAttributedString *attrTitleSelected = [self getAttributedStringForFilterButtonSelected:filterItems[i]];
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(offsetX, 0, 100, buttonHeight)];
        [button setTitle:filterItems[i] forState:UIControlStateNormal];
        [button setTitleColor:buttonTitleNormalColor forState:UIControlStateNormal];
        [button setTitleColor:buttonTitleSelectedColor forState:UIControlStateSelected];
        
        [button setAttributedTitle:attrTitleNormal forState:UIControlStateNormal];
        [button setAttributedTitle:attrTitleSelected forState:UIControlStateSelected];
        
        [button addTarget:self action:@selector(onFilterClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if (i == 0) {
            [button setSelected:YES];
        }
        [self.filterView addSubview:button];
        
        offsetX += button.frame.size.width;
    }
    
    self.filterView.contentSize = CGSizeMake(offsetX, self.filterView.frame.size.height);
}

- (NSAttributedString *)getAttributedStringForFilterButtonNormal:(NSString *)string
{
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setLineBreakMode:NSLineBreakByTruncatingTail];
    [style setAlignment:NSTextAlignmentCenter];
    
    UIColor *color = [UIColor piq_colorFromHexString:kOrangeColor];
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:17.0];
    
    NSDictionary *attributes = @{
                                 NSParagraphStyleAttributeName : style,
                                 NSForegroundColorAttributeName : color,
                                 NSFontAttributeName : font,
                                 };
    
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:string attributes:attributes];
    
    return attrString;
}

- (NSAttributedString *)getAttributedStringForFilterButtonSelected:(NSString *)string
{
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setLineBreakMode:NSLineBreakByTruncatingTail];
    [style setAlignment:NSTextAlignmentCenter];
    
    UIColor *color = [UIColor piq_colorFromHexString:kSelectedColor];
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:17.0];
    
    NSDictionary *attributes = @{
                                 NSParagraphStyleAttributeName : style,
                                 NSForegroundColorAttributeName : color,
                                 NSFontAttributeName : font,
                                 };
    
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:string attributes:attributes];
    
    return attrString;
}

- (void)onFilterClicked:(id)sender
{
    PQLog(@"filter tapped");
}

#pragma mark - Segment Control

- (IBAction)handleSegmentControl:(id)sender {
    UISegmentedControl * control = sender;
    if (control.selectedSegmentIndex == ExplorerGridView) {
        [self.cameraButton setHidden:NO];
        [self.containerViewController changeToViewWithSegueIdentifier:[PQGridViewController segueIdentifier]];
    } else if (control.selectedSegmentIndex == ExplorerMapView) {
        [self.containerViewController changeToViewWithSegueIdentifier:[PQMapViewController segueIdentifier]];
        [PQMainTabBarController setTabBarHidden:NO];
    }
}

- (void) onScroll:(PQScrollDirection)direction
{
    switch (direction) {
        case UP:
            self.buttonConstraint.constant = self.cameraButtonBottomLimitConstraintConstant;
            break;
        case DOWN:
            self.buttonConstraint.constant = self.cameraButtonTopLimitConstraintConstant;
            break;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void) gridViewDidScrolled:(UIScrollView *)scrollView
{
    [self.handler onViewScrolled:scrollView];
}

@end
