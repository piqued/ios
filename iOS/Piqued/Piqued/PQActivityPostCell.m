//
//  PQActivityPostCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 2/21/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQActivityPostCell.h"
#import "PQPostManager.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "EventModel.h"
#import "User.h"
#import "Post.h"

@interface PQActivityPostCell()<UIGestureRecognizerDelegate>

// UI Elements
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeStamp;
@property (weak, nonatomic) IBOutlet UIView *nameShadow;
@property (weak, nonatomic) IBOutlet UIImageView *postImage;
@property (weak, nonatomic) IBOutlet UILabel *postContent;
@property (weak, nonatomic) IBOutlet UIView *postContentShadow;

// Model Manager
@property (weak, nonatomic) PQPostManager * manager;

// Gestures
@property (strong, nonatomic) UITapGestureRecognizer *tapGestureRecognizer;

@end

@implementation PQActivityPostCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self setManager:[PQPostManager sharedInstance]];
    
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    self.tapGestureRecognizer.delegate = self;
    [self addGestureRecognizer:self.tapGestureRecognizer];
}

- (void)handleTapGesture:(UITapGestureRecognizer *)tapGesture
{
    CGPoint tapPoint = [tapGesture locationInView:self];
    
    CGRect profileImageRect = [self convertRect:self.profileImage.frame fromView:self.profileImage.superview];
    CGRect userNameLabelRect = [self convertRect:self.nameLabel.frame fromView:self.nameLabel.superview];
    
    if (CGRectContainsPoint(profileImageRect, tapPoint) ||
        CGRectContainsPoint(userNameLabelRect, tapPoint)) {
        [self.delegate didTapUserArea:self];
    } else {
        [self.delegate didTapCell:self];
    }
}

- (void)setEvent:(EventModel *)event
{
    _event = event;
    User *user = event.actor;
    [self setUserImage:user.profileImage];
    [self setUserName:user.userName];
    [self setPostContentString:event.decodedMessage];
    
    self.postImage.hidden = event.eventType == PQEventTypeFollow;
    if (event.eventType != PQEventTypeFollow && event.post != nil) {
        Post *post = event.post;
        [self.manager addPost:post];
        [self updatePostImage:post.imageUrlSmall];
    }
}

- (void)prepareForReuse
{
    [self setEvent:nil];
    [self setUserImage:nil];
    [self setUserName:nil];
    [self updatePostImage:nil];
    [self setPostContentString:nil];
}

- (void)setImage:(UIImageView *)image withURL:(NSString *) imageUrl withPlaceHolderName:(NSString *)placeHolderName
{
    if (imageUrl && imageUrl.length > 0) {
        [image sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:placeHolderName]];
    } else {
        [self.profileImage setImage:[UIImage imageNamed:placeHolderName]];
    }
}

- (void)updatePostImage:(NSString *) postImageUrl
{
    [self setImage:self.postImage withURL:postImageUrl withPlaceHolderName:kBlankPostImage];
}

- (void)setUserImage:(NSString *) imageUrl
{
    [self setImage:self.profileImage withURL:imageUrl withPlaceHolderName:kBlankProfileImage];
}

- (void)setUserName:(NSString *) name
{
    BOOL showName = name && name.length > 0;
    
    [self.nameShadow setHidden:showName];
    [self.nameLabel setHidden:!showName];
    
    [self.nameLabel setText:name];
}

- (void)setPostContentString:(NSString *) content
{
    BOOL showContent = content && content.length > 0;
    
    [self.postContentShadow setHidden:showContent];
    [self.postContent setHidden:!showContent];
    
    [self.postContent setText:content];
}

@end
