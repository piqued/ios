//
//  PQAPI.m
//  Piqued
//
//  Created by Kenny M. Liou on 8/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQiMessageAPI.h"
#import "PQEnvironment.h"
#import "PQiMessageUserDefault.h"
#import "NSHTTPURLResponse+PIQHelper.h"
#import "PQEnvironment.h"

#define kBookmarkedPostsPath           @"/bookmarks.json"

// Four Square API
#define FourSquareAPIEndpointVenues @"https://api.foursquare.com/v2/venues/%@?"
#define FourSquareClientID @"Z2A30YOWRDA4EK0IZORLVD34EQ2MCESXJYFHDI54IPGOIKN3"
#define FourSquareClientSecret @"HQL12015KJFVBCQVOP1DXZSXBFBJS4DSZ5BFRT5L0IWVUX13"
#define FourSquareVersionParam @"20160227" // 2016, Feb 27


@implementation PQiMessageAPI

+ (NSString *) prepareUserCreds:(NSString *) hostUrl
{
    User * user = [PQiMessageUserDefault getUser];
    
    NSString * creds = [NSString stringWithFormat:@"%@?creds[token]=%@&creds[secret]=%@", hostUrl, user.token, user.secret];
    
    return creds;
}


+ (void)retrieveBookmarkedPosts:(void (^)(NSArray *bookmarkedPosts, NSError *error))callbackBlock {
    // Make network call to get bookmarked posts
    // API call returns array
    
    NSMutableString * hostURLString = [NSMutableString string];
    [hostURLString appendString:PQEnvironment.serverBaseURLString];
    [hostURLString appendString:kBookmarkedPostsPath];
    
    NSString * requestURLString = [self prepareUserCreds:hostURLString];
    NSLog(@"url: %@", requestURLString);
    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:requestURLString]];
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            // Error in response or making request
            NSLog(@"Network error: %@", responseError);
        } else {
            // Response is OK
            NSError * jsonParseError = NULL;
            NSData * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParseError];
            
            if (jsonParseError) {
                NSLog(@"Json parser error: %@", jsonParseError);
                responseError = jsonParseError;
            } else {
                NSMutableArray * posts = [[NSMutableArray alloc] init];
                
                for (id obj in ((NSArray *)json)) {
                    NSDictionary * dic = obj;
                    
                    [posts addObject:[Post modelWithDictionary:dic]];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    callbackBlock(posts, nil);
                });
                return;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callbackBlock(nil, responseError);
        });
        
    }] resume];
}

#pragma mark Four Square

+ (NSString *) addCredentialsToURLString:(NSString *) url
{
    NSString * newCredentialsString = [NSString stringWithFormat:@"%@&client_id=%@", url, FourSquareClientID];
    newCredentialsString = [NSString stringWithFormat:@"%@&client_secret=%@", newCredentialsString, FourSquareClientSecret];
    return newCredentialsString;
}

+ (NSString *) addVersionParam: (NSString *) url {
    return [NSString stringWithFormat:@"%@&v=%@", url, FourSquareVersionParam];
}


+ (void) fetchPostNumberFromFourSquare:(Post *)post withCallback:(void (^)(Post * post, NSError * error))callback {
    NSString * venueId = nil;
    
    venueId = post.foursquareVenueID;
    
    if (venueId == NULL || [venueId isEqual:[NSNull null]] || [venueId isEqualToString:@""]) {
        
        if (callback) {
            callback(NULL, NULL);
        }
        return;
    }
    
    NSString * urlString = [self addCredentialsToURLString:
                            [self addVersionParam:
                             [NSString stringWithFormat:FourSquareAPIEndpointVenues, venueId]]];
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSError * jsonParseError = NULL;
            NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParseError];
            
            NSDictionary * response = [json objectForKey:@"response"];
            NSDictionary * venue = [response objectForKey:@"venue"];
            
            NSDictionary * contact = [venue objectForKey:@"contact"];
            
            NSString * phoneNumber = [contact objectForKey:@"formattedPhone"];
            
            if (jsonParseError) {
                NSLog(@"Json seralization error: %@", jsonParseError);
            } else {
                
                [post setPhoneNumber:phoneNumber];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    callback(post, nil);
                });
                return;
            }
        }
    }] resume];
}

@end
