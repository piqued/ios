//
//  PQFullyReplaceSegue.m
//  Piqued
//
//  Created by Matt Mo on 11/8/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQFullyReplaceSegue.h"
#import "AppDelegate.h"

@implementation PQFullyReplaceSegue

- (void)perform
{
    CGFloat dur = 0.3;
    UIView *destView = [self.destinationViewController view];
    UIView *srcView = [self.sourceViewController view];
    CGFloat viewWidth = srcView.frame.size.width;
    CGPoint center = srcView.center;
    AppDelegate *appDel = [[UIApplication sharedApplication] delegate];
    destView.frame = srcView.bounds;
    [appDel.window insertSubview:destView belowSubview:srcView];
    [appDel.window sendSubviewToBack:destView];
    
    [UIView animateWithDuration:dur animations:^{
        destView.frame = CGRectOffset(destView.frame, viewWidth, 0);
    } completion:^(BOOL finished) {
        [appDel.window bringSubviewToFront:destView];
        [UIView animateWithDuration:dur animations:^{
            destView.center = center;
            srcView.center = center;
        } completion:^(BOOL finished) {
            [srcView removeFromSuperview];
            appDel.window.rootViewController = self.destinationViewController;
        }];
    }];
}

@end
