//
//  EventModel.m
//  Piqued
//
//  Created by Matt Mo on 7/23/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "NSString+HTML.h"
#import "EventModel.h"

#pragma mark - String Constants
NSString *const kEventModelTypeComment = @"Comment";
NSString *const kEventModelTypeBookmark = @"Bookmark";
NSString *const kEventModelTypeFollow = @"Relationship";
NSString *const kEventModelTypeReaction = @"Reaction";

@interface EventModel()

@property (nonatomic, readwrite) User *targetUser;
@property (nonatomic, readwrite) Post *post;

@end

@implementation EventModel

#pragma ###############################################
#pragma mark - Helper
#pragma ###############################################

- (PQEventType)eventType
{
    if ([self.eventTypeString isEqualToString:kEventModelTypeReaction]) {
        return PQEventTypeReaction;
    }
    if ([self.eventTypeString isEqualToString:kEventModelTypeBookmark]) {
        return PQEventTypeBookmark;
    }
    if ([self.eventTypeString isEqualToString:kEventModelTypeFollow]) {
        return PQEventTypeFollow;
    }
    if ([self.eventTypeString isEqualToString:kEventModelTypeComment]) {
        return PQEventTypeComment;
    }
    return PQEventTypeLast;
}

- (Post *)post
{
    if (_post == nil && self.eventType != PQEventTypeFollow) {
        _post = [Post modelWithDictionary:self.eventTarget];
    }
    return _post;
}

- (User *)targetUser
{
    if (_targetUser == nil && self.eventType == PQEventTypeFollow) {
        _targetUser = [User modelWithDictionary:self.eventTarget];
    }
    return _targetUser;
}

- (NSDate *)timeStamp
{
    return [NSDate dateWithTimeIntervalSince1970:self.createdAtTimeStamp];
}

- (NSString *)decodedMessage
{
    return [self.message kv_decodeHTMLCharacterEntities];
}

#pragma ###############################################
#pragma mark - Mantle
#pragma ###############################################

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"actor"                   : @"actor",
             @"eventTypeString"         : @"eventType",
             @"message"                 : @"message",
             @"eventTarget"             : @"eventTarget",
             @"createdAtTimeStamp"      : @"created_at",
             };
}

@end
