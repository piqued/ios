//
//  PQAPI.h
//  Piqued
//
//  Created by Kenny M. Liou on 8/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post.h"

@interface PQiMessageAPI : NSObject

+ (void)retrieveBookmarkedPosts:(void (^)(NSArray *bookmarkedPosts, NSError *error))callbackBlock;

+ (void) fetchPostNumberFromFourSquare:(Post *)post withCallback:(void (^)(Post * post, NSError * error))callback;

@end
