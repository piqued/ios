//
//  PQImageHandler.h
//  Piqued
//
//  Created by Kenny M. Liou on 2/27/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#ifndef PQImageHandler_h
#define PQImageHandler_h

@protocol PQImageHandler

- (void) openPinDetailsWithImage:(UIImage *) image
                   withThumbnail:(UIImage *) thumbnail
                    withLocation:(CLLocation *) location;
- (void) showSpinner;
- (void) hideSpinner;

@end

#endif /* PQImageHandler_h */
