//
//  PQMainTabBarController.m
//  Piqued
//
//  Created by Kenny M. Liou on 11/28/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQMainTabBarController.h"
#import "PQBasketViewController.h"
#import "PQExplorerViewController.h"
#import "PQProfileViewController.h"
#import "AppDelegate.h"
#import "PQNotifications.h"

@interface PQMainTabBarController ()

@property (weak, nonatomic) Post * selectedPost;
@property (weak, nonatomic) PQProfileViewController *profileViewController;
@property (strong, nonatomic) PQMainTabBarControllerDelegate *mainDelegate;

@end

@implementation PQMainTabBarController


static PQMainTabBarController * mainTabBarController = NULL;

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.mainDelegate = [[PQMainTabBarControllerDelegate alloc] init];
        self.delegate = self.mainDelegate;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

#warning TODO - get rid of this stupid static variable
    // For some reaosn, initWithCoder gets called twice
    mainTabBarController = self;
    
    [PQNotifications updateBadgeOnLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (void)showTab:(PQMainTabBarControllerTabType)type
{
    mainTabBarController.selectedIndex = type;
    
    UIViewController *viewController = mainTabBarController.viewControllers[mainTabBarController.selectedIndex];
    UINavigationController *navVC = (UINavigationController *)viewController;
    UIViewController *navVCRootViewController = navVC.viewControllers[0];
    id<PQTabBarControllerTabProtocol> tabViewController = (id<PQTabBarControllerTabProtocol>) navVCRootViewController;
    [tabViewController tabBarControllerDidSwitchToViewController:mainTabBarController];
}

+ (void)setBadgeValue:(nullable NSString *)badgeValue forTab:(PQMainTabBarControllerTabType)type
{
    if (type < mainTabBarController.viewControllers.count) {
        mainTabBarController.viewControllers[type].tabBarItem.badgeValue = badgeValue;
    }
}

+ (void)showBasketView:(Post *) post
{
    [mainTabBarController setSelectedPost:post];
    [mainTabBarController performSegueWithIdentifier:[PQBasketViewController segueIdentifier] sender:mainTabBarController];
}

+ (void)setTabBarHidden:(BOOL) hidden
{
    int newY = mainTabBarController.view.frame.size.height;
    
    if (!hidden) {
        newY = mainTabBarController.view.frame.size.height - mainTabBarController.tabBar.frame.size.height;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect frame = mainTabBarController.tabBar.frame;
        frame.origin.y = newY;
        [mainTabBarController.tabBar setFrame:frame];
    }];
}

+ (void)setTabBarDelegate:(PQMainTabBarControllerDelegate *) delegate
{
    mainTabBarController.delegate = delegate;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController * destinationController = segue.destinationViewController;
    
    if ([destinationController isKindOfClass:[PQBasketViewController class]]) {
        
        PQBasketViewController * controller = (PQBasketViewController *) segue.destinationViewController;;
        [controller setHideNavBarWhenBack:YES];
        [controller setPostID:self.selectedPost.postId];
    }

}

@end

@implementation PQMainTabBarControllerDelegate

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    UINavigationController *navVC = (UINavigationController *)viewController;
    UIViewController *navVCRootViewController = navVC.viewControllers[0];
    id<PQTabBarControllerTabProtocol> tabViewController = (id<PQTabBarControllerTabProtocol>) navVCRootViewController;
    [tabViewController tabBarControllerDidSwitchToViewController:tabBarController];
    
    if ([navVCRootViewController isKindOfClass:PQExplorerViewController.class]) {
        PQExplorerViewController *explorerVC = (PQExplorerViewController *)navVCRootViewController;
        if (tabBarController.selectedIndex == PQMainTabBarControllerTapTypeMain) {
            if (explorerVC.view.window != nil) {
                [explorerVC.gridViewController scrollToTop];
            } else {
                [navVC popToRootViewControllerAnimated:YES];
            }
        }
    }
}

@end
