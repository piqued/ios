//
//  UICollectionViewCell+NibHelpers.h
//  Piqued
//
//  Created by Matt Mo on 7/21/18.
//  Copyright © 2018 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICollectionViewCell (NibHelpers)

+ (UINib *)piq_nib;
+ (NSString *)piq_nibName;
+ (NSString *)piq_reuseIdentifier;

@end
