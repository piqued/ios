//
//  PQCameraEmptyPreviewCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 2/27/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PQCameraEmptyPreviewCell : UICollectionViewCell

@end
