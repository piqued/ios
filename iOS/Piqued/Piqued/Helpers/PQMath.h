//
//  PQMath.h
//  Piqued
//
//  Created by Kenny M. Liou on 1/23/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQMath : NSObject

+ (double) getMiles:(Post *)post;

+ (NSString *) getMilesImage:(double) miles;

@end
