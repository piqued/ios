//
//  UIColor+PIQHex.m
//  Piqued
//
//  Created by Kenny M. Liou on 10/26/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "UIColor+PIQHex.h"

@implementation UIColor (PIQHex)

+ (UIColor *)piq_colorFromHexString:(NSString *) hex
{
    NSUInteger hexLength = [hex length];
    
    if (hexLength == 6) {
        return [self colorFromHexStringRGB:hex];
    } else if (hexLength == 7) {
        return [self colorFromStringWithHashRGB:hex];
    } else if (hexLength == 8) {
        return [self colorFromHexStringRGBA:hex];
    } else if (hexLength == 9) {
        return [self colorFromStringWithHashRGBA:hex];
    }
    
    return NULL;
}

+ (UIColor *)colorFromStringWithHashRGB:(NSString *) hexString
{
    return [UIColor colorFromHexStringRGB:[hexString substringFromIndex:1]]; // ignore #
}

+ (UIColor *)colorFromStringWithHashRGBA:(NSString *) hexString
{
    return [UIColor colorFromHexStringRGBA:[hexString substringFromIndex:1]]; // ignore #
}

// #RRGGBB
+ (UIColor *)colorFromHexStringRGB:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:0];
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

// #RRGGBBAA
+ (UIColor *)colorFromHexStringRGBA:(NSString *) hexString
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:0];
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF000000) >> 24)/255.0 green:((rgbValue & 0xFF0000) >> 16)/255.0 blue:((rgbValue & 0xFF00) >> 8)/255.0 alpha:(rgbValue & 0xFF)/255.0];
}

@end
