//
//  PQGlobalAlerts.h
//  Piqued
//
//  Created by Matt Mo on 4/13/18.
//  Copyright © 2018 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQGlobalAlerts : NSObject

+ (void)presentAlertWithTitle:(NSString *)title message:(NSString *)message;

@end
