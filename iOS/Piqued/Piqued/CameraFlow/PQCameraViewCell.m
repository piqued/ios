//
//  PQCameraViewCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 2/23/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQCameraViewCell.h"

@interface PQCameraViewCell()

@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;

@end

@implementation PQCameraViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (IBAction)buttonPressed:(UIButton *)sender {
    sender.enabled = NO;
    [self.delegate takePhoto];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.takePhotoButton.enabled = YES;
}

@end
