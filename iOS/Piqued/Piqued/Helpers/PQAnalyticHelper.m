//
//  PQAnalyticHelper.m
//  Piqued
//
//  Created by Kenny M. Liou on 1/8/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQAnalyticHelper.h"
#import "PQUserDefaults.h"
#import <Google/Analytics.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface PQAnalyticHelper()

@property (strong, nonatomic) NSString * currentViewName;
@property (strong, nonatomic) NSDate * viewAppearTimeStamp; // second
@property (nonatomic) BOOL isDebug;

@end
@implementation PQAnalyticHelper

+ (instancetype) sharedInstance
{
    static PQAnalyticHelper * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        [instance setIsDebug: [PQEnvironment debugAnalytics]];
    });
    
    return instance;
}

- (void) initialize
{
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // setup notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationEnteredForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnteredBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
}

- (void) enterForeGround
{
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    [gai.logger setLogLevel:kGAILogLevelVerbose];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions

    if (self.isDebug) {
        [gai setDispatchInterval:1];
        gai.logger.logLevel = kGAILogLevelNone;
        [gai.defaultTracker set:[GAIFields customDimensionForIndex:2]
               value:@"Internal"];
    } else {
        [gai.defaultTracker set:[GAIFields customDimensionForIndex:2]
                          value:@"External"];
    }
}

- (void)applicationEnteredForeground:(NSNotification *)notification {
    [self trackViewAppearWithName:self.currentViewName];
}

- (void)applicationDidEnteredBackground:(NSNotification *)notification {
    [self trackViewDuration:self.currentViewName];
}

- (void) viewWillAppear:(NSString *) viewName
{
    [self setCurrentViewName:viewName];
    
    [self trackViewAppearWithName:viewName];
}

- (void) viewWillDisappear:(NSString *) viewName
{
    [self trackViewDuration:viewName];
    
    [self setCurrentViewName:NULL];
}

- (void) sendEventWithTracker:(id<GAITracker>) tracker withBuilder:(GAIDictionaryBuilder *) builder
{
    NSString * userType = self.isDebug ? @"Internal" : @"External";
    User * user = [PQUserDefaults getUser];
    
    [tracker set:[GAIFields customDimensionForIndex:2] value:userType];
    [builder set:userType forKey:[GAIFields customDimensionForIndex:2]];
    
    if (user != nil) {
        [tracker set:[GAIFields customDimensionForIndex:1] value:user.userName];
        [builder set:[GAIFields customDimensionForIndex:1] forKey:user.userName];
    }
    
    [tracker send:[builder build]];
}

- (void) trackViewAppearWithName:(NSString *) viewName
{
    if (!viewName || [@"" isEqualToString:viewName]) return;
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker set:kGAIScreenName value:viewName];
    
    GAIDictionaryBuilder * builder = [GAIDictionaryBuilder createScreenView];

    [self sendEventWithTracker:tracker withBuilder:builder];
    
    [self setViewAppearTimeStamp:[NSDate date]];
}

- (void) trackViewDuration:(NSString *) viewName
{
    if (!viewName || [@"" isEqualToString:viewName]) return;
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    double duration = [[NSDate date] timeIntervalSinceDate:self.viewAppearTimeStamp];
    NSNumber * durationObj = [NSNumber numberWithDouble:duration];
    
    [tracker set:kGAIScreenName value:viewName];
    
    GAIDictionaryBuilder * builder = [GAIDictionaryBuilder createTimingWithCategory:@"View Duration"
                                                                           interval:durationObj
                                                                               name:viewName
                                                                              label:nil];
    [self sendEventWithTracker:tracker withBuilder:builder];
    
    if (!self.isDebug) {
        [Answers logCustomEventWithName:@"view duration"
                   customAttributes:@{
                                      @"view name"  : viewName,
                                      [NSString stringWithFormat:@"%@ duration", viewName]   : durationObj}];
    }
}

- (void) tearDown
{
    [self trackViewDuration:self.currentViewName];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
