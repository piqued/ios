//
//  PQFourSquareAPI.h
//  Piqued
//
//  Created by Matt Mo on 2/27/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQFourSquareAPI : NSObject

+ (void) requestFSExploreResults:(NSDictionary *)options withCallback:(void (^)(NSArray * venues, NSError * error))callbackBlock;

+ (void) fetchVenueDataAndUpdatePost:(Post *)post withCallback:(void (^)(Post * post, NSString * formattedAddressInfo, NSString * fsurl, NSError * error))callback;

// Note: this is only accepting FourSquare Location dictionary (i.e. venueData[@"location"]
+ (NSString *) getAddressString:(NSDictionary *) location;

@end
