#!/bin/bash -
#===============================================================================
#
#          FILE:  BundleVersion.sh
# 
#         USAGE:  ./BundleVersion.sh 
# 
#   DESCRIPTION:  Update the Bundle Version for Piqued Info.plist files.
#                 This script is run in the phase "Update Bundle Version Script"
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR: Matt Mo, matt@piqued.io
#       COMPANY: 
#       CREATED: 02/12/2017
#      REVISION:  ---
#===============================================================================

git=`sh /etc/profile; which git`

PLISTBUDDY="/usr/libexec/PlistBuddy"

# Synchronize bundle short version across all targets
echo "Begin synchronizing bundle short version"
$PLISTBUDDY -c "Set :CFBundleShortVersionString ${INFO_PLIST_VERSION}" "${INFOPLIST_FILE}"
$PLISTBUDDY -c "Set :CFBundleShortVersionString ${INFO_PLIST_VERSION}" "${INFOPLIST_FILE}"
echo "Synchronized bundle short version: ${INFO_PLIST_VERSION} for target: ${TARGET_NAME}"

# Increment the bundle version according to git commits
echo "Begin incrementing the bundle version"
ROUND_MIN_TO_FIVE=$(echo "$(date -u "+%M") - ($(date -u +%M)%5)" | bc)
ROUND_MIN_TO_FIVE=$(printf "%02d" $ROUND_MIN_TO_FIVE)
BUNDLE_DATE=$(echo "$(date -u "+%Y.%m.%d.%H.")$ROUND_MIN_TO_FIVE")
BUNDLE_VERSION=""

if [ $CONFIGURATION = "Debug" ]; then
  BRANCH_NAME=`"$git" rev-parse --abbrev-ref HEAD`
  BUNDLE_VERSION="$BUNDLE_DATE-$BRANCH_NAME"
else
  BUNDLE_VERSION="$BUNDLE_DATE"
fi

$PLISTBUDDY -c "Set :CFBundleVersion $BUNDLE_VERSION" "${INFOPLIST_FILE}"
echo "Incremented the bundle version to $BUNDLE_VERSION ${INFOPLIST_FILE}"
