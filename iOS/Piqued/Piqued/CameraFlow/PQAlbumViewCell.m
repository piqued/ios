//
//  PQAlbumCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 2/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQAlbumViewCell.h"
#import "PQAlbumThumbnailCell.h"

static NSString * const kThumbnailCell = @"PQAlbumThumbnailCell";
@import Photos;

@interface PQAlbumViewCell() <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) PHFetchResult *photoResult;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation PQAlbumViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.collectionView registerNib:[UINib nibWithNibName:kThumbnailCell bundle:NULL] forCellWithReuseIdentifier:kThumbnailCell];
    
    PHAuthorizationStatus currentStatus = [PHPhotoLibrary authorizationStatus];
    if (currentStatus == PHAuthorizationStatusNotDetermined) {
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            if (status == PHAuthorizationStatusAuthorized) {
                [self fetchPhotos];
            } else {
                // TODO: Show need permission
            }
        }];
    } else if (currentStatus == PHAuthorizationStatusAuthorized) {
        [self fetchPhotos];
    } else {
        // TODO: Show need permission
    }
}

- (void) fetchPhotos
{
    PHFetchOptions *allPhotosOptions = [PHFetchOptions new];
    allPhotosOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    options.wantsIncrementalChangeDetails = YES;
    options.predicate = [NSPredicate predicateWithFormat:@"mediaType == %d",PHAssetMediaTypeImage];
    
    PHFetchResult * collections = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary options:NULL];
    
    PHAssetCollection * cameraRoll = collections.firstObject;
    
    [self setPhotoResult:[PHAsset fetchAssetsInAssetCollection:cameraRoll options:allPhotosOptions]];
    
    if ([NSThread isMainThread]) {
        [self.collectionView reloadData];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
           [self.collectionView reloadData];
        });
    }
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PQAlbumThumbnailCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:kThumbnailCell forIndexPath:indexPath];
    
    // increment the cell tag
    NSInteger currentTag = cell.tag + 1;
    cell.tag = currentTag;
    
    PHAsset *asset = self.photoResult[indexPath.item];
    
    if (self.delegate) {
        
        PHCachingImageManager * manager = [self.delegate getManager];
        
        [manager requestImageForAsset:asset
                           targetSize:cell.frame.size
                          contentMode:PHImageContentModeAspectFill
                              options:nil
                        resultHandler:^(UIImage *result, NSDictionary *info) {
                            
                            // Only update the thumbnail if the cell tag hasn't changed. Otherwise, the cell has been re-used.
                            if (cell.tag == currentTag) {
                                [cell setImage:result];
                            }}
         ];
    }
    
    return cell;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (self.photoResult) {
        return [self.photoResult count];
    } else {
        return 0;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PHAsset *asset = self.photoResult[indexPath.item];
    
    if (self.delegate) {
        [self.delegate onImageAssetPicked:asset];
    }
}

@end
