//
//  PQFourSquareAPI.m
//  Piqued
//
//  Created by Matt Mo on 2/27/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQFourSquareAPI.h"
#import "PQLocationManager.h"
#import "NSHTTPURLResponse+PIQHelper.h"
#import "Post.h"
#import "PQInstagramPost.h"
#import "Tip.h"

#define FourSquareClientID @"3MAJVUVC2LV44RJQXUCS1LL5V5B5NAMWD5SQPFJRN5M42KSR"
#define FourSquareClientSecret @"VD4AD2DQJKSUAFINBG3OP3EPHHVV5XXZAKVTY1Q2TKI1F4T3"
#define FourSquareVersionParam @"20170729" // 2017, JUL 29

#define FourSquareAPIEndpointExplore @"https://api.foursquare.com/v2/venues/explore?"
#define FourSquareAPIEndpointSearch @"https://api.foursquare.com/v2/venues/search?"
#define FourSquareAPIEndpointVenues @"https://api.foursquare.com/v2/venues/%@?"

@implementation PQFourSquareAPI

+ (void) fetchVenueDataAndUpdatePost:(Post *)post withCallback:(void (^)(Post * post, NSString * formattedAddressInfo, NSString * fsurl, NSError * error))callback {
    NSString * venueId = nil;
    
    venueId = post.foursquareVenueID;
    
    if (venueId == NULL || [venueId isEqual:[NSNull null]] || [venueId isEqualToString:@""]) {
        
        if (callback) {
            callback(NULL, NULL, NULL, NULL);
        }
        return;
    }
    
    NSString * urlString = [PQFourSquareAPI addCredentialsToURLString:
                            [PQFourSquareAPI addVersionParam:
                             [NSString stringWithFormat:FourSquareAPIEndpointVenues, venueId]]];
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            PQLog(@"Error: %@", error);
        } else {
            NSError * jsonParseError = NULL;
            NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParseError];
            
            NSDictionary * response = [json objectForKey:@"response"];
            NSDictionary * venue = [response objectForKey:@"venue"];
            NSDictionary * tips = [venue objectForKey:@"tips"];
            NSDictionary * contact = [venue objectForKey:@"contact"];
            
            NSDictionary * location = [venue objectForKey:@"location"];
            
            NSDictionary * popular = [venue objectForKey:@"popular"];
            
            NSDictionary * hours;
            NSDictionary * openTimeFrame;
            if (popular) {
                openTimeFrame = [[popular objectForKey:@"timeframes"] firstObject];
            } else {
                hours = [venue objectForKey:@"hours"];
                openTimeFrame = [[hours objectForKey:@"timeframes"] firstObject];
            }
            
            NSDictionary * attributes = [venue objectForKey:@"attributes"];
            NSDictionary * priceInfo = [[attributes objectForKey:@"groups"] firstObject];
            NSDictionary * priceItem = [[priceInfo objectForKey:@"items"] firstObject];
            
            NSString * canonicalUrl = [venue objectForKey:@"canonicalUrl"];
            if ([canonicalUrl isEqual:[NSNull null]]) {
                canonicalUrl = @"";
            }
            
            NSString * formattedAddressInfo = [self getPostalAddressString:location];
            
            NSString * name = [venue objectForKey:@"name"];
            NSString * phoneNumber = [contact objectForKey:@"formattedPhone"];
            
            
            NSString * openHours = [PQFourSquareAPI generateOpenHours:openTimeFrame];
            
            double lat = [((NSNumber *)[location objectForKey:@"lat"]) doubleValue];
            double lng = [((NSNumber *)[location objectForKey:@"lng"]) doubleValue];
            BOOL isOpen = [[hours objectForKey:@"isOpen"] boolValue];
            int priceTier = [((NSNumber *) [priceItem objectForKey:@"priceTier"]) intValue];
            NSArray * fsTips = [PQFourSquareAPI getTips:tips];
            
            
            if (jsonParseError) {
                PQLog(@"Json seralization error: %@", jsonParseError);
            } else {
                [post setNewTitle:name];
                [post setPhoneNumber:phoneNumber];
                [post setIsOpenNow:isOpen];
                [post setPriceTier:priceTier];
                [post setOpenHours:openHours];
                [post setNewLat:lat andLng:lng];
                [post setTips:fsTips];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    callback(post, formattedAddressInfo, canonicalUrl, nil);
                });
                return;
            }
        }
    }] resume];
}

+ (NSString *) generateOpenHours:(NSDictionary *)timeframeDic
{
    NSMutableString * resultString = [NSMutableString string];
    
    if (timeframeDic != nil) {
        NSArray * openHours = [timeframeDic objectForKey:@"open"];
        
        [resultString appendFormat:@"%@: ", [timeframeDic objectForKey:@"days"]];
        
        for (int i = 0; i < openHours.count; i++) {
            
            NSDictionary * opensToday = [timeframeDic objectForKey:@"open"][i];
            
            if (i == 0) {
                [resultString appendFormat:@"%@", [opensToday objectForKey:@"renderedTime"]];
            } else {
                [resultString appendFormat:@", %@", [opensToday objectForKey:@"renderedTime"]];
            }
        }
    }
    
    return resultString;
}

+ (void) requestFSExploreResults:(NSDictionary *)options withCallback:(void (^)(NSArray * venues, NSError * error))callbackBlock
{
    NSString * urlString = [PQFourSquareAPI addCredentialsToURLString:[PQFourSquareAPI addVersionParam:FourSquareAPIEndpointSearch]];
    urlString = [PQFourSquareAPI parseOptions:options withString:urlString];
    
    NSURL * url = [NSURL URLWithString:urlString];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    // 3: Give the user feedback, while downloading the data source by enabling network activity indicator.
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        });
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            // Check for error with NSURLSession
            PQLog(@"Error: %@", responseError);
        } else {
            // Response is OK
            NSError * jsonParseError = NULL;
            NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParseError];
            
            if (jsonParseError) {
                PQLog(@"Json seralization error: %@", jsonParseError);
                responseError = jsonParseError;
            } else {
                NSArray * venues = json[@"response"][@"venues"];
                if (callbackBlock) {
                    callbackBlock(venues, nil);
                }
                return;
            }
        }
        if (callbackBlock) {
            callbackBlock(nil, responseError);
        }
    }] resume];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

#pragma mark - Private methods

+ (NSString *) parseOptions: (NSDictionary *)options withString:(NSString *)urlString
{
    __block NSMutableString * string = [urlString mutableCopy];
    [options enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        if ([key isKindOfClass:[NSString class]] && [obj isKindOfClass:[NSString class]]) {
            NSString *keyString = key;
            NSString *objString = obj;
            objString = [obj stringByReplacingOccurrencesOfString:@" " withString:@"+"];
            [string appendFormat:@"&%@=%@", keyString, objString];
        }
    }];
    return [string copy];
}

+ (NSString *) addNearbyParam:(NSString *)nearby withString:(NSString *)string {
    return [NSString stringWithFormat:@"%@&near=%@", string, nearby];
}

+ (NSString *) addLocationParam:(CLLocation *)location withString:(NSString *)string {
    return [NSString stringWithFormat:@"%@&ll=%f,%f", string, location.coordinate.latitude, location.coordinate.longitude];
}

+ (NSString *) addVersionParam: (NSString *) url {
    return [NSString stringWithFormat:@"%@&v=%@", url, FourSquareVersionParam];
}

+ (NSString *) addCredentialsToURLString:(NSString *) url
{
    NSString * newCredentialsString = [NSString stringWithFormat:@"%@&client_id=%@", url, FourSquareClientID];
    newCredentialsString = [NSString stringWithFormat:@"%@&client_secret=%@", newCredentialsString, FourSquareClientSecret];
    return newCredentialsString;
}

+ (NSString *) getAddressString:(NSDictionary *) location
{
    //TODO: this is a hack, keep this until post contain city, state, country info so basket view can use it
    if ([location objectForKey:@"formattedAddress"]) {
        // concat formatted address
        NSArray * formattedAddress = location[@"formattedAddress"];
        return [formattedAddress componentsJoinedByString:@", "];

    } else {
        // construct with address
        if ([location objectForKey:@"address"]) {
            NSMutableString * formattedAddressInfo = [NSMutableString string];
            
            [formattedAddressInfo appendString:[location objectForKey:@"address"]];
            [formattedAddressInfo appendString:@","];
            [formattedAddressInfo appendString:[self getPostalAddressString:location]];
            
            return formattedAddressInfo;
        } else {
            return @"";
        }
            
    }
}

+ (NSString *) getPostalAddressString:(NSDictionary *) location
{
    NSMutableString * formattedAddressInfo = [NSMutableString string];
    //TODO: this is a hack, keep this until post contain city, state, country info so basket view can use it
    if ([location objectForKey:@"city"]) {
        [formattedAddressInfo appendString:[location objectForKey:@"city"]];
        [formattedAddressInfo appendString:@", "];
    }
    if ([location objectForKey:@"state"]) {
        [formattedAddressInfo appendString:[location objectForKey:@"state"]];
        [formattedAddressInfo appendString:@" "];
    }
    if ([location objectForKey:@"postalCode"]) {
        [formattedAddressInfo appendString:[location objectForKey:@"postalCode"]];
        [formattedAddressInfo appendString:@", "];
    }
    if ([location objectForKey:@"cc"]) {
        [formattedAddressInfo appendString:[location objectForKey:@"cc"]];
    }
    
    return formattedAddressInfo;
}

+ (NSArray *) getTips:(NSDictionary *)tips
{
    NSMutableArray * result = [NSMutableArray array];
    
    if (tips) {
        
        
        NSDictionary * groups = [tips objectForKey:@"groups"][0];
        NSArray * items = [groups objectForKey:@"items"];
        
        if (items) {
            for (int i = 0; i < items.count; i++) {
                NSDictionary * item = items[i];
                NSDictionary * user = [item objectForKey:@"user"];
                NSDictionary * photo = [user objectForKey:@"photo"];
                
                Tip * tip = [[Tip alloc] init];
                
                [tip setFirstName:[user objectForKey:@"firstName"]];
                [tip setLastName:[user objectForKey:@"lastName"]];
                [tip setImagePrefix:[photo objectForKey:@"prefix"]];
                [tip setImageSuffix:[photo objectForKey:@"suffix"]];
                [tip setTipText:[item objectForKey:@"text"]];
                
                [result addObject:tip];
            }
        }
    }
    
    
    return result;
}

@end
