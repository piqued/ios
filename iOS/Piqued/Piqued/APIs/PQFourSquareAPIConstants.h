//
//  PQFourSquareAPIConstants.h
//  Piqued
//
//  Created by Matt Mo on 10/14/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef PQFourSquareAPIConstants_importingFromDotM
    #define PQFourSquare_NSString_CONST(key, value) NSString* const key = value;
#else
    #define PQFourSquare_NSString_CONST(key, value) FOUNDATION_EXPORT NSString* key;
#endif

PQFourSquare_NSString_CONST(kPQFourSquareVenueKeyName, @"name")
PQFourSquare_NSString_CONST(kPQFourSquareVenueKeyLocation, @"location")
