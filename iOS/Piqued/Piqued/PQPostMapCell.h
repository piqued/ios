//
//  PQPostMapCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 10/29/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQBasketBaseCell.h"

@interface PQPostMapCell : PQBasketBaseCell

+ (int) estimatedHeight;

@end
