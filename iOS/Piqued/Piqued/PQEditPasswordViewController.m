//
//  PQEditPasswordViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 8/20/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQEditPasswordViewController.h"
#import "PQEditProfileLoadingView.h"
#import "PQAPIs.h"
#import "PopupMessage.h"

@interface PQEditPasswordViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *passwordField;

@property (strong, nonatomic) PQEditProfileLoadingView * overlay;

@end

@implementation PQEditPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem.rightBarButtonItem setTarget:self];
    [self.navigationItem.rightBarButtonItem setAction:@selector(onSave)];
    
    [self.passwordField setDelegate:self];
}

- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [self.overlay removeFromSuperview];
    [self setOverlay:NULL]; // make sure we clear this view
}

- (PQEditProfileLoadingView *) overlay
{
    if (!_overlay) {
        _overlay = [self createOverlay];
    }
    
    return _overlay;
}

- (PQEditProfileLoadingView *) createOverlay
{
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PQEditProfileLoadingView class])  owner:self options:nil];
    
    PQEditProfileLoadingView * overlay = (PQEditProfileLoadingView *)[subviewArray objectAtIndex:0];
    
    [overlay setFrame:self.view.bounds];
    
    [overlay setHidden:YES];
    
    [[UIApplication sharedApplication].keyWindow addSubview:overlay];
    
    return overlay;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showPasswordPressed:(UIButton *)sender {
    
    [self.passwordField setSecureTextEntry:!self.passwordField.isSecureTextEntry];
}

#pragma mark Text view delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    if (newLength > 5) {
        [self.navigationItem.rightBarButtonItem setEnabled:YES];
    }
    
    return newLength <= 20;
}


- (void) onSave
{
    NSString * newPassword = self.passwordField.text;
    [self.overlay setHidden:NO];
    
    NSMutableDictionary *profileContents = [NSMutableDictionary dictionary];
    
    if (newPassword.length != 0) {
        profileContents[@"password"] = newPassword;
        profileContents[@"password_confirmation"] = newPassword;
    }
    
    [[PQNetworking.sharedInstance editProfile:profileContents] continueWithBlock:^id _Nullable(BFTask * _Nonnull t) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.overlay setHidden:YES];
            [self.view endEditing:YES];
            
            if (t.error) {
                [PopupMessage displayTitle:@"Account Update" withMessage:@"Sorry, there seems to be an issue. Please try again." withOkAction:^(UIAlertAction *action) {
                    
                    [self goBack:NO];
                    
                } controller:self];
            } else {
                [PopupMessage displayTitle:@"Account Update" withMessage:@"Your password Successfully!" withOkAction:^(UIAlertAction *action) {
                    
                    [self goBack:YES];
                    
                } controller:self];
            }
            
        });
        
        return nil;
    }];
}

- (void) goBack:(BOOL) success
{
    if (success) {
        [self.delegate showDoneButton];
    }
    
    [self.view endEditing:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
