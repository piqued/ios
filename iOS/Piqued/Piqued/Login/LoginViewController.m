//
//  WelcomeViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 10/18/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "LoginViewController.h"
#import "AnimationHelper.h"
#import "UIColor+PIQHex.h"
#import "UIView+PIQHelper.h"
#import "PopupMessage.h"
#import "PQAPIs.h"
#import "PQAnalyticHelper.h"
#import <FBSDKCoreKit/FBSDKAccessToken.h>
#import "AppDelegate.h"
#import <TTTAttributedLabel/TTTAttributedLabel.h>
@import SafariServices;

static NSString * const kWelcomeScreenSegue = @"WelcomeScreenSeque";
static NSString * const kLoginAppSegue = @"LoginAppSegue";

@interface LoginViewController () <UITextFieldDelegate, TTTAttributedLabelDelegate>
{
    NSMutableData *receivedData;
}

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UIView *loginContainer;
@property (weak, nonatomic) IBOutlet UIView *loginContent;
@property (weak, nonatomic) IBOutlet UIView *buttonsContainer;
@property (weak, nonatomic) IBOutlet UIButton *instagramButton;
@property (weak, nonatomic) IBOutlet FBSDKLoginButton *FBButton;
@property (weak, nonatomic) IBOutlet UIView *containerEmail;
@property (weak, nonatomic) IBOutlet UIView *containerPassword;
@property (weak, nonatomic) IBOutlet UITextField *userNameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet UIView *signupView;
@property (weak, nonatomic) IBOutlet UITextField *emailTextView;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextView;
@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *termsPrivacyLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonContainerBottomConstrain;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.FBButton.delegate = self;
    self.FBButton.readPermissions = @[@"public_profile", @"email"];
    
    [self setupView];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[PQAnalyticHelper sharedInstance] viewWillAppear:kLogin];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [[PQAnalyticHelper sharedInstance] viewWillDisappear:kLogin];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupView
{
    [self.containerEmail piq_setBorderColor:[UIColor piq_colorFromHexString:@"#979797"]];
    [self.containerPassword piq_setBorderColor:[UIColor piq_colorFromHexString:@"#979797"]];
    
    self.userNameField.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailTextView.keyboardType = UIKeyboardTypeEmailAddress;
    [self.emailTextView addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.passwordTextView addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.nameTextView addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [self addTermsPrivacyLinks];
}

- (void)addTermsPrivacyLinks
{
    // Terms of Service and Privacy link
    self.termsPrivacyLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink; // Automatically detect links when the label text is subsequently changed
    self.termsPrivacyLabel.delegate = self; // Delegate methods are called when the user taps on a link (see `TTTAttributedLabelDelegate` protocol)
    
    self.termsPrivacyLabel.linkAttributes = @{
                                    NSForegroundColorAttributeName: [UIColor piq_colorFromHexString:kOrangeColor],
                                    NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    
    self.termsPrivacyLabel.text = @"By tapping create you are indicating that you’ve\nread the terms of service and privacy policy"; // Repository URL will be automatically detected and linked
    
    NSRange termsRange = [self.termsPrivacyLabel.text rangeOfString:@"terms of service"];
    NSRange privacyRange = [self.termsPrivacyLabel.text rangeOfString:@"privacy policy"];
    
    [self.termsPrivacyLabel addLinkToURL:[NSURL URLWithString:@"http://piqued.io/terms-of-service.html"] withRange:termsRange]; // Embedding a terms link
    [self.termsPrivacyLabel addLinkToURL:[NSURL URLWithString:@"http://piqued.io/privacy-policy.html"] withRange:privacyRange]; // Embedding a privacy link
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)activateLogin:(id)sender {
    
    [AnimationHelper fadeOutAndHide:self.loginButton duration:0.5];
    [AnimationHelper fadeOutAndHide:self.logo duration:0.5];
    
    [AnimationHelper fadeInWithShift:self.loginContent
                              startX:self.loginContainer.frame.size.width
                                endX:0
                            duration:0.5];
    CGRect frame = self.FBButton.frame;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    float newUp = screenRect.size.height - (frame.origin.y +
                                            frame.size.height + 5 +
                                            self.buttonsContainer.frame.size.height);
    
    [AnimationHelper constrainChange:self.buttonContainerBottomConstrain
                           withValue:newUp
               withAnimationDuration:0.3
                            withView:self.view];
    
    [self.userNameField becomeFirstResponder];
}

- (void) deactivateLogin {
    [self.logo setHidden:NO];
    [self.logo setAlpha:1.0];
    [self.loginButton setHidden:NO];
    [self.loginButton setAlpha:1.0];
    [self.loginContent setHidden:YES];
    CGRect frame = self.loginContent.frame;
    frame.origin.x = self.loginContainer.frame.size.width;
    [self.loginContent setFrame:frame];
    [self.buttonContainerBottomConstrain setConstant:20.0];
    [self.view layoutIfNeeded];
}

- (IBAction)onPerformLogin:(id)sender {
    
    [self.view endEditing:YES];
    if ([[self.userNameField text] length] < 1) {
        [PopupMessage displayTitle:@"Missing User Name" withMessage:@"Please enter your user name." controller:self];
    } else if ([[self.passwordField text] length] < 1) {
        [PopupMessage displayTitle:@"Missing Password" withMessage:@"Please enter your password." controller:self];
    } else {
        
        [self loadingViewEnabled:YES];
        [PQAPIs loginWithName:[self.userNameField text] andPassword:[self.passwordField text] withCallback:^(User *user, NSError *error) {
            
            if (user) {
                // record user info and move on
                [self performSegueWithIdentifier:kWelcomeScreenSegue sender:self];
            } else {
                [PopupMessage displayTitle:@"Incorrect user name or password" withMessage:@"Please try again" controller:self];
            }
            [self loadingViewEnabled:NO];
        }];
    }
    
}

- (void) loadingViewEnabled:(BOOL) enabled
{
    [self.loadingView setHidden:!enabled];
    if (enabled) {
        [self.loadingIndicator startAnimating];
    } else {
        [self.loadingIndicator stopAnimating];
    }
}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    SFSafariViewController *vc = [[SFSafariViewController alloc] initWithURL:url];
    [self presentViewController:vc animated:YES completion:nil];
}

#pragma mark - FBSDKLoginButtonDelegate

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
    if (!error && !result.isCancelled) {
        [self loadingViewEnabled:YES];
        [PQAPIs loginWithSiteName:@"facebook" withSiteToken:[FBSDKAccessToken currentAccessToken].tokenString withCallback:^(User *user, NSError *error) {
            [self loadingViewEnabled:NO];
            if (user) {
                [self performSegueWithIdentifier:kWelcomeScreenSegue sender:self];
            } else {
                [PopupMessage displayTitle:@"Facebook Login" withMessage:@"Failed" controller:self];
            }
        }];
    } else {
        PQLog(@"Error: %@", error);
    }
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
    
}

- (BOOL)loginButtonWillLogin:(FBSDKLoginButton *)loginButton {
    return YES;
}

- (IBAction)showSignupView:(id)sender {
    [self.view endEditing:YES];
    [self setSignupViewVisiable:YES];
    [self.emailTextView setText:NULL];
    [self.nameTextView setText:NULL];
    [self.passwordTextView setText:NULL];
    [self setCreateAccountButtonEnabled:NO];
}

- (IBAction)hideSignupView:(id)sender {
    [self setSignupViewVisiable:NO];
}

- (void) setSignupViewVisiable:(BOOL) visiable {
    
    float endAlpha = visiable ? 1.0f :0.0f;
    if (visiable) {
        [self.signupView setAlpha:0];
        [self.signupView setHidden:NO];
    } else {
        [self.view endEditing:YES];
        [self.signupView setAlpha:1];
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        [self.signupView setAlpha:endAlpha];
    } completion:^(BOOL finished) {
        if (finished && !visiable) {
            [self.signupView setHidden:NO];
        }
        
        if (finished && visiable) {
            [self deactivateLogin];
        }
    }];
    
}

- (void) dismissKeyboard {
    [self.emailTextView resignFirstResponder];
    [self.passwordTextView resignFirstResponder];
    [self.nameTextView resignFirstResponder];
    [self.userNameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
}

- (IBAction)createAccount:(id)sender {
    [self dismissKeyboard];
    [self loadingViewEnabled:YES];
    
    [PQAPIs createAccount:self.nameTextView.text withEmail:self.emailTextView.text andPassword:self.passwordTextView.text andConfirmPassword:self.passwordTextView.text withCallback:^(User * user, NSError * error)  {
        
        if (user) {
            // record user info and move on
            [self performSegueWithIdentifier:kWelcomeScreenSegue sender:self];
        } else {
            [PopupMessage displayTitle:@"Failed to create account" withMessage:@"Please try again" controller:self];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadingViewEnabled:NO];
        });
    }];
}


#pragma mark TextField delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    UITextField * nextView = NULL;
    
    if (textField == self.emailTextView) {
        nextView = self.passwordTextView;
    } else if (textField == self.passwordTextView) {
        nextView = self.nameTextView;
    }
    
    [textField resignFirstResponder];
    if (nextView) {
        [nextView becomeFirstResponder];
    }
    
    return YES;
}

- (void) textFieldDidChange:(UITextField *) textField
{
    NSString * regex = @"\\A\\S+@\\S+\\.\\S+\\z";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isEmailValid = [emailTest evaluateWithObject:self.emailTextView.text];
    BOOL createAccountButtonEnabled =
        isEmailValid &&
        self.passwordTextView.text.length >= 6 &&
        self.nameTextView.text.length > 2;
    
    [self setCreateAccountButtonEnabled:createAccountButtonEnabled]; 
}

- (void) setCreateAccountButtonEnabled:(BOOL) enabled
{
    [self.createAccountButton setEnabled:enabled];
    
    if (!enabled) {
        [self.createAccountButton setBackgroundColor:[UIColor piq_colorFromHexString:@"#38363619"]];
        [self.createAccountButton.layer setBorderColor:[UIColor piq_colorFromHexString:@"#38363619"].CGColor];
        [self.createAccountButton setTitleColor:[UIColor piq_colorFromHexString:@"#CBCCCB"] forState:UIControlStateNormal];
    } else {
        [self.createAccountButton setBackgroundColor:[UIColor whiteColor]];
        [self.createAccountButton.layer setBorderColor:[UIColor piq_colorFromHexString:@"#F5A62399"].CGColor];
        [self.createAccountButton setTitleColor:[UIColor piq_colorFromHexString:kOrangeColor] forState:UIControlStateNormal];
    }
    
}


@end
