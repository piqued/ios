//
//  NSString+PIQHelper.m
//  Piqued
//
//  Created by Matt Mo on 4/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "NSString+PIQHelper.h"

@implementation NSString (PIQHelper)

+ (NSString *) piq_parseHashTags:(NSString *) string {
    NSArray * words = [string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    for (int i = 0; i < words.count; i++) {
        if ([words[i] containsString:@"#"]) {
            return words[i];
        }
    }
    
    return nil;
}

@end
