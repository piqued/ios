//
//  PopupMessage.h
//  Piqued
//
//  Created by Kenny M. Liou on 10/31/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopupMessage : NSObject

+ (void) displayTitle:(NSString *)title withMessage:(NSString *)message controller:(UIViewController *)controller;

+ (void) displayTitle:(NSString *)title withMessage:(NSString *)message withOkAction:(void(^) (UIAlertAction * action)) callback controller:(UIViewController *)controller;

+ (void) showActionSheetWithOneAction:(NSString *) action andCallback: (void(^) (UIAlertAction * action)) callback controller:(UIViewController *)controller;

@end
