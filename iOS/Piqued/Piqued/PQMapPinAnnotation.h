//
//  PQMapPinAnnotation.h
//  Piqued
//
//  Created by Kenny M. Liou on 11/12/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQMapPinAnnotation : NSObject <MKAnnotation>

// MKAnnotation Protocol
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

- (instancetype) initWithCoordinates:(CLLocationCoordinate2D) coords;

@end
