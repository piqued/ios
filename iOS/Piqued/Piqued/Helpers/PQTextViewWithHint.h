//
//  PQTextViewWithHint.h
//  Piqued
//
//  Created by Kenny M. Liou on 11/21/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PQTextViewWithHintDelegate

@required
- (void) onTabGestureDetected;

@end
@interface PQTextViewWithHint : UITextView

@property(nonatomic, strong) NSString *placeholder;

@property (nonatomic, strong) UIColor *realTextColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *placeholderColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, weak) id<PQTextViewWithHintDelegate> pq_delegate;


- (void) startTabGesture:(BOOL) start;

@end
