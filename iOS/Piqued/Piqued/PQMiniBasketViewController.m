//
//  PQMiniBasketViewController.m
//  Piqued
//
//  Created by Matt Mo on 1/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQMiniBasketViewController.h"
#import "PQProfileViewController.h"
#import "PQExplorerViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PQBasketViewController.h"
#import "NSString+PIQHelper.h"
#import "UIView+PIQHelper.h"
#import "PQPostManager.h"
#import "PQMath.h"
#import "PQImageCacheDelegate.h"

@interface PQMiniBasketViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *postImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIView *infoContainer;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UIButton *bookmarkButton;
@property (weak, nonatomic) IBOutlet UIView *distanceContainer;
@property (weak, nonatomic) IBOutlet UIImageView *distanceImage;


@property (weak, nonatomic) Post * currentPost;
@property (nonatomic) double currentPostId;

@property (strong, nonatomic) UITapGestureRecognizer * recognizer;

@end

@implementation PQMiniBasketViewController

+ (NSString *)segueIdentifier {
    return @"MiniBasketViewSegue";
}

- (UITapGestureRecognizer *) recognizer
{
    if (_recognizer == Nil) {
        _recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(displayBasketView:)];
    }
    
    return _recognizer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.view addGestureRecognizer:self.recognizer];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view removeGestureRecognizer:self.recognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // setup shadow
    NSArray * colors = [NSArray arrayWithObjects:(id)[[UIColor piq_colorFromHexString:@"0000008C"] CGColor], (id)[[UIColor piq_colorFromHexString:@"00000000"] CGColor], nil];
    [self.infoContainer piq_setGradientBackgroundWithColors:colors start:CGPointMake(0.0, 1.0) end:CGPointMake(0.0, 0.02)];
}
#pragma mark - Public Methods
- (void) setPostId:(double)postId
{
    _currentPostId = postId;
    // setup mini basket title
    Post *post = [[PQPostManager sharedInstance] getPost:@(postId)];
    self.currentPost = post;
    
    if (post.title && post.title.length > 0) {
        [self.locationLabel setText:post.title];
    } else {
        NSString * firstHashTag = [NSString piq_parseHashTags:post.desc];
        if (firstHashTag) {
            [self.locationLabel setText:firstHashTag];
        } else {
            [self.locationLabel setText:post.address];
        }
    }
    
    // setup distance
    double distance = [PQMath getMiles:post];
    NSString * distanceString;
    if (distance >= 20) {
        distanceString = [NSString stringWithFormat:@"%.lf mi", distance];
    } else {
        distanceString = [NSString stringWithFormat: @"%.1lf mi", distance];
    }
    
    [self.distanceLabel setText:distanceString];
    
    [self.distanceImage setImage:[UIImage imageNamed:[PQMath getMilesImage:distance]]];
    
    // setup bookmark button
    [self.bookmarkButton setSelected:post.bookmarked];
    
    // setup user info
    [self.userNameLabel setText:post.username];
    [self.userImageView sd_setImageWithURL:[NSURL URLWithString:post.userimage] placeholderImage:[UIImage imageNamed:@"blankProfile"]];
    
    // hide post content until image is downloaded
    [self.postImageView setHidden:YES];
    [self.distanceContainer setHidden:YES];
    [self.infoContainer setHidden:YES];
    
    // start downloading images
    NSString *imageUrl = post.imageUrlLarge;
    if (post.imageUrlLarge.length > 0) {
        imageUrl = post.imageUrlLarge;
    }
    SDWebImageManager* manager = [SDWebImageManager sharedManager];
    
    [manager downloadImageWithURL:[NSURL URLWithString:imageUrl]
                          options:0
                         progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                             // Stuff Here
                         }
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (error) {
                                // LAME....
                            } else if (finished && image) {
                                // Have image
                                [self.postImageView setAlpha:0.0f];
                                [self.infoContainer setAlpha:0.0f];
                                [self.distanceContainer setAlpha:0.0f];
                                
                                [self.postImageView setImage:image];
                                [self.postImageView setHidden:NO];
                                [self.infoContainer setHidden:NO];
                                [self.distanceContainer setHidden:NO];
                                
                                [UIView animateWithDuration:0.2 animations:^{
                                    [self.postImageView setAlpha:1.0f];
                                    [self.infoContainer setAlpha:1.0f];
                                    [self.distanceContainer setAlpha:1.0f];
                                }];
                            }
                        }];
}

- (void) displayBasketView:(UITapGestureRecognizer *) recognizer {
    CGPoint tapPoint = [recognizer locationInView:self.view];
    CGRect profileImageRect = [self.view convertRect:self.userImageView.frame fromView:self.userImageView.superview];
    CGRect userNameLabelRect = [self.view convertRect:self.userNameLabel.frame fromView:self.userNameLabel.superview];
    if (CGRectContainsPoint(profileImageRect, tapPoint) ||
        CGRectContainsPoint(userNameLabelRect, tapPoint)) {
        PQLog(@"Profile opened");
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"TabProfile" bundle:nil];
        PQProfileViewController *profileVC = [storyBoard instantiateViewControllerWithIdentifier:[PQProfileViewController storyboardIdentifier]];
        profileVC.userId = self.currentPost.userId;
        
        UINavigationController *navigationVC = [[UINavigationController alloc] initWithRootViewController:profileVC];
        [self presentViewController:navigationVC animated:YES completion:nil];
    } else {
        [self performSegueWithIdentifier:[PQBasketViewController segueIdentifier] sender:self];
    }
}

#pragma mark - UI methods
- (IBAction)onBookmarkPressed:(UIButton *)sender {
    
    Post * post = self.currentPost;
    
    // We send a 0 to the server, but actually this means 1 for bookmarked
    PQPostManager * manager = [PQPostManager sharedInstance];
    [manager addPost:post];
    
    BOOL bookmarked = !self.bookmarkButton.selected;
    
    [manager setPostBookmarked:post isBookmarked:bookmarked];
    
    [post setBookmarked:bookmarked];
    [self setIsBookmarked:bookmarked];
    
    // Fire notification to update posts across app
    NSDictionary * postUpdateInfo = @{@"post": post};
    [[NSNotificationCenter defaultCenter] postNotificationName:PostDidGetBookmarkedNotification object:self userInfo:postUpdateInfo];
}

- (void) setIsBookmarked:(BOOL) isBookmarked {
    [self.bookmarkButton setSelected:isBookmarked];
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UIViewController * destinationController = segue.destinationViewController;
    if ([destinationController isKindOfClass:[PQBasketViewController class]]) {
        PQBasketViewController * newPostView = (PQBasketViewController *) segue.destinationViewController;
        [newPostView setPostID:self.currentPost.postId];
    }
}

@end
