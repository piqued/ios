//
//  SettingsBundle.h
//  Piqued
//
//  Created by Matt Mo on 1/14/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettingsBundle : NSObject

+ (nullable NSString *)hostString;
+ (nullable NSURL *)hostURL;

@end

NS_ASSUME_NONNULL_END
