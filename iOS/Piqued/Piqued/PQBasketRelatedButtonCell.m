//
//  PQBasketRelatedButtonCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQBasketRelatedButtonCell.h"

@interface PQBasketRelatedButtonCell()

@property (weak, nonatomic) IBOutlet UIImageView *image;

@end

@implementation PQBasketRelatedButtonCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void) setType:(BasketRelatedType)type
{
    _type = type;
    
    NSString * imagename = NULL;
    switch (_type) {
        case Yelp:
            imagename = @"yelpLink";
            break;
        case Zagat:
            imagename = @"zagatLink";
            break;
        case FourSquare:
            imagename = @"foursquareLink";
            break;
        case TripAdvisor:
            imagename = @"tripadvisorLink";
            break;
        default:
            break;
    }
    
    if (imagename) {
        [self.image setImage:[UIImage imageNamed:imagename]];
    } else {
        [self.image setImage:NULL];
    }
    
}

- (IBAction)onButtonPressed:(UIButton *)sender {
    
    [[UIApplication sharedApplication] openURL:self.linkUrl];
}


@end
