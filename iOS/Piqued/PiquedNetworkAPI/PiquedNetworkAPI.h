//
//  PiquedNetworkAPI.h
//  PiquedNetworkAPI
//
//  Created by Matt Mo on 4/22/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for PiquedNetworkAPI.
FOUNDATION_EXPORT double PiquedNetworkAPIVersionNumber;

//! Project version string for PiquedNetworkAPI.
FOUNDATION_EXPORT const unsigned char PiquedNetworkAPIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PiquedNetworkAPI/PublicHeader.h>


