//
//  PQProfileViewController.h
//  Piqued
//
//  Created by Kenny M. Liou on 11/7/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "PQMainTabBarController.h"

@interface PQProfileViewController : UIViewController <PQTabBarControllerTabProtocol>

@property (nonatomic) double userId;

+ (NSString *)storyboardIdentifier;
+ (NSString *)segueIdentifier;

/*!
 Should only be called from PQMainTabBarController
 */
- (void)setCurrentUser;

@end
