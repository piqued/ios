//
//  PQInstagramButton.m
//  Piqued
//
//  Created by Matt Mo on 5/14/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQInstagramButton.h"
#import "PQUtilities.h"
#import "UIColor+PIQHex.h"
#import <InstagramKit/InstagramKit.h>

@interface PQInstagramButton()

@property (strong, nonatomic) PQInstagramManager * loginManager;

@end

@implementation PQInstagramButton

#pragma mark - Object LifeCycle

+ (instancetype)button
{
    PQInstagramButton * button = [self buttonWithType:UIButtonTypeCustom];
    [button configureButton];
    
    return button;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Login

- (void)configureButton
{
    self.loginManager = [PQInstagramManager sharedInstance];
    
    [self setImage:[UIImage imageNamed:@"Instagram Login"] forState:UIControlStateNormal];
    
    if ([[InstagramEngine sharedEngine] accessToken]) {
        [self setTitle:@"Sign Out of Instagram" forState:UIControlStateNormal];
        [self addTarget:self action:@selector(logOutOfInstagram) forControlEvents:UIControlEventTouchUpInside];
        UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 20);
        [self setImageEdgeInsets:insets];
    } else {
        [self setTitle:@"Sign Into Instagram" forState:UIControlStateNormal];
        [self addTarget:self action:@selector(loginToInstagram) forControlEvents:UIControlEventTouchUpInside];
        UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 33);
        [self setImageEdgeInsets:insets];
    }
    
    [self setBackgroundColor:[UIColor piq_colorFromHexString:@"#125688"]];
    [self setFrame:CGRectMake(0, 0, 240, 40)];
    self.layer.cornerRadius = 5;
}

- (void)loginToInstagram
{    
//    [self.loginManager logInFromViewController:[PQUtilities viewControllerforView:self]
//                                    handler:^(BOOL success) {
//                                        if (self.delegate && success) {
//                                            [self.delegate loginButton:self didCompleteWithResult:success error:nil];
//                                            [self updateButton];
//                                        }
//                                    }];
    NSString * message = @"We require an access token.";
    UIAlertController * instagramTokenAlert = [UIAlertController alertControllerWithTitle:@"Login to Instagram" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"login" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField * textField = instagramTokenAlert.textFields[0];
        [InstagramEngine sharedEngine].accessToken = textField.text;
    }];
    
    [instagramTokenAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Paste access token here.";
        [[NSNotificationCenter defaultCenter] addObserverForName:UITextFieldTextDidChangeNotification object:textField queue: [NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull notification) {
            okayAction.enabled = [textField.text length];
        }];
    }];
    
    [instagramTokenAlert addAction:cancelAction];
    [instagramTokenAlert addAction:okayAction];
    
    [[PQUtilities viewControllerforView:self] presentViewController:instagramTokenAlert animated:YES completion:nil];
}

- (void)logOutOfInstagram
{
    [[InstagramEngine sharedEngine] logout];
    [self updateButton];

}

#pragma mark - Private

- (void)updateButton
{
    if ([[InstagramEngine sharedEngine] accessToken]) {
        [self setTitle:@"Sign Out of Instagram" forState:UIControlStateNormal];
        [self addTarget:self action:@selector(logOutOfInstagram) forControlEvents:UIControlEventTouchUpInside];
        UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 20);
        [self setImageEdgeInsets:insets];
    } else {
        [self setTitle:@"Sign Into Instagram" forState:UIControlStateNormal];
        [self addTarget:self action:@selector(loginToInstagram) forControlEvents:UIControlEventTouchUpInside];
        UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 33);
        [self setImageEdgeInsets:insets];
    }
}

@end
