//
//  PQDeviceOrientation.h
//  Piqued
//
//  Created by Kenny M. Liou on 3/4/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQDeviceOrientationManager : NSObject

+ (instancetype) sharedInstance;
+ (UIDeviceOrientation) orientation;

@end
