//
//  PQUserManager.h
//  Piqued
//
//  Created by Kenny M. Liou on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface PQUserManager : NSObject

+ (void) getUserWithID:(NSNumber *) userID withCallback:(void (^)(User * user)) callback;

@end
