//
//  PopupMessage.m
//  Piqued
//
//  Created by Kenny M. Liou on 10/31/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PopupMessage.h"

@implementation PopupMessage

+ (void) displayTitle:(NSString *)title withMessage:(NSString *)message controller:(UIViewController *)controller
{
    [PopupMessage displayTitle:title withMessage:message withOkAction:NULL controller:controller];
}

+ (void) displayTitle:(NSString *)title withMessage:(NSString *)message withOkAction:(void(^) (UIAlertAction * action)) callback controller:(UIViewController *)controller
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              if (callback) {
                                                                  callback(action);
                                                              }
                                                          }];
    
    [alert addAction:defaultAction];
    
    [controller presentViewController:alert animated:YES completion:nil];
}

+ (void) showActionSheetWithOneAction:(NSString *) action andCallback: (void(^) (UIAlertAction * action)) callback controller:(UIViewController *)controller
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* firstAction = [UIAlertAction actionWithTitle:action
                                                            style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction * action) {
                                                              if (callback) {
                                                                  callback(action);
                                                              }
                                                          }];
    
    [alert addAction:firstAction];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    
    [alert addAction:cancelAction];
    
    [controller presentViewController:alert animated:YES completion:nil];
}

@end
