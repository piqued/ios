//
//  PQUserDefaultsConstants.h
//  Piqued
//
//  Created by Matt Mo on 8/7/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifdef PQUserDefaultsConstants_importingFromDotM

#define PQUserDefaults_NSString_CONST(key, value) NSString* const key = value;
#else
#define PQUserDefaults_NSString_CONST(key, value) FOUNDATION_EXPORT NSString* key;
#endif

PQUserDefaults_NSString_CONST(kPQUserDefaultsKeyOldPosts, @"OldPosts")
PQUserDefaults_NSString_CONST(kPQUserDefaultsKeyBookmarkArray, @"BookmarkArray")
PQUserDefaults_NSString_CONST(kPQUserDefaultsKeyActivitiesArray, @"ActivityArray")
PQUserDefaults_NSString_CONST(kPQUserDefaultsKeyPostsNearBy, @"NearByPostsArray")
