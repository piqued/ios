//
//  BaseModel.m
//  Piqued
//
//  Created by Matt Mo on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{};
}

+ (instancetype)modelWithDictionary:(NSDictionary *)dictionary
{
    NSError *error = nil;
    id model = [MTLJSONAdapter modelOfClass:self.class fromJSONDictionary:dictionary error:&error];
    
    if (error) {
        return nil;
    } else {
        [(BaseModel *)model setBackingJSON:dictionary];
    }
    
    return model;
}

- (nullable instancetype)initWithDictionary:(NSDictionary *)dictionary error:(NSError **)error
{
    self = [super initWithDictionary:dictionary error:error];
    if (self == nil) return nil;
    return self;
}

+ (NSArray *)getModelsFromArray:(NSArray *)array
{
    NSString *modelName = [NSString stringWithFormat:@"%@", [self class]];
    
    if ([modelName isEqualToString:NSStringFromClass([BaseModel class])]) {
        return nil;
    }
    
    NSMutableArray *mutableArray = [NSMutableArray new];
    for (NSDictionary *data in array) {
        id object = [NSClassFromString(modelName) modelWithDictionary:data];
        [mutableArray addObject:object];
    }
    return [mutableArray copy];
}

@end
