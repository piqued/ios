//
//  PQUtilities.h
//  Piqued
//
//  Created by Matt Mo on 5/14/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQUtilities : NSObject

+ (UIViewController *)topMostViewController;
+ (UIViewController *)viewControllerforView:(UIView*)view;

UIImage *scaleAndRotateImage(UIImage *image, NSUInteger maxResolution);

@end
