//
//  PQPostMapCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 10/29/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQPostMapCell.h"
#import "PQMapSelectionHelper.h"
#import "PQMapPinAnnotation.h"
#import <SDWebImage/SDWebImageManager.h>


#define pinRadius   (70)

@interface PQPostMapCell() <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property BOOL mapLoaded;

@end

@implementation PQPostMapCell

+ (int) estimatedHeight
{
    return 140;
}

- (void) reloadData
{
    [self setupMapview];
}


- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)onClick:(UIButton *)sender {
    
    [self startNavigation];
}

#pragma mark Helper Methods

- (void) startNavigation
{
    Post * post = [self.basketDelegate getPost];
    
    PQSupportedMaps defaultMap = [PQMapSelectionHelper getDefaultMap];
    
    if (defaultMap == pq_map_UNKNOWN) {
        
        NSArray<NSNumber *> * availableMapNames = [PQMapSelectionHelper getCurrentlyInstalledMaps];
        
        if ([availableMapNames count] == 1) {
            // well, how sad, you really should learn the other amazing app out there
            [self startNavigationWithURLScheme:[PQMapSelectionHelper getMapURLWithPost:post]];
        } else {
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Choose default"
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel Action")
                                           style:UIAlertActionStyleCancel
                                           handler:^(UIAlertAction *action)
                                           {
                                               [alertController dismissViewControllerAnimated:YES completion:Nil];
                                           }];
            
            [alertController addAction:cancelAction];
            
            for (int i = 0; i < [availableMapNames count]; i++) {
                NSNumber * mapType = availableMapNames[i];
                NSString * mapName = [PQMapSelectionHelper getMapName:mapType.intValue];
                
                UIAlertAction * action = [UIAlertAction
                                          actionWithTitle:mapName
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * _Nonnull action) {
                                              
                                              [PQMapSelectionHelper setDefaultMap:(PQSupportedMaps)mapType.intValue];
                                              [self startNavigationWithURLScheme:[PQMapSelectionHelper getMapURLWithPost:post]];
                                          }];
                
                [alertController addAction:action];
            }
            
            [self.basketDelegate presentViewController:alertController];
        }
    } else {
        
        [self startNavigationWithURLScheme:[PQMapSelectionHelper getMapURLWithPost:post]];
    }
}

- (void) startNavigationWithURLScheme: (NSString *) urlScheme
{
    NSURL *myURL = [NSURL URLWithString: urlScheme];
    [[UIApplication sharedApplication] openURL:myURL];
}

- (void) setupMapview
{
    if (self.mapLoaded) return;
    
    [self setMapLoaded:YES];
    
//    [self.mapButtonWidth setConstant: [[UIScreen mainScreen] bounds].size.width * self.pinWidthPercentage];
    
    Post * post = [self.basketDelegate getPost];
    
    [self.mapView setDelegate:self];
    
    CLLocationCoordinate2D coords = CLLocationCoordinate2DMake(post.lat, post.lng);
    CLLocationCoordinate2D regionCoords = CLLocationCoordinate2DMake(post.lat, post.lng);
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(regionCoords, 200, 50);
    [self.mapView setRegion:region animated:NO];
    
    PQMapPinAnnotation * newAnnotation = [[PQMapPinAnnotation alloc] initWithCoordinates:coords];
    
    [self.mapView addAnnotation:newAnnotation];
    [self.mapView setShowsUserLocation:YES];
    
    [self setNeedsLayout];
}

#pragma mark - MKMapViewDelegate methods

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    if (annotation == mapView.userLocation) return nil;
    
    // All other post pins
    PQMapPinAnnotation * pqannotation = nil;
    if ([annotation isKindOfClass:[PQMapPinAnnotation class]]) {
        pqannotation = annotation;
    }
    NSString * identifier = @"pin";
    
    // Get pin view
    MKAnnotationView * view = [self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (!view) {
        view = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
    }
    
    [view setImage:[UIImage imageNamed:@"basketMapLocation"]];
    
    return view;
}


@end
