//
//  PQHashTagRecord.m
//  Piqued
//
//  Created by Kenny M. Liou on 6/11/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQHashTagRecord.h"

@interface PQHashTagRecord()

@property (nonatomic, strong) NSString * hashTag;
@property (nonatomic) int usageCount;
@property (nonatomic) int viewedCount;
@property (nonatomic) NSTimeInterval lastUsedTime;
@property (nonatomic) NSTimeInterval lastViewedTime;


@end

@implementation PQHashTagRecord

- (instancetype) initWithHashTag:(NSString *) hashTag
{
    self = [super init];
    if (self) {
        
        self.hashTag = hashTag;
        self.usageCount = 0;
        self.viewedCount = 0;
    }
    
    return self;
}

- (void) viewed
{
    @synchronized (self) {
        self.viewedCount += 1;
        [self setLastViewedTime:[[NSDate date] timeIntervalSince1970]];
    }
}

- (void) used
{
    @synchronized (self) {
        self.usageCount += 1;
        [self setLastUsedTime:[[NSDate date] timeIntervalSince1970]];
    }
}

- (BOOL) isEqual:(id)object
{
    if ([object isKindOfClass:[PQHashTagRecord class]]) {
        PQHashTagRecord * record = object;
        return [self.hashTag isEqual:record.hashTag];
    }
    
    return false;
}

@end
