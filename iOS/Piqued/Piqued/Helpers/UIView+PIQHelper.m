//
//  UIView+PIQHelper.m
//  Piqued
//
//  Created by Kenny M. Liou on 10/26/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "UIView+PIQHelper.h"
#import <objc/runtime.h>

NSString * const piq_kGradientKey = @"kGradientKey";

@implementation UIView (PIQHelper)

- (void) piq_setBorderColor:(UIColor *)color
{
    self.layer.borderColor = [color CGColor];
    self.layer.borderWidth = 0.5f;
}

- (void) piq_setGradientBackgroundWithColorStart:(UIColor *) startColor end:(UIColor *) endColor
{
    NSArray * colors = [NSArray arrayWithObjects:(id)[startColor CGColor], (id)[endColor CGColor], nil];
    
    [self piq_setGradientBackgroundWithColors:colors start:CGPointMake(0.5, 0) end:CGPointMake(0.5, 1.0)];
}

- (void) piq_setGradientBackgroundWithColors:(NSArray *)colors start:(CGPoint) startPoint end:(CGPoint) endPoint
{
    if (![[self.layer sublayers][0] isKindOfClass:[CAGradientLayer class]]) {
        CAGradientLayer *gradient = [CAGradientLayer layer];
        
        [gradient setFrame:self.bounds];
        [gradient setColors:colors];
        [gradient setStartPoint:startPoint];
        [gradient setEndPoint:endPoint];
        
        [self.layer insertSublayer:gradient atIndex:0];
    }
}

@end
