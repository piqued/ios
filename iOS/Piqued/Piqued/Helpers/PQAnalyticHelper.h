//
//  PQAnalyticHelper.h
//  Piqued
//
//  Created by Kenny M. Liou on 1/8/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQAnalyticHelper : NSObject

+ (instancetype) sharedInstance;
// *** don't use these two functions in places other than appDelegate
- (void) initialize;
- (void) enterForeGround;
- (void) tearDown;
// ***

- (void) viewWillAppear:(NSString *) viewName;
- (void) viewWillDisappear:(NSString *) viewName;

@end
