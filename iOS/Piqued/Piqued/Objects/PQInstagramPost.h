//
//  PQInstagramPost.h
//  Piqued
//
//  Created by Matt Mo on 5/21/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "Post.h"
#import <InstagramKit/InstagramKit.h>

@interface PQInstagramPost : Post

- (instancetype)initWithMedia:(InstagramMedia *)media;

- (void) updateFsVenueIdWithCallback: (void(^)(Post * updatedPost))callback;

@end
