//
//  NotificationService.h
//  PiquedNotificationServiceExtension
//
//  Created by Matt Mo on 7/22/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
