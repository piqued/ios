//
//  PQEnvironment.h
//  Piqued
//
//  Created by Matt Mo on 12/10/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

enum {
    PQBuildConfigurationAppStore = 0,
    PQBuildConfigurationRelease,
    PQBuildConfigurationDebug,
    PQBuildConfigurationUnknown
};
typedef NSUInteger PQBuildConfiguration;

@interface PQEnvironment : NSObject

+ (NSString *)serverBaseURLString;
+ (NSURL *)serverBaseURL;
+ (PQBuildConfiguration)buildConfiguration;
+ (NSString *)stringForBuildConfiguration:(PQBuildConfiguration)buildConfiguration;
+ (BOOL)debugAnalytics;
+ (NSString *)appGroupName;

@end
