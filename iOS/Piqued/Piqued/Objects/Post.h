//
//  Post.h
//  Piqued
//
//  Created by Kenny M. Liou on 10/26/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "BaseModel.h"
@import MapKit;

NS_ASSUME_NONNULL_BEGIN

extern NSString * const PostDidGetBookmarkedNotification;

@interface Post : BaseModel

@property (strong, nonatomic, readonly) NSNumber *postId;
//TODO: TODO - Remove nullable when all models are converted over and imageUrl is deprecated.
@property (nullable, strong, nonatomic, readonly) NSArray *allImageUrls;
@property (nullable, strong, nonatomic, readonly) NSString *imageUrlLarge;
@property (nullable, strong, nonatomic, readonly) NSString *imageUrlSmall;
@property (strong, nonatomic, readonly) NSString *imageUrl;
@property (strong, nonatomic, readonly) NSString *title;
@property (strong, nonatomic, readonly) NSString *desc;
@property (nonatomic, readonly) double  userId;
@property (strong, nonatomic, readonly) NSString *username;
@property (strong, nonatomic, readonly) NSString *userimage;
@property (strong, nonatomic, readonly) NSString *address;
@property (strong, nonatomic, readonly) NSString *foursquareVenueID;
@property (nonatomic, readonly) NSUInteger createdAtTimeStamp;
@property (nonatomic, readonly) CLLocationDegrees lat;
@property (nonatomic, readonly) CLLocationDegrees lng;
@property (nonatomic, readonly) int likes;
@property (nonatomic) BOOL bookmarked;


// Helper properties
@property (strong, nonatomic, readonly) CLLocation *location;
@property (strong, nonatomic, readonly) NSDate *timeStamp;

// dynamic data
@property (strong, nonatomic) NSArray *tips;
@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *openHours;
@property (strong, nonatomic) NSNumber * myReaction;
@property (nonatomic) BOOL isOpenNow;
@property (nonatomic) int priceTier;

- (void) updateWithPost:(Post *)newPostInfo;

- (void) setNewTitle:(NSString *)title;
- (void) setNewLat:(double)lat andLng:(double)lng;

NS_ASSUME_NONNULL_END
@end
