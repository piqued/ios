//
//  PQKeyChain.h
//  Piqued
//
//  Created by Matt Mo on 1/6/18.
//  Copyright © 2018 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface PQKeyChain : NSObject

@property (nonatomic, readonly) BOOL isFirstInstall;
@property (nonatomic, readonly) BOOL hasDeviceChanged;

+ (instancetype) sharedKeyChain;
- (void)verifyDeviceId;

+ (void)setDeviceModel;
+ (NSString *)getDeviceModel;
+ (void)setDeviceId:(NSString *)deviceId;
+ (NSString *)getDeviceId;

NS_ASSUME_NONNULL_END
@end
