//
//  User.m
//  Piqued
//
//  Created by Kenny M. Liou on 10/18/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "User.h"
#import "Constants.h"

@interface User()

@property (nonatomic) double userId;
@property (nonatomic) int followerCount;
@property (nonatomic) int followingCount;
@property (nonatomic) int totalPostsCount;
@property (nonatomic) BOOL isFollowed;

@property (strong, nonatomic) NSString * userName;
@property (strong, nonatomic) NSString * email;
@property (strong, nonatomic) NSString * profileImage;
@property (strong, nonatomic) NSString * socialMediaId; // for now, array later?
@property (strong, nonatomic) NSString * secret;
@property (strong, nonatomic) NSString * token;
@property (strong, nonatomic) NSArray * posts;
@property (strong, nonatomic) NSArray * bookmarks;
@property (strong, nonatomic) NSArray * activities;
@property (strong, nonatomic) NSArray * followers;
@property (strong, nonatomic) NSArray * following;

@end

@implementation User

+ (User *) createUser:(NSString *)token withSecret:(NSString *)secret withId:(double)userId
{
    User * user = [[User alloc] init];
    
    [user setToken:token];
    [user setSecret:secret];
    [user setUserId:userId];
    
    return user;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self) {
        [self setToken:[coder decodeObjectForKey:kUserToken]];
        [self setSecret:[coder decodeObjectForKey:kUserSecret]];
        [self setUserId:((NSNumber *)[coder decodeObjectForKey:kUserId]).doubleValue];
        [self setUserName:[coder decodeObjectForKey:kUserName]];
        [self setEmail:[coder decodeObjectForKey:kUserEmail]];
        [self setProfileImage:[coder decodeObjectForKey:kProfileImage]];
        [self setFollowerCount:((NSNumber *)[coder decodeObjectForKey:kFollowerCount]).intValue];
        [self setFollowingCount:((NSNumber *)[coder decodeObjectForKey:kFollowingCount]).intValue];
        [self setTotalPostsCount:((NSNumber *)[coder decodeObjectForKey:kTotalPostsCount]).intValue];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.token forKey:kUserToken];
    [coder encodeObject:self.secret forKey:kUserSecret];
    [coder encodeObject:[NSNumber numberWithDouble:self.userId] forKey:kUserId];
    [coder encodeObject:self.userName forKey:kUserName];
    [coder encodeObject:self.email forKey:kUserEmail];
    [coder encodeObject:self.profileImage forKey:kProfileImage];
    [coder encodeObject:[NSNumber numberWithInt:self.followerCount] forKey:kFollowerCount];
    [coder encodeObject:[NSNumber numberWithInt:self.followingCount] forKey:kFollowingCount];
    [coder encodeObject:[NSNumber numberWithInt:self.totalPostsCount] forKey:kTotalPostsCount];
}

- (void) setUserName:(NSString *)userName
{
    _userName = userName;
}

- (void) setEmail:(NSString *)email
{
    _email = email;
}

- (void) setProfileImage:(NSString *)profileImage
{
    _profileImage = profileImage;
}

- (int) getFollowerCount
{
    return self.followerCount;
}

- (int) getFollowingCount
{
    return self.followingCount;
}

#pragma ###############################################
#pragma mark - Mantle
#pragma ###############################################

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"email"           : @"email",
             @"userName"        : @"name",
             @"userId"          : @"id",
             @"profileImage"    : @"profileImage",
             @"followerCount"   : @"followers",
             @"followingCount"  : @"following",
             @"totalPostsCount" : @"total_posts",
             @"isFollowed"      : @"followed",
#warning TODO - move these into keychain instead
             @"secret"          : @"secret",
             @"token"           : @"token_id",
             };
}

@end
