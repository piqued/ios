//
//  PQProfileViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 11/7/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQProfileViewController.h"
#import "PQEditProfileViewController.h"
#import "PQProfileHeaderCollectionViewCell.h"
#import "PQPostGridCollectionViewCell.h"
#import "PQAnalyticHelper.h"
#import "PQUserDefaults.h"
#import <InstagramKit/InstagramKit.h>
#import "PQInstagramButton.h"
#import "PQNetworking.h"
#import "PQPostManager.h"
#import "PQUserManager.h"
#import "NSArray+Unioning.h"
#import "NSArray+Diffing.h"
#import "InfiniteScrollActivityView.h"
#import "PQBasketViewController.h"
#import "PQUserListViewController.h"
#import "PQImageCacheDelegate.h"

typedef enum {
    ShortInfo,
    Following,
    SocialNetwork,
    MyAccount,
    NotifSetting,
    Security
} ProfileSections;

@interface PQProfileViewController() <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, PQProfileHeaderCollectionViewCellDelegate, PQPostGridCollectionViewCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UILabel *userPostEmptyLabel;
@property (weak, nonatomic) IBOutlet UILabel *otherUserPostEmptyLabel;

@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) InfiniteScrollActivityView *loadingMoreView;

@property (strong, nonatomic) User *user;
@property (strong, nonatomic) NSArray< Post*> *userPosts;

@property (nonatomic) BOOL isTabBarProfile;
@property (nonatomic) BOOL isUser;

// Pagination
@property (atomic) BOOL isRefreshing;
@property (atomic) int pageNumber;
@property (atomic) BOOL isMoreLoading;
@property (atomic) BOOL isEndOfResults;

// User Header
@property (atomic) BOOL isFetchingHeader;

// Basket
@property (weak, nonatomic) Post *openPost;

@end

@implementation PQProfileViewController

+ (NSString *)storyboardIdentifier
{
    return @"ProfileStoryboardIdentifier";
}

+ (NSString *)segueIdentifier
{
    return [NSString stringWithFormat:@"%@%@", NSStringFromClass([self class]), @"SegueIdentifier"];
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        _userId = -1;
    }
    return self;
}
- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self.collectionView registerNib:[UINib nibWithNibName:[PQPostGridCollectionViewCell nibName] bundle:nil] forCellWithReuseIdentifier:[PQPostGridCollectionViewCell reuseIdentifier]];
    [self.collectionView registerNib:[UINib nibWithNibName:[PQProfileHeaderCollectionViewCell nibName] bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[PQProfileHeaderCollectionViewCell reuseIdentifier]];
    
    // Add Refresh Control
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshControlAction:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
    
    // Add infinite scroll spinner
    CGRect frame = CGRectMake(0, self.collectionView.contentSize.height, self.collectionView.bounds.size.width, kInfiniteScrollActivityViewDefaultHeight);
    self.loadingMoreView = [[InfiniteScrollActivityView alloc] initWithFrame:frame];
    self.loadingMoreView.hidden = YES;
    [self.collectionView addSubview:self.loadingMoreView];
    
    UIEdgeInsets insets = self.collectionView.contentInset;
    insets.bottom += kInfiniteScrollActivityViewDefaultHeight;
    self.collectionView.contentInset = insets;
    
    self.flowLayout.headerReferenceSize = [PQProfileHeaderCollectionViewCell headerReferenceSize];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.isTabBarProfile) {
        [self.navigationController setNavigationBarHidden:YES animated:NO];
    } else {
        [self setupNavigationBar];
        [self.navigationController setNavigationBarHidden:NO animated:NO];
    }
    
    [self fetchHeader];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [[PQAnalyticHelper sharedInstance] viewWillDisappear:kProfile];
}

#pragma mark - PQTabBarControllerTabProtocol

- (void)tabBarControllerDidSwitchToViewController:(UITabBarController *)tabBarController
{
    [[PQAnalyticHelper sharedInstance] viewWillAppear:kProfile];
    [self setCurrentUser];
}

#pragma mark Navigation

- (void)setupNavigationBar
{
    UIBarButtonItem *dismissButton = [[UIBarButtonItem alloc] initWithTitle:@"back" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setLineBreakMode:NSLineBreakByTruncatingTail];
    [style setAlignment:NSTextAlignmentLeft];
    UIColor *color = [UIColor piq_colorFromHexString:kOrangeColor];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:17.0];
    
    NSDictionary *attributes = @{
                                 NSParagraphStyleAttributeName : style,
                                 NSForegroundColorAttributeName : color,
                                 NSFontAttributeName : font,
                                 };
    
    [dismissButton setTitleTextAttributes:attributes forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = dismissButton;
}

- (void)dismiss
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma ----------------------------------------------------------
#pragma mark - Collection View
#pragma ----------------------------------------------------------

- (void)refreshControlAction:(UIRefreshControl *)refreshControl {
    // If data is already loading, don't refresh
    if (self.isMoreLoading) {
        [self.refreshControl endRefreshing];
        return;
    }
    
    if (!self.isRefreshing) {
        self.isRefreshing = YES;
        [self fetchHeader];
        [self refreshPosts];
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *view = nil;
    
    view = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[PQProfileHeaderCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
    
    if ([view isKindOfClass:[PQProfileHeaderCollectionViewCell class]]) {
        PQProfileHeaderCollectionViewCell *header = (PQProfileHeaderCollectionViewCell*)view;
        header.delegate = self;
        
        [self updateHeaderView:header];
    }
    
    return view;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.userPosts count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PQPostGridCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[PQPostGridCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
    cell.post = self.userPosts[indexPath.row];
    cell.delegate = self;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    return [PQProfileHeaderCollectionViewCell headerReferenceSize];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat margin = 5;
    CGFloat numberOfColumns = 4;
    CGFloat interItemSpacing = [self interItemSpacing];
    CGFloat squareDimension = ([UIScreen mainScreen].bounds.size.width - margin * 2 - (numberOfColumns - 1)*interItemSpacing)/numberOfColumns;
    return CGSizeMake(squareDimension, squareDimension);
}

- (CGFloat)interItemSpacing
{
    return 3;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return [self interItemSpacing];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 3;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 5, 0, 5);
}

#pragma ----------------------------------------------------------
#pragma mark - UI Helper
#pragma ----------------------------------------------------------

- (void)updateHeaderView:(PQProfileHeaderCollectionViewCell *)headerView
{
    User *currentUser = [PQUserDefaults getUser];
    if (self.userId == currentUser.userId) {
        [headerView setUser:currentUser isAccountOwner:YES isTabBarProfile:self.isTabBarProfile];
    } else {
        [headerView setUser:self.user isAccountOwner:NO isTabBarProfile:NO];
    }
}

#pragma ----------------------------------------------------------
#pragma mark - PQProfileHeaderCollectionViewCellDelegate
#pragma ----------------------------------------------------------

- (void)startEditing
{
    PQEditProfileViewController *commentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PQEditProfileViewController"];
    
    if (self.parentViewController.parentViewController != NULL && [self.parentViewController.parentViewController isKindOfClass:[PQMainTabBarController class]]) {
        [self.parentViewController.parentViewController.navigationController pushViewController:commentViewController animated:YES];
    }
}

- (void)didPressFollowersView
{
    [self performSegueWithIdentifier:[PQUserListViewController segueIdentifier] sender:@(PQFollowTypeFollower)];
}

- (void)didPressFollowingView
{
    [self performSegueWithIdentifier:[PQUserListViewController segueIdentifier] sender:@(PQFollowTypeFollowing)];
}

- (void)didPressFollowButton:(UIButton *)sender
{
    [[PQNetworking sharedInstance] follow:sender.isSelected user:self.user];
}

#pragma ----------------------------------------------------------
#pragma mark - UI methods
#pragma ----------------------------------------------------------

- (void) checkEmptyState
{
    BOOL isEmpty = [self.userPosts count] < 1;
    if (isEmpty) {
        [self.collectionView setScrollEnabled:NO];
        [self.userPostEmptyLabel setHidden:!self.isUser];
        [self.otherUserPostEmptyLabel setHidden:self.isUser];
        
        [self.emptyView setHidden:NO];
    } else {
        [self.collectionView setScrollEnabled:YES];
        [self.emptyView setHidden:YES];
    }
}


#pragma ----------------------------------------------------------
#pragma mark - PQPostGridCollectionViewCell
#pragma ----------------------------------------------------------

- (void)openBasketViewWithID:(NSNumber *)postID
{
    Post *post = [[PQPostManager sharedInstance] getPost:postID];
    
    if (post != nil) {
        self.openPost = post;
        [self performSegueWithIdentifier:[PQBasketViewController segueIdentifier] sender:self];
    }
}

#pragma ----------------------------------------------------------
#pragma mark - Custom Setters
#pragma ----------------------------------------------------------

/*
 Should only be called from PQMainTabBarController
 */
- (void)setCurrentUser
{
    if (self.userId == -1) {
        self.isTabBarProfile = YES;
        User *currentUser = [PQUserDefaults getUser];
        self.userId = currentUser.userId;
        [self setIsUser:YES];
    }
}


- (void)setUserId:(double)userId
{
    _userId = userId;
    
    // No user set
    if (userId < 0) return;
    
    [self setIsUser:NO];
    
    [self resetProfileStatus];
    [self fetchHeader];
    [self loadMorePosts];
}

- (void)setUser:(User *)user
{
    _user = user;
}

- (void)fetchHeader
{
    if (!self.isFetchingHeader) {
        self.isFetchingHeader = YES;
        [PQAPIs fetchUserInfo:self.userId withCallback:^(User *user) {
            double currentUserId = [PQUserDefaults getUser].userId;
            if (self.userId == currentUserId) {
                [PQUserDefaults updateUser:user];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                self.isFetchingHeader = NO;
                self.user = user;
                NSIndexPath *indexPathZero = [NSIndexPath indexPathForItem:0 inSection:0];
                [self refreshHeader:indexPathZero];
            });
        }];
    }
}

- (void)refreshHeader:(NSIndexPath *)indexPath
{
    UICollectionReusableView *header = [self.collectionView supplementaryViewForElementKind:UICollectionElementKindSectionHeader atIndexPath:indexPath];
    if ([header isKindOfClass:PQProfileHeaderCollectionViewCell.class]) {
        PQProfileHeaderCollectionViewCell *headerView = (PQProfileHeaderCollectionViewCell *)header;
        [self updateHeaderView:headerView];
    }
}

#pragma ----------------------------------------------------------
#pragma mark - Scroll View Delegate
#pragma ----------------------------------------------------------

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // Handle loading more
    // Profile is not already loading
    
    // User is refreshing
    if (self.isRefreshing) return;
    
    if (!self.isMoreLoading) {
        // If all results are already loaded, do not check for more
        if (self.isEndOfResults) return;
        
        // Calculate the position of one screen length before the bottom of the results
        CGFloat scrollViewContentHeight = scrollView.contentSize.height;
        CGFloat scrollOffsetThreshold = scrollViewContentHeight - scrollView.bounds.size.height;
        
        // When the user has scrolled past the threshold, start requesting
        if(scrollView.contentOffset.y > scrollOffsetThreshold && scrollView.dragging) {
            
            // Update position of loadingMoreView, and start loading indicator
            CGRect frame = CGRectMake(0, scrollView.contentSize.height, scrollView.bounds.size.width, kInfiniteScrollActivityViewDefaultHeight);
            self.loadingMoreView.frame = frame;
            [self.loadingMoreView startAnimating];
            
            // Code to load more results
            [self loadMorePosts];
        }
    }
}

#pragma ----------------------------------------------------------
#pragma mark - Data
#pragma ----------------------------------------------------------

- (void)resetProfileStatus
{
    self.isEndOfResults = NO;
    self.isMoreLoading = NO;
    self.pageNumber = 1;
}

- (void)refreshPosts
{
    [PQAPIs fetchUserPostHistory:self.userId withPage:1 andSize:25 andCallback:^(BOOL result, long count, NSArray *posts) {
        // If there is a difference between the set of refresh results and
        // existing results, update the list
        
        // If user has added a new post, posts will be longer than self.userPosts
        // If user has deleted a post, posts will be shorter than self.userPosts
        // comparisonSize is to avoid out of bounds exceptions
        long comparisonSize = MIN([posts count], [self.userPosts count]);
        
        NSSet<Post *> *newPostsSet = [NSSet setWithArray:posts];
        NSArray<Post *> *firstNPosts = [self.userPosts subarrayWithRange:NSMakeRange(0, comparisonSize)];
        NSSet<Post *> *firstNPostsSet = [NSSet setWithArray:firstNPosts];
        
        BOOL isDifferent = ![newPostsSet isEqualToSet:firstNPostsSet];
        
        if (isDifferent) {
            [[PQPostManager sharedInstance] addPosts:posts];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isDifferent) {
                if (count < 25) {
                    self.isEndOfResults = YES;
                    self.pageNumber = 1;
                } else {
                    self.isEndOfResults = NO;
                    self.pageNumber = 2;
                }
                
                self.userPosts = posts;
                [self.collectionView reloadData];
            }
            
            [self.refreshControl endRefreshing];
            self.isRefreshing = NO;
        });
        
    }];
}

- (void)loadMorePosts
{
    // User has no more posts to load
    if (self.isEndOfResults) return;
    
    // Profile page is already loading posts
    if (self.isMoreLoading) return;
    
    self.isMoreLoading = YES;
    
    [PQAPIs fetchUserPostHistory:self.userId withPage:self.pageNumber andSize:25 andCallback:^(BOOL result, long count, NSArray *posts) {
        if ([posts count] == 0) {
            self.isEndOfResults = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self checkEmptyState];
                [self.loadingMoreView stopAnimating];
                self.isMoreLoading = NO;
            });
            return;
        }
        
        if ([posts count] < 25) {
            self.isEndOfResults = YES;
        }
        
        NSIndexSet *insertions, *deletions;
        NSArray *aggregatedPosts = [self.userPosts arrayByAddingObjectsFromArray:posts];
        [self.userPosts asdk_diffWithArray:aggregatedPosts insertions:&insertions deletions:&deletions];
        
        NSMutableArray *selectedItemsArray = [NSMutableArray array];
        [insertions enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            [selectedItemsArray addObject:[NSIndexPath indexPathForItem:idx inSection:0]];
        }];
        NSArray<NSIndexPath *> *insertionsArray = [selectedItemsArray copy];
        
        selectedItemsArray = [NSMutableArray array];
        [deletions enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
            [selectedItemsArray addObject:[NSIndexPath indexPathForItem:idx inSection:0]];
        }];
        NSArray<NSIndexPath *> *deletionsArray = [selectedItemsArray copy];
        
        self.userPosts = aggregatedPosts;
        [[PQPostManager sharedInstance] addPosts:posts];
        self.pageNumber += 1;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView performBatchUpdates:^{
                [self.collectionView insertItemsAtIndexPaths:insertionsArray];
                [self.collectionView deleteItemsAtIndexPaths:deletionsArray];
            } completion:nil];
            
            [self checkEmptyState];
            [self.loadingMoreView stopAnimating];
            self.isMoreLoading = NO;
        });
    }];
}

#pragma ----------------------------------------------------------
#pragma mark - Navigation
#pragma ----------------------------------------------------------

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UIViewController * destinationController = segue.destinationViewController;
    if ([destinationController isKindOfClass:[PQBasketViewController class]]) {
        PQBasketViewController * newPostView = (PQBasketViewController *) segue.destinationViewController;
        [newPostView setPostID:self.openPost.postId];
    } else if ([destinationController isKindOfClass:[PQUserListViewController class]]) {
        PQUserListViewController *userListVC = (PQUserListViewController *)segue.destinationViewController;
        if ([sender isKindOfClass:[NSNumber class]]) {
            userListVC.userListType = [(NSNumber *)sender unsignedIntegerValue];
            [PQUserManager getUserWithID:@(self.userId) withCallback:^(User *user) {
                userListVC.user = user;
            }];
        }
    }
}

#pragma ----------------------------------------------------------
#pragma mark - Lazy Initializers
#pragma ----------------------------------------------------------

- (NSArray<Post *> *)userPosts
{
    return !_userPosts ? _userPosts =
    ({
        [NSArray new];
    }) : _userPosts;
}

@end

