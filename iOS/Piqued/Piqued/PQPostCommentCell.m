//
//  PQPostCommentCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 6/12/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQPostCommentCell.h"
#import "Constants.h"
#import "PQUserManager.h"
#import "PQTimeHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PQPostCommentCell()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;

@property (weak, nonatomic) IBOutlet UIView *dividerPostOne;
@property (weak, nonatomic) IBOutlet UIView *dividerPostMany;
@property (weak, nonatomic) IBOutlet UIView *dividerComment;

@property (strong, nonatomic) UITapGestureRecognizer *userImageTapGesture;
@property (strong, nonatomic) UITapGestureRecognizer *userNameTapGesture;

@end

@implementation PQPostCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    if (!self.userImageTapGesture) {
        self.userImageTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        [self.userImage addGestureRecognizer:self.userImageTapGesture];
    }
    if (!self.userNameTapGesture) {
        self.userNameTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        [self.nameLabel addGestureRecognizer:self.userNameTapGesture];
    }
}

+ (int) estimatedHeight
{
    return 81;
}

- (void) layoutSubviews
{
    [self updateUIData];
}

- (void) reloadData
{
    [self updateUIData];
}

- (void) setNumberOfLines:(int) numberOfLines
{
    [self.commentLabel setNumberOfLines:numberOfLines];
}

- (void) updateUIData
{
    if (self.sourceType == SourceComment) {
        
        Comment * comment = [self.basketDelegate getComment:self.commentIndex];
        
        [self.commentLabel setText:comment.message];
        [self.timeLabel setText:[PQTimeHelper timeFromNow:comment.timeStamp]];
        [self updateDivider];
        [self.nameLabel setText:comment.userName];
        [self setImageWithURL:comment.userProfileImage];

    } else {
        Post * post = [self.basketDelegate getPost];
        
        [self.nameLabel setText:post.username];
        [self.commentLabel setText:post.desc];
        [self setImageWithURL:post.userimage];
        [self.timeLabel setText:[PQTimeHelper timeFromNow:post.timeStamp]];
        [self updateDivider];
    }
}

- (void) updateDivider
{
    [self.dividerPostOne setHidden:YES];
    [self.dividerPostMany setHidden:YES];
    [self.dividerComment setHidden:YES];
    
    switch (self.type) {
        case None:
            break;
        case PostSingle:
            [self.dividerPostOne setHidden:NO];
            break;
        case PostMany:
            [self.dividerPostMany setHidden:NO];
            break;
        case CommentCell:
            [self.dividerComment setHidden:NO];
            break;
    }
}

- (void) setImageWithURL:(NSString *) imageUrl
{
    if (imageUrl && imageUrl.length > 0) {
        [self.userImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:kBlankProfileImage]];
    } else {
        [self.userImage setImage:[UIImage imageNamed:kBlankProfileImage]];
    }
}

- (void)handleTapGesture:(UITapGestureRecognizer *)tapGesture
{
    Comment * comment = [self.basketDelegate getComment:self.commentIndex];
    if (comment == nil) {
        Post * post = [self.basketDelegate getPost];
        [self.basketDelegate openProfile:post.userId];
    } else {
        [self.basketDelegate openProfile:comment.commenterId];
    }
    
}

@end
