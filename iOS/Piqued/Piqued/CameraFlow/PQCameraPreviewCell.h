//
//  PQCameraPreviewCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 2/27/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQImageHandler.h"

@interface PQCameraPreviewCell : UICollectionViewCell

@property (weak, nonatomic) id<PQImageHandler> handler;

- (void) startCamera;
- (void) stopCamera;
- (void) takePhoto;
@end
