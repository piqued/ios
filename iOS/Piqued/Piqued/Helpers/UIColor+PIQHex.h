//
//  UIColor+PIQHex.h
//  Piqued
//
//  Created by Kenny M. Liou on 10/26/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PIQHex)

+ (UIColor *)piq_colorFromHexString:(NSString *) hex;

@end
