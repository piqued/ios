//
//  PQAPICore.m
//  Piqued
//
//  Created by Kenny M. Liou on 8/31/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQAPICore.h"
#import "PQEnvironment.h"
#import "PQThreadHelper.h"
#import "NSHTTPURLResponse+PIQHelper.h"
#import "LoginModel.h"
#import "PQUserDefaults.h"
#import "PQPostManager.h"

#define kProfileURL             @"/users/%.f.json"

@implementation PQAPICore

+ (instancetype) sharedInstance
{
    static PQAPICore * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (void) onNetworkStart
{
    // Override me if needed
}

- (void) onNetworkEnd
{
    // Override me if needed
}

/*
 * General base function to handle http request. You probably won't need to call this in most cases.
 * callback block may/may not called on main thread.
 */
- (void) performRequestWithMethod:(NSString *) methodName url:(NSURL *)url data:(NSDictionary *)data callback:(void (^)(NSError * error, NSDictionary * responseData))callback
{
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:methodName];
    
    [request setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    
    // this request contains data type json
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSError * error = NULL;
    
    NSData *jsonData = NULL;
    
    if (data) {
        jsonData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
    }
    
    if (!error) {
        
        NSURLSession * session = [NSURLSession sharedSession];
        
        [[session uploadTaskWithRequest:request fromData:jsonData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            // Handle Errors
            NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
            if (responseError) {
                callback(responseError, nil);
            } else {
                // we have data!!
                
                NSError * parseError = nil;
                NSDictionary * responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                
                if (parseError) {
                    callback(parseError, nil);
                } else {
                    callback(nil, responseData);
                }
            }
            
        }] resume];
        
    } else {
        callback(error, NULL);
    }
}
/*
 * There is no data for GET request
 */
- (void) performGet:(NSURL *)url callback:(void (^)(NSError * error, NSDictionary * responseData))callback
{
    [self performRequestWithMethod:@"GET" url:url data:NULL callback:callback];
}

- (void) performPost:(NSURL *)url data:(NSDictionary *)data callback:(void (^)(NSError * error, NSDictionary * responseData))callback
{
    [self performRequestWithMethod:@"POST" url:url data:data callback:callback];
}

- (void) performDelete:(NSURL *)url callback:(void (^)(NSError *error, NSDictionary * responseData))callback
{
    // TODO: this method should be shared with GET
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"DELETE"];
   
    NSError * error = NULL;
    
    if (!error) {
        
        NSURLSession * session = [NSURLSession sharedSession];
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            // Handle Errors
            NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
            if (responseError) {
                callback(responseError, NULL);
                
            } else {
                // we have data!!
                
                NSError * parseError = nil;
                NSDictionary * responseData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                
                if (parseError) {
                    callback(parseError, NULL);
                } else {
                    callback(NULL, responseData);
                }
            }
            
        }] resume];
    }
}

- (void) loginWithData:(NSDictionary *) loginInfo andUrl:(NSString *) url withCallback:(void (^)(User * user, NSError * error))callbackBlock
{
    [self performPost:[NSURL URLWithString:url] data:loginInfo callback:^(NSError *error, NSDictionary *responseData) {
        if (error) {
            PQLog(@"Error: Login %@", error);
            [PQThreadHelper performOnMainThread:^{
                callbackBlock(NULL, error);
            }];
        } else {
            NSString * passwordInvalid = [responseData valueForKey:@"password"];
            
            if (passwordInvalid) {
                PQLog(@"We got an error ");
                
                [PQThreadHelper performOnMainThread:^{
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:passwordInvalid forKey:NSLocalizedDescriptionKey];
                    
                    callbackBlock(NULL,
                                  [NSError errorWithDomain:@"error" code:401 userInfo:details]);
                }];
                
            } else {
                
                [self storeUserInfo:responseData withCallback:callbackBlock];
            }
        }
    }];
}

- (void) createAccount:(NSDictionary *) createAccountInfo andUrl:(NSString *) url withCallback:(void (^)(User * user, NSError * error))callback
{
    [[PQAPICore sharedInstance] performPost:[NSURL URLWithString:url] data:createAccountInfo callback:^(NSError *error, NSDictionary *responseData) {
        if (error) {
            PQLog(@"Error: failed to create account. %@", error);
            callback(nil, error);
        } else {
            [self storeUserInfo:responseData withCallback:callback];
        }
    }];
}

/*
 * This method is meant to unify storing user info after login and create account
 *
 */
- (void) storeUserInfo:(NSDictionary *)userData withCallback:(void (^)(User * user, NSError * error))callbackBlock
{
    LoginModel * user = [LoginModel modelWithDictionary:userData];
    
    [PQUserDefaults setUser:user];
    
    [[PQPostManager sharedInstance] reloadAllData:^(bool success) {
        if (success) {
            [self fetchCurrentUserWithCallback:^(User *user) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    callbackBlock(user, NULL);
                });
            }];
        }
    }];
}

- (void)fetchCurrentUserWithCallback:(void (^) (User * user)) callback
{
    User *user = [PQUserDefaults getUser];
    [PQAPIs fetchUserInfo:user.userId withCallback:^(User *updatedUser) {
        User *user = [PQUserDefaults updateUser:updatedUser];
        callback(user);
    }];
}

- (NSString *) createRequestURLWithUser:(NSString *) requestURL
{
    User * user = [PQUserDefaults getUser];
    
    NSString * creds = [NSString stringWithFormat:@"%@?creds[token]=%@&creds[secret]=%@", requestURL, user.token, user.secret];
    
    return creds;
}

- (void)fetchUserInfo:(double) userId withCallback:(void (^) (User * user)) callback
{
    NSString * host = [NSString stringWithFormat:[PQEnvironment.serverBaseURLString stringByAppendingString:kProfileURL], userId];
    
    NSString * url = [self createRequestURLWithUser:host];
    
    [self performGet:[NSURL URLWithString:url] callback:^(NSError *error, NSDictionary *responseData) {
        if (error) {
            PQLog(@"Error: Failed to fetch user info %@", error);
        } else {
            User * user = [User modelWithDictionary:responseData];
            callback(user);
        }
    }];
}

@end
