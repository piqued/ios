//
//  PQBasketLocationCellNew.m
//  Piqued
//
//  Created by Kenny M. Liou on 8/6/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQBasketLocationCellNew.h"
#import "PQMapAnnotation.h"
#import "UIColor+PIQHex.h"
#import "PQMapSelectionHelper.h"
#import <SDWebImage/SDWebImageManager.h>

#define pinRadius   (70)

@interface PQBasketLocationCellNew()

@property (weak, nonatomic) IBOutlet UIButton *pqAddressBtn;
@property (weak, nonatomic) IBOutlet UIButton *pqHourBtn;
@property (weak, nonatomic) IBOutlet UIButton *pqPhoneBtn;
@property (weak, nonatomic) IBOutlet UIView *priceWrap;




@property (nonatomic) float pinWidthPercentage;
@property (nonatomic) BOOL mapLoaded;

@end

@implementation PQBasketLocationCellNew

+ (int) estimatedHeight
{
    return 106;
}

- (void) reloadData
{
    [self setupUIData];
}


#pragma mark UI methods

- (IBAction)addressPressed:(id)sender {
    [self startNavigation];    
}

#pragma mark Helper Methods

- (void) startNavigation
{
    Post * post = [self.basketDelegate getPost];
    
    PQSupportedMaps defaultMap = [PQMapSelectionHelper getDefaultMap];
    
    if (defaultMap == pq_map_UNKNOWN) {
        
        NSArray<NSNumber *> * availableMapNames = [PQMapSelectionHelper getCurrentlyInstalledMaps];
        
        if ([availableMapNames count] == 1) {
            // well, how sad, you really should learn the other amazing app out there
            [self startNavigationWithURLScheme:[PQMapSelectionHelper getMapURLWithPost:post]];
        } else {
            
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Choose default"
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleActionSheet];
            UIAlertAction *cancelAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel Action")
                                           style:UIAlertActionStyleCancel
                                           handler:^(UIAlertAction *action)
                                           {
                                               [alertController dismissViewControllerAnimated:YES completion:Nil];
                                           }];
            
            [alertController addAction:cancelAction];
            
            for (int i = 0; i < [availableMapNames count]; i++) {
                NSNumber * mapType = availableMapNames[i];
                NSString * mapName = [PQMapSelectionHelper getMapName:mapType.intValue];
                
                UIAlertAction * action = [UIAlertAction
                                          actionWithTitle:mapName
                                          style:UIAlertActionStyleDefault
                                          handler:^(UIAlertAction * _Nonnull action) {
                                              
                                              [PQMapSelectionHelper setDefaultMap:(PQSupportedMaps)mapType.intValue];
                                              [self startNavigationWithURLScheme:[PQMapSelectionHelper getMapURLWithPost:post]];
                                          }];
                
                [alertController addAction:action];
            }
            
            [self.basketDelegate presentViewController:alertController];
        }
    } else {
    
        [self startNavigationWithURLScheme:[PQMapSelectionHelper getMapURLWithPost:post]];
    }
}

- (void) startNavigationWithURLScheme: (NSString *) urlScheme
{
    NSURL *myURL = [NSURL URLWithString: urlScheme];
    [[UIApplication sharedApplication] openURL:myURL];
}

- (float) pinWidthPercentage
{
    if (_pinWidthPercentage < 1) {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        // (105 - pinRadius) / 2
        int vertialPadding = (105 - pinRadius) / 2;
        // use vertical padding to define horizontal padding
        int pinWidth = pinRadius + 2 * vertialPadding;
        
        _pinWidthPercentage = pinWidth / iOSDeviceScreenSize.width;
    }
    
    return _pinWidthPercentage;
}

- (void) setPriceTier:(int) priceTier
{
    NSArray * priceTag = self.priceWrap.subviews;
    
    if (priceTier < 1) {
        
        for (int i = 0; i < priceTag.count; i++) {
            UIImageView * price = priceTag[i];
            
            if (i == 0) {
                [price setImage:[UIImage imageNamed:@"basketPriceOff"]];
            } else {
                [price setImage:[UIImage imageNamed:@"basketPriceGray"]];
            }
        }
    } else {
    
        for (int i = 0; i < priceTag.count; i++) {
            UIImageView * price = priceTag[i];
            
            if (i == 0) {
                [price setImage:[UIImage imageNamed:@"basketPrice"]];
            } else {
            
                if (i < priceTier + 1) { // the fist one is the actual sign
                    [price setImage:[UIImage imageNamed:@"basketPriceOrange"]];
                } else {
                    [price setImage:[UIImage imageNamed:@"basketPriceGray"]];
                }
            }
        }
    }
}

#pragma mark Setup Functions

- (void) setupUIData
{
    Post * post = [self.basketDelegate getPost];
    
    [self.pqAddressBtn setTitle:[[post.address componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "] forState:UIControlStateNormal];
    [self.pqAddressBtn.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
    [self.pqAddressBtn.titleLabel setNumberOfLines:0];
    
    if (post.foursquareVenueID != (id)[NSNull null] && post.foursquareVenueID.length > 0) {
        NSString * openHour = (post.openHours != NULL && ![post.openHours isEqualToString:@""]) ? post.openHours: @"N/A";
        NSString * phoneNumber = (post.phoneNumber != NULL && ![post.phoneNumber isEqualToString:@""]) ? post.phoneNumber: @"N/A";
        
        [self.pqHourBtn setTitle:openHour forState:UIControlStateNormal];
        [self.pqPhoneBtn setTitle:phoneNumber forState:UIControlStateNormal];
        [self setPriceTier:post.priceTier];
        
    }

    if (post.openHours != NULL && ![post.openHours isEqualToString:@""]) {
        [self.pqHourBtn setImage:[UIImage imageNamed:@"basketHours"] forState:UIControlStateNormal];
        
        [self.pqHourBtn setTitle:post.openHours forState:UIControlStateNormal];
        [self.pqHourBtn setHidden:NO];
        [self.pqHourBtn setTitleColor:[UIColor piq_colorFromHexString:kOrangeColor] forState:UIControlStateNormal];
    } else {
        [self.pqHourBtn setImage:[UIImage imageNamed:@"basketHoursOff"] forState:UIControlStateNormal];
        [self.pqHourBtn setTitleColor:[UIColor piq_colorFromHexString:kGrayColor] forState:UIControlStateNormal];
    }
    
    if (post.phoneNumber != NULL && ![post.phoneNumber isEqualToString:@""]) {
        [self.pqPhoneBtn setTitle:post.phoneNumber forState:UIControlStateNormal];
        [self.pqPhoneBtn setImage:[UIImage imageNamed:@"basketPhone"] forState:UIControlStateNormal];
        [self.pqPhoneBtn setTitleColor:[UIColor piq_colorFromHexString:kOrangeColor] forState:UIControlStateNormal];
    } else {
        [self.pqPhoneBtn setImage:[UIImage imageNamed:@"basketPhoneOff"] forState:UIControlStateNormal];
        [self.pqPhoneBtn setTitleColor:[UIColor piq_colorFromHexString:kGrayColor] forState:UIControlStateNormal];
    }
    
}

@end
