//
//  PQGlobalAlerts.m
//  Piqued
//
//  Created by Matt Mo on 4/13/18.
//  Copyright © 2018 First Brew. All rights reserved.
//

#import "PQGlobalAlerts.h"

@implementation PQGlobalAlerts

+ (void)presentAlertWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:title
                                                                        message:message
                                                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [controller addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIViewController *rootVC = UIApplication.sharedApplication.delegate.window.rootViewController;
        [rootVC presentViewController:controller animated:YES completion:nil];
    });
}
@end
