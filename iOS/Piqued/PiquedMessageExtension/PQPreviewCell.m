//
//  PQPreviewCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 8/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQPreviewCell.h"

#define kAnimationDuration (0.5)

@interface PQPreviewCell()

@property (weak, nonatomic) IBOutlet UIButton * directionButton;
@property (weak, nonatomic) IBOutlet UIButton * phoneButton;

@property (weak, nonatomic) IBOutlet UIView * overlayWrap;

@end

@implementation PQPreviewCell

- (void) awakeFromNib
{
    [super awakeFromNib];

}

#pragma mark UI methods

- (IBAction)onShareDirection:(id)sender {
    
    [self.delegate offerString:self.post.address];
    
    [self setSelected:NO];
}

- (IBAction)onSharePhone:(id)sender {
    [self.delegate offerString:self.post.phoneNumber];
    
    [self setSelected:NO];
}


#pragma mark Helper methods

- (void) setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    if (selected) {
        [self showOverlay];
    } else {
        [self hideOverlay];
    }
}

- (void) showOverlay
{
    [UIView animateWithDuration:kAnimationDuration animations:^{
        [self.overlayWrap setAlpha:1.0];
    } completion:^(BOOL finished) {
        if (finished) {
            [self.overlayWrap setUserInteractionEnabled:YES];
        }
    }];
}

- (void) hideOverlay
{
    [UIView animateWithDuration:kAnimationDuration animations:^{
        [self.overlayWrap setAlpha:0.0];
    } completion:^(BOOL finished) {
        if (finished) {
            [self.overlayWrap setUserInteractionEnabled:NO];
        }
    }];
}



@end
