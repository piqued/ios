//
//  PQLocationManager.m
//  Piqued
//
//  Created by Kenny M. Liou on 10/26/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQLocationManager.h"

@implementation PQLocationManager

+ (instancetype) sharedInstance {
    static PQLocationManager * sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.distanceFilter = 30;
    }
    return self;
}

#pragma mark - CLLocationManagerDelegate Protocol methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [self.delegate locationManagerDidUpdateLocation:locations];
    [self setLocation:[locations lastObject]];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    [self.delegate locationManagerDidChangeAuthorizationStatus:status];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    switch ([error code])
    {
        case kCLErrorNetwork: // General, network-related error
        {
            [[PQLocationManager sharedInstance].locationManager stopUpdatingLocation];
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please check your network connection or that you are not in airplane mode" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            [alert addAction:defaultAction];
            [self.delegate presentViewController:alert animated:YES completion:nil];
        }
        case kCLErrorDenied: // Location permission has been denied
        {
            [[PQLocationManager sharedInstance].locationManager stopUpdatingLocation];
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please enable location permissions.\nSettings->Privacy->LocationServices" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [alert dismissViewControllerAnimated:YES completion:nil];
            }];
            [alert addAction:defaultAction];
            [self.delegate presentViewController:alert animated:YES completion:nil];
        }
    }
    
    // Take additional action?
    if ([self.delegate respondsToSelector:@selector(locationManagerDidFailWithError:)]) {
        [self.delegate locationManagerDidFailWithError:error];
    }
}

@end
