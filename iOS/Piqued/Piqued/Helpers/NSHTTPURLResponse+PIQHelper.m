//
//  NSHTTPURLResponse+PIQHelper.m
//  Piqued
//
//  Created by Matt Mo on 12/7/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "NSHTTPURLResponse+PIQHelper.h"

@implementation NSHTTPURLResponse (PIQHelper)

+ (NSError *) piq_isValidResponse:(NSHTTPURLResponse *)response {
    if (response.statusCode >= 200 && response.statusCode < 300) {
        return nil;
    }
    
    NSString * errorDetailString = [NSString stringWithFormat:@"%ld: %@", (long)response.statusCode, [NSHTTPURLResponse localizedStringForStatusCode:response.statusCode]];
    NSDictionary * errorDetail = [NSDictionary dictionaryWithObject:errorDetailString forKey:NSLocalizedDescriptionKey];
    NSError * errorHTTPStatus = [NSError errorWithDomain:@"HTTPResponseError" code:response.statusCode userInfo:errorDetail];
    
    return errorHTTPStatus;
}

@end
