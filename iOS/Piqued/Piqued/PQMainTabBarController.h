//
//  PQMainTabBarController.h
//  Piqued
//
//  Created by Kenny M. Liou on 11/28/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    PQMainTabBarControllerTapTypeMain = 0,
    PQMainTabBarControllerTapTypeBookmarks,
    PQMainTabBarControllerTapTypeActivities,
    PQMainTabBarControllerTapTypeProfile
} PQMainTabBarControllerTabType;

/**
 PQTabBarControllerTabProtocol
 
 This protocol is for controllers that are part of the UITabBarControllers viewControllers. These are methods that should exist, but do not.
 */
@protocol PQTabBarControllerTabProtocol <NSObject>

@required
- (void)tabBarControllerDidSwitchToViewController:(UITabBarController *)tabBarController;

@end

@interface PQMainTabBarControllerDelegate : NSObject<UITabBarControllerDelegate>

@end

@interface PQMainTabBarController : UITabBarController

+ (void)showBasketView:(Post *) post;
+ (void)showTab:(PQMainTabBarControllerTabType)type;
+ (void)setBadgeValue:(nullable NSString *)badgeValue forTab:(PQMainTabBarControllerTabType)type;
+ (void)setTabBarHidden:(BOOL) hidden;
+ (void)setTabBarDelegate:(PQMainTabBarControllerDelegate *) delegate;

@end
