//
//  PQMath.m
//  Piqued
//
//  Created by Kenny M. Liou on 1/23/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQMath.h"
#import "PQLocationManager.h"

@implementation PQMath

+ (double) getMiles:(Post *)post
{
    CLLocation * currentLocation = [PQLocationManager sharedInstance].location;
    CLLocation * postLocation = [[CLLocation alloc] initWithLatitude:post.lat longitude:post.lng];
    
    return [currentLocation distanceFromLocation:postLocation] * 0.000621371192;
}

+ (NSString *) getMilesImage:(double) miles
{
    if (miles < 1) {
        return @"location1";
    } else if (miles < 5) {
        return @"location2";
    } else if (miles < 100) {
        return @"location3";
    } else {
        return @"location4";
    }
}

@end
