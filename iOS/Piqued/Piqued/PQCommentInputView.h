//
//  PQCommentInputView.h
//  Piqued
//
//  Created by Kenny M. Liou on 7/8/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PQCommentInputDelegate <NSObject>

@optional

- (void) onOverlayTapped;
- (void) addComment:(NSString *) comment;
- (void) resetView;

@end

@interface PQCommentInputView : UIView

@property (nonatomic) CGFloat currentKeyboardHeight;
@property (weak, nonatomic) id<PQCommentInputDelegate> delegate;

- (void) viewIsVisiable:(BOOL) visiable;
- (NSString *) getComment;
- (void) resetView;

@end
