//
//  PQPostTipCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 4/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQPostTipCell.h"
#import "Tip.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface PQPostTipCell()

@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userCommentLabel;

@end

@implementation PQPostTipCell

+ (int) estimatedHeight
{
    return 82;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) setCellIndex:(int) index
{
    Post * post = [self.basketDelegate getPost];
    Tip * tip = [post tips][index];
    
    [self.userNameLabel setText:[self nameFormatFirstName:tip.firstName lastName:tip.lastName]];
    [self.userCommentLabel setText:tip.tipText];
    [self.userImage sd_setImageWithURL:[NSURL URLWithString:[tip getImageUrl]]];
    
    CALayer * userImageLayer = [self.userImage layer];
    [userImageLayer setCornerRadius:self.userImage.frame.size.width/2];
}

- (NSString *)nameFormatFirstName:(NSString *)firstName lastName:(NSString *)lastName
{
    if (firstName.length == 0) {
        return lastName;
    }
    
    if (lastName.length == 0) {
        return firstName;
    }
    
    return [NSString stringWithFormat:@"%@ %@", firstName, lastName];
}

@end
