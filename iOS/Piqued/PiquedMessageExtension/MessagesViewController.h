//
//  MessagesViewController.h
//  PiquedExtension
//
//  Created by Kenny M. Liou on 7/16/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Messages/Messages.h>


@interface MessagesViewController : MSMessagesAppViewController

@end
