//
//  PQUserCollectionViewCell.h
//  Piqued
//
//  Created by Matt Mo on 9/11/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@protocol PQUserCollectionViewCellDelegate <NSObject>

- (void)didPressFollowButton:(UIButton *)sender;
- (void)openProfile:(double)userId;

@end

@interface PQUserCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) id<PQUserCollectionViewCellDelegate> delegate;
@property (weak, nonatomic) User* user;

+ (UINib *)nib;
+ (NSString *)nibName;
+ (NSString *)reuseIdentifier;

@end
