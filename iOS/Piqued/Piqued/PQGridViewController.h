//
//  PQGridViewController.h
//  Piqued
//
//  Created by Matt Mo on 10/25/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQLocationManager.h"

@protocol PQGridViewControllerDelegate <NSObject>

- (void)gridViewDidScrolled:(UIScrollView *)scrollView;

@end

@interface PQGridViewController: UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, PQLocationManagerDelegate>

@property (weak, nonatomic) id<PQGridViewControllerDelegate> delegate;

+ (NSString *)segueIdentifier;

+ (void) showBasketView:(Post *)post;

- (void) viewSwitched;
- (void) scrollToTop;

@end
