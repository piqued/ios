//
//  PQProfileHeaderCollectionViewCell.m
//  Piqued
//
//  Created by Matt Mo on 9/2/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQProfileHeaderCollectionViewCell.h"
#import "PQPostManager.h"
#import "Constants.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PQProfileHeaderCollectionViewCell()

@property (weak, nonatomic) IBOutlet UILabel *shareCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *followersCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *followingCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UIView *editContainer;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIView *followersContainerView;
@property (weak, nonatomic) IBOutlet UIView *followingContainerView;

@property (strong, nonatomic) UITapGestureRecognizer *followersTapGestureRecognizer;
@property (strong, nonatomic) UITapGestureRecognizer *followingTapGestureRecognizer;

@end

@implementation PQProfileHeaderCollectionViewCell

+ (NSString *)nibName
{
    return NSStringFromClass([self class]);
}

+ (UINib *)nib
{
    return [UINib nibWithNibName:[self nibName] bundle:nil];
}

+ (NSString *)reuseIdentifier
{
    return [NSString stringWithFormat:@"%@%@", [self nibName], @"ReuseIdentifier"];
}

+ (CGSize)headerReferenceSize
{
    return CGSizeMake([[UIScreen mainScreen] bounds].size.width, 133);
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self.followersTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(followersViewPressed:)];
        self.followingTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(followingViewPressed:)];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    // flip edit button layout
    self.editButton.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.editButton.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.editButton.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.editButton.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    
    [self.followersContainerView addGestureRecognizer:self.followersTapGestureRecognizer];
    [self.followingContainerView addGestureRecognizer:self.followingTapGestureRecognizer];
}

- (void) setUser:(User *) user isAccountOwner:(BOOL)isAccountOwner isTabBarProfile:(BOOL)isTabBarProfile
{
    [self.followButton setSelected:user.isFollowed];
    self.followButton.hidden = isAccountOwner;
    self.editButton.hidden = !isTabBarProfile;
    
    [self.userNameLabel setText:user.userName];
    
    [self.shareCountLabel setText:[NSString stringWithFormat:@"%d", user.totalPostsCount]];
    
    [self.followersCountLabel setText:[NSString stringWithFormat:@"%d", user.followerCount]];
    self.followersContainerView.userInteractionEnabled = user.followerCount > 0;
    [self.followingCountLabel setText:[NSString stringWithFormat:@"%d", user.followingCount]];
    self.followingContainerView.userInteractionEnabled = user.followingCount > 0;
    
    //TODO need to get location info somehow
    [self.addressLabel setText:@""];
    //TODO need to get status somehow
    [self.statusLabel setText:@"N/A"];
    
    NSString * imageUrl = user.profileImage;
    
    if (imageUrl && imageUrl.length > 0) {
        [self.userImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:kBlankProfileImage]];
    } else {
        [self.userImage setImage:[UIImage imageNamed:kBlankProfileImage]];
    }
}

- (IBAction)editButtonPressed:(id)sender
{
    [self.delegate startEditing];
}

- (IBAction)followButtonPressed:(UIButton *)sender
{
    sender.selected = !sender.selected;
    [self.delegate didPressFollowButton:sender];
}

- (void)followersViewPressed:(UITapGestureRecognizer *)recognizer
{
    [self.delegate didPressFollowersView];
}

- (void)followingViewPressed:(UITapGestureRecognizer *)recognizer
{
    PQLog(@"following tapped");
    [self.delegate didPressFollowingView];
}

@end
