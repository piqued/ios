//
//  PQBasketRelatedCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQBasketBaseCell.h"

@interface PQBasketRelatedCell : PQBasketBaseCell

+ (int) estimatedHeight;
@end
