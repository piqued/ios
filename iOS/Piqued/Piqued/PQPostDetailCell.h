//
//  PQPostDetailCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 12/5/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQBasketBaseCell.h"

@interface PQPostDetailCell : PQBasketBaseCell

+ (CGFloat) estimatedHeight;
@end
