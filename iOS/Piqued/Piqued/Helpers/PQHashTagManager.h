//
//  PQHashTagManager.h
//  Piqued
//
//  Created by Kenny M. Liou on 6/11/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PQHashTagRecord.h"

@interface PQHashTagManager : NSObject

+ (instancetype) sharedInstance;

- (void) tagsViewed:(NSArray *) tags;
- (void) tagsUsed:(NSArray *) tags;

- (NSArray *) getHashTag:(NSString *) keyword;

@end
