//
//  NSString+PIQHelper.h
//  Piqued
//
//  Created by Matt Mo on 4/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (PIQHelper)

+ (NSString *) piq_parseHashTags:(NSString *) string;

@end
