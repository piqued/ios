//
//  PQPreviewCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 8/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"

@protocol PQPreviewCellDelegate <NSObject>

@required
- (void) offerString:(NSString *) string;

@end

@interface PQPreviewCell : UICollectionViewCell

@property (weak, nonatomic) id<PQPreviewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView * postImage;

@property (strong, nonatomic) Post * post;

@end
