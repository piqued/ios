//
//  PQYourActivitiesCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 2/6/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQYourActivitiesCell.h"
#import "PQActivityPostCell.h"
#import "PQPostManager.h"
#import "PQNetworking.h"
#import "EventModel.h"
#import "PQNotifications.h"

static NSString * const kPostCell = @"PQActivityPostCell";
static NSString * const kEmptyCell = @"PQYourActivitiesEmptyCell";

@interface PQYourActivitiesCell() <UICollectionViewDelegate, UICollectionViewDataSource, PQActivityPostCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;

@property (strong, nonatomic) NSArray *activityIds;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (weak, nonatomic) PQPostManager *manager;

@property (strong, nonatomic) NSArray *activityNotifications;

@property (nonatomic) BOOL shouldTryToPull; // TODO: this is a HACK!! FIX ME when there's timestamp

@end

@implementation PQYourActivitiesCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setRefreshControl:[[UIRefreshControl alloc] init]];
    [self.refreshControl addTarget:self action:@selector(pullToRefresh) forControlEvents:UIControlEventValueChanged];
    
    [self.collectionView addSubview:self.refreshControl];
    [self.collectionView registerNib:[UINib nibWithNibName:kPostCell bundle:Nil] forCellWithReuseIdentifier:kPostCell];
    [self.collectionView registerNib:[UINib nibWithNibName:kEmptyCell bundle:Nil] forCellWithReuseIdentifier:kEmptyCell];
    
    
    [self setManager:[PQPostManager sharedInstance]];
    
    [self setShouldTryToPull:YES];

    [self refetchActivities];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell * resultView = NULL;
    
    if ([self isListEmpty]) {
        
        resultView = [collectionView dequeueReusableCellWithReuseIdentifier:kEmptyCell forIndexPath:indexPath];
        
    } else {
        PQActivityPostCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPostCell forIndexPath:indexPath];
        
        EventModel *event = self.activityNotifications[indexPath.row];
        cell.event = event;
        cell.delegate = self;
        
        resultView = cell;
    }
    
    return resultView;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self getPostCount];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize cellSize = collectionView.frame.size;
    
    if (![self isListEmpty]) {
        cellSize.height = 95; // TODO fix me so I'm not a constant
    }
    
    return cellSize;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

#pragma mark - PQActivityPostCellDelegate
- (void)didTapCell:(PQActivityPostCell *)cell
{
    if (!self.delegate || !cell) {
        return;
    }
    
    EventModel *event = [cell event];
    
    switch (event.eventType) {
        case PQEventTypeComment: {
            [self.delegate openBasketViewWithID:event.post.postId withComment:YES];
            break;
        }
        case PQEventTypeFollow: {
            [self.delegate openProfile:event.actor.userId];
            break;
        }
        default: {
            [self.delegate openBasketViewWithID:event.post.postId withComment:NO];
            break;
        }
    }
}

- (void)didTapUserArea:(PQActivityPostCell *)cell
{
    if (!self.delegate || !cell) {
        return;
    }
    
    EventModel *event = [cell event];
    User *user = [event actor];
    [self.delegate openProfile:user.userId];
}

#pragma mark - Data
- (void)pullToRefresh
{
    [[[PQNetworking sharedInstance] fetchEvents] continueWithBlock:^id _Nullable(BFTask * _Nonnull task) {
        
        if (task.error) {
            
        } else {
            BOOL check = [task.result isKindOfClass:[NSArray class]] && [[task.result firstObject] isKindOfClass:[EventModel class]];
            self.activityNotifications = task.result;
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.refreshControl endRefreshing];
            [self refetchActivities];
            
            [self.collectionView reloadData];
            [PQNotifications markAllNotificationsRead];
        });
        return nil;
    }];
}

- (void)refetchActivities
{
    if ([self.activityIds count] < 1 && self.shouldTryToPull) {
        [self setShouldTryToPull:NO];
        [self.refreshControl beginRefreshing];
        [self pullToRefresh];
    }
}

- (BOOL)isListEmpty
{
    return !self.activityNotifications || [self.activityNotifications count] < 1;
}

- (int)getPostCount
{
    if ([self isListEmpty]) {
        return 1;
    } else {
        return (int) [self.activityNotifications count];
    }
}

#pragma - PQActivityBaseCellProtocol

- (void)refreshData
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self pullToRefresh];
    });
}

@end
