//
//  PQEditPasswordViewController.h
//  Piqued
//
//  Created by Kenny M. Liou on 8/20/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PQEditPasswordViewControllerDelegate <NSObject>

@required
- (void) showDoneButton;

@end

@interface PQEditPasswordViewController : UIViewController

@property (weak, nonatomic) id<PQEditPasswordViewControllerDelegate> delegate;


@end
