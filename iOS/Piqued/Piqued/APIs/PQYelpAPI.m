//
//  PQYelpAPI.m
//  Piqued
//
//  Created by Kenny M. Liou on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQYelpAPI.h"
#import "YelpAPI.h"

#define YelpConsumerKey     @"Xa8MWvc7usPDkC8p4BdEFA"
#define YelpConsumerSecret  @"_myNdcVRclOv1DzBMv-w26sd6sk"
#define YelpToken           @"mIr4GFQ-DgWcf-vAq5zKvNF6Dh1674NU"
#define YelpTokenSecret     @"cHcMbdxtQ3Qg4WsvjJ3hOtmoCzA"

@interface PQYelpAPI()

@property (strong, nonatomic) YLPClient * client;

@end

@implementation PQYelpAPI

+ (instancetype) sharedInstance
{
    static PQYelpAPI * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

+ (void) getYelpURL:(Post * _Nonnull )post withLocationHint:(NSString * _Nonnull)hint withCallback:(void( ^ _Nonnull )(NSURL * _Nullable yelpLink)) callback
{
    [PQYelpAPI getYelpURL:post.title location:post.location withLocationHint:hint withCallback:callback];
}

+ (void) getYelpURL:(NSString *)postTitle location:(CLLocation *)location withLocationHint:(NSString * _Nonnull)hint withCallback:(void( ^ _Nonnull )(NSURL * _Nullable yelpLink)) callback
{
    YLPCoordinate * coord = [[YLPCoordinate alloc] initWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
    
    NSData *temp = [postTitle dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *cleanupTitleName = [[[NSString alloc] initWithData:temp encoding:NSASCIIStringEncoding] lowercaseString];
    
    [[[PQYelpAPI sharedInstance] client] searchWithLocation:hint currentLatLong:coord term:cleanupTitleName limit:0 offset:0 sort:YLPSortTypeBestMatched completionHandler:^(YLPSearch * _Nullable search, NSError * _Nullable error) {
        
        if (error) {
            PQLog(@"Cannot find location info from Yelp");
        } else {
            
            NSArray *titleArray = [cleanupTitleName componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            int minCount = (int) titleArray.count / 2 + 1;
            
            for (YLPBusiness * business in search.businesses) {
                NSString * cleanUpBusinessName = [[[business.name componentsSeparatedByCharactersInSet:[NSCharacterSet symbolCharacterSet]] componentsJoinedByString:@""] lowercaseString];
                
                int hitCount = 0;
                for (NSString * subString in titleArray) {
                    if ([cleanUpBusinessName containsString:subString]) {
                        hitCount ++;
                    }
                }
                
                if (hitCount >= minCount) {
                    // Found it!
                    dispatch_async(dispatch_get_main_queue(), ^{
                        callback(business.mobileURL);
                    });
                    return;
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(NULL);
        });
    }];
}

- (YLPClient *) client
{
    if (_client == NULL) {
        _client = [[YLPClient alloc] initWithConsumerKey:YelpConsumerKey
                                          consumerSecret:YelpConsumerSecret
                                                   token:YelpToken
                                             tokenSecret:YelpTokenSecret];
    }
    
    return _client;
}
@end
