//
//  PQLogoutTableViewCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 11/7/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PQLogoutTableViewCell : UITableViewCell

+ (CGFloat)estimatedHeight;
+ (NSString *)reuseIdentifier;

@end
