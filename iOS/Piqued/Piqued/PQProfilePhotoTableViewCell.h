//
//  PQProfilePhotoTableViewCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 8/20/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQEditProfileHeader.h"

@interface PQProfilePhotoTableViewCell : UITableViewCell

@property (weak, nonatomic) id<PQEditProfileHeaderDelegate> delegate;

+ (int) estimatedHeight;



@end
