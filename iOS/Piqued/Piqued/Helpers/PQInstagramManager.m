//
//  PQInstagramManager.m
//  Piqued
//
//  Created by Matt Mo on 5/14/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQInstagramManager.h"
#import "PQUtilities.h"
@import SafariServices;
#import <InstagramKit/InstagramKit.h>

#pragma mark - temporary facebook
@class FBSDKContainerViewController;

@protocol FBSDKContainerViewControllerDelegate <NSObject>

- (void)viewControllerDidDisappear:(FBSDKContainerViewController *)viewController animated:(BOOL)animated;

@end

@interface FBSDKContainerViewController : UIViewController

@property (nonatomic, weak) id<FBSDKContainerViewControllerDelegate> delegate;

- (void)displayChildController:(UIViewController *)childController;

@end

#pragma mark - PQInstagramManager

@interface PQInstagramManager() <FBSDKContainerViewControllerDelegate>

@property (nonatomic, copy) void (^handler)(BOOL);
@property (strong, nonatomic) SFSafariViewController * safariViewController;

@end

@implementation PQInstagramManager

+ (instancetype)sharedInstance
{
    static PQInstagramManager *_sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    return _sharedInstance;
}

- (void)logInFromViewController:(UIViewController *)fromViewController
                         handler:(void(^)(BOOL))handler
{
    InstagramEngine *engine = [InstagramEngine sharedEngine];
    NSURL *oAuthURL = [engine authorizationURL];
    
    [self openURLWithSafariViewController:oAuthURL sender:nil fromViewController:fromViewController handler:handler];
}

- (void)openURLWithSafariViewController:(NSURL *)url
                                 sender:(id)sender
                     fromViewController:(UIViewController *)fromViewController
                                handler:(void(^)(BOOL))handler
{
//    _expectingBackground = NO;
//    _pendingURLOpen = sender;
    
    UIViewController *parent = fromViewController ?: [PQUtilities topMostViewController];
    NSURLComponents *components = [NSURLComponents componentsWithURL:url resolvingAgainstBaseURL:NO];
    NSURLQueryItem *sfvcQueryItem = [[NSURLQueryItem alloc] initWithName:@"sfvc" value:@"1"];
    [components setQueryItems:[components.queryItems arrayByAddingObject:sfvcQueryItem]];
    url = components.URL;
    FBSDKContainerViewController *container = [[FBSDKContainerViewController alloc] init];
    container.delegate = self;
    
//    if (parent.transitionCoordinator != nil) {
//        // Wait until the transition is finished before presenting SafariVC to avoid a blank screen.
//        [parent.transitionCoordinator animateAlongsideTransition:NULL completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
//            // Note SFVC init must occur inside block to avoid blank screen.
//            _safariViewController = [[SFSafariViewControllerClass alloc] initWithURL:url];
//            [_safariViewController performSelector:@selector(setDelegate:) withObject:self];
//            [container displayChildController:_safariViewController];
//            [parent presentViewController:container animated:YES completion:nil];
//        }];
//    } else {
//        _safariViewController = [[SFSafariViewControllerClass alloc] initWithURL:url];
//        [_safariViewController performSelector:@selector(setDelegate:) withObject:self];
//        [container displayChildController:_safariViewController];
//        [parent presentViewController:container animated:YES completion:nil];
//    }
    
    self.safariViewController = [[SFSafariViewController alloc] initWithURL:url];
    [self.safariViewController performSelector:@selector(setDelegate:) withObject:self];
    [container displayChildController:self.safariViewController];
    [parent presentViewController:container animated:YES completion:nil];
    
    self.handler = handler;
    // Assuming Safari View Controller always opens
//    if (handler) {
//        handler(YES);
//    }
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    if (sourceApplication != nil && ![sourceApplication isKindOfClass:[NSString class]]) {
        @throw [NSException exceptionWithName:NSInvalidArgumentException
                                       reason:@"Expected 'sourceApplication' to be NSString. Please verify you are passing in 'sourceApplication' from your app delegate (not the UIApplication* parameter). If your app delegate implements iOS 9's application:openURL:options:, you should pass in options[UIApplicationOpenURLOptionsSourceApplicationKey]. "
                                     userInfo:nil];
    }
    NSError* error = NULL;
    BOOL isValid = [[InstagramEngine sharedEngine] receivedValidAccessTokenFromURL:url error:&error];
    
    if (isValid) {
        // if they completed a SFVC flow, dismiss it.
        [self.safariViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{
            if (self.handler) {
                self.handler(YES);
            }
        }];
        self.safariViewController = nil;
    }
    
    return isValid;
}

#pragma mark - Delegate methods

- (void)viewControllerDidDisappear:(FBSDKContainerViewController *)viewController animated:(BOOL)animated
{
    if (self.safariViewController) {
//        [self safariViewControllerDidFinish:_safariViewController];
    }
}

#pragma mark - Utilities


@end
