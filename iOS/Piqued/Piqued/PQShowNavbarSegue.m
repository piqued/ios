//
//  PQShowNavbarSegue.m
//  Piqued
//
//  Created by Kenny M. Liou on 11/28/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQShowNavbarSegue.h"

@implementation PQShowNavbarSegue

- (void) perform
{
    [super perform];
    
    UINavigationController * navController = self.sourceViewController.navigationController;
    
    [navController.navigationBar setTranslucent:NO];
    [navController setNavigationBarHidden:NO animated:NO];
}

@end
