//
//  AnimationHelper.m
//  Piqued
//
//  Created by Kenny M. Liou on 10/25/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "AnimationHelper.h"

typedef enum {
    FadeOutAndHide,
} AnimationType;

@implementation AnimationHelper

+ (void) fadeOutAndHide:(UIView *) view duration:(float)sec
{
    view.alpha = 1.0;
    
    [UIView transitionWithView:view
                      duration:sec
                       options:UIViewAnimationOptionCurveEaseIn
                    animations:^{
                        view.alpha = 0.0;
                    } completion:^(BOOL finished) {
                        if (finished) {
                            [view setHidden:YES];
                        }
                    }];

}

+ (void) fadeInWithShift:(UIView *) view
                  startX:(float)start
                    endX:(float)end
                duration:(float)sec
{
    view.alpha = 0.0;
    [view setHidden:NO];
    
    CGRect frame = view.frame;
    frame.origin.x = start;
    [view setFrame:frame];
    frame.origin.x = end;
    [UIView transitionWithView:view
                      duration:sec
                       options:UIViewAnimationOptionCurveEaseIn
                    animations:^{
                        [view setFrame:frame];
                        view.alpha = 1.0;
                    } completion:NULL];
}

+ (void) moveUp:(UIView *) view endY:(float)end duration:(float)sec
{
    [UIView transitionWithView:view
                      duration:sec
                       options:UIViewAnimationOptionCurveEaseIn
                    animations:^{
                        CGRect frame = view.frame;
                        frame.origin.y = end;
                        [view setFrame:frame];
                    } completion:NULL];

}

+ (void) constrainChange:(NSLayoutConstraint* )constrain withValue:(float)value withAnimationDuration:(float)sec withView:(UIView *)parentView
{
    constrain.constant = value;
    [UIView animateWithDuration:sec animations:^{
        [parentView layoutIfNeeded];
    }];
}

@end
