//
//  PQYourActivitiesCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 2/6/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQActivityBaseCell.h"

@interface PQYourActivitiesCell<PQActivityBaseCellProtocol> : PQActivityBaseCell

- (void)refreshData;

@end
