//
//  PQPostManager.h
//  Piqued
//
//  Created by Matt Mo on 7/15/15.
//  Copyright (c) 2015 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post.h"

@protocol PQPostManagerDelegate <NSObject>

@optional
- (void) bookmarkUpdated;

@end

@interface PQPostManager : NSObject

+ (instancetype) sharedInstance;
/**
 * Reload function, the assumption is there is a new user login
 *
 **/

- (void) reloadAllData:(void (^)(bool success)) callback;
- (void) deletePost:(Post *)post withCallback:(void (^)(BOOL success))callback;
- (void) addPost:(Post *) post;
- (void) addPosts:(NSArray *) posts;
- (void) getPostAsync:(NSNumber *) postID callback:(void (^)(Post * post)) callback;
- (void) getPostAsync:(NSNumber *) postID andRefresh:(BOOL)refresh callback:(void (^)(Post * post)) callback;
- (Post *) getPost:(NSNumber *) postID;
- (NSArray *) getPosts:(NSArray *) postIDs;
- (void) saveCurrentData;

- (void) refreshBookmarks:(void (^)(bool success)) callback;
- (void) updateBookmarks:(void (^)(BOOL success, NSError *error)) callback;
- (void) setPostBookmarked:(Post *)post isBookmarked:(BOOL)isBookmarked;
- (void) setPostLiked:(Post *) post isLiked:(BOOL) isLiked;
- (BOOL) isPostLiked:(Post *) post;
- (BOOL) myPostsAreUpdated;

- (void) updateActivitiesPosts:(void (^)(bool success, NSArray * posts)) callback;

- (void) addPostMangerDelegate:(id<PQPostManagerDelegate>) delegate;
- (void) removePostManagerDelegate:(id<PQPostManagerDelegate>) delegate;
- (void) clearAllData;

+ (void) fetchCurrentUserPostHistoryWithPage:(int) page andSize:(int)size andCallback:(void (^)(BOOL result, long count, NSArray * posts))callbackBlock;
+ (void) fetchUserPostHistory:(double)userId withPage:(int) page andSize:(int)size andCallback:(void (^)(BOOL result, long count, NSArray * posts))callbackBlock;

/**
 This method will fetch posts based on current location. If current location is the same as previous one, it will try to fetch more.

 @param location current location we want to get location based on
 @param isRefresh indicate if this is a refresh, if refresh, current post ID will not be removed, nor search range increased
 @param getMore indicate if this is a get more, if get more, current post ID will not be removed
 @param callbackBlock this is used to provide result posts. Posts will never be null if success, use PQPostManager:getPost to fetch real post
 */
+ (void) getPostsBasedOnCurrentLocation:(CLLocation *)location isRefresh:(BOOL)isRefresh andGetMore:(BOOL)getMore withCallback:(void (^)(BOOL success, NSArray<NSNumber *> * postIDs)) callbackBlock;


- (NSArray *) getActivityIds;
- (NSArray *) getBookmarkIds;
@end
