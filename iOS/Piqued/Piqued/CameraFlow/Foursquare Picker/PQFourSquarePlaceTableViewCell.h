//
//  PQFourSquarePlaceTableViewCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 9/5/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PQFourSquarePlaceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel * venueName;
@property (weak, nonatomic) IBOutlet UILabel * address;

@end
