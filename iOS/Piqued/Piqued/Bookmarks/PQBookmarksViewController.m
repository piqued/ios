//
//  PQBookmarksViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 11/7/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQBookmarksViewController.h"
#import "PQBookmarkCell.h"
#import "PQAPIs.h"
#import "PQPostManager.h"
#import "Constants.h"
#import "PQAnalyticHelper.h"
#import "PQBasketViewController.h"
#import "PQScrollHandler.h"

// UI
#import "InfiniteScrollActivityView.h"

static NSString * const kBookmarkCell = @"PQBookmarkCell";

@interface PQBookmarksViewController () <UITableViewDelegate, UITableViewDataSource, PQPostManagerDelegate, PQScrollHandlerDelegate>

@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet UITableView *mainTable;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *searchButton;
@property (strong, nonatomic) InfiniteScrollActivityView *loadingMoreView;

@property (strong, nonatomic) NSArray * postIds;
@property (strong, nonatomic) UIRefreshControl * refreshControl;
@property (strong, nonatomic) Post * openPost;
@property (strong, nonatomic) PQScrollHandler * handler;

@property (weak, nonatomic) UIPanGestureRecognizer *scrollViewGestureRecognizer;

@property BOOL isLoading;
@property BOOL isEndOfResults;

@end

@implementation PQBookmarksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setHandler:[[PQScrollHandler alloc] init]];
    [self.handler setDelegate:self];
    [self setupUI];
    
#warning TODO - remove when implementing search
    [self.searchButton setTintColor:[UIColor clearColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setupUI
{
    [self setRefreshControl:[[UIRefreshControl alloc] init]];
    
    // Add infinite scroll spinner
    CGRect frame = CGRectMake(0, self.mainTable.contentSize.height, self.mainTable.bounds.size.width, kInfiniteScrollActivityViewDefaultHeight);
    self.loadingMoreView = [[InfiniteScrollActivityView alloc] initWithFrame:frame];
    self.loadingMoreView.hidden = YES;
    [self.mainTable addSubview:self.loadingMoreView];
    
    // Setup table
    [self.mainTable setBackgroundView:self.emptyView];
    [self.mainTable addSubview:self.refreshControl];
    [self.mainTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.mainTable registerNib:[UINib nibWithNibName:kBookmarkCell bundle:NULL] forCellReuseIdentifier:kBookmarkCell];
    
    [self setupScrollViewPanGestureRecognizer];
    [self.refreshControl addTarget:self action:@selector(refreshTableData) forControlEvents:UIControlEventValueChanged];
    
    UIEdgeInsets insets =  self.mainTable.contentInset;
    // Bottom inset for loading spinner
    insets.bottom += 64;
    self.mainTable.contentInset = insets;
    
    [self setupNavigationBar];
    
    self.isLoading = YES;
    [self loadMoreData];
}

- (void)setupScrollViewPanGestureRecognizer
{
    UIScrollView *scrollView = self.mainTable;
    for (UIGestureRecognizer *recognizer in scrollView.gestureRecognizers) {
        if ([recognizer isKindOfClass:UIPanGestureRecognizer.class]) {
            self.scrollViewGestureRecognizer = (UIPanGestureRecognizer *)recognizer;
        }
    }
}

- (void)setupNavigationBar
{
    UINavigationBar * navBar = self.navigationController.navigationBar;
    
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setLineBreakMode:NSLineBreakByTruncatingTail];
    [style setAlignment:NSTextAlignmentCenter];
    
    UIColor *color = [UIColor piq_colorFromHexString:kOrangeColor];
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:17.0];
    
    NSDictionary<NSString *,id> *textAttr = @{
                                              NSParagraphStyleAttributeName : style,
                                              NSForegroundColorAttributeName : color,
                                              NSFontAttributeName : font,
                                              };
    self.navigationItem.title = @"bookmarks";
    [navBar setTitleTextAttributes:textAttr];
}

- (void) viewWillAppear:(BOOL)animated
{
    [[PQAnalyticHelper sharedInstance] viewWillAppear:kBookmark];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTableData) name:kBookmarkListModified object:NULL];
    
    if (!self.isLoading) {
        [self refreshTableData];
    }
    
    [PQPostManager.sharedInstance addPostMangerDelegate:self];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [[PQAnalyticHelper sharedInstance] viewWillDisappear:kBookmark];   
}

- (void) viewDidDisappear:(BOOL)animated
{
    [[PQPostManager sharedInstance] removePostManagerDelegate:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - PQTabBarControllerTabProtocol

- (void)tabBarControllerDidSwitchToViewController:(UITabBarController *)tabBarController
{
    
}

- (void) refreshWithDelay
{
    [self performSelector:@selector(refreshTableData) withObject:NULL afterDelay:3];
}

- (void) refreshTableData
{
    [[PQPostManager sharedInstance] refreshBookmarks:^(bool success) {
        [self.refreshControl endRefreshing];
        
        if (success) {
            [self updateTableData];
        }
        
        self.isLoading = NO;
    }];
}

- (void) loadMoreData
{
    [self.loadingMoreView startAnimating];
    [[PQPostManager sharedInstance] updateBookmarks:^(BOOL success, NSError *error) {
        [self.loadingMoreView stopAnimating];
        
        if (success) {
            [self updateTableData];
        } else if (error == nil) {
            self.isEndOfResults = YES;
        }
        
        self.isLoading = NO;
    }];
}

- (void) updateTableData
{
    NSArray * postIds = [[PQPostManager sharedInstance] getBookmarkIds];
    [self setPostIds:postIds];
    [self.mainTable reloadData];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger count = self.postIds ? [self.postIds count] : 0;
    [tableView.backgroundView setHidden:(count > 0)];
    
    return count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PQBookmarkCell * cell = (PQBookmarkCell *) [tableView dequeueReusableCellWithIdentifier:kBookmarkCell];
    
    Post * post = [[PQPostManager sharedInstance] getPost:[self.postIds objectAtIndex:indexPath.row]];
    
    [cell setPost:post];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.openPost = [[PQPostManager sharedInstance] getPost:[self.postIds objectAtIndex:indexPath.row]];
    
    if (self.openPost != nil) {
        [self performSegueWithIdentifier:[PQBasketViewController segueIdentifier] sender:self];
    }
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (self.openPost != nil) {
        PQBasketViewController * controller = (PQBasketViewController *) segue.destinationViewController;
        
        [controller setPostID:self.openPost.postId];
        
        [PQMainTabBarController setTabBarHidden:NO];
    }
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.handler onViewScrolled:scrollView];
    
    // Handle loading more
    // Profile is not already loading
    
    // User is refreshing
    if (!self.isLoading) {
    
        // If all results are already loaded, do not check for more
        if (self.isEndOfResults) {
            return;
        }
        
        // Calculate the position of one screen length before the bottom of the results
        CGFloat scrollViewContentHeight = scrollView.contentSize.height;
        CGFloat scrollOffsetThreshold = scrollViewContentHeight - scrollView.bounds.size.height;
        
        // When the user has scrolled past the threshold, start requesting
        if(scrollView.contentOffset.y > scrollOffsetThreshold
           && scrollView.dragging
           && [self.scrollViewGestureRecognizer velocityInView:scrollView.superview].y < 0) {
            
            // Update position of loadingMoreView, and start loading indicator
            CGRect frame = CGRectMake(0, scrollView.contentSize.height, scrollView.bounds.size.width, kInfiniteScrollActivityViewDefaultHeight);
            self.loadingMoreView.frame = frame;
            [self.loadingMoreView startAnimating];
            
            self.isLoading = YES;
            // Code to load more results
            [self loadMoreData];
        }
    }
}

#pragma mark PQPostManagerDelegate method
- (void) bookmarkUpdated
{
    [self updateTableData];
}

- (void) onScroll:(PQScrollDirection)direction
{
    switch (direction) {
        case UP:
            [PQMainTabBarController setTabBarHidden:YES];
            break;
        case DOWN:
            [PQMainTabBarController setTabBarHidden:NO];
            break;
    }
}


@end
