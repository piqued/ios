//
//  PQCommentShowMore.m
//  Piqued
//
//  Created by Kenny M. Liou on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQCommentShowMore.h"
#import "UIColor+PIQHex.h"
#import <QuartzCore/QuartzCore.h>

@interface PQCommentShowMore()


@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation PQCommentShowMore

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.button.layer setBorderColor:[[UIColor piq_colorFromHexString:kOrangeColor] CGColor]];
}

+ (int) estimatedHeight
{
    return 49;
}

- (IBAction)showMorePressed:(UIButton *)sender {
    
    [self.basketDelegate showComment];
}

@end
