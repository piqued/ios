//
//  PQAlertControllerHelper.m
//  Piqued
//
//  Created by Kenny M. Liou on 9/3/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQAlertControllerHelper.h"

@implementation PQAlertControllerHelper

+ (UIAlertAction *) createActionWithTitle:(NSString *) title andIntegerValue:(NSInteger)integer withHandler:(void (^ __nullable)(UIAlertAction *action))handler
{
    return [UIAlertAction actionWithTitle:title
                             style:UIAlertActionStyleDefault
                           handler:handler];
}

@end
