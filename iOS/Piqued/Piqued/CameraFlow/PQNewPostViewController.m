//
//  PQNewPostViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 11/14/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQNewPostViewController.h"
#import "PQTextViewWithHint.h"
#import "UIColor+PIQHex.h"
#import "PQAPIs.h"
#import "PQScreenHelper.h"
#import "PQHashTagManager.h"
#import "PQHashTagCell.h"
#import "PQUserDefaults.h"
#import "PQYelpAPI.h"

#import "PQFourSquarePickerViewController.h"
#import "PQFourSquareAPI.h"

#import "Constants.h"

#import "CLLocation+PIQHelper.h"

#import <SDWebImage/UIImageView+WebCache.h>

#define kHashCellName @"PQHashTagCell"

static NSString * const kPlaceHolder = @"add your comment with #hashtag and emojis";

@interface PQNewPostViewController() <UITextViewDelegate, UITableViewDataSource, UITableViewDelegate, PQFourSquarePickerDelegate, PQTextViewWithHintDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet PQTextViewWithHint *commentView;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property (strong, nonatomic) CLGeocoder *geoCoder;
@property (strong, nonatomic) NSString * postTitle;
@property (strong, nonatomic) NSString * address;
@property (strong, nonatomic) UIImage * photo;
@property (strong, nonatomic) UIImage * thumbnail;

// defaultLocation is the data we get from either the user or the photo meta data
@property (strong, nonatomic) CLLocation * defaultLocation;
@property (strong, nonatomic) CLLocation * foursquareLocation;
@property PQUseFoursquareLocation useFourSquareLocationStatus;
@property (nonatomic) BOOL isPhotoLocation;

@property (weak, nonatomic) IBOutlet UIView *foursquareWrapperView;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;

@property NSString *locationName;
@property NSDictionary * foursquareSelectedResult;
@property NSArray * foursquareCategories;
@property NSString * foursquareID;
@property NSString * foursquareCC;
@property NSString * foursquareCountry;
@property NSString * foursquareState;
@property NSString * foursquareCity;

@property NSURL *yelpLink;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hashtagSuggestionHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hashtagSuggestionBottom;
@property (weak, nonatomic) IBOutlet UITableView *hashtagSuggestionTableView;



@property (strong, nonatomic) NSArray * foursquareResults;
@property (strong, nonatomic) NSArray * currentHashTagArray;
@property (strong, nonnull) NSString * currentHashTag;

@property (nonatomic) int keyboardHeight;

@end

@implementation PQNewPostViewController

// Initial properties
- (void) setPostInfo:(NSDictionary *)postInfo
{
    _postInfo = postInfo;
    [self setPhoto:[postInfo objectForKey:@"image"]];
    [self setThumbnail:[postInfo objectForKey:@"thumbnail"]];
    CLLocation *location = [postInfo objectForKey:@"location"];
    if ((id)location != [NSNull null]) { [self setDefaultLocation:location]; }
    self.isPhotoLocation = [[postInfo objectForKey:@"isPhotoLocation"] boolValue];
    if (self.defaultLocation == nil) {
        self.useFourSquareLocationStatus = PQUseFoursquareLocationYes;
    }
}

- (CLLocation *)location
{
    return (self.useFourSquareLocationStatus == PQUseFoursquareLocationYes) ? self.foursquareLocation : self.defaultLocation;
}

#pragma mark - Life Cycle
- (void) viewDidLoad
{
    if (self.photo) {
        [self.imageView setImage:self.photo];
    }
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tapGesture];
    
    self.useFourSquareLocationStatus = self.defaultLocation ? PQUseFoursquareLocationUndetermined : PQUseFoursquareLocationYes;
    if (self.defaultLocation) {
        [self autoloadFoursquareResults];
    }
    
    [self.foursquareWrapperView setUserInteractionEnabled:YES];
    UITapGestureRecognizer * openFoursquarePickerGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapFoursquareWrapper:)];
    [self.foursquareWrapperView addGestureRecognizer:openFoursquarePickerGesture];

    [self.commentView setPlaceholder:kPlaceHolder];
    [self.commentView setDelegate:self];
    [self.commentView setPq_delegate:self];
    
    [self updateSubmitButton];
    
    [self.hashtagSuggestionTableView registerNib:[UINib nibWithNibName:kHashCellName bundle:nil] forCellReuseIdentifier:kHashCellName];
    
    User * user = [PQUserDefaults getUser];
    
    [self.userImage sd_setImageWithURL:[NSURL URLWithString:user.profileImage] placeholderImage:[UIImage imageNamed:kBlankProfileImage]];
    
}

- (void) handleTap:(UIGestureRecognizer *)gesture
{
    CGPoint point = [gesture locationInView:self.view];
    CGRect commentViewInView = [self.view convertRect:self.commentView.frame fromView:self.commentView];
    if (!CGRectContainsPoint(commentViewInView, point)) {
        [self.commentView resignFirstResponder];
    }
}

- (int) getMoveUpDelta
{
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
    
    if (iOSDeviceScreenSize.width == 320) { // iPhone 5
        return -250;
    } else if (iOSDeviceScreenSize.width == 375) { // iPhone 6
        return -200;
    } else { // iPhone 6 plus
        return -150;
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.tintColor = [UIColor piq_colorFromHexString:@"#444444"];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void) viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onKeyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onKeyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onKeyboardFrameChange:)
                                                 name:UIKeyboardDidChangeFrameNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    [self.commentView becomeFirstResponder];
}

- (void) updateUIWithKeyboardHeight:(float) height
{
    [self updateUIWithKeyboardHeight:height andAnimationDuration:0.5];
}

- (void) updateUIWithKeyboardHeight:(float) height andAnimationDuration:(NSTimeInterval)duration
{
    BOOL keyboardIsShowing = height != 0;
    
    [self.view layoutIfNeeded];
    
    [self.imageViewTop setConstant:keyboardIsShowing ? [self getMoveUpDelta]:0];
    [self.buttonsContainer setConstant: keyboardIsShowing ? height:0];
    
    [UIView animateWithDuration:duration
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) onKeyboardFrameChange:(NSNotification *)notification
{
    
    if ([self.commentView isFirstResponder]) {
        NSDictionary* keyboardInfo = [notification userInfo];
        
        NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
        CGRect keyboardFrame = [keyboardFrameBegin CGRectValue];
        
        float height = keyboardFrame.size.height;
        
        int keyboardHeight = [PQScreenHelper getScreenHeight] - keyboardFrame.origin.y;
        [self setKeyboardHeight:keyboardHeight];
        
        [self updateUIWithKeyboardHeight:height andAnimationDuration:0.2];
    }
}

- (void) onKeyboardWillHide:(NSNotification *)notification
{
    [self updateUIWithKeyboardHeight:0];
}

- (void) onKeyboardWillShow:(NSNotification *)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    float height = keyboardFrameBeginRect.size.height;
    
    [self updateUIWithKeyboardHeight:height];
}

- (CLGeocoder *) geoCoder
{
    if (!_geoCoder) {
        _geoCoder = [[CLGeocoder alloc] init];
    }
    
    return _geoCoder;
}

/*
 Autoload Foursquare Result
 As of 2017/03/18, we require foursquare locations for submission.
 To make things easier, we autoload the first foursquare result,
 if we already have a location from the photo data.
*/
- (void) autoloadFoursquareResults {
    NSDictionary *options = @{
                              @"ll" : [NSString stringWithFormat:@"%f,%f", self.defaultLocation.coordinate.latitude, self.defaultLocation.coordinate.longitude]
                              };
    [PQFourSquareAPI requestFSExploreResults:options withCallback:^(NSArray * venues, NSError *error) {
        
        if (!error && venues) {
            self.foursquareResults = venues;
            
            if (venues.count > 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self updatePostWithFoursquareVenue:venues[0]];
                });
            }
        }
    }];
}

- (IBAction)clearButton:(id)sender {
    [self.commentView setText:@""];
}

- (IBAction)submit:(id)sender {
    [self.overlayView setHidden:NO];
    
    [self hideHashTagSuggestions];
    [self.view endEditing:YES];
    [self.navigationItem setHidesBackButton:YES];

    CLLocation * location = (self.useFourSquareLocationStatus == PQUseFoursquareLocationYes) ? [self.location piq_randomLocation:40] : self.location;
    NSDictionary * postContents = @{
                                    @"title": self.postTitle ?: @"",
                                    @"location": location,
                                    @"location_name": self.locationName ?: @"",
                                    @"address": self.address ?: @"",
                                    @"description": self.commentView.text ?: @"",
                                    @"image": self.photo,
                                    @"categories": self.foursquareCategories ?: @[],
                                    @"foursquare_venue_id": self.foursquareID ?: @"",
                                    @"foursquare_cc": self.foursquareCC ?: @"",
                                    @"foursquare_state": self.foursquareState ?: @"",
                                    @"foursquare_city": self.foursquareCity ?: @"",
                                    @"yelp_id": self.yelpLink.absoluteString ?: @""
                                    };
    
    [[PQNetworking.sharedInstance uploadPost:postContents] continueWithBlock:^id _Nullable(BFTask * _Nonnull t) {
        if (t.error) {
            PQLog(@"Failed to upload post %@", t.error);
            [self createDialog:@"Cannot upload post"
                       message:@"Oops, there seems to be some problem. Please try again"];
        } else {
            [self createDialog:@"Post Created!"
                       message:@"Your post is uploaded!"];

        }
        return nil;
    }];
//
//    // If using foursquare location, generate a random location so they don't all overlap
//    CLLocation * location = (self.useFourSquareLocationStatus == PQUseFoursquareLocationYes) ? [self.location piq_randomLocation:40] : self.location;
//    
//    NSDictionary * postContents = @{
//                                    @"title": self.postTitle ?: @"",
//                                    @"location": location,
//                                    @"address": self.address ?: @"",
//                                    @"description": self.commentView.text ?: @"",
//                                    @"image": self.photo,
//                                    @"thumbnail": self.thumbnail ?: @"",
//                                    @"foursquare_venue_id": self.foursquareID
//                                    };
//    [PQAPIs apiCallToCreatePost:postContents withCallback:^(NSError *error) {
//        
//        if (error) {
//            PQLog(@"Failed to upload post %@", error);
//            [self createDialog:@"Cannot upload post"
//                       message:@"Oops, there seems to be some problem. Please try again"];
//        } else {
//            [self createDialog:@"Post Created!"
//                       message:@"Your post is uploaded!"];
//
//        }
//        
//    }];
}

- (void) createDialog:(NSString *) title message:(NSString *) message
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [alertController dismissViewControllerAnimated:YES completion:Nil];
                                   [self dismissViewControllerAnimated:YES completion:Nil];
                               }];
    
    [alertController addAction:okAction];
    
    if ([NSThread isMainThread]) {
        [self presentViewController:alertController animated:YES completion:nil];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alertController animated:YES completion:nil];
        });
    }
}

// Update submit button accordingly
- (void) updateSubmitButton
{
    BOOL shouldEnable = [self isDataValidForPost];
    
    [self.submitButton setEnabled:shouldEnable];
    [self.submitButton setBackgroundColor:[UIColor piq_colorFromHexString: shouldEnable ? kOrangeColor : @"#CCCCCC"]];
}

- (void)tapFoursquareWrapper:(id)sender
{
    [self performSegueWithIdentifier:[PQFourSquarePickerViewController segueIdentifier] sender:sender];
}

- (void)updatePostWithFoursquareVenue:(NSDictionary *)venue
{
    self.foursquareSelectedResult = venue;
    self.address = [PQFourSquareAPI getAddressString:venue[@"location"]];
    self.postTitle = venue[@"name"] ?: @"";
    self.locationName = venue[@"name"] ?: @"";
    [self.addressLabel setText:self.postTitle];
    self.foursquareID = venue[@"id"];
    NSDictionary *locationInfo = venue[@"location"];
    self.foursquareCC = locationInfo[@"cc"];
    self.foursquareCountry = locationInfo[@"country"];
    self.foursquareState = locationInfo[@"state"];
    self.foursquareCity = locationInfo[@"city"];
    
    NSArray *foursquareCategories = venue[@"categories"];
    __block NSMutableArray *categories = [NSMutableArray array];
    [foursquareCategories enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull category, NSUInteger idx, BOOL * _Nonnull stop) {
        NSDictionary *categoryToUpload = @{
                                           @"foursquare_id" : category[@"id"] ?: @"",
                                           @"name" : category[@"name"] ?: @"",
                                           @"short_name" : category[@"shortName"] ?: @"",
                                           @"plural_name" : category[@"pluralName"] ?: @"",
                                           };
        [categories addObject:categoryToUpload];
    }];
    
    self.foursquareCategories = categories;
    
    CLLocationDegrees lat = [venue[@"location"][@"lat"] doubleValue];
    CLLocationDegrees lng = [venue[@"location"][@"lng"] doubleValue];
    CLLocation * location = [[CLLocation alloc] initWithLatitude:lat longitude:lng];
    
    self.foursquareLocation = location;
    [self updateYelpWithFoursquare];
}

- (void)handleFourSquarePlacePicked:(NSDictionary *)venue
{
    [self updatePostWithFoursquareVenue:venue];
    
    // Determine whether or not to use foursquare location data over default data
    if (self.useFourSquareLocationStatus == PQUseFoursquareLocationUndetermined) {
        BOOL isFourSquareLocationFarAway = [self.foursquareLocation distanceFromLocation:self.defaultLocation] > 50;
        self.useFourSquareLocationStatus = isFourSquareLocationFarAway ? PQUseFoursquareLocationYes : PQUseFoursquareLocationNo;
    }
}

- (void)updateYelpWithFoursquare
{
    [PQYelpAPI getYelpURL:self.postTitle location:self.location withLocationHint:self.foursquareCountry withCallback:^(NSURL * _Nullable yelpLink) {
        self.yelpLink = yelpLink;
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:[PQFourSquarePickerViewController segueIdentifier]]) {
        PQFourSquarePickerViewController * vc = (PQFourSquarePickerViewController *)segue.destinationViewController;
        /*
         * Location Type is:
         * NewPostLocationTypeUnavailable !defaultLocation (photo or user location)
         * NewPostLocationTypePhoto defaultLocation && isPhotoLocation
         * NewPostLocationTypeUser defaultLocation && !isPhotoLocation
         */
        NewPostLocationType locationType = self.defaultLocation == nil ? NewPostLocationTypeUnavailable : self.isPhotoLocation ? NewPostLocationTypePhoto : NewPostLocationTypeUser;
        [vc loadWithInitialResults:self.foursquareResults location:self.location locationType:locationType];
        vc.delegate = self;
    }
}

#pragma mark - UITableView

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    [self setCurrentHashTagArray:[[PQHashTagManager sharedInstance] getHashTag:self.currentHashTag]];
    return [self.currentHashTagArray count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PQHashTagCell * result = (PQHashTagCell *) [tableView dequeueReusableCellWithIdentifier:kHashCellName];
    
    PQHashTagRecord * record = self.currentHashTagArray[indexPath.row];
    
    [result setRecord:record];
    
    return result;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PQHashTagRecord * record = self.currentHashTagArray[indexPath.row];
    
    [self hashTagSelected:record.hashTag];
    [self hideHashTagSuggestions];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [PQHashTagCell getCellHeight];
}

#pragma mark - Text View Delegate

- (void)textViewDidChange:(UITextView *)textView
{
    BOOL enable = textView.text.length > 0;
    [self.clearButton setEnabled:enable];
    [self.clearButton setHidden:!enable];
    [self updateSubmitButton];
    
    [self detectHashTag];
}

- (NSRange) getHashTagRange:(NSRange) selectedRange
{
    // This function is used to detect if any given cursor position has a hashtag before with without a space.
    NSRange preStringRange = NSMakeRange(0, selectedRange.location);
    NSString * currentString = [self.commentView.text substringWithRange:preStringRange];
    NSArray * substrings = [currentString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSUInteger count = [substrings count];
    NSRange hashTagRange = NSMakeRange(selectedRange.location, 0); // we return a range of length 0 to indicate no hashtag
    if (count > 0) {
        NSString * lastComponent = substrings[count - 1];
        if ([lastComponent hasPrefix:@"#"] && ![lastComponent hasPrefix:@"##"]) {
            // we found an hash tag
            hashTagRange = NSMakeRange(selectedRange.location - lastComponent.length, lastComponent.length);
        }
    }
    
    return hashTagRange;
}

- (void) detectHashTag
{
    NSRange hashTagRange = [self getHashTagRange:[self.commentView selectedRange]];
    
    if (hashTagRange.length > 0) {
        NSString * currentHashtag = [[self.commentView text] substringWithRange:NSMakeRange(hashTagRange.location + 1, hashTagRange.length - 1)];
        
        [self setCurrentHashTag:currentHashtag];
        [self.hashtagSuggestionTableView reloadData];
        [self showHashTagSuggestions];
    } else {
        [self hideHashTagSuggestions];
    }
}

#pragma mark - Helpers

- (void) hashTagSelected:(NSString *) hashTag
{
    NSRange selectedRange = [self.commentView selectedRange];
    NSRange hashTagRange = [self getHashTagRange:selectedRange];
    NSString * currentString = self.commentView.text;
    
    NSString * characterAtRight;
    if (selectedRange.location < currentString.length) {
        characterAtRight = [currentString substringWithRange:NSMakeRange(selectedRange.location, 1)];
    } else {
        characterAtRight = @"";
    }
    
    NSString * replaceentPattern;
    if ([@" " isEqualToString:characterAtRight]) {
        replaceentPattern = @"#%@";
    } else {
        replaceentPattern = @"#%@ ";
    }
    
    NSString * result = [currentString stringByReplacingCharactersInRange:hashTagRange
                                                               withString:[NSString stringWithFormat:replaceentPattern, hashTag]];
    
    [self.commentView setText:result];
}

- (void) showHashTagSuggestions
{
    CGFloat suggestionTop = self.keyboardHeight;
    
    [self.commentView startTabGesture:YES];
    
    if (self.hashtagSuggestionBottom.constant != suggestionTop) {
        
        [self.hashtagSuggestionTableView setHidden:NO];
        self.hashtagSuggestionBottom.constant = suggestionTop;
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

- (void) hideHashTagSuggestions
{
    if (self.hashtagSuggestionBottom.constant != -300) {
        self.hashtagSuggestionBottom.constant = -300;
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [self.hashtagSuggestionTableView setHidden:YES];
        }];
    }
}

- (BOOL) isDataValidForPost
{
    /*
     Location cannot be nil
     Comment cannot be empty
     Comment cannot be just #
     
     postTitle must not be empty (Foursquare venue title)
     address must not be empty (Foursquare venue address)
     foursquareId must not be empty (Foursquare venue ID)
     
     Must have image
     Must have thumbnail
     */
    
    BOOL valid =    piq_notNilOrNull(self.location) &&
                    self.commentView.text.length > 0 &&
                    ![@"#" isEqualToString:self.commentView.text.trimmed] &&
    
                    self.postTitle.length > 0 &&
                    self.address.length > 0 &&
                    self.foursquareID.length > 0 &&
    
                    piq_notNilOrNull(self.photo) &&
                    piq_notNilOrNull(self.thumbnail);
    return valid;
}

#pragma mark - Keyboard show

-(void) keyboardDidShow:(NSNotification *)notification{

    self.hashtagSuggestionBottom.constant = -300; // start with hidden place
    self.hashtagSuggestionHeight.constant = [PQHashTagCell getCellHeight] * 3.5; // show 3 at min
}

#pragma mark - PQTextViewWithHintDelegate

- (void) onTabGestureDetected
{
    [self hideHashTagSuggestions];
    [self.commentView startTabGesture:NO];
}

@end
