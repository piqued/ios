//
//  PQActivityViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 2/6/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQActivityNewViewController.h"
#import "PQActivityBaseCell.h"
#import "PQYourActivitiesCell.h"

// Navigation
#import "PQBasketViewController.h"
#import "PQProfileViewController.h"

// Model managers
#import "PQPostManager.h"

// Helpers
#import "UIColor+PIQHex.h"
#import "PQAnalyticHelper.h"

static float const kIndicatorAnimationDuration = 0.3;
static NSString * const kActivityCell = @"PQYourActivitiesCell";
static NSString * const kFollowersCell = @"PQFollowersCell";

typedef enum {
    PQActivityTabTypeYourActivity = 0,
    PQActivityTabTypeFollowerActivity,
    PQActivityTabTypeCount
} PQActivityTabType;

@interface PQActivityNewViewController () <UICollectionViewDataSource, UICollectionViewDelegate, PQActivityBaseCellDelegate>

@property (weak, nonatomic) IBOutlet UIButton *button01;
@property (weak, nonatomic) IBOutlet UIButton *button02;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *indicatorLeftPadding;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *indicatorColorLeftPadding;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *indicatorColorRightPadding;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *toggleView;


@property (strong, nonatomic) NSArray * buttons;
@property (strong, nonatomic) Post * openPost;
@property (nonatomic) BOOL openPostWithComment;

@property (nonatomic) int currentIndex;
@property (nonatomic) int indicatorWidth;
@property (nonatomic) BOOL scrollByButton;
@end

@implementation PQActivityNewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupUI];
    
    [self itemSelected:0];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[PQAnalyticHelper sharedInstance] viewWillAppear:kActivity];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[PQAnalyticHelper sharedInstance] viewWillDisappear:kActivity];
}

#pragma mark - PQTabBarControllerTabProtocol

- (void)tabBarControllerDidSwitchToViewController:(UITabBarController *)tabBarController
{
    // Refresh data
    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:PQActivityTabTypeYourActivity inSection:0]];
    if ([cell isKindOfClass:PQYourActivitiesCell.class]) {
        PQYourActivitiesCell *yourActivityCell = (PQYourActivitiesCell *)cell;
        [yourActivityCell refreshData];
    }
}

- (void) setupUI
{
    for (UIButton * button in self.buttons) {
        [self attachButtonClickListener:button];
    }
    
    [self.collectionView registerNib:[UINib nibWithNibName:kActivityCell bundle:Nil] forCellWithReuseIdentifier:kActivityCell];
    [self.collectionView registerNib:[UINib nibWithNibName:kFollowersCell bundle:Nil] forCellWithReuseIdentifier:kFollowersCell];
    
}

- (NSArray *) buttons
{
    if (!_buttons) {
        NSMutableArray * array =[NSMutableArray array];
        [array addObject:self.button01];
        [array addObject:self.button02];
        
        _buttons = array;
    }
    
    return _buttons;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Toggle methods

- (void) attachButtonClickListener:(UIButton *)button
{
    [button addTarget:self
               action:@selector(onButtonTouchUpInside:)
     forControlEvents:UIControlEventTouchUpInside];
}

- (void) onButtonTouchUpInside:(UIButton *) button
{
    NSUInteger index = [self.buttons indexOfObject:button];
    
    [self itemSelected:(int)index];
}

- (void) itemSelected:(int)index
{
    [self updateButton:index];
    
    [self setCurrentIndex:index];
    
    [self updateIndicator:index];
}

- (void) updateIndicator:(int)index
{
    [self.view layoutIfNeeded];
    
    [self setScrollByButton:YES];
 
    if (self.indicatorWidth < 1) {
        [self setIndicatorWidth:(self.view.frame.size.width / [self.buttons count])];
        
        CGSize stringsize = [@"your activities" sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]}];
        
        float indicatorPadding = (self.indicatorWidth - stringsize.width) / 2 - 10;
        
        [self.indicatorColorLeftPadding setConstant:indicatorPadding];
        [self.indicatorColorRightPadding setConstant:indicatorPadding];
    }

    float padding = self.indicatorWidth * index;

    [self.indicatorLeftPadding setConstant:padding];
    [self.collectionView setContentOffset:CGPointMake(padding * [self.buttons count], 0) animated:YES];
    
    [UIView animateWithDuration:kIndicatorAnimationDuration
                     animations:^{
                         [self.view layoutIfNeeded]; // Called on parent view
                     }];
}

- (void) updateButton:(int)index
{
    UIButton * currentButton = [self.buttons objectAtIndex:self.currentIndex];
    UIButton * nextButton = [self.buttons objectAtIndex:index];
    
    [currentButton setTitleColor:[UIColor piq_colorFromHexString:kOrangeColor] forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor piq_colorFromHexString:kSelectedColor] forState:UIControlStateNormal];
}

#pragma mark UICollectionView methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return PQActivityTabTypeCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row;
    
    UICollectionViewCell * result;
    
    PQYourActivitiesCell * cell;
    switch (index) {
        case PQActivityTabTypeYourActivity:
            result = [collectionView dequeueReusableCellWithReuseIdentifier:kActivityCell forIndexPath:indexPath];
            cell = (PQYourActivitiesCell *) result;
            [cell setDelegate:self];
            break;
        case PQActivityTabTypeFollowerActivity:
            result = [collectionView dequeueReusableCellWithReuseIdentifier:kFollowersCell forIndexPath:indexPath];
            break;
        default:
            result = NULL;
            break;
    }
    
    return result;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section;
{
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size = collectionView.frame.size;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0")) {
        size.height -= self.navigationController.navigationBar.frame.size.height;
    }
    
    return size;
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    int offsetX = scrollView.contentOffset.x;
    
    if (!self.scrollByButton) {
        [self.indicatorLeftPadding setConstant:offsetX / [self.buttons count]];
    }
}

- (void) scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self setScrollByButton:NO];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int index = self.collectionView.contentOffset.x / self.collectionView.frame.size.width;
    
    [self itemSelected:index];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController class] == [PQBasketViewController class]) {
        
        PQBasketViewController * controller = segue.destinationViewController;
        
        [controller setPostID:self.openPost.postId];
        [controller setHideNavBarWhenBack:YES];
        [controller setOpenCommentImmediately:self.openPostWithComment];
        
        self.openPost = nil;
    }
}

#pragma mark PQActivityBaseCellDelegate method

- (void) openBasketViewWithID:(NSNumber *)postID withComment:(BOOL)openComment
{
    self.openPost = [[PQPostManager sharedInstance] getPost:postID];
    self.openPostWithComment = openComment;
    
    if (self.openPost != nil) {
        [self performSegueWithIdentifier:[PQBasketViewController segueIdentifier] sender:self];
    }
}

- (void)openProfile:(double)userId
{
    PQLog(@"Profile opened");
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"TabProfile" bundle:nil];
    PQProfileViewController *profileVC = [storyBoard instantiateViewControllerWithIdentifier:[PQProfileViewController storyboardIdentifier]];
    profileVC.userId = userId;
    
    UINavigationController *navigationVC = [[UINavigationController alloc] initWithRootViewController:profileVC];
    [self presentViewController:navigationVC animated:YES completion:nil];
}

@end
