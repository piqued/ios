//
//  PQPostCommentCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 6/12/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PQBasketBaseCell.h"
#import "Comment.h"

typedef enum {
    None,
    PostSingle,
    PostMany,
    CommentCell
} CommentType;

typedef enum {
    SourcePost,
    SourceComment
} CommentSourceType;

@interface PQPostCommentCell : PQBasketBaseCell

@property (nonatomic) CommentType type;
@property (nonatomic) CommentSourceType sourceType;

@property (nonatomic) int commentIndex;

+ (int) estimatedHeight;

- (void) setNumberOfLines:(int) numberOfLines;
@end
