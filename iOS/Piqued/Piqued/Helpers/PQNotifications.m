//
//  PQNotifications.m
//  Piqued
//
//  Created by Matt Mo on 7/2/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import "PQNotifications.h"

// Navigation
#import "PQMainTabBarController.h"

// Model Helpers
#import "PQUserDefaults.h"
#import "PQKeyChain.h"

// Alerts
#import "PQGlobalAlerts.h"

// Third-party
#import <OneSignal/OneSignal.h>

@implementation PQNotifications

+ (instancetype)sharedManager {
    static PQNotifications *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    
    return sharedManager;
}

+ (void)registerForPushNotifications
{
    UNUserNotificationCenter *notifCenter = [UNUserNotificationCenter currentNotificationCenter];
    [notifCenter requestAuthorizationWithOptions:(UNAuthorizationOptionAlert |
                                                  UNAuthorizationOptionBadge |
                                                  UNAuthorizationOptionSound)
                               completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                   [self getNotificationSettings];
                                   if (PQEnvironment.buildConfiguration == PQBuildConfigurationDebug) {
                                       NSString *title = @"Push Notifications";
                                       NSString *message = [NSString stringWithFormat:@"permissions granted: %@", granted ? @"YES" : @"NO"];
                                       [PQGlobalAlerts presentAlertWithTitle:title message:message];
                                   }
                               }];
}

+ (void)getNotificationSettings
{
    UNUserNotificationCenter *notifCenter = [UNUserNotificationCenter currentNotificationCenter];
    [notifCenter getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        PQLog(@"Notification settings: %@", settings);
        
        if (settings.authorizationStatus != UNAuthorizationStatusAuthorized) {
            return;
        }
        
        [UIApplication.sharedApplication registerForRemoteNotifications];
    }];
}

+ (void)handleRemoteNotificationsDeviceToken:(NSData *)deviceToken
{
    if (!deviceToken.length) {
        PQLog(@"Error: device token unavailable");
        return;
    }
    
    NSMutableString* token = [NSMutableString stringWithCapacity: deviceToken.length * 2];
    
    [deviceToken enumerateByteRangesUsingBlock:^(const void * _Nonnull bytes, NSRange byteRange, BOOL * _Nonnull stop) { //Turn a binary to a string expressing the value as a hexidecimal
        for (NSUInteger i = 0; i < byteRange.length; i++) {
            [token appendFormat: @"%02.2hhx", ((char*) bytes)[i]];
        }
    }];
    
    [PQUserDefaults setAPNSDeviceToken:token];
    PQLog(@"Device Token: %@", token);
    
    // Need to upload new APNS device token if the device id has changed
    BFTask* result = [PQNetworking.sharedInstance registerDeviceToken:token];
    [result continueWithBlock:^id _Nullable(BFTask * _Nonnull t) {
        PQLog(@"Uploaded device token response: %@", t.result);
        return nil;
    }];
}

+ (void)piquedApplication:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    PQLog(@"didReceiveRemoteNotification: %@ applicationState: %@", userInfo, [self stringForApplicationState:application.applicationState]);
    switch (application.applicationState) {
        case UIApplicationStateActive: {
            break;
        }
        case UIApplicationStateInactive: {
            break;
        }
        case UIApplicationStateBackground: {
            break;
        }
    }
    
    if (completionHandler != nil) {
        completionHandler(UIBackgroundFetchResultNoData);
    }
}

+ (NSString *)stringForApplicationState:(UIApplicationState)state
{
    switch (state) {
        case UIApplicationStateActive:
            return @"UIApplicationStateActive";
        case UIApplicationStateInactive:
            return @"UIApplicationStateInactive";
        case UIApplicationStateBackground:
            return @"UIApplicationStateBackground";
    }
}

#pragma mark - Badge Updates

+ (void)updateBadgeOnLoad
{
    [PQMainTabBarController setBadgeValue:UIApplication.sharedApplication.applicationIconBadgeNumber ? @"" : nil forTab:PQMainTabBarControllerTapTypeActivities];
}

+ (void)markAllNotificationsRead
{
    [PQMainTabBarController setBadgeValue:nil forTab:PQMainTabBarControllerTapTypeActivities];
}

#pragma mark - UNUserNotificationCenterDelegate

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler
{
    PQLog(@"didReceiveNotificationResponse: %@", response);
    
    if ([response.actionIdentifier isEqualToString:UNNotificationDefaultActionIdentifier]) {
        [PQMainTabBarController showTab:PQMainTabBarControllerTapTypeActivities];
    }
    
    if (completionHandler != nil) {
        completionHandler();
    }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
    PQLog(@"willPresentNotification: %@", notification);
    [PQMainTabBarController setBadgeValue:@"" forTab:PQMainTabBarControllerTapTypeActivities];
    if (completionHandler != nil) {
        completionHandler(UNNotificationPresentationOptionNone);
    }
}

@end
