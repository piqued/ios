//
//  PQPostCollectionViewCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 8/17/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"

@protocol PQPostCollectionViewCellDelegate <NSObject>

- (void)openProfile:(double)userId;

@end

@interface PQPostCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) id<PQPostCollectionViewCellDelegate> delegate;
@property (weak, nonatomic) Post * post;

+ (NSString *) reuseIdentifier;
+ (NSString *) nibName;

@end
