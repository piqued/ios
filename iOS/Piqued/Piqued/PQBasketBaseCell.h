//
//  PQBasketBaseCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 12/5/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Post.h"
#import "Comment.h"

typedef enum {
    BasketDetail,
    BasketButtons,
    BasketComment,
    BasketShowMore,
    BasketMap,
    BasketLocation,
    BasketRelatedContent,
    BasketTipTitle,
    BasketTips,
    // new thing go above
    BasketCellCount,
} PQBasketCellType;

@protocol PQBasketCellDelegate <NSObject>

- (Post *) getPost;
- (void) showComment;
- (void) openProfile:(double)userId;
- (void) shareButtonPressed;
- (void) presentViewController: (UIViewController *) controller;

@optional

- (Comment *) getComment:(int)index;
- (NSString *) getPostLocationInfo;
- (NSURL *) getFSLink;
- (NSURL *) getYelpLink;
- (void) setLike:(BOOL) like;

@end

@interface PQBasketBaseCell : UITableViewCell

@property (weak, nonatomic) id<PQBasketCellDelegate> basketDelegate;

-(void) reloadData;

@end
