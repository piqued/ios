//
//  PQCameraViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 11/8/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQCameraFlowController.h"
#import "UIColor+PIQHex.h"
#import "PQImagePickerDelegate.h"
#import "PQImageHandler.h"
#import "PQImageViewCell.h"
#import "PQLocationManager.h"
#import "PQNewPostViewController.h"
#import "PQAnalyticHelper.h"
#import "PQCameraCollectionViewController.h"
#import "PQUtilities.h"

#import <MBProgressHUD/MBProgressHUD.h>

typedef enum {
    ALBUM,
    CAMERA,
    VIDEO,
    COUNT
} CameraFlowPager;

static NSString * const kCameraFlow =       @"CameraFlow";
static NSString * const kAlbum =            @"Album";
static NSString * const kCamera =           @"Camera";
static NSString * const kVideo =            @"Video";
static NSString * const kCameraCell =       @"PQCameraViewCell";
static NSString * const kVideoCell =        @"PQVideoViewCell";
static NSString * const kAlbumCell =        @"PQAlbumViewCell";

static int const        kTabCount =         COUNT - 1;

@interface PQCameraFlowController () <UICollectionViewDataSource, UICollectionViewDelegate, PQImagePickerDelegate, PQImageHandler>

@property (weak, nonatomic) IBOutlet UICollectionView *viewContainer;
@property (weak, nonatomic) IBOutlet UIScrollView *pageIndicator;
@property (weak, nonatomic) IBOutlet UICollectionView *cameraCollectionView;


@property (strong, nonatomic) NSMutableDictionary * titleDic;
@property (strong, nonatomic) PHImageManager * imageManager;
@property (nonatomic) BOOL isPhotoLocationNil;
@property (strong, nonatomic) PQCameraCollectionViewController * controller;
@property (nonatomic) int cameraPreviewOffset;
@property (nonatomic) BOOL shouldIgnoreProgrammaticScroll;
@property (nonatomic) BOOL showDefaultCell;
@property (nonatomic) CameraFlowPager currentView;
@property (strong, nonatomic) NSDictionary *postInfo;

@end

@implementation PQCameraFlowController

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
 
    [self setTitleDic:[NSMutableDictionary dictionary]];
    [self setCurrentView:CAMERA];
    [self.pageIndicator setDelegate:self];
    [self setNeedsStatusBarAppearanceUpdate];
    [self setupCollectionView];
    
    self.navigationItem.title = @"cancel"; // so create post view has "cancel" on back button
}

- (void) viewWillAppear:(BOOL)animated
{
    [self createIndicatorLabel];
    
    [self.viewContainer reloadData];
}

- (void) setupCollectionView
{
    [self.viewContainer registerNib:[UINib nibWithNibName:kCameraCell bundle:NULL] forCellWithReuseIdentifier:kCameraCell];
    [self.viewContainer registerNib:[UINib nibWithNibName:kVideoCell bundle:NULL] forCellWithReuseIdentifier:kVideoCell];
    [self.viewContainer registerNib:[UINib nibWithNibName:kAlbumCell bundle:NULL] forCellWithReuseIdentifier:kAlbumCell];
    
    [self setShowDefaultCell:YES];
    
    [self setupCameraPreview];
}

- (void) setupCameraPreview
{
    NSString * cellName = [PQCameraCollectionViewController getCellName];
    NSString * emptyCellName = [PQCameraCollectionViewController getEmptyCellName];
    
    [self setController:[[PQCameraCollectionViewController alloc] init]];
    [self.controller setHandler:self];
    
    [self.cameraCollectionView registerNib:[UINib nibWithNibName:emptyCellName bundle:NULL] forCellWithReuseIdentifier:emptyCellName];
    [self.cameraCollectionView registerNib:[UINib nibWithNibName:cellName bundle:NULL] forCellWithReuseIdentifier:cellName];

    
    [self.cameraCollectionView setContentInset:UIEdgeInsetsZero];
    [self.cameraCollectionView setScrollIndicatorInsets:UIEdgeInsetsZero];
    [self.cameraCollectionView setDelegate:self.controller];
    [self.cameraCollectionView setDataSource:self.controller];
    
}

#pragma mark - Container Collection View methods

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return kTabCount;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    PQImageViewCell * cell;
    
    switch (indexPath.row) {
        case 0:
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:kAlbumCell forIndexPath:indexPath];
            break;
        case 1:
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCameraCell forIndexPath:indexPath];
            break;
        case 2:
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:kVideoCell forIndexPath:indexPath];
            break;
    }
    
    [cell setDelegate:self];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return collectionView.frame.size;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // update camera view
    if (scrollView == self.viewContainer) {
        [self updateCameraOffset:scrollView];
    }
    
    CGFloat labelWidth = self.view.frame.size.width / COUNT;
    
    if (!self.shouldIgnoreProgrammaticScroll) {
        
        if (scrollView == self.pageIndicator) {
            
            CGPoint offset = scrollView.contentOffset;
            
            offset.x = (offset.x + labelWidth) * 3;
            
            [self setShouldIgnoreProgrammaticScroll:YES];
            [self.viewContainer setContentOffset:offset];
            
        } else {
            
            CGPoint offset = scrollView.contentOffset;
            
            offset.x = offset.x / 3 - labelWidth;
            
            [self setShouldIgnoreProgrammaticScroll:YES];
            [self.pageIndicator setContentOffset:offset];
        }
    } else {
        [self setShouldIgnoreProgrammaticScroll:NO];
    }
    
    // update labels
    for (id obj in self.titleDic) {
        UILabel * label = [self.titleDic objectForKey:obj];
        
        [label setTextColor:[UIColor whiteColor]];
    }
    
    UILabel * label;
    if (self.pageIndicator.contentOffset.x > labelWidth / 2) {
        label = [self.titleDic objectForKey:[NSNumber numberWithInt:VIDEO]];
    } else if (self.pageIndicator.contentOffset.x < -1 * (labelWidth / 2)) {
        label = [self.titleDic objectForKey:[NSNumber numberWithInt:ALBUM]];
    } else {
        label = [self.titleDic objectForKey:[NSNumber numberWithInt:CAMERA]];
    }
    [label setTextColor:[UIColor piq_colorFromHexString:kOrangeColor]];
}


- (void) updateCameraOffset:(UIScrollView *) scrollView
{
    CGPoint offsetPoint = scrollView.contentOffset;
    offsetPoint.x = MIN(offsetPoint.x, scrollView.frame.size.width);
    
    [self.cameraCollectionView setContentOffset:offsetPoint];
}

#pragma mark - Misc

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void) viewDidLayoutSubviews
{
    if (self.showDefaultCell) {
        [self.controller start];
        [self setShowDefaultCell:NO];
        [self.viewContainer setContentOffset:CGPointMake(self.viewContainer.frame.size.width, 0) animated:NO];
        [self.cameraCollectionView setContentOffset:CGPointMake(self.cameraCollectionView.frame.size.width, 0) animated:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) createIndicatorLabel
{
    CGFloat labelWidth = self.view.frame.size.width / COUNT;
    int height = 30;
    
    
    for (int i = 0; i < (kTabCount); i++) {
        
        UILabel * label = [self.titleDic objectForKey:[NSNumber numberWithInt:i]];
        
        if (label == NULL) {
            label = [[UILabel alloc] init];
            [label setTag:i];
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
            tapGestureRecognizer.numberOfTapsRequired = 1;
            [label addGestureRecognizer:tapGestureRecognizer];
            label.userInteractionEnabled = YES;
            
            switch (i) {
                case ALBUM:
                    [label setText:kAlbum];
                    break;
                case CAMERA:
                    [label setText:kCamera];
                    break;
                case VIDEO:
                    [label setText:kVideo];
                    break;
                default:
                    break;
            }
            
            if (i == self.currentView) {
                [label setTextColor:[UIColor piq_colorFromHexString:kOrangeColor]];
            } else {
                [label setTextColor:[UIColor whiteColor]];
            }
            [label sizeToFit];
            CGRect frame = label.frame;
            
            frame.size.width = labelWidth;
            frame.size.height = height;
            frame.origin.x = i * labelWidth;
            frame.origin.y = 0;
            
            [label setFrame:frame];
            [label setTextAlignment:NSTextAlignmentCenter];
            
            [self.titleDic setObject:label forKey:[NSNumber numberWithInt:i]];
            [self.pageIndicator addSubview:label];
        }
    }
    
    [self.pageIndicator setContentSize:CGSizeMake(labelWidth * 3, height)];
    [self.pageIndicator setContentInset:UIEdgeInsetsMake(0, labelWidth, 0, labelWidth)];
}

- (void) labelTapped:(id) sender
{
    CGFloat labelWidth = self.view.frame.size.width / COUNT;
    UITapGestureRecognizer * recognizer = sender;

    UILabel * label = (UILabel *) recognizer.view;
    int type = (int) label.tag;
    
    [self.pageIndicator setContentOffset:CGPointMake(labelWidth * (type - 1), 0) animated:YES];
}


- (IBAction)exitButtonPressed:(id)sender {
    
    [self exit];
}

- (void) exit
{
    [self dismissViewControllerAnimated:self completion:NULL];
}

- (PHImageManager *) imageManager
{
    if (!_imageManager) {
        _imageManager = [[PHImageManager alloc] init];
    }
    
    return _imageManager;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController * destViewController = segue.destinationViewController;
    
    if ([destViewController isKindOfClass:[PQNewPostViewController class]]) {
        
        PQNewPostViewController * newPostView = (PQNewPostViewController *) destViewController;
        newPostView.postInfo = self.postInfo;
    }
}

#pragma mark - PQImageHandler

- (void) openPinDetailsWithImage:(nonnull UIImage *) image
                   withThumbnail:(nonnull UIImage *) thumbnail
                    withLocation:(nullable CLLocation *) location
                 isPhotoLocation:(BOOL) isPhotoLocation
{
    if (image && thumbnail) {
        self.postInfo = @{
                          @"image" : image,
                          @"thumbnail" : thumbnail,
                          @"location" : location ?: [NSNull null],
                          @"isPhotoLocation" : @(isPhotoLocation),
                          };
        [self performSegueWithIdentifier:@"createNewPost" sender:self.navigationController];
    } else {
        //TODO: - add proper error
        PQLog(@"Error: Invalid post, no image");
    }
}

- (void) showSpinner
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void) hideSpinner
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark - PQImagePickerDelegate

- (PHImageManager *) getManager
{
    return self.imageManager;
}

- (void) takePhoto
{
    [self showSpinner];
    [self.controller takePhoto];
}

- (void) onImageAssetPicked:(PHAsset *) asset
{
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.networkAccessAllowed = YES;
    [self.imageManager requestImageDataForAsset:asset
                                        options:options
                                  resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
        
        UIImage * image = [UIImage imageWithData:imageData];
        
        UIImage * compressedImage = scaleAndRotateImage(image, 1080);
        UIImage * thumbnail = scaleAndRotateImage(image, 480);
        CLLocation *location = asset.location ?: [PQLocationManager sharedInstance].location;
        [self openPinDetailsWithImage:compressedImage withThumbnail:thumbnail withLocation:location isPhotoLocation:asset.location];
    }];
}

@end
