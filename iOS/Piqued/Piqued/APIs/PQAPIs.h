//
//  PQAPIs.h
//  Piqued
//
//  Created by Kenny M. Liou on 10/26/15.
//  Copyright © 2015 First Brew. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "Post.h"
#import "User.h"
@import MapKit;

@interface PQAPIs : NSObject

/*
 * Login with username and password
 * callback block is a function that takes a boolean, true if success false otherwise
 *
 */
+ (void) loginWithName:(NSString *) userName andPassword:(NSString *) password withCallback:(void (^)(User * user, NSError * error))callbackBlock;
/*
 * Login with user email and token from a social media site
 *
 */
+ (void) loginWithSiteName:(NSString *) siteName withSiteToken:(NSString *) token withCallback:(void (^)(User * user, NSError * error))callbackBlock;
/*
 * create account with name,
 * email, and password
 *
 * callback block is a function that takes a boolean, true if success false otherwise
 *
 */
+ (void) createAccount:(NSString *) userName withEmail:(NSString *)email andPassword:(NSString *) password andConfirmPassword:(NSString *) cPassword withCallback:(void (^)(User *user, NSError *error))callbackBlock;

/*
 * Edit password
 *
 */
+ (void) editUserPassword:(NSString *) password andConfirmed:(NSString *) cPassword withCallback:(void (^)(BOOL success))callbackBlock;

/*
 * Update User Info
 *
 */
+ (void) editUserInfo:(NSString *) newUserName withImageData:(UIImage *) imageData withEmail:(NSString *)newEmail withCallback:(void (^)(BOOL success))callbackBlock;

/*
 * Fetch User info
 *
 */
+ (void) fetchUserInfo:(double) userId withCallback:(void (^) (User * user))callback;

/*
 * Fetch User's Post History
 *
 */
+ (void) fetchUserPostHistory:(double)userId withPage:(int) page andSize:(int)size andCallback:(void (^)(BOOL result, long count, NSArray * posts))callbackBlock;

/*
 * Fetch Current User's post history
 */
+ (void) fetchCurrentUserPostHistoryWithPage:(int) page andSize:(int)size andCallback:(void (^)(BOOL result, long count, NSArray * posts))callbackBlock;

/*
 * Retrieve posts within a specified area
 *
 */
+ (void)retrievePostsForOuterRegion:(MKCoordinateRegion)outerRegion notInnerRegion:(MKCoordinateRegion)innerRegion withOptions:(NSDictionary *)options withCallback:(void (^)(NSDictionary *postInfo, NSError * error))callbackBlock;

/*
 * Retrieve liked posts from Instagram within a specified area
 *
 */
+ (void)retrieveInstagramPostsForOuterRegion:(MKCoordinateRegion)outerRegion notInnerRegion:(MKCoordinateRegion)innerRegion withOptions:(NSDictionary *)options withCallback:(void (^)(NSDictionary *, NSError *))callbackBlock;

/*
 * Retrieve posts that are bookmarked
 *
 */
+ (void)retrieveBookmarkedPosts:(NSDictionary *)options
                       callback:(void (^)(NSArray *bookmarkedPosts, NSError *error))callbackBlock;

/*
 * Create new post
 *
 */
+ (void)apiCallToCreatePost:(NSDictionary *)postContents withCallback:(void (^)(NSError * error))callback ;

/*
 * Bookmark
 *
 * isDirty only indicate the post's bookmark status is differ from original state. Server will flip the status
 */
+ (void)apiCallToBookmarkPost:(BOOL)isBookmarked withPostID:(NSNumber *) postID withCallback:(void (^)(BOOL successfulCall, BOOL isBookmarked))callback;

/*
 * Get Post by ID
 *
 * Use this method to fetch individual post by ID
 */
+ (void) getPostByID:(NSNumber *)postID withCallback:(void(^)(Post * post)) callback;

/*
 * Add comment
 *
 * Add comment to a particular post
 */
+ (void) addComment:(NSString *)comment toPost:(Post *)post withCallback:(void(^)(BOOL finished)) callback;

/*
 * Get Comments
 *
 * Get comments given a post
 */
+ (void) getComments:(Post *) post withCallback:(void(^) (Post * post, NSArray * comments)) callback;

/*
 * Like a post
 *
 * Like a given post
 */
+ (void) setPost:(Post *) post liked:(BOOL) liked withCallback:(void(^) (BOOL success, Post * post)) callback;


/**
 Delete a post

 @param post     post to delete, method will work only when post.userid equals to [PQUserDefaults getUser].userid
 @param callback callback method, with boolean indicate success or not
 */
+ (void) deletePost:(Post *) post withCallback:(void(^) (BOOL success)) callback;

@end
