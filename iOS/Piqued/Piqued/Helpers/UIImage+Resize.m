//
//  UIImage+Resize.m
//  Piqued
//
//  Created by Matt Mo on 1/21/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import "UIImage+Resize.h"

@implementation UIImage (Resize)

- (UIImage *)compressImageByRatio:(CGFloat)compressScale;
{
    UIImage *compressedImage = nil;
    
    if (compressScale <= 0) return compressedImage;
    
    CGImageRef cgImage = self.CGImage;
    
    // Get compressed image
    CGFloat width = CGImageGetWidth(cgImage) * compressScale;
    CGFloat height = CGImageGetHeight(cgImage) * compressScale;
    size_t bitsPerComponent = CGImageGetBitsPerComponent(cgImage);
    size_t bytesPerRow = CGImageGetBytesPerRow(cgImage);
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(cgImage);
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(cgImage);
    
    CGContextRef context = CGBitmapContextCreate(nil, width, height, bitsPerComponent, bytesPerRow, colorSpace, bitmapInfo);
    
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), cgImage);
    
    CGImageRef scaledImage = CGBitmapContextCreateImage(context);
    compressedImage = [UIImage imageWithCGImage:scaledImage];
    
    NSData *imageData = [[NSData alloc] initWithData:UIImageJPEGRepresentation((compressedImage), 1.0)];
    
    int imageSize = imageData.length;
    NSLog(@"SIZE OF IMAGE: %i ", imageSize);
    
    // Release refs
    // NULL is okay
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CGImageRelease(cgImage);
    CGImageRelease(scaledImage);
    
    return compressedImage;
}

@end
