//
//  PQLocationSearchDataSource.m
//  Piqued
//
//  Created by Matt Mo on 10/14/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import "PQLocationSearchDataSource.h"
#import "PQSearchHandler.h"
#import <GooglePlaces/GooglePlaces.h>
@import MapKit;

NSString * const kSearchTableIdentifier = @"PQLocationSearchTableViewCell";
NSString * const kSearchTableDefaultCellIdentifier = @"PQLocationSearchOptionCell";

@interface PQLocationSearchDataSource()

@property (nonatomic, strong) PQSearchHandler *searchHandler;

@end

@implementation PQLocationSearchDataSource

- (void)setSearchBar:(UISearchBar *)searchBar
{
    _searchBar = searchBar;
}

- (PQSearchHandler *)searchHandler
{
    if (_searchHandler == nil) {
        _searchHandler = [[PQSearchHandler alloc] init];
        @weakify(self);
        _searchHandler.searchCallback = ^(NSString *query) {
            @strongify(self);
            
            GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
            filter.type = kGMSPlacesAutocompleteTypeFilterCity;
            
            [GMSPlacesClient.sharedClient autocompleteQuery:query
                                      bounds:nil
                                      filter:filter
                                    callback:^(NSArray *results, NSError *error) {
                                        if (error != nil) {
                                            NSLog(@"Autocomplete error %@", [error localizedDescription]);
                                            return;
                                        }
                                        
                                        for (GMSAutocompletePrediction* result in results) {
                                            NSLog(@"Result '%@' with placeID %@", result.attributedFullText.string, result.placeID);
                                        }
                                        
                                        self.data = results;
                                        [self.tableView reloadData];
                                    }];
        };
    }
    return _searchHandler;
}

- (instancetype)initWithTableView:(UITableView *)tableView
{
    if (self = [super init]) {
        self.tableView = tableView;
        [self registerIdentifier:kSearchTableIdentifier];
    }
    return self;
}

- (void)setupTableView:(UITableView *)tableView
{
    self.tableView = tableView;
    [self registerIdentifier:kSearchTableIdentifier];
    [self registerIdentifier:kSearchTableDefaultCellIdentifier];
}

#pragma mark - Table View Support

- (void)registerIdentifier:(NSString *)tableIdentifier
{
    [self.tableView registerNib:[UINib nibWithNibName:tableIdentifier bundle:NSBundle.mainBundle]
         forCellReuseIdentifier:tableIdentifier];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchBar.text.length > 0 ? self.data.count : self.availableLocationTypes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *tableCell = nil;
    if (self.searchBar.text.length > 0) {
        GMSAutocompletePrediction* result = self.data[indexPath.row];
        tableCell = [tableView dequeueReusableCellWithIdentifier:kSearchTableIdentifier];
        tableCell.textLabel.text = result.attributedFullText.string;
    } else {
        tableCell = [tableView dequeueReusableCellWithIdentifier:kSearchTableDefaultCellIdentifier];
        NewPostLocationType locationType = self.availableLocationTypes[indexPath.row].intValue;
        tableCell.textLabel.text = NewPostLocationTypeConstants[locationType];
    }
    
    return tableCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searchBar.text.length > 0) {
        GMSAutocompletePrediction* result = self.data[indexPath.row];
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:result.attributedFullText.string completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
            CLPlacemark *topResult = placemarks.firstObject;
            [self.delegate locationDataSource:self didSelectNewLocation:topResult.location withPlaceName:result.attributedFullText.string]; 
        }];
    } else {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.selected = NO;
        NewPostLocationType locationType = self.availableLocationTypes[indexPath.row].intValue;
        if (self.currentLocationType != locationType) {
            self.currentLocationType = locationType;
            [self.delegate locationDataSource:self didSelectLocationType:locationType];
        }
    }
}

#pragma mark - Search

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    PQLog(@"Location Query: %@", searchText);
    if (searchText.length == 0) {
        [self.tableView reloadData];
    } else {
        [self.searchHandler scheduleSearch:searchText withMinimumLength:3];
    }
}

#pragma mark - Data Source Access

- (id)objectAtIndexedSubscript:(NSUInteger)index
{
    return self.data[index];
}

- (void)setObject:(id)obj atIndexedSubscript:(NSUInteger)index
{
    return;
}

@end
