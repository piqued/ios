//
//  UIImage+Resize.h
//  Piqued
//
//  Created by Matt Mo on 1/21/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resize)

- (UIImage *)compressImageByRatio:(CGFloat)compressScale;

@end
