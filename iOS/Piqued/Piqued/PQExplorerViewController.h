//
//  FirstViewController.h
//  Piqued
//
//  Created by Kenny M. Liou on 10/18/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQGridViewController.h"
#import "PQMapViewController.h"
#import "PQMainTabBarController.h"

@interface PQExplorerViewController : UIViewController <PQMapViewControllerDelegate, PQGridViewControllerDelegate, PQTabBarControllerTabProtocol>

@property (weak, nonatomic) PQGridViewController * gridViewController;
@property (weak, nonatomic) PQMapViewController * mapViewController;

- (void)prepareForMiniBasket:(BOOL)showBasket;

@end

