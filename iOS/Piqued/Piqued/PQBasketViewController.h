//
//  PQBasketViewController.h
//  Piqued
//
//  Created by Kenny M. Liou on 11/28/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQInstagramPost.h"

@interface PQBasketViewController : UIViewController

@property (strong, nonatomic) NSNumber * postID;
@property (nonatomic) BOOL hideNavBarWhenBack;
@property (nonatomic) BOOL openCommentImmediately;

+ (NSString *) segueIdentifier;

@end
