//
//  PQDeviceOrientation.m
//  Piqued
//
//  Created by Kenny M. Liou on 3/4/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQDeviceOrientationManager.h"
@import CoreMotion;

@interface PQDeviceOrientationManager()

@property (strong, nonatomic) CMMotionManager *motionManager;

@end

@implementation PQDeviceOrientationManager

+ (instancetype) sharedInstance {
    static PQDeviceOrientationManager * orientationManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        orientationManager = [[self alloc] init];
    });
    
    return orientationManager;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        self.motionManager = [[CMMotionManager alloc] init];
        self.motionManager.accelerometerUpdateInterval = .2;
        [self.motionManager startAccelerometerUpdatesToQueue:[[NSOperationQueue alloc] init]
                                                 withHandler:^(CMAccelerometerData  *accelerometerData, NSError *error) {
                                                     [self updateOrientation:accelerometerData];
                                                     if(error) {
                                                         PQLog(@"%@", error);
                                                     }
                                                 }];
    }
    return self;
}

+ (UIDeviceOrientation) orientation
{
    return orientation;
}

UIDeviceOrientation orientation;

- (void) updateOrientation:(CMAccelerometerData *) data
{
    UIDeviceOrientation orientationNew;
    
    NSString * orientationString;
    CMAcceleration acceleration = data.acceleration;
    
    if (acceleration.x >= 0.75) {
        orientationNew = UIDeviceOrientationLandscapeRight;
        orientationString = @"UIDeviceOrientationLandscapeRight";
    }
    else if (acceleration.x <= -0.75) {
        orientationNew = UIDeviceOrientationLandscapeLeft;
        orientationString = @"UIDeviceOrientationLandscapeLeft";
    }
    else if (acceleration.y <= -0.75) {
        orientationNew = UIDeviceOrientationPortrait;
        orientationString = @"UIDeviceOrientationPortrait";
    }
    else if (acceleration.y >= 0.75) {
        orientationNew = UIDeviceOrientationPortraitUpsideDown;
        orientationString = @"UIDeviceOrientationPortraitUpsideDown";
    }
    else {
        // Consider same as last time
        return;
    }
    
    orientation = orientationNew;
}

@end
