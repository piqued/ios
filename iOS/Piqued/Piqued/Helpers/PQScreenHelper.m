//
//  PQScreenHelper.m
//  Piqued
//
//  Created by Kenny M. Liou on 6/13/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQScreenHelper.h"

@interface PQScreenHelper()

@property (nonatomic) CGRect screenFrame;

@end

@implementation PQScreenHelper

+ (instancetype) sharedInstance {
    static PQScreenHelper * screenHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        screenHelper = [[self alloc] init];
        [screenHelper setScreenFrame:[[UIScreen mainScreen] bounds]];
    });
    
    return screenHelper;
}

+ (CGFloat) getScreenWidth
{
    return [PQScreenHelper sharedInstance].screenFrame.size.width;
}

+ (CGFloat) getScreenHeight
{
    return [PQScreenHelper sharedInstance].screenFrame.size.height;
}

@end
