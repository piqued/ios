//
//  PQPostCollectionViewCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 8/17/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQPostCollectionViewCell.h"
#import "PQMainTabBarController.h"
#import "PQGridViewController.h"
#import "PQLocationManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PQPostManager.h"
#import "PQAPIs.h"
#import "PQMath.h"
#import "PQImageURLHelper.h"
#import "NSString+PIQHelper.h"
#import "UIView+PIQHelper.h"
#import "UIColor+PIQHex.h"

CGRect CGRectExpandByPoints(CGRect rect, CGFloat points)
{
    CGRect newRect = rect;
    newRect.origin.x -= points;
    newRect.origin.y -= points;
    newRect.size.width += (points * 2.);
    newRect.size.height += (points * 2.);
    return newRect;
}

@interface PQPostCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UIButton *bookmarkButton;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *postAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *postCommentLabel;
@property (weak, nonatomic) IBOutlet UIView *infoContainer;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UIImageView *distanceImage;

@end

@implementation PQPostCollectionViewCell

+ (NSString *)nibName
{
    return NSStringFromClass([self class]);
}

+ (UINib *)nib
{
    return [UINib nibWithNibName:[self nibName] bundle:nil];
}

+ (NSString *)reuseIdentifier
{
    return [NSString stringWithFormat:@"%@%@", [self nibName], @"ReuseIdentifier"];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGestureRecognizer:)];
    [singleTap setNumberOfTapsRequired:1];
    [self.infoContainer addGestureRecognizer:singleTap];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // push the execution to next execution cycle to get the width/height value
    dispatch_async(dispatch_get_main_queue(), ^{
        NSArray * colors = [NSArray arrayWithObjects:(id)[[UIColor piq_colorFromHexString:@"0000008C"] CGColor], (id)[[UIColor piq_colorFromHexString:@"00000000"] CGColor], nil];
        [self.infoContainer piq_setGradientBackgroundWithColors:colors start:CGPointMake(0.0, 1.0) end:CGPointMake(0.0, 0.02)];
        
    });
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.post = nil;
    self.mainImageView.image = nil;
    self.distanceLabel.text = nil;
    self.bookmarkButton.selected = NO;
    self.profileImageView.image = [UIImage imageNamed:@"blankProfile"];
    self.usernameLabel.text = nil;
    self.postAddressLabel.text = nil;
}

- (void)setPost:(Post *)post {
    _post = post;
    
    double distance = [PQMath getMiles:post];
    
    NSString * distanceString;
    
    if (distance >= 20) {
        distanceString = [NSString stringWithFormat:@"%.lf mi", distance];
    } else {
        distanceString = [NSString stringWithFormat: @"%.1lf mi", distance];
    }
    
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:self.post.userimage] placeholderImage:[UIImage imageNamed:@"blankProfile"]];
    
    NSString * shrunkURL = [PQImageURLHelper adjustImageURLString:post.imageUrlLarge toSize:PQImageSizeLarge];
    if (post.imageUrlLarge.length > 0) {
        shrunkURL = post.imageUrlLarge;
        PQLog(@"Large link: %@", shrunkURL);
    }
    [self setImage:shrunkURL withSize:self.frame.size];
    
    if (post.title && post.title.length > 0) {
        self.postAddressLabel.text = post.title;
    } else {
        [self.postAddressLabel setText:self.post.address];
    }
    
    [self.postCommentLabel setText:post.desc];
    
    if (post.username) {
        self.usernameLabel.text = post.username;
    } else {
        self.usernameLabel.text = @"";
    }
    self.distanceLabel.text = distanceString;
    [self.distanceImage setImage:[UIImage imageNamed:[PQMath getMilesImage:distance]]];
    
    [self.bookmarkButton setSelected:post.bookmarked];
}

- (void)setImage:(NSString *) imageUrl withSize:(CGSize) size {
    if (imageUrl) {
        
        [self.mainImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
        
        [self.mainImageView setUserInteractionEnabled:YES];
        UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openBasket)];
        [singleTap setNumberOfTapsRequired:1];
        [self.mainImageView addGestureRecognizer:singleTap];
    }
}

- (void)handleTapGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
{
    CGRect tappableProfileImageViewArea = self.profileImageView.frame;
    tappableProfileImageViewArea = CGRectExpandByPoints(tappableProfileImageViewArea, 10);
    CGPoint point = [gestureRecognizer locationInView:self.infoContainer];
    if (CGRectContainsPoint(tappableProfileImageViewArea, point)) {
        [self openProfile];
    } else {
        [self openBasket];
    }
}

- (void)openBasket
{
    PQLog(@"open post: %@", self.post);
    [PQGridViewController showBasketView:self.post];
}

- (IBAction)bookmarkButtonPressed:(id)sender
{
    PQLog(@"Bookmark button pressed");
    
    Post * post = self.post;
    
    // We send a 0 to the server, but actually this means 1 for bookmarked
    PQPostManager * manager = [PQPostManager sharedInstance];
    [manager addPost:post];
    
    BOOL bookmarked = !self.bookmarkButton.selected;
    
    [manager setPostBookmarked:post isBookmarked:bookmarked];
    
    [post setBookmarked:bookmarked];
    [self setIsBookmarked:bookmarked];
    
    // Fire notification to update posts across app
    NSDictionary * postUpdateInfo = @{@"post": post};
    [[NSNotificationCenter defaultCenter] postNotificationName:PostDidGetBookmarkedNotification object:self userInfo:postUpdateInfo];
}

- (void)setIsBookmarked:(BOOL)isBookmarked
{
    [self.bookmarkButton setSelected:isBookmarked];
}

- (void)openProfile
{
    [self.delegate openProfile:self.post.userId];
}

@end
