//
//  PQSupportTeamHelper.h
//  Piqued
//
//  Created by Kenny M. Liou on 9/10/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post.h"

@import MessageUI;

@interface PQSupportTeamHelper : NSObject

+ (void) reportPost:(Post *) post withViewController:(UIViewController *)controller withDelegate:(id<MFMailComposeViewControllerDelegate>) delegate;

@end
