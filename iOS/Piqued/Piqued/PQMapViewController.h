//
//  PQMapViewController.h
//  Piqued
//
//  Created by Matt Mo on 10/27/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQLocationManager.h"
#import <FBAnnotationClustering/FBAnnotationClustering.h>
@import MapKit;

@protocol PQMapViewControllerDelegate <NSObject>

- (void)prepareForMiniBasket:(BOOL)showBasket;

@end

@interface PQMapViewController : UIViewController <MKMapViewDelegate, PQLocationManagerDelegate, FBClusteringManagerDelegate>

@property (weak, nonatomic) id<PQMapViewControllerDelegate> delegate;

+ (NSString *)segueIdentifier;

- (void)showMiniBasket:(BOOL)show;

@end
