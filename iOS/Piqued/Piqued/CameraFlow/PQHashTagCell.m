//
//  PQHashTagCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 6/13/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQHashTagCell.h"

@interface PQHashTagCell()

@property (weak, nonatomic) IBOutlet UILabel *hashTag;


@end

@implementation PQHashTagCell

+ (CGFloat) getCellHeight
{
    return 31;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void) setRecord:(PQHashTagRecord *)record
{
    _record = record;
    [self.hashTag setText:[NSString stringWithFormat:@"#%@", record.hashTag]];
}

@end
