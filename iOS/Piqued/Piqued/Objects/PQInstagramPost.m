//
//  PQInstagramPost.m
//  Piqued
//
//  Created by Matt Mo on 5/21/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQInstagramPost.h"
#import "PQFourSquareAPI.h"
#import "PQPostManager.h"

#define NSUINT_BIT (CHAR_BIT * sizeof(NSUInteger))
#define NSUINTROTATE(val, howmuch) ((((NSUInteger)val) << howmuch) | (((NSUInteger)val) >> (NSUINT_BIT - howmuch)))

@interface PQInstagramPostManager : NSObject

@property (nonatomic, strong)   NSMutableDictionary * instagramPostsDict;

+ (instancetype) sharedManager;
- (NSNumber *) getPostIDForInstagramID:(NSString *)instagramID;

@end

@interface PQInstagramPost()

@property (nonatomic, readwrite) InstagramMedia * instagramMedia;
@property (nonatomic, readwrite) NSString * realAddress;
@property (nonatomic, readwrite) NSString * instagramFsVenueId;
@property (nonatomic, readwrite) NSNumber *instagramPostId;

@end

@implementation PQInstagramPost

- (instancetype)initWithMedia:(InstagramMedia *)media
{
    if (self = [super init]) {
        self.instagramMedia = media;
    }
    return self;
}

- (NSNumber *)postId
{
    if (!self.instagramPostId) {
        NSNumber * number = [[PQInstagramPostManager sharedManager] getPostIDForInstagramID:[self.instagramMedia Id]];
        self.instagramPostId = number;
    }
    return self.instagramPostId;
}

- (NSString *)imageUrl
{
    return [self.instagramMedia standardResolutionImageURL].absoluteString;
}

- (NSString *)title
{
    return nil;
}

- (NSString *)desc
{
    return [[self.instagramMedia caption] text];
}

- (NSString *)username
{
    return [[self.instagramMedia user] username];
}

- (NSString *)userimage
{
    return [[self.instagramMedia user] profilePictureURL].absoluteString;
}

- (NSString *)address
{
    return [self.realAddress length] ? self.realAddress : [self.instagramMedia locationName];
}

- (NSString *)foursquareVenueID
{
    return self.instagramFsVenueId;
}

- (double)lat
{
    return [self.instagramMedia location].latitude;
}

- (double)lng
{
    return [self.instagramMedia location].longitude;
}

- (BOOL)bookmarked
{
    return YES;
}

#pragma mark - InstagramMedia unique

- (NSString *)instagramID
{
    return [self.instagramMedia Id];
}

- (NSUInteger)hash
{
    return NSUINTROTATE(self.lat, NSUINT_BIT / 3) ^ NSUINTROTATE(self.lng, NSUINT_BIT * 2/ 3) ^ [self.postId hash];
}

- (BOOL) isEqual:(id)object {
    if (self == object) {
        return YES;
    } else if (![object isKindOfClass:[self class]]) {
        return NO;
    } else {
        return [self isEqualPosts:object];
    }
}

- (BOOL) isEqualPosts:(PQInstagramPost *)post {
    BOOL isEqual = self.lat == post.lat && self.lng == post.lng &&
    self.postId == post.postId &&
    [self.imageUrl isEqualToString:post.imageUrl] &&
    [self.username isEqualToString:post.username];
    return isEqual;
}

- (void) updateFsVenueIdWithCallback: (void(^)(Post * updatedPost))callback
{
    if (!self.foursquareVenueID) {
        NSDictionary * options = @{
                                   @"query" : [self.instagramMedia locationName],
                                   @"ll"    : [NSString stringWithFormat:@"%f,%f", self.lat, self.lng]
                                   };
        __weak PQInstagramPost * weakSelf = self;
        [PQFourSquareAPI requestFSExploreResults:options withCallback:^(NSArray * venues, NSError *error) {
            __block PQInstagramPost * strongSelf = weakSelf;
            strongSelf.realAddress = [PQFourSquareAPI getAddressString:venues[0][@"location"]];
            strongSelf.instagramFsVenueId = venues[0][@"id"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[PQPostManager sharedInstance] addPost:strongSelf];
                callback(strongSelf);
            });
        }];
    } else {
        callback(self);
    }
}

@end

@implementation PQInstagramPostManager

+ (instancetype) sharedManager
{
    static PQInstagramPostManager * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
        instance.instagramPostsDict = [NSMutableDictionary dictionary];
    });
    
    return instance;
}

- (NSNumber *) getPostIDForInstagramID:(NSString *)instagramID
{
    NSNumber * postID = [self.instagramPostsDict objectForKey:instagramID];
    if (!postID) {
        int total = -((int)[[self.instagramPostsDict allKeys] count] + 1);
        postID = [NSNumber numberWithInt:total];
        [self.instagramPostsDict setObject:postID forKey:instagramID];
    }
    return postID;
}

@end
