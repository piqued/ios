//
//  InfiniteScrollActivityView.h
//  Piqued
//
//  Created by Matt Mo on 9/6/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfiniteScrollActivityView : UIView

@property (nonatomic, strong) UIActivityIndicatorView * activityIndicatorView;
extern const CGFloat kInfiniteScrollActivityViewDefaultHeight;

- (void)stopAnimating;
- (void)startAnimating;

@end
