//
//  NSString+PQConvenience.m
//  Piqued
//
//  Created by Matt Mo on 3/18/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import "NSString+PQConvenience.h"

@implementation NSString (PQConvenience)

- (NSString *)trimmed
{
    NSString *trimmedString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    return trimmedString;
}

@end
