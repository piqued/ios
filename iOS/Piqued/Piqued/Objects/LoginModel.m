//
//  LoginModel.m
//  Piqued
//
//  Created by Matt Mo on 7/30/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "LoginModel.h"

@implementation LoginModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"secret"          : @"secret",
             @"token"           : @"token_id",
             @"userId"          : @"user_id",
             };
}

@end
