//
//  PQPostGridCollectionViewCell.m
//  Piqued
//
//  Created by Matt Mo on 9/3/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQPostGridCollectionViewCell.h"
#import "PQImageURLHelper.h"
#import "PQImageCacheDelegate.h"
#import <SDWebImage/SDWebImageManager.h>
#import "PQUtilities.h"
#import "PQPostManager.h"

@interface PQPostGridCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) UIGestureRecognizer *recognizer;
@property BOOL refetching;

@end

@implementation PQPostGridCollectionViewCell

+ (NSString *)nibName
{
    return NSStringFromClass([self class]);
}

+ (UINib *)nib
{
    return [UINib nibWithNibName:[self nibName] bundle:nil];
}

+ (NSString *)reuseIdentifier
{
    return [NSString stringWithFormat:@"%@%@", [self nibName], @"ReuseIdentifier"];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    if (!_recognizer) {
        self.recognizer = [[UITapGestureRecognizer alloc] init];
        [self.recognizer addTarget:self action:@selector(handleTapGesture:)];
        [self addGestureRecognizer:self.recognizer];
    }
}

- (void)setPost:(Post *)post
{
    _post = post;
    if (post != nil) {
        NSString *urlString = [PQImageURLHelper adjustImageURLString:post.imageUrlSmall toSize:PQImageSizeMedium];
        if (post.imageUrlSmall.length > 0) {
            urlString = post.imageUrlSmall;
        }
        NSURL *url = [NSURL URLWithString:urlString];
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL:url
                              options:0
                             progress:nil
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
         {
             if (image && finished) {
                 UIImage *thumbnailImage = scaleAndRotateImage(image, 480);
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if ([imageURL.absoluteString isEqualToString:url.absoluteString]) {
                         self.imageView.image = thumbnailImage;
                     }
                 });
             } else {
                 if (self.refetching) {
                     return;
                 } else {
                     [self refetchPost:post.postId];
                 }
             }
         }];
    }
}

- (void)refetchPost:(NSNumber *)postID
{
    self.refetching = YES;
    [[PQPostManager sharedInstance] getPostAsync:postID andRefresh:YES callback:^(Post *post) {
        [self setPost:post];
        self.refetching = NO;
    }];
}

- (void)handleTapGesture:(UIGestureRecognizer *)recognizer
{
    [self.delegate openBasketViewWithID:self.post.postId];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.imageView.image = nil;
    self.post = nil;
}

@end
