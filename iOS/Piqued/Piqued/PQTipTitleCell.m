//
//  PQTipTitleCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 4/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQTipTitleCell.h"

@interface PQTipTitleCell()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation PQTipTitleCell

+ (int) estimatedHeight
{
    return 25;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void) setTipCount:(int) count
{
    if (count == 1) {
        [self.titleLabel setText:@"1 Tip"];
    } else {
        [self.titleLabel setText:[NSString stringWithFormat:@"%d Tips", count]];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) reloadData
{
    Post * post = [self.basketDelegate getPost];
    
    [self setTipCount:(int) [post.tips count]];
}


@end
