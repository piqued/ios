//
//  PQClusterAnnotationView.h
//  Piqued
//
//  Created by Matt Mo on 12/19/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface PQClusterAnnotationView : MKAnnotationView

@property (assign, nonatomic) NSUInteger count;
@property (assign, nonatomic) NSURL * imageURL;

@end
