//
//  InfiniteScrollActivityView.m
//  Piqued
//
//  Created by Matt Mo on 9/6/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "InfiniteScrollActivityView.h"

@implementation InfiniteScrollActivityView

const CGFloat kInfiniteScrollActivityViewDefaultHeight = 60.0;

- (instancetype)init
{
    if (self = [super init]) {
        self.activityIndicatorView = [[UIActivityIndicatorView alloc] init];
        [self setupActivityIndicator];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.activityIndicatorView = [[UIActivityIndicatorView alloc] init];
        [self setupActivityIndicator];
    }
    return self;
}

- (void)layoutSubviews
{
    self.activityIndicatorView.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
}

- (void)setupActivityIndicator
{
    self.activityIndicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    self.activityIndicatorView.hidesWhenStopped = YES;
    [self addSubview:self.activityIndicatorView];
}

- (void)stopAnimating
{
    [self.activityIndicatorView stopAnimating];
    self.hidden = YES;
}

- (void)startAnimating
{
    self.hidden = NO;
    [self.activityIndicatorView startAnimating];
}

@end

