//
//  Comment.h
//  Piqued
//
//  Created by Kenny M. Liou on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface Comment : NSObject

@property (strong, nonatomic, readonly) NSNumber * commentId;
@property (nonatomic, readonly) double commenterId;
@property (strong, nonatomic, readonly) NSString * message;
@property (strong, nonatomic, readonly) NSString * userProfileImage;
@property (strong, nonatomic, readonly) NSString * userName;
@property (strong, nonatomic, readonly) NSDate * timeStamp;

+ (instancetype) createCommentWithDictionary:(NSDictionary *)dic;

@end
