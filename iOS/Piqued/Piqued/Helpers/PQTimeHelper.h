//
//  PQTimeHelper.h
//  Piqued
//
//  Created by Kenny M. Liou on 6/12/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQTimeHelper : NSObject

+ (NSString *) timeFromNow:(NSDate *) timeStamp;

@end
