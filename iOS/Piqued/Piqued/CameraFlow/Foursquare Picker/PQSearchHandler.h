//
//  PQSearchHandler.h
//  Piqued
//
//  Created by Matt Mo on 12/2/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQSearchHandler : NSObject

@property (nonatomic, copy) void (^searchCallback)(NSString *query);

- (void)scheduleSearch:(NSString *)query withMinimumLength:(NSUInteger)minLength;

@end
