//
//  PQLocationSearchDataSource.h
//  Piqued
//
//  Created by Matt Mo on 10/14/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PQFourSquarePickerViewController.h"

@class PQLocationSearchDataSource;

@protocol PQLocationSearchDataSourceDelegate

- (void)locationDataSource:(PQLocationSearchDataSource *)dataSource didSelectNewLocation:(CLLocation *)location withPlaceName:(NSString *)placeName;
- (void)locationDataSource:(PQLocationSearchDataSource *)dataSource didSelectLocationType:(NewPostLocationType)locationType;

@end

@interface PQLocationSearchDataSource : NSObject <UISearchBarDelegate>

@property (nonatomic, weak) id<PQLocationSearchDataSourceDelegate> delegate;
@property (nonatomic, weak) UISearchBar *searchBar;
@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) NSArray <NSNumber *> *availableLocationTypes;
@property (nonatomic, assign) NewPostLocationType currentLocationType;

/*
 Initialization
 */
- (instancetype)initWithTableView:(UITableView *)tableView;
- (void)setupTableView:(UITableView *)tableView;

/*
 Table View methods
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;

/*
 Array Accessor
 */
- (id)objectAtIndexedSubscript:(NSUInteger)index; //getter
- (void)setObject:(id)obj atIndexedSubscript:(NSUInteger)index; //setter

@end
