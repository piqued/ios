//
//  PQMapAnnotation.h
//  Piqued
//
//  Created by Matt Mo on 5/20/15.
//  Copyright (c) 2015 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MapKit;

@interface PQMapAnnotation : NSObject <MKAnnotation>

// MKAnnotation Protocol
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

// Other properties
@property (nonatomic, readonly) NSNumber * postID;
@property (nonatomic, strong) NSString * imageURL;
@property (nonatomic, readonly) long numNeighbors;

- (instancetype) initWithCoordinates:(CLLocationCoordinate2D) coords postID:(NSNumber *)postID imageURL:(NSString *)imageURL numNeighbors:(long) numNeighbors;

@end
