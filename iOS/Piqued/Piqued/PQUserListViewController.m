//
//  PQUserListViewController.m
//  Piqued
//
//  Created by Matt Mo on 9/10/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQUserListViewController.h"
#import "PQUserCollectionViewCell.h"
#import "PQProfileViewController.h"

@interface PQUserListViewController()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PQUserCollectionViewCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *flowLayout;
@property (strong, nonatomic) NSArray<User *> *users;

@end

@implementation PQUserListViewController

+ (NSString *)segueIdentifier
{
    return @"OpenUserListSegueIdentifier";
}

- (void)setUser:(User *)user
{
    _user = user;
    
    BFTask *task = [[PQNetworking sharedInstance] fetchFollowTypeUsers:self.userListType ForUser:user];
    
    [task continueWithBlock:^id _Nullable(BFTask * _Nonnull task) {
        if (task.error) {
            
        } else {
            self.users = task.result;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionView reloadData];
            });
        }
        return nil;
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerNib:[PQUserCollectionViewCell nib] forCellWithReuseIdentifier:[PQUserCollectionViewCell reuseIdentifier]];
    
    [self setupNavigationBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupNavigationBar
{
    [self.navigationController setNavigationBarHidden:NO];
    UIBarButtonItem *dismissButton = [[UIBarButtonItem alloc] initWithTitle:@"back" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setLineBreakMode:NSLineBreakByTruncatingTail];
    [style setAlignment:NSTextAlignmentLeft];
    UIColor *color = [UIColor piq_colorFromHexString:kOrangeColor];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:17.0];
    
    NSDictionary *attributes = @{
                                 NSParagraphStyleAttributeName : style,
                                 NSForegroundColorAttributeName : color,
                                 NSFontAttributeName : font,
                                 };
    
    [dismissButton setTitleTextAttributes:attributes forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = dismissButton;
}

- (void)dismiss
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma ----------------------------------------------------------
#pragma mark - Collection View Delegate
#pragma ----------------------------------------------------------

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.users count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PQUserCollectionViewCell *userCell = [collectionView dequeueReusableCellWithReuseIdentifier:[PQUserCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
    userCell.user = self.users[indexPath.row];
    userCell.delegate = self;
    return userCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width, 70);
}

#pragma ----------------------------------------------------------
#pragma mark - PQUserCollectionViewCellDelegate
#pragma ----------------------------------------------------------

- (void)didPressFollowButton:(UIButton *)sender
{
    
}

- (void)openProfile:(double)userId
{
    [self performSegueWithIdentifier:[PQProfileViewController segueIdentifier] sender:@(userId)];
}

#pragma ----------------------------------------------------------
#pragma mark - Navigation
#pragma ----------------------------------------------------------

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:[PQProfileViewController segueIdentifier]]) {
        if ([segue.destinationViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *navVC = segue.destinationViewController;
            if ([navVC.topViewController isKindOfClass:[PQProfileViewController class]]) {
                PQProfileViewController *profileVC = (PQProfileViewController *)navVC.topViewController;
                if ([sender isKindOfClass:[NSNumber class]]) {
                    profileVC.userId = [sender doubleValue];
                }
            }
        }
    }
}

@end
