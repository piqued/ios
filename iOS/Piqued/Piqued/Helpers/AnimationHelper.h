//
//  AnimationHelper.h
//  Piqued
//
//  Created by Kenny M. Liou on 10/25/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnimationHelper : NSObject

+ (void) fadeOutAndHide:(UIView *) view duration:(float)sec;
+ (void) fadeInWithShift:(UIView *) view startX:(float)start endX:(float)end duration:(float)sec;
+ (void) moveUp:(UIView *) view endY:(float)end duration:(float)sec;
+ (void) constrainChange:(NSLayoutConstraint* )constrain withValue:(float)value withAnimationDuration:(float)sec withView:(UIView *)parentView;
@end
