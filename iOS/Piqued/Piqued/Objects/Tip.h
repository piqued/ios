//
//  Tip.h
//  Piqued
//
//  Created by Kenny M. Liou on 4/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tip : NSObject

@property (strong, atomic) NSString * firstName;
@property (strong, atomic) NSString * lastName;
@property (strong, atomic) NSString * imagePrefix;
@property (strong, atomic) NSString * imageSuffix;
@property (strong, atomic) NSString * tipText;


- (NSString *) getImageUrl;

@end
