//
//  PiquedUserDefault.h
//  Piqued
//
//  Created by Kenny M. Liou on 10/18/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PQUserDefaultsConstants.h"
#import "User.h"


@interface PQUserDefaults : NSObject

+ (NSString *) getStringWithKey:(NSString *) key;
+ (NSInteger) getIntegerWithKey:(NSString *) key;


+ (BOOL) isUserLoggedIn;
+ (BOOL) isUserFinishedWelcomeScreenMain;
+ (void) setUserFinishedWelcomeScreen:(BOOL) value;
+ (void) setKey:(NSString *) key withString:(NSString *) string;
+ (void) setKey:(NSString *) key withInteger:(NSInteger) integer;
+ (void) setKey:(NSString *)key withEncodedArrayObject:(NSData *) array;
+ (NSData *) getEncodedArrayObjectWithKey:(NSString *)key;
+ (void) setUser:(User *)user;
+ (User *) getUser;
+ (User *)updateUser:(User *)updatedUser;
+ (NSArray *) getArrayForKey:(NSString *)key;
+ (void) setArray:(NSArray *) array forKey:(NSString *)key;

// User Defaults getter setters
// APNS device token
+ (NSString *)getAPNSDeviceToken;
+ (void)setAPNSDeviceToken:(NSString *)deviceToken;

+ (void) setInstagramRequestSeen;
+ (BOOL) hasInstagramRequestBeenSeen;

+ (BOOL) bookmarkListChanged;
+ (void) setBookmarkListChanged:(BOOL)changed;
+ (void) removeObjectForKey:(nonnull NSString *)key;

@end
