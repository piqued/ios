//
//  PQLogoutTableViewCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 11/7/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQLogoutTableViewCell.h"
#import "PQUserDefaults.h"
#import "AppDelegate.h"
#import "UIColor+PIQHex.h"
#import "PQPostManager.h"
#import <InstagramKit/InstagramKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <SDWebImage/SDWebImageManager.h>

@interface PQLogoutTableViewCell()
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;

@end

@implementation PQLogoutTableViewCell

+ (CGFloat)estimatedHeight
{
    return 84;
}

+ (NSString *)reuseIdentifier
{
    return @"PQLogoutTableViewCell";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.logoutButton.layer setBorderColor:[UIColor piq_colorFromHexString:kOrangeColor].CGColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onLogout:(id)sender
{
    [[PQPostManager sharedInstance] clearAllData];
    [[SDImageCache sharedImageCache] clearMemory];
    [[SDImageCache sharedImageCache] clearDisk];
    [PQUserDefaults setUserFinishedWelcomeScreen:NO];
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];
    [UIApplication.sharedApplication unregisterForRemoteNotifications];
    [[InstagramEngine sharedEngine] logout];
    [PQUserDefaults setUser:NULL];
    [((AppDelegate *) [UIApplication sharedApplication].delegate) startApp];
}

@end
