//
//  User.h
//  Piqued
//
//  Created by Kenny M. Liou on 10/18/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface User : BaseModel

@property (nonatomic, readonly)         double userId;
@property (strong, nonatomic, readonly) NSString * secret;
@property (strong, nonatomic, readonly) NSString * token;
@property (strong, nonatomic, readonly) NSString * userName;
@property (strong, nonatomic, readonly) NSString * profileImage;
@property (strong, nonatomic, readonly) NSString * email;
@property (nonatomic, readonly)         int followerCount;
@property (nonatomic, readonly)         int followingCount;
@property (nonatomic, readonly)         int totalPostsCount;
@property (nonatomic, readonly)         BOOL isFollowed;

+ (User *) createUser:(NSString *)token withSecret:(NSString *)secret withId:(double)userId;

- (void) setUserName:(NSString *)userName;
- (void) setEmail:(NSString *)email;
- (void) setProfileImage:(NSString *)profileImage;
- (void) setFollowerCount:(int)count;
- (void) setFollowingCount:(int)count;

@end
