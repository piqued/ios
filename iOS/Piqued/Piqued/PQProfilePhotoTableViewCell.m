//
//  PQProfilePhotoTableViewCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 8/20/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQProfilePhotoTableViewCell.h"

@interface PQProfilePhotoTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@end

@implementation PQProfilePhotoTableViewCell

+ (int) estimatedHeight
{
    return 110;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectNewImage)];
    
    [tapGesture setNumberOfTapsRequired:1];
    
    [self addGestureRecognizer: tapGesture];
}

- (void) layoutSubviews
{
    [self.delegate getUserImage:^(UIImage *userImage) {
        [self.profileImage setImage:userImage];
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void) setDelegate:(id<PQEditProfileHeaderDelegate>)delegate
{
    _delegate = delegate;
    
}

#pragma mark UI methods

- (IBAction)onChangeImage:(id)sender {
    
    [self selectNewImage];
}


#pragma mark Helper Methods

- (void) selectNewImage
{
    [self.delegate selectImage];
}
@end
