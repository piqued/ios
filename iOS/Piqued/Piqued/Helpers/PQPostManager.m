//
//  PQPostManager.m
//  Piqued
//
//  Created by Matt Mo on 7/15/15.
//  Copyright (c) 2015 First Brew. All rights reserved.
//

#import "PQPostManager.h"
#import "PQUserDefaults.h"

static int const kPageSize = 250;
static int const kPostSearchRange = 200000; // default
static int const kPostSearchRangeDelta = 5000;

@interface PQPostManager()

@property (nonatomic, strong) NSMutableDictionary * postsDict;
@property (nonatomic, strong) NSMutableArray * bookmarkIDs;
@property (nonatomic, strong) NSMutableArray * activityIDs;
@property (nonatomic, strong) NSMutableArray * delegates;
@property (nonatomic, strong) NSMutableArray<NSNumber *> * postsNearBy;

@property (nonatomic, strong) CLLocation * currentLocation;
@property (nonatomic) int postSearchRange;
@property (nonatomic) int postSearchPage;

@property (nonatomic) int bookmarkPage;

@property (nonatomic) BOOL userPostsUpdated;

@end

@implementation PQPostManager

#pragma mark - Public Methods

+ (instancetype) sharedInstance {
    static PQPostManager * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        self.postSearchPage = 0;
        self.bookmarkPage = 0;
        self.postsDict = [NSMutableDictionary dictionary];
//        NSData * encodedList = [PQUserDefaults getEncodedArrayObjectWithKey:kPQUserDefaultsKeyOldPosts];
//        NSArray * oldPostList = [NSKeyedUnarchiver unarchiveObjectWithData:encodedList];
        
//        for (id obj in oldPostList) {
//            Post * post = (Post *) obj;
//            
//            [self addPost:post];
//        }
        
    }
    return self;
}

- (void) reloadAllData:(void (^)(bool success)) callback
{
    //TODO fix me, reload all data instead of calling callback right away
    [self updateActivitiesPosts:^(bool success, NSArray *posts) {
        if (success) {
            callback(YES);
        } else {
            callback(NO);
        }
    }];
    
    
}

- (void) saveCurrentData
{
    NSMutableArray * allPosts = [NSMutableArray array];
    
    for (id key in self.postsDict) {
        Post * post = [self.postsDict objectForKey:key];
        if (post) {
            [allPosts addObject:post];
        }
    }
    
    NSData * encodedList = [NSKeyedArchiver archivedDataWithRootObject:allPosts];
    
    [PQUserDefaults setKey:kPQUserDefaultsKeyOldPosts withEncodedArrayObject:encodedList];
}

- (NSMutableArray *) activityIDs
{
    if (!_activityIDs) {
        _activityIDs = [NSMutableArray array];
        
        NSArray * oldActivities = [PQUserDefaults getArrayForKey:kPQUserDefaultsKeyActivitiesArray];
        
        if (oldActivities) {
            [_activityIDs addObjectsFromArray:oldActivities];
        }
    }
    
    return _activityIDs;
}

- (NSMutableArray *) bookmarkIDs
{
    if (!_bookmarkIDs) {
        _bookmarkIDs = [NSMutableArray array];
        
    }
    
    return _bookmarkIDs;
}

- (NSMutableArray<NSNumber *> *) postsNearBy
{
    if (!_postsNearBy) {
        _postsNearBy = [NSMutableArray array];
        
        NSArray * oldNearByPosts = [PQUserDefaults getArrayForKey:kPQUserDefaultsKeyPostsNearBy];
        
        if (oldNearByPosts) {
            [_postsNearBy addObjectsFromArray:oldNearByPosts];
        }
    }
    
    return _postsNearBy;
}

- (NSMutableArray *) delegates
{
    if (!_delegates) {
        _delegates = [NSMutableArray array];
    }
    
    return _delegates;
}

- (void) addPost:(Post *) post {
    
    if (post.postId) {
        // NOTE: NSDictionary only contain one object per key. If pervide same key,
        // old reference will be removed.
        Post * oldPost = [self.postsDict objectForKey:post.postId];
        if (!oldPost) {
            [self.postsDict setObject:post forKey:post.postId];
        } else {
            [oldPost updateWithPost:post];
        }
    }
}

- (void) addPosts:(NSArray *) posts {
    for (Post * post in posts) {
        if ([post isKindOfClass:[Post class]]) {
            if (post.postId) {
                [self.postsDict setObject:post forKey:post.postId];
            }
        }
    }
}

- (void) getPostAsync:(NSNumber *) postID callback:(void (^)(Post * post)) callback
{
    [self getPostAsync:postID andRefresh:NO callback:callback];
}

- (void) getPostAsync:(NSNumber *) postID andRefresh:(BOOL)refresh callback:(void (^)(Post * post)) callback
{
    if (postID != nil) {
        
        Post *post = nil;
        
        if (!refresh) {
            post = [self getPost:postID];
        }
        
        if (post) {
            callback(post);
        } else {
            [PQAPIs getPostByID:postID withCallback:^(Post *post) {
                
                if (post) {
                    [self.postsDict setObject:post forKey:postID];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    callback(post);
                });
                
            }];
        }
        
    } else {
        if ([NSThread isMainThread]) {
            callback(NULL);
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                callback(NULL);
            });
        }
    }
}

- (Post *) getPost:(NSNumber *) postID {
    
    Post * post = nil;
    if (postID != nil) {
        post = [self.postsDict objectForKey:postID];
    }
    
    return post;
}

- (NSArray *) getPosts:(NSArray *) postIDs {
    NSMutableArray * posts = [NSMutableArray array];
    for (NSNumber * postID in postIDs) {
        if ([postIDs isKindOfClass:[NSNumber class]]) {
            [posts addObject:[self.postsDict objectForKey:postID]];
        }
    }
    return posts;
}

- (NSArray *) getActivityIds
{
    return self.activityIDs;
}

- (NSArray *) getBookmarkIds
{
    return self.bookmarkIDs;
}

- (void) updateActivitiesWithIds:(NSArray *) newActivityIds
{
    [self.activityIDs removeAllObjects];
    
    [self.activityIDs addObjectsFromArray:newActivityIds];
    
    [PQUserDefaults setArray:self.activityIDs forKey:kPQUserDefaultsKeyActivitiesArray];
}

- (void) updateActivitiesPosts:(void (^)(bool success, NSArray * posts)) callback {
    
    [self updateActivitiesWithPage:0 andSize:kPageSize callback:callback];
}

- (void) updateActivitiesWithPage:(int)page andSize:(int)size callback:(void (^)(bool success, NSArray * posts)) callback
{
    [PQAPIs fetchCurrentUserPostHistoryWithPage:page andSize:size andCallback:^(BOOL result, long count, NSArray *posts) {
        
        if (posts) {
            NSMutableArray * newActivityArray = [NSMutableArray array];
            
            for (id obj in posts) {
                Post * post = (Post *) obj;
                
                [newActivityArray addObject:post.postId];
                
                [self addPost:post];
            }
            
            [self updateActivitiesWithIds:newActivityArray];
        }
        
        if ([self.activityIDs count] < count) {
            [self updateActivitiesWithPage:page+1 andSize:size callback:callback];
        } else {
            if (callback) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    callback(true, self.activityIDs);
                });
            }
        }
    }];
}

- (void) refreshBookmarks:(void (^)(bool success)) callback
{
    [PQAPIs retrieveBookmarkedPosts:@{
                                      @"page": @(1),
                                      @"size": @(25),
                                      }
                           callback:^(NSArray *bookmarkedPosts, NSError *error) {
       if (error) {
           PQLog(@"Failed to retrive bookmarks: %@", error);
           
           callback(false);
       } else if (bookmarkedPosts) {
           
           NSMutableArray * newBookmarkArray = [NSMutableArray array];
           
           for (id obj in bookmarkedPosts) {
               Post * post = (Post *) obj;
               [newBookmarkArray addObject:post.postId];
               
               [self addPost:post];
           }
           
           [self refreshBookmarksIds:newBookmarkArray];
           
           callback(true);
       } else {
           PQLog(@"received null array for bookmarked posts. Please verify PQAPI is definded correctly");
           
           callback(false);
       }
                           }];
}

- (void) updateBookmarks:(void (^)(BOOL success, NSError *error)) callback
{
    
    [PQAPIs retrieveBookmarkedPosts:@{
                                      @"page": @(self.bookmarkPage+1),
                                      @"size": @(25),
                                      }
                           callback:^(NSArray *bookmarkedPosts, NSError *error) {
        if (error) {
            PQLog(@"Failed to retrieve bookmarks: %@", error);
            
            callback(false, error);
        } else if (bookmarkedPosts) {
            
            NSMutableArray * newBookmarkArray = [NSMutableArray array];
            
            for (id obj in bookmarkedPosts) {
                Post * post = (Post *) obj;
                
                [newBookmarkArray addObject:post.postId];
                
                [self addPost:post];
            }
            
            [self updateBookmarksWithIds:newBookmarkArray];
            self.bookmarkPage = self.bookmarkPage + 1;
            callback(true, nil);
        } else {
            PQLog(@"received null array for bookmarked posts. Please verify PQAPI is definded correctly");
            
            callback(false, nil);
        }
    }];
}

- (void) refreshBookmarksIds:(NSArray *) newBookmarkIds
{
    NSMutableSet *setToAdd = [NSMutableSet setWithArray:newBookmarkIds];
    NSMutableSet *oldSet = [NSMutableSet setWithArray:self.bookmarkIDs];
    [setToAdd minusSet:oldSet];
    
    if (setToAdd.count > 0) {
        NSMutableArray *newBookmarkIds = [NSMutableArray arrayWithArray:[setToAdd allObjects]];
        [newBookmarkIds addObjectsFromArray:self.bookmarkIDs];
        self.bookmarkIDs = newBookmarkIds;
    }
}

- (void) updateBookmarksWithIds:(NSArray *) newBookmarkIds
{
    NSMutableSet *setToAdd = [NSMutableSet setWithArray:newBookmarkIds];
    NSMutableSet *oldSet = [NSMutableSet setWithArray:self.bookmarkIDs];
    [setToAdd minusSet:oldSet];
    
    [self.bookmarkIDs addObjectsFromArray:[setToAdd allObjects]];
    
    [PQUserDefaults setArray:self.bookmarkIDs forKey:kPQUserDefaultsKeyBookmarkArray];
}

- (void) setPostBookmarked:(Post *)post isBookmarked:(BOOL)isBookmarked
{
    NSNumber * postId = post.postId;
    
    Post * oldPost;
    
    if ([self.bookmarkIDs containsObject:postId]) {
        oldPost = [self.postsDict objectForKey:postId];
    }
    
    if (isBookmarked) {
        if (!oldPost) {
            // if we are bookmarking a post, and post is not already bookmarked, add it to list
            [PQAPIs apiCallToBookmarkPost:isBookmarked withPostID:post.postId withCallback:^(BOOL successfulCall, BOOL isBookmarked) {
                if (successfulCall) {
                    PQLog(@"success!");
                    [self.bookmarkIDs addObject:postId];
                    [self notifyBookmarkChanged];
                    
                } else {
                    PQLog(@"apiCallToBookmark call failed");
                }
            }];
        }
    } else {
        if (oldPost) {
            // if we are unbookmarking a post, and the post exists in the list, remove it
            [PQAPIs apiCallToBookmarkPost:isBookmarked withPostID:postId withCallback:^(BOOL successfulCall, BOOL isBookmarked) {
                if (successfulCall) {
                    PQLog(@"success!");
                    [self.bookmarkIDs removeObject:postId];
                    [self notifyBookmarkChanged];
                } else {
                    PQLog(@"apiCallToBookmark call failed");
                }
            }];
        }
    }
}

- (void) setPostLiked:(Post *) post isLiked:(BOOL) isLiked
{
    
    [PQAPIs setPost:post liked:isLiked withCallback:^(BOOL success, Post *updatedPost) {
        if (success) {
            [post mergeValueForKey:@"likes" fromModel:updatedPost];
            [post mergeValueForKey:@"myReaction" fromModel:updatedPost];
        }
    }];
}

- (BOOL) isPostLiked:(Post *) post
{
    return post.myReaction.intValue > 0;
}

- (void) notifyBookmarkChanged
{
    for (id<PQPostManagerDelegate> delegate in self.delegates) {
        if ([delegate respondsToSelector:@selector(bookmarkUpdated)]) {
            [delegate bookmarkUpdated];
        }
    }
}

- (void) addPostMangerDelegate:(id<PQPostManagerDelegate>) delegate
{
    if (![self.delegates containsObject:delegate]) {
        [self.delegates addObject:delegate];
        
        //TODO: this is a hack to make sure home screen got updated correctly. Fix this!
        if ([delegate respondsToSelector:@selector(bookmarkUpdated)]) {
            [delegate bookmarkUpdated];
        }
    }
}

- (void) removePostManagerDelegate:(id<PQPostManagerDelegate>) delegate
{
    if ([self.delegates containsObject:delegate]) {
        [self.delegates removeObject:delegate];
    }
}

- (void) deletePost:(Post *)post withCallback:(void (^)(BOOL success))callback
{
    [PQAPIs deletePost:post withCallback:^(BOOL success) {
        if (success) {
            [self.postsDict removeObjectForKey:post.postId];
            [self.postsNearBy removeObject:post.postId];
            
            [self setUserPostsUpdated:YES];
        }
        
        callback(success);
    }];
}

- (BOOL) myPostsAreUpdated
{
    return self.userPostsUpdated;
}

+ (void) fetchCurrentUserPostHistoryWithPage:(int) page andSize:(int)size andCallback:(void (^)(BOOL result, long count, NSArray * posts))callbackBlock
{
    [[PQPostManager sharedInstance] setUserPostsUpdated:NO];
    [PQAPIs fetchCurrentUserPostHistoryWithPage:page andSize:size andCallback:callbackBlock];
}

+ (void) fetchUserPostHistory:(double)userId withPage:(int) page andSize:(int)size andCallback:(void (^)(BOOL result, long count, NSArray * posts))callbackBlock
{
    [[PQPostManager sharedInstance] setUserPostsUpdated:NO];
    [PQAPIs fetchUserPostHistory:userId withPage:page andSize:size andCallback:callbackBlock];
}

#pragma mark -
#pragma mark Post fetch
+ (void) getPostsBasedOnCurrentLocation:(CLLocation *)location isRefresh:(BOOL)isRefresh andGetMore:(BOOL)getMore withCallback:(void (^)(BOOL success, NSArray<NSNumber *> * postIDs)) callbackBlock;
{
    PQPostManager *manager = [PQPostManager sharedInstance];
    
    if (isRefresh) {
        [manager setPostSearchPage:1];
    } else {
        [manager setPostSearchPage:manager.postSearchPage + 1];
    }
    
    CLLocationCoordinate2D coords = location ? location.coordinate : CLLocationCoordinate2DMake(0, 0);
    MKCoordinateRegion viewRegion = MKCoordinateRegionMake(coords, MKCoordinateSpanMake(90, 180));
    MKCoordinateRegion innerRegion = MKCoordinateRegionMakeWithDistance(coords, 0, 0);
    [[PQPostManager sharedInstance] fetchPostsWithViewRegion:viewRegion withInnerRegion:innerRegion withPage:manager.postSearchPage isRefresh:isRefresh withCallback:callbackBlock];
//    [[PQPostManager sharedInstance] fetchPosts:location isRefresh:isRefresh andGetMore:getMore withCallback:callbackBlock];
}

- (BOOL) locationChanged:(CLLocation *) location
{
    if (self.currentLocation && location) {
        if (self.currentLocation.coordinate.latitude == location.coordinate.latitude &&
            self.currentLocation.coordinate.longitude == location.coordinate.longitude) {
            return NO;
        }
    }
    
    // if current location is not the same as new location, or current location is not initialized
    return YES;
}

- (void) fetchPosts:(CLLocation *) location isRefresh:(BOOL)isRefresh andGetMore:(BOOL)getMore withCallback:(void (^)(BOOL success, NSArray<NSNumber *> * postIDs)) callback
{
    if ([self locationChanged:location] && !isRefresh) {
        // location changed, fetch it like it's new (no refresh, no get more)
        [self fetchPostsForNewLocation:location withCallback:callback];
    } else {
        // location haven't change
        [self fetchPostsWithCurrentLocation:location isRefresh:isRefresh isGetMore:getMore withCallback:callback];
    }
}

- (void) fetchPostsForNewLocation:(CLLocation *) location withCallback:(void (^)(BOOL success, NSArray<NSNumber *> * postIDs)) callback
{
    // new location, set it as current location
    [self setCurrentLocation:location];
    [self setPostSearchRange:kPostSearchRange]; // reset to default value
    [self setPostSearchPage:1]; // start with page 1
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, self.postSearchRange, self.postSearchRange);
    MKCoordinateRegion innerRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 0, 0);
    
    [self fetchPostsWithViewRegion:viewRegion
                   withInnerRegion:innerRegion
                          withPage:self.postSearchPage
                         isRefresh:NO
                      withCallback:callback];
}

- (void) fetchPostsWithCurrentLocation:(CLLocation *)location
                             isRefresh:(BOOL)isRefresh
                             isGetMore:(BOOL)getMore
                          withCallback:(void (^)(BOOL success, NSArray<NSNumber *> * postIDs)) callback
{
    if (!isRefresh && !getMore) {
        // simply return whatever we have if it is not a refresh nor get more
        if ([self.postsNearBy count] > 0) {
            callback(YES, self.postsNearBy);
            return;
        }
    }
    
    [self setPostSearchRange:self.postSearchRange + kPostSearchRangeDelta];
    
    if (isRefresh) {
        [self setPostSearchPage:1];
    } else {
        [self setPostSearchPage:self.postSearchPage + 1];
    }
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, self.postSearchRange, self.postSearchRange);
    MKCoordinateRegion innerRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 0, 0);
    
    [self fetchPostsWithViewRegion:viewRegion
                   withInnerRegion:innerRegion
                          withPage:self.postSearchPage
                         isRefresh:isRefresh
                      withCallback:callback];
}

- (void) fetchPostsWithViewRegion:(MKCoordinateRegion)viewRegion
                  withInnerRegion:(MKCoordinateRegion)innerRegion
                         withPage:(int) page
                        isRefresh:(BOOL) isRefresh
                     withCallback:(void (^)(BOOL success, NSArray<NSNumber *> * postIDs)) callback
{
    if (page == 1 && !isRefresh) {
        // we are fetching page 1, and this is not a refresh. It's either a new fetch or a change of location, thus, remove all objects in posts near by
        [self.postsNearBy removeAllObjects];
    }
    
    NSDictionary * options = @{
                               @"page" : [NSNumber numberWithInt:page],
                               @"size" : @5
                               };
    
    [PQAPIs retrievePostsForOuterRegion:viewRegion notInnerRegion:innerRegion withOptions:options withCallback:^(id postInfo, NSError *error) {
        
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                callback(NO, NULL); // something went wrong
            });
        } else {
            if (postInfo) {
                
                NSMutableArray * postIDs = [NSMutableArray array];
                NSMutableArray * posts = [NSMutableArray array];
                
                
                for (NSNumber * currentPostID in self.postsNearBy) {
                    Post * post = [self getPost:currentPostID];
                    if (post != nil) [posts addObject:post];
                }
                
                // loop through new data
                NSArray * locationPosts = [postInfo objectForKey:@"posts"];
                
                for (NSDictionary * newPostJson in locationPosts) {
                    Post * newPost = [Post modelWithDictionary:newPostJson];
                    [self addPost:newPost];
                    
                    if (![posts containsObject:newPost]) {
                        [posts addObject:newPost];
                    }
                }
                
                NSArray * sortedPosts = [posts sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                    Post * post1 = (Post *) obj1;
                    Post * post2 = (Post *) obj2;
                    
                    if (post1.createdAtTimeStamp > post2.createdAtTimeStamp) {
                        return  (NSComparisonResult)NSOrderedAscending;
                    } else {
                        return  (NSComparisonResult)NSOrderedDescending;
                    }
                }];
                
                for (Post * post in sortedPosts) {
                    [postIDs addObject:post.postId];
                }
                
                [self setPostsNearBy:postIDs];
                
            }
            
            [PQUserDefaults setArray:self.postsNearBy forKey:kPQUserDefaultsKeyPostsNearBy];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                callback(YES, self.postsNearBy);
            });

        }
    }];
}

#pragma mark - Clear All Data
- (void) clearAllData
{
    [PQUserDefaults removeObjectForKey:kPQUserDefaultsKeyOldPosts];
    [PQUserDefaults removeObjectForKey:kPQUserDefaultsKeyBookmarkArray];
    [PQUserDefaults removeObjectForKey:kPQUserDefaultsKeyActivitiesArray];
    [PQUserDefaults removeObjectForKey:kPQUserDefaultsKeyPostsNearBy];    
}


@end
