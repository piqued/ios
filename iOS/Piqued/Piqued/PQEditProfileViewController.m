//
//  PQEditProfileViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 8/20/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQEditProfileViewController.h"
#import "PQEditPasswordViewController.h"
#import "PQProfilePhotoTableViewCell.h"
#import "PQEditProfileTableViewCell.h"
#import "PQEditProfileLoadingView.h"
#import "PQEditProfileHeaderTableViewCell.h"
#import "PQEditProfileHeader.h"
#import "PQLogoutTableViewCell.h"
#import "PQUserDefaults.h"
#import "PopupMessage.h"
#import "PQUtilities.h"
#import "PQAPIs.h"

#import <SDWebImage/SDWebImageManager.h>



@interface PQEditProfileViewController () <UITableViewDelegate, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, PQEditProfileHeaderDelegate, PQEditPasswordViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) PQEditProfileLoadingView * overlay;

@property (strong, nonatomic) User *        user;
@property (strong, nonatomic) NSString *    modifiedName;
@property (strong, nonatomic) NSString *    modifiedEmail;
@property (strong, nonatomic) UIImage *     modifiedImage;

@property (nonatomic) BOOL                  showDone;

@end

@implementation PQEditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTintColor:[UIColor piq_colorFromHexString:kOrangeColor]];

    [self.navigationItem.leftBarButtonItem setTarget:self];
    [self.navigationItem.leftBarButtonItem setAction:@selector(backButtonPressed:)];
    
    // setup save button
    
    [self.navigationItem.rightBarButtonItem setTarget:self];
    [self.navigationItem.rightBarButtonItem setAction:@selector(saveProfile:)];
    
    // Hack to get over with back
    self.navigationItem.titleView = [[UIView alloc] init];
    
    // setup table view
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PQProfilePhotoTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PQProfilePhotoTableViewCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PQEditProfileTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PQEditProfileTableViewCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PQEditProfileHeaderTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PQEditProfileHeaderTableViewCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PQLogoutTableViewCell class]) bundle:nil] forCellReuseIdentifier:[PQLogoutTableViewCell reuseIdentifier]];
    
    
    [self setUser:[PQUserDefaults getUser]];
}

- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [self.overlay removeFromSuperview];
    [self setOverlay:NULL]; // make sure we clear this view
}

- (PQEditProfileLoadingView *) overlay
{
    if (!_overlay) {
        _overlay = [self createOverlay];
    }
    
    return _overlay;
}

- (PQEditProfileLoadingView *) createOverlay
{
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PQEditProfileLoadingView class])  owner:self options:nil];
    
    PQEditProfileLoadingView * overlay = (PQEditProfileLoadingView *)[subviewArray objectAtIndex:0];
    
    [overlay setFrame:self.view.bounds];
    
    [overlay setHidden:YES];
    
    [[UIApplication sharedApplication].keyWindow addSubview:overlay];
    
    return overlay;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([@"editPassword" isEqualToString:segue.identifier]) {
        PQEditPasswordViewController * controller = (PQEditPasswordViewController *)segue.destinationViewController;
        
        [controller setDelegate:self];
    }
}

#pragma mark UITableView delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 2;
        case 1:
            return 5;
        default:
            return 0;
    }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell;
    
    switch (indexPath.section) {
        case 0:
            cell = [self getFirstSectionCell:indexPath.row];
            break;
        case 1:
            cell = [self getSecondSectionCell:indexPath.row];
            break;
    }
    
    
    return cell;
}

- (UITableViewCell *) getFirstSectionCell:(NSInteger) row
{
    
    if (row == 0) {
        PQProfilePhotoTableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PQProfilePhotoTableViewCell class])];
        
        [cell setDelegate:self];
        
        return cell;
    } else {
        PQEditProfileTableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PQEditProfileTableViewCell class])];
        
        [cell setDelegate:self];
        
        switch (row) {
            case 1:
                [cell setCellType:EditProfile_Name];
                break;
        }
        
        return cell;
    }
}

- (UITableViewCell *) getSecondSectionCell:(NSInteger) row
{
    if (row == 0) {
        PQEditProfileHeaderTableViewCell * header = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PQEditProfileHeaderTableViewCell class])];
        
        return header;
    } else {
        UITableViewCell * cell = nil;
        
        PQEditProfileTableViewCell * editCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PQEditProfileTableViewCell class])];
        [editCell setDelegate:self];
        
        switch (row) {
            case 1:
                [editCell setCellType:EditProfile_Email];
                break;
            case 2:
                [editCell setCellType:EditProfile_Password];
                break;
            case 3:
                [editCell setCellType:EditProfile_DefaultMap];
                break;
            case 4:
                cell = [self.tableView dequeueReusableCellWithIdentifier:[PQLogoutTableViewCell reuseIdentifier]];
        }
        if (cell == nil) {
            cell = editCell;
        }
        
        return cell;

    }
}

- (CGFloat) getFirstSectionHeights:(NSInteger) row
{
    switch (row) {
        case 0:
            return [PQProfilePhotoTableViewCell estimatedHeight];
        default:
            return [PQEditProfileTableViewCell estimatedHeight];
    }
}

- (CGFloat) getSecondSectionHeights:(NSInteger) row
{
    switch (row) {
        case 0:
            return [PQEditProfileHeaderTableViewCell estimatedHeight];
        case 4:
            return [PQLogoutTableViewCell estimatedHeight];
        default:
            return [PQEditProfileTableViewCell estimatedHeight];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            return [self getFirstSectionHeights:indexPath.row];
        case 1:
            return [self getSecondSectionHeights:indexPath.row];
        default:
            return 0;
    }
}

#pragma mark PQEditProfileHeader delegate

- (User *) getUser
{
    return self.user;
}

- (void) checkHasModification
{
    [self.navigationItem.rightBarButtonItem setEnabled:(self.showDone || (self.modifiedName != nil && self.modifiedName.length > 4) || self.modifiedEmail != nil || self.modifiedImage)];
}

- (void) setNewName:(NSString *) newName
{
    if ([self.user.userName isEqualToString:newName]) {
        
        [self setModifiedName:nil];
    } else {
        [self setModifiedName:newName];
    }
    
    [self checkHasModification];
}

- (void) setNewEmail:(NSString *) newEmail
{
    if ([self.user.email isEqualToString:newEmail]) {
        
        [self setModifiedEmail:nil];
    } else {
        
        [self setModifiedEmail:newEmail];
    }
    
    [self checkHasModification];
}

- (void) getUserImage:(void (^)(UIImage * userImage))callbackBlock
{
    if (self.modifiedImage) {
        callbackBlock(self.modifiedImage);
    } else {
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        
        NSURL * imageURL = [NSURL URLWithString:self.user.profileImage];
        
        [manager downloadImageWithURL:imageURL options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            if (finished) {
                
                if (image) {
                    callbackBlock(image);
                } else {
                    callbackBlock([UIImage imageNamed:@"blankProfile"]);
                }
                
            }
        }];
    }
}

- (void) selectImage
{
    UIImagePickerController *pickerLibrary = [[UIImagePickerController alloc] init];
    pickerLibrary.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [pickerLibrary setDelegate:self];
    
    [self.navigationController presentViewController:pickerLibrary animated:YES completion:nil];
}

#pragma mark UIImagePickerControllerDelegate method
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    UIImage *compressedImage = scaleAndRotateImage(image, 480);
    [self setModifiedImage:compressedImage];
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    
    [self checkHasModification];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark PQEditPasswordViewControllerDelegate method

- (void) showDoneButton
{
    [self setShowDone:YES];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    [self.navigationItem.rightBarButtonItem setTitle:@"done"];
}

#pragma mark helper method

- (void)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) saveProfile:(UIButton *) button
{
    [self.view endEditing:YES];
    
    [self.overlay setHidden:NO];
    
    NSMutableDictionary *profileContents = [NSMutableDictionary dictionary];
    
    if (self.modifiedEmail.length != 0) {
        profileContents[@"email"] = self.modifiedEmail;
    }
    if (self.modifiedName.length != 0) {
        profileContents[@"name"] = self.modifiedName;
    }
    if (self.modifiedImage != nil) {
        profileContents[@"image"] = self.modifiedImage;
    }

    [[PQNetworking.sharedInstance editProfile:profileContents] continueWithBlock:^id _Nullable(BFTask * _Nonnull t) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.overlay setHidden:YES];
            
            if (t.error != nil) {
                [PopupMessage displayTitle:@"Account Update" withMessage:@"Sorry, there seems to be an issue. Please try again." withOkAction:^(UIAlertAction *action) {
                    
                    [self goBack];
                    
                } controller:self];
            } else {
                [PopupMessage displayTitle:@"Account Update" withMessage:@"Your information is updated Successfully!" withOkAction:^(UIAlertAction *action) {
                    
                    [self goBack];
                    
                } controller:self];
            }
        });
        return nil;
    }];
}

- (void) showEditPassword
{
    [self performSegueWithIdentifier:@"editPassword" sender:self];
}

- (void) showEditMap
{
    [self performSegueWithIdentifier:@"editDefaultMap" sender:self];
}

- (void) goBack
{
    [self.view endEditing:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
