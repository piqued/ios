//
//  PQCommentViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 6/29/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQCommentViewController.h"
#import "PQBasketBaseCell.h"
#import "PQPostCommentCell.h"
#import "PQCommentInputView.h"

// Data source
#import "PQAPIs.h"

// Navigation
#import "PQProfileViewController.h"

// Helpers
#import "PQScreenHelper.h"
#import "PopupMessage.h"
#import "UIColor+PIQHex.h"


static CGFloat kAnimationDuration = 0.5;
static CGFloat kShadeAnimationDuration = 0.2;

@interface PQCommentViewController () <UITableViewDelegate, UITableViewDataSource, PQBasketCellDelegate, UITextViewDelegate, PQCommentInputDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *mockInputContainer;

@property (nonatomic, strong) PQCommentInputView * overlay;
@property (strong, nonatomic) UIRefreshControl * refreshControl;

@end

@implementation PQCommentViewController

#pragma mark Life Cycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [self registerClassWithTable:NSStringFromClass([PQPostCommentCell class])];
    
    UITapGestureRecognizer * tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showOverlay)];
    [tapGes setNumberOfTapsRequired:1];
    
    [self.mockInputContainer addGestureRecognizer:tapGes];
    
    [self setRefreshControl:[[UIRefreshControl alloc] init]];
    [self.refreshControl addTarget:self action:@selector(loadComments) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    [self.tableView setRowHeight:UITableViewAutomaticDimension];
    [self.tableView setEstimatedRowHeight:82];
    
    [self loadComments];
    
}

- (void) viewDidDisappear:(BOOL)animated
{
    [self.overlay removeFromSuperview];
    [self setOverlay:NULL]; // make sure we clear this view
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    [self.navigationController.navigationBar setTintColor:[UIColor piq_colorFromHexString:kOrangeColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardUpdate:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardUpdate:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardUpdate:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void) viewWillDisappear:(BOOL)animated
{
    
    [self.navigationController setNavigationBarHidden:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [self.view endEditing:YES];
}

- (void) viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    [self.overlay setFrame:self.view.bounds];
}
#pragma mark Lazy load

- (UIView *) overlay
{
    if (_overlay == NULL) {
        
        _overlay = [self createOverlay];
    }
    
    return _overlay;
}

- (PQCommentInputView *) createOverlay
{
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([PQCommentInputView class])  owner:self options:nil];
    
    PQCommentInputView * overlay = (PQCommentInputView *)[subviewArray objectAtIndex:0];
    [overlay setDelegate:self];
    [overlay setHidden:YES];
    
    [[UIApplication sharedApplication].keyWindow addSubview:overlay];
    
    return overlay;
}

#pragma mark PQCommentInputDelegate methods

- (void) onOverlayTapped
{
    [self hideOverlay];
}

- (void) hideOverlay
{
    [self.overlay setHidden:YES];
    [self.overlay viewIsVisiable:NO];
}

- (void) addComment:(NSString *) comment
{
    [PQAPIs addComment:comment toPost:self.post withCallback:^(BOOL success) {
        
        [self.overlay resetView];
        [self hideOverlay];
        
        NSString * message = @"Your comment was sent!";
        if (!success) {
            message = @"Oh no! There seems to be a problem! Please try again";
        }
        
        [PopupMessage displayTitle:@"Commenting" withMessage:message withOkAction:^(UIAlertAction *action) {
                [self loadComments];
        } controller:self];
    }];
}

#pragma mark Helper Methods

- (void) loadComments
{
    [PQAPIs getComments:self.post withCallback:^(Post *post, NSArray *comments) {
        
        self.comments = comments;
        [self.refreshControl endRefreshing];
        [self.tableView reloadData];
    }];
}

- (void) registerClassWithTable:(NSString *) nibName
{
    [self.tableView registerNib:[UINib nibWithNibName:nibName bundle:nil] forCellReuseIdentifier:nibName];
}

- (void) keyboardUpdate:(NSNotification *)notification
{
    CGFloat startY = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].origin.y;
    CGFloat endY = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin.y;
    CGFloat screenHeight = [PQScreenHelper getScreenHeight];
    
    CGFloat animationDuration = kAnimationDuration;
    
    if (screenHeight == endY) {
        // we are hidding keyboard
        [self.overlay setCurrentKeyboardHeight:0];
    } else {
        if (startY == screenHeight) {
            // we are showing keyboard
        } else {
            // keyboard hight changed (i.e. change input language)
            animationDuration = 0.0;
        }
        // new keyboard Y
        [self.overlay setCurrentKeyboardHeight:screenHeight - endY];
    }
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark -
#pragma mark PQBasketCell Delegate Methods

- (Post *) getPost
{
    return self.post;
}

- (void) openProfile:(double)userId
{
    PQLog(@"Profile opened");
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"TabProfile" bundle:nil];
    PQProfileViewController *profileVC = [storyBoard instantiateViewControllerWithIdentifier:[PQProfileViewController storyboardIdentifier]];
    profileVC.userId = userId;
    
    UINavigationController *navigationVC = [[UINavigationController alloc] initWithRootViewController:profileVC];
    [self presentViewController:navigationVC animated:YES completion:nil];
}

- (void) showComment
{
    // we borrow PQBasketCell, so this method does nothing
}

- (void) presentViewController:(UIViewController *)controller
{
    // we borrow PQBasketCell, so this method does nothing
}

#pragma mark -

- (Comment *) getComment:(int)index
{
    Comment *comment = nil;
    if (index >= 0 && index < self.comments.count) {
        comment = [self.comments objectAtIndex:index];
    }
    return comment;
}

- (void) showOverlay
{
    [self.overlay setAlpha:0];
    [self.overlay setHidden:NO];
    
    [UIView animateWithDuration:kShadeAnimationDuration animations:^{
        [self.overlay setAlpha:1];
    } completion:^(BOOL finished) {
        if (finished) {
            [self.overlay viewIsVisiable:YES];
        }
    }];
}

#pragma mark UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1 + [self.comments count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PQPostCommentCell * cell = (PQPostCommentCell *) [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PQPostCommentCell class])];
    
    if (indexPath.row == 0) {
        [cell setType:CommentCell];
        [cell setSourceType:SourcePost];
        
    } else {
        [cell setType:CommentCell];
        [cell setSourceType:SourceComment];
        [cell setCommentIndex:(int) indexPath.row - 1]; // first one is post
    }
    
    [cell setBasketDelegate:self];
    
    [cell setNumberOfLines:0];
    [cell reloadData];
    
    return cell;
}

@end
