//
//  PQEnvironment.m
//  Piqued
//
//  Created by Matt Mo on 12/10/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQEnvironment.h"
#import "SettingsBundle.h"
#import "PQUserDefaults.h"

@implementation PQEnvironment

+ (NSString *)serverBaseURLString
{
    NSString *serverURL = @"";
#if CONFIG_DEBUG
    serverURL = @"http://localhost:3001";
#elif CONFIG_APPSTORE
    serverURL = @"https://api.getpiqued.com";
#elif CONFIG_RELEASE
    serverURL = @"https://staging1.piqued.io";
#endif
    
    // If we have set the serverURL via the Host in Settings
    serverURL = [SettingsBundle hostString] ?: serverURL;
    
    return serverURL;
}

+ (NSURL *)serverBaseURL
{
    return [NSURL URLWithString:PQEnvironment.serverBaseURLString];
}

+ (PQBuildConfiguration)buildConfiguration
{   
#if CONFIG_DEBUG
    return PQBuildConfigurationDebug;
#elif CONFIG_APPSTORE
    return PQBuildConfigurationAppStore;
#elif CONFIG_RELEASE
    return PQBuildConfigurationRelease;
#endif
    return PQBuildConfigurationUnknown;
}

+ (NSString *)stringForBuildConfiguration:(PQBuildConfiguration)buildConfiguration
{
    switch (buildConfiguration) {
        case PQBuildConfigurationDebug:
            return @"io.piqued.debug";
        case PQBuildConfigurationRelease:
            return @"io.piqued.staging";
        case PQBuildConfigurationAppStore:
            return @"io.piqued.production";
        default:
            return @"unknown";
    }
}

+ (BOOL)debugAnalytics
{
    #if CONFIG_DEBUG
        return YES;
    #else
        return NO;
    #endif
}

+ (NSString *)appGroupName
{
    NSString *appGroupName = @"group.io.piqued";
#if CONFIG_DEBUG
    appGroupName = @"group.io.piqued-Debug";
#elif CONFIG_APPSTORE
    appGroupName = @"group.io.piqued";
#elif CONFIG_RELEASE
    appGroupName = @"group.io.piqued-Release";
#endif
    return appGroupName;
}

+ (NSString *)description
{
    NSMutableString *debugInfo = [NSMutableString string];
    NSBundle* mainBundle;
    
    // The Info.plist is considered the mainBundle.
    mainBundle = [NSBundle mainBundle];
    
    // Reads the value of the custom key I added to the Info.plist
    NSString *facebookURLSuffix = [mainBundle objectForInfoDictionaryKey:@"FacebookUrlSchemeSuffix"];
    
    [debugInfo appendFormat:@"serverBaseURLString: %@\n", PQEnvironment.serverBaseURLString];
    [debugInfo appendFormat:@"appGroupName: %@\n", [PQEnvironment appGroupName]];
    [debugInfo appendFormat:@"facebook: %@\n", facebookURLSuffix];
    [debugInfo appendFormat:@"deviceToken: %@\n", [PQUserDefaults getAPNSDeviceToken]];
    
    return debugInfo;
}

@end
