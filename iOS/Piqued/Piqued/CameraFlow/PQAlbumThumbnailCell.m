//
//  PQAlbumThumbnailCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 11/9/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQAlbumThumbnailCell.h"

@interface PQAlbumThumbnailCell()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;


@end

@implementation PQAlbumThumbnailCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void) setImage:(UIImage *)thumbnailImage
{
    [self.imageView setImage:thumbnailImage];
}

@end
