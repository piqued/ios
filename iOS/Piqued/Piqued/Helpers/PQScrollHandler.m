//
//  PQScrollHandler.m
//  Piqued
//
//  Created by Kenny M. Liou on 5/21/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQScrollHandler.h"

@interface PQScrollHandler()

@property (nonatomic) CGFloat lastContentOffset;
@end

@implementation PQScrollHandler

- (void) onViewScrolled:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y > 0) {
        if (scrollView.contentOffset.y - self.lastContentOffset > 100) {
            if (self.delegate) {
                [self.delegate onScroll:UP];
            }
        } else if (scrollView.contentOffset.y - self.lastContentOffset < -100) {
            if (self.delegate) {
                [self.delegate onScroll:DOWN];
            }
        }
    }
    
    if (fabs(scrollView.contentOffset.y - self.lastContentOffset) > 101) {
        [self setLastContentOffset:scrollView.contentOffset.y];
    }
}
@end
