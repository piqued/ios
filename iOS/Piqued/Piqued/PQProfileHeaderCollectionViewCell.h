//
//  PQProfileHeaderCollectionViewCell.h
//  Piqued
//
//  Created by Matt Mo on 9/2/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PQProfileHeaderCollectionViewCellDelegate <NSObject>

@required

- (void)startEditing;
- (void)didPressFollowersView;
- (void)didPressFollowingView;
- (void)didPressFollowButton:(UIButton *)sender;


@end

@interface PQProfileHeaderCollectionViewCell : UICollectionReusableView

@property (nonatomic, weak) id<PQProfileHeaderCollectionViewCellDelegate> delegate;

+ (NSString *)nibName;
+ (NSString *)reuseIdentifier;
+ (CGSize)headerReferenceSize;

- (void) setUser:(User *) user isAccountOwner:(BOOL)isAccountOwner isTabBarProfile:(BOOL)isTabBarProfile;

@end
