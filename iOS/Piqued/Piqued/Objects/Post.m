//
//  Post.m
//  Piqued
//
//  Created by Kenny M. Liou on 10/26/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "Post.h"

NSString * const PostDidGetBookmarkedNotification = @"PostDidGetBookmarkedNotification";

static NSString * const kTitle =        @"title";
static NSString * const kDescription =  @"description";
static NSString * const kAddress =      @"address";
static NSString * const kImageUrl =     @"imageUrl";
static NSString * const kTimeStamp =    @"timeStamp";
static NSString * const kLatitude =     @"latitude";
static NSString * const kLongitude =    @"longitude";
static NSString * const kBookmarked =   @"bookmarked";
static NSString * const kPostID =       @"postID";
static NSString * const kUserName =     @"userName";
static NSString * const kUserImage =    @"userImage";
static NSString * const kFourSquareVenueID = @"foursquareVenueID";
static NSString * const kLikes =        @"likes";
static NSString * const kMyReaction =   @"myReaction";

@interface Post()

@property (strong, nonatomic) NSNumber * postId;
@property (strong, nonatomic) NSString * title;
@property (strong, nonatomic) NSString * desc;
@property (strong, nonatomic) NSString * address;
@property (strong, nonatomic) NSArray * allImageUrls;
@property (strong, nonatomic) NSString * imageUrl;
@property (strong, nonatomic) NSString * username;
@property (strong, nonatomic) NSString * userimage;
@property (strong, nonatomic) NSString * foursquareVenueID;
@property (strong, nonatomic) NSDate * timeStamp;

@property (nonatomic) double lat;
@property (nonatomic) double lng;

@property (nonatomic) int likes;

@end

@implementation Post

- (void) updateWithPost:(Post *)newPostInfo
{
    if (self.postId == newPostInfo.postId) {
        [self setTitle:newPostInfo.title];
        [self setDesc:newPostInfo.desc];
        [self setAddress:newPostInfo.address];
        [self setAllImageUrls:newPostInfo.allImageUrls];
        [self setImageUrl:newPostInfo.imageUrl];
        [self setUsername:newPostInfo.username];
        [self setUserimage:newPostInfo.userimage];
        [self setTimeStamp:newPostInfo.timeStamp];
        [self setLat:newPostInfo.lat];
        [self setLng:newPostInfo.lng];
        [self setBookmarked:newPostInfo.bookmarked];
        [self setFoursquareVenueID:newPostInfo.foursquareVenueID];
        [self setLikes:newPostInfo.likes];
    }
}

+ (NSString *) getString:(NSString *)string
{
    if (string != NULL && ![string isEqual:[NSNull null]]) {
        return string;
    }
    
    return NULL;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self) {
        [self setTitle:[coder decodeObjectForKey:kTitle]];
        [self setDesc:[coder decodeObjectForKey:kDescription]];
        [self setAddress:[coder decodeObjectForKey:kAddress]];
        [self setImageUrl:[coder decodeObjectForKey:kImageUrl]];
        [self setUsername:[coder decodeObjectForKey:kUserName]];
        [self setUserimage:[coder decodeObjectForKey:kUserImage]];
        [self setFoursquareVenueID:[coder decodeObjectForKey:kFourSquareVenueID]];        
        [self setTimeStamp:[NSDate dateWithTimeIntervalSince1970:[coder decodeIntForKey:kTimeStamp]]];
        [self setLat:[coder decodeDoubleForKey:kLatitude]];
        [self setLng:[coder decodeDoubleForKey:kLongitude]];
        [self setBookmarked:[coder decodeBoolForKey:kBookmarked]];
        [self setPostId:[NSNumber numberWithInt:[coder decodeIntForKey:kPostID]]];
        [self setPriceTier:-1];
        [self setLikes:[coder decodeIntForKey:kLikes]];
        [self setMyReaction:[coder decodeObjectForKey:kMyReaction]];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.title forKey:kTitle];
    [coder encodeObject:self.desc forKey:kDescription];
    [coder encodeObject:self.address forKey:kAddress];
    [coder encodeObject:self.imageUrl forKey:kImageUrl];
    [coder encodeObject:self.username forKey:kUserName];
    [coder encodeObject:self.userimage forKey:kUserImage];
    [coder encodeObject:self.foursquareVenueID forKey:kFourSquareVenueID];
    [coder encodeInt:[self.timeStamp timeIntervalSince1970] forKey:kTimeStamp];
    [coder encodeDouble:self.lat forKey:kLatitude];
    [coder encodeDouble:self.lng forKey:kLongitude];
    [coder encodeBool:self.bookmarked forKey:kBookmarked];
    [coder encodeInt:self.postId.intValue forKey:kPostID];
    [coder encodeInt:self.likes forKey:kLikes];
    [coder encodeObject:self.myReaction forKey:kMyReaction];
}

- (BOOL) isEqual:(id)object {
    if (self == object) {
        return YES;
    } else if (![object isKindOfClass:[self class]]) {
        return NO;
    } else {
        return [self isEqualPosts:object];
    }
}

- (BOOL) isEqualPosts:(Post *)post {
    BOOL isEqual = self.lat == post.lat && self.lng == post.lng &&
    [self.postId isEqualToNumber:post.postId] &&
    [self.username isEqualToString:post.username];
    return isEqual;
}

#define NSUINT_BIT (CHAR_BIT * sizeof(NSUInteger))
#define NSUINTROTATE(val, howmuch) ((((NSUInteger)val) << howmuch) | (((NSUInteger)val) >> (NSUINT_BIT - howmuch)))

- (NSUInteger)hash {
    return NSUINTROTATE(self.lat, NSUINT_BIT / 3) ^ NSUINTROTATE(self.lng, NSUINT_BIT * 2/ 3) ^ [self.postId hash];
}

- (void) setBookmarked:(BOOL)bookmarked
{
    _bookmarked = bookmarked;
}

- (void) setNewTitle:(NSString *)title
{
    _title = title;
}

- (void) setNewLat:(double)lat andLng:(double)lng
{
    _lat = lat;
    _lng = lng;
}

#pragma ###############################################
#pragma mark - Helper Properties
#pragma ###############################################

- (NSString *)imageUrlLarge
{
    if (self.allImageUrls.count > 0) {
        NSDictionary *imageUrls = self.allImageUrls[0];
        return [imageUrls objectForKey:@"lg"];
    }
    return nil;
}

- (NSString *)imageUrlSmall
{
    if (self.allImageUrls.count > 0) {
        NSDictionary *imageUrls = self.allImageUrls[0];
        return [imageUrls objectForKey:@"sm"];
    }
    return nil;
}

- (CLLocation *)location
{
    CLLocationCoordinate2D coords = CLLocationCoordinate2DMake(self.lat, self.lng);
    CLLocation *location = [[CLLocation alloc] initWithCoordinate:coords altitude:0 horizontalAccuracy:kCLLocationAccuracyBest verticalAccuracy:kCLLocationAccuracyBest timestamp:self.timeStamp];
    return location;
}

- (NSDate *)timeStamp
{
    return [NSDate dateWithTimeIntervalSince1970:self.createdAtTimeStamp];
}

#pragma ###############################################
#pragma mark - Mantle
#pragma ###############################################

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"postId"              : @"id",
             @"allImageUrls"        : @"images",
             @"title"               : @"postTitle",
             @"desc"                : @"postDescription",
             @"userId"              : @"userId",
             @"username"            : @"userName",
             @"userimage"           : @"userImage",
             @"address"             : @"address",
             @"foursquareVenueID"   : @"foursquareVenueID",
             @"createdAtTimeStamp"  : @"createdAt",
             @"bookmarked"          : @"bookmarked",
             @"lat"                 : @"latitude",
             @"lng"                 : @"longitude",
             @"likes"               : @"likes",
             @"myReaction"          : @"myReaction",
             };
}


@end
