//
//  NSHTTPURLResponse+PIQHelper.h
//  Piqued
//
//  Created by Matt Mo on 12/7/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSHTTPURLResponse (PIQHelper)

/*!
    @method piq_isValidResponse:
    @abstract Convenience method which returns an NSError
    corresponding to the status code for this response.
    @param response - the original http response.
    @result Nil for status code 200. An NSError corresponding
    to the given status code. The error domain is set to 
    "HTTPResponseError". The error code is set to the status code.
 */
+ (NSError *) piq_isValidResponse:(NSHTTPURLResponse *)response;


@end
