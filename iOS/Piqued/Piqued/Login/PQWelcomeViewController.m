//
//  PQWelcomeViewController.m
//  Piqued
//
//  Created by Kenny M. Liou on 9/5/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQWelcomeViewController.h"
#import "PQWelcomeViewCell.h"
#import "PQUserDefaults.h"

typedef enum {
    pq_view_Welcome,
    pq_view_Places,
    pq_view_Bookmark,
    pq_view_Share,
    pq_view_END
} WelcomeViews;

@interface PQWelcomeViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, weak) IBOutlet UICollectionView * collectionView;
@property (nonatomic, weak) IBOutlet UICollectionViewFlowLayout * flowLayout;
@property (nonatomic, weak) IBOutlet UIButton * doneButton;
@property (nonatomic, weak) IBOutlet UIPageControl * pageControl;

@property (nonatomic) BOOL seeSecondView;
@end

@implementation PQWelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (@available(iOS 11.0, *)) {
        self.collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    
    self.flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    self.flowLayout.minimumLineSpacing = 0;
    self.flowLayout.minimumInteritemSpacing = 0;
    self.flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.collectionView registerNib:PQWelcomeViewCell.piq_nib forCellWithReuseIdentifier:PQWelcomeViewCell.piq_reuseIdentifier];
    [self.doneButton.layer setBorderWidth:1.0];
    [self.doneButton.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (!self.seeSecondView) {
            [self.collectionView setContentOffset:CGPointMake(self.collectionView.bounds.size.width, 0) animated:YES];
        }
    });
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return pq_view_END;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (PQWelcomeViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PQWelcomeViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:PQWelcomeViewCell.piq_reuseIdentifier forIndexPath:indexPath];

    [cell.imageView setImage:[UIImage imageNamed:[self getImageName:indexPath.row]]];

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return collectionView.bounds.size;
}

- (void) collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    int currentIndex = collectionView.contentOffset.x / collectionView.bounds.size.width;

    if (currentIndex == pq_view_END - 1) {
        [self showDoneButton];
    }

    if (currentIndex > 0) {
        [self showPageControl:YES];
        [self.pageControl setCurrentPage:currentIndex - 1];
    } else {
        [self showPageControl:NO];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetX = scrollView.contentOffset.x;
    CGFloat width = scrollView.bounds.size.width;

    if (offsetX < width) { // we are at pq_view_Welcome
        [self showPageControl:NO];
    }

    if (offsetX > width / 8) {
        [self setSeeSecondView:YES];
    }
}

#pragma mark UICollectionView helper methods

- (NSString *) getImageName:(NSInteger) index
{
    NSString * imageName;

    switch (index) {
        case pq_view_Places:
            imageName = @"welcome2";
            break;
        case pq_view_Bookmark:
            imageName = @"welcome3";
            break;
        case pq_view_Share:
            imageName = @"welcome4";
            break;
        default:
            imageName = @"welcome1";
            break;
    }

    return imageName;
}

- (void) showPageControl:(BOOL) show
{
    if (!show) {
        if (![self.pageControl isHidden]) {
            [self.pageControl setHidden:YES];
        }
    } else {
        if ([self.pageControl isHidden]) {
            [self.pageControl setAlpha:0];
            [self.pageControl setHidden:NO];
            [UIView animateWithDuration:0.5 animations:^{
                [self.pageControl setAlpha:1.0];
            }];
        }
    }
}

- (void) showDoneButton
{
    if ([self.doneButton isHidden]) {
        [self.doneButton setAlpha:0];
        [self.doneButton setHidden:NO];
        [UIView animateWithDuration:0.5 animations:^{
            [self.doneButton setAlpha:1.0];
        }];
    }
}

#pragma mark UI methods

- (IBAction)doneButtonPressed:(UIButton *)sender {
    [self showMainApp];
}

- (void) showMainApp
{
    [PQUserDefaults setUserFinishedWelcomeScreen:YES];
    [self performSegueWithIdentifier:@"showMainApp" sender:self];
}

@end
