//
//  CLLocation+PIQHelper.h
//  Piqued
//
//  Created by Matt Mo on 3/25/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface CLLocation (PIQHelper)


/**
 Random Location

 @param meters - Radius around location
 @return random location - that should be within a radius of specified meters (this may be off by a couple meters)
 
 */
- (CLLocation *)piq_randomLocation:(double)meters;

@end
