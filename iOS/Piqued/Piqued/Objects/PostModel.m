//
//  PostModel.m
//  Piqued
//
//  Created by Matt Mo on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PostModel.h"

@interface PostModel()

@end

@implementation PostModel

#pragma ###############################################
#pragma mark - Helper Properties
#pragma ###############################################

- (CLLocation *)location
{
    CLLocationCoordinate2D coords = CLLocationCoordinate2DMake(self.lat, self.lng);
    CLLocation *location = [[CLLocation alloc] initWithCoordinate:coords altitude:0 horizontalAccuracy:kCLLocationAccuracyBest verticalAccuracy:kCLLocationAccuracyBest timestamp:self.timeStamp];
    return location;
}

- (NSDate *)timeStamp
{
    return [NSDate dateWithTimeIntervalSince1970:self.createdAtTimeStamp];
}

#pragma ###############################################
#pragma mark - Mantle
#pragma ###############################################

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"postId"              : @"id",
             @"imageUrl"            : @"imageUrl",
             @"title"               : @"postTitle",
             @"desc"                : @"postDescription",
             @"username"            : @"userName",
             @"userimage"           : @"userImage",
             @"address"             : @"address",
             @"foursquareVenueID"   : @"foursquareVenueID",
             @"createdAtTimeStamp"  : @"createdAt",
             @"bookmarked"          : @"bookmarked",
             @"lat"                 : @"latitude",
             @"lng"                 : @"longitude",
             };
}

@end
