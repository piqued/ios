//
//  NSURL+PIQHelper.m
//  Piqued
//
//  Created by admin on 3/13/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "NSURL+PIQHelper.h"

@implementation NSURL (PIQHelper)

+ (nullable instancetype)URLWithURLEncodingString:(NSString *)nonURLEncodedString
{
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[output UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    
    return [NSURL URLWithString:output];
}

@end
