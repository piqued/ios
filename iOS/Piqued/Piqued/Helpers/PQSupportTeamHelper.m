//
//  PQSupportTeamHelper.m
//  Piqued
//
//  Created by Kenny M. Liou on 9/10/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQSupportTeamHelper.h"

#define kSupportEMailURL @"support@piqued.io" // ?subject=Report Post %d&body=content"

@implementation PQSupportTeamHelper


+ (void) reportPost:(Post *) post withViewController:(UIViewController *)controller withDelegate:(id<MFMailComposeViewControllerDelegate>) delegate
{
    MFMailComposeViewController * mcvc = [[MFMailComposeViewController alloc] init];
    [mcvc setMailComposeDelegate:delegate];
    
    NSString *toAddress = kSupportEMailURL;
    [mcvc setToRecipients:[NSArray arrayWithObjects:toAddress,nil]];
    [mcvc setSubject:[NSString stringWithFormat:@"[Report]Post Name: %@, Post ID: %d'", post.desc, post.postId.intValue]];
    
    [controller.navigationController showViewController:mcvc sender:controller];
}

@end
