//
//  EventModel.h
//  Piqued
//
//  Created by Matt Mo on 7/23/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "BaseModel.h"
#import "User.h"
#import "Post.h"

enum {
    PQEventTypeReaction = 0,
    PQEventTypeBookmark,
    PQEventTypeFollow,
    PQEventTypeComment,
    PQEventTypeLast
};
typedef NSUInteger PQEventType;

@interface EventModel : BaseModel

#warning TODO - convert User to BaseModel subclass
@property (nonatomic, copy, readonly)   User *actor;
@property (nonatomic, copy, readonly)   Post *post;
@property (nonatomic, copy, readonly)   User *targetUser;
@property (nonatomic, readonly)         NSDictionary *eventTarget;
@property (readonly) PQEventType eventType;
@property (nonatomic, readonly)         NSString *eventTypeString;
@property (nonatomic, copy, readonly)   NSString *message;
@property (nonatomic, copy, readonly)   NSString *decodedMessage;
@property (nonatomic, readonly)         NSUInteger createdAtTimeStamp;

// Helper properties
@property (nonatomic, copy, readonly) NSDate *timeStamp;

@end
