//
//  BaseModel.h
//  Piqued
//
//  Created by Matt Mo on 7/9/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Mantle/Mantle.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseModel : MTLModel<MTLJSONSerializing>

@property NSDictionary * backingJSON;

+ (nullable instancetype)modelWithDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END