//
//  PQConvenienceMethods.m
//  Piqued
//
//  Created by Matt Mo on 3/18/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import "PQConvenienceMethods.h"

@implementation PQConvenienceMethods

BOOL piq_notNilOrNull(id object)
{
    return object != nil && object != [NSNull null];
}

BOOL piq_deviceIsSimulator()
{
    #if (TARGET_OS_SIMULATOR)
        return YES;
    #else
        return NO;
    #endif
}

@end
