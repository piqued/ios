//
//  PQFourSquarePickerViewController.h
//  Piqued
//
//  Created by Matt Mo on 3/5/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    NewPostLocationTypePhoto = 0,
    NewPostLocationTypeUser,
    NewPostLocationTypeCustom,
    NewPostLocationTypeUnavailable,
} NewPostLocationType;

extern NSString * const NewPostLocationTypeConstants[];

@protocol PQFourSquarePickerDelegate <NSObject>

@required
- (void)handleFourSquarePlacePicked:(NSDictionary *)venue;

@end

@interface PQFourSquarePickerViewController : UIViewController

+ (NSString *)segueIdentifier;

@property (nonatomic, strong) CLLocation * location;
@property (nonatomic, strong) CLLocation * photoLocation;
@property (nonatomic, strong) NSArray * data;
@property (nonatomic, weak) id<PQFourSquarePickerDelegate> delegate;

- (void)loadWithInitialResults:(NSArray *)data location:(CLLocation *)location locationType:(NewPostLocationType)type;

@end
