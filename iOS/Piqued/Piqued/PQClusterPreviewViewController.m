//
//  PQClusterPreviewViewController.m
//  Piqued
//
//  Created by Matt Mo on 1/15/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQClusterPreviewViewController.h"
#import "PQBookmarkCell.h"
#import "PQBasketViewController.h"
#import "UIColor+PIQHex.h"

@interface PQClusterPreviewViewController ()

@property (strong, nonatomic) NSArray * posts;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation PQClusterPreviewViewController

static NSString * const kBookmarkCell = @"PQBookmarkCell";

+ (NSString *)segueIdentifier {
    return @"ClusterPreviewSegue";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.tableView.estimatedRowHeight = 81;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView registerNib:[UINib nibWithNibName:kBookmarkCell bundle:NULL] forCellReuseIdentifier:kBookmarkCell];
    
    [self.navigationController.navigationBar setTintColor:[UIColor piq_colorFromHexString:kOrangeColor]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [[PQPostManager sharedInstance] addPostMangerDelegate:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[PQPostManager sharedInstance] removePostManagerDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.posts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PQBookmarkCell * bookmarkCell = [tableView dequeueReusableCellWithIdentifier:kBookmarkCell forIndexPath:indexPath];
    if (bookmarkCell != nil) {
        Post* post = [[PQPostManager sharedInstance] getPost:self.posts[indexPath.row]];
        [bookmarkCell setPost:post];
        return bookmarkCell;
    } else {
        return [[UITableViewCell alloc] init];
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:[PQBasketViewController segueIdentifier] sender:self];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - PQPostManager

- (void) bookmarkUpdated
{
    [self.tableView reloadData];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.destinationViewController isKindOfClass:[PQBasketViewController class]]) {
        PQBasketViewController * basketVC = (PQBasketViewController *)segue.destinationViewController;
        [basketVC setPostID:self.posts[self.tableView.indexPathForSelectedRow.row]];
    }
}

@end
