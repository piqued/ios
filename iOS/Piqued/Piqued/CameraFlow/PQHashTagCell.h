//
//  PQHashTagCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 6/13/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQHashTagRecord.h"

@protocol PQHashTagCellDelegate <NSObject>

@required
- (void) onCellTapped:(PQHashTagRecord *) record;

@end

@interface PQHashTagCell : UITableViewCell

@property (nonatomic, strong) PQHashTagRecord * record;

+ (CGFloat) getCellHeight;

@end
