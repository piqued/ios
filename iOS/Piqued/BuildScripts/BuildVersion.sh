#!/bin/bash -
#===============================================================================
#
#          FILE:  BuildVersion.sh
# 
#         USAGE:  ./BuildVersion.sh 
# 
#   DESCRIPTION:  Update the Build Version for Piqued Info.plist files.
#                 This script is run in the phase "Update Build Version Script"
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR: Matt Mo, matt@piqued.io
#       COMPANY: 
#       CREATED: 02/12/2017
#      REVISION:  ---
#===============================================================================

# Synchronize bundle short version across all targets
echo "${INFO_PLIST_VERSION}"
echo "${INFOPLIST_FILE}"
/usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString ${INFO_PLIST_VERSION}" "${INFOPLIST_FILE}"
/usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString ${INFO_PLIST_VERSION}" "${INFOPLIST_FILE}"

git=`sh /etc/profile; which git`
appBuild=`"$git" rev-list HEAD --count`
if [ $CONFIGURATION = "Debug" ]; then
branchName=`"$git" rev-parse --abbrev-ref HEAD`
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $appBuild-$branchName" "${PROJECT_DIR}/${INFOPLIST_PATH}"
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $appBuild-$branchName" "${TARGET_BUILD_DIR}/${INFOPLIST_PATH}"
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $appBuild-$branchName" "$(BUILT_PRODUCTS_DIR)/$INFOPLIST_PATH"
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $appBuild-$branchName" "${BUILT_PRODUCTS_DIR}/${WRAPPER_NAME}.dSYM/Contents/Info.plist"
else
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $appBuild" "${TARGET_BUILD_DIR}/${INFOPLIST_PATH}"
/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $appBuild" "${BUILT_PRODUCTS_DIR}/${WRAPPER_NAME}.dSYM/Contents/Info.plist"
fi
echo "Incremented the build number ${TARGET_BUILD_DIR}/${INFOPLIST_PATH}"
