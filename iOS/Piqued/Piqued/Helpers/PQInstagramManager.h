//
//  PQInstagramManager.h
//  Piqued
//
//  Created by Matt on 5/14/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQInstagramManager : NSObject

+ (instancetype)sharedInstance;

- (void)logInFromViewController:(UIViewController *)fromViewController
                        handler:(void(^)(BOOL))handler;
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation;

@end
