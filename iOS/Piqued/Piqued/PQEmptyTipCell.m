//
//  PQEmptyTipCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 4/27/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQEmptyTipCell.h"

@implementation PQEmptyTipCell

+ (int) estimatedHeight
{
    return 30;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
