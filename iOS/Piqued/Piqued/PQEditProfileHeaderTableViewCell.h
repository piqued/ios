//
//  PQEditProfileHeaderTableViewCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 8/20/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PQEditProfileHeaderTableViewCell : UITableViewCell

+ (int) estimatedHeight;

@end
