//
//  PQiMessageUserDefault.m
//  Piqued
//
//  Created by Kenny M. Liou on 8/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQiMessageUserDefault.h"
#import "Constants.h"
#import "PQEnvironment.h"

@implementation PQiMessageUserDefault

+ (NSUserDefaults *) getAppUserDefaults
{
    static NSUserDefaults * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NSUserDefaults alloc] initWithSuiteName:PQEnvironment.appGroupName];
    });
    
    return instance;
}

+ (BOOL) userLoggedIn
{
    User * user = [self getUser];
    
    return user != NULL;

}

+ (User *) getUser
{
    NSData * encodedObject = [[self getAppUserDefaults] objectForKey:@"userInfo"];
    User * user = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    
    return user;
}

@end
