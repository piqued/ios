//
//  PQCameraPreviewCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 2/27/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQCameraPreviewCell.h"
#import "PQLocationManager.h"
#import "PQDeviceOrientationManager.h"
#import "PQUtilities.h"

@import Photos;

@interface PQCameraPreviewCell()

@property (strong, nonatomic) AVCaptureSession * cameraSession;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer * previewLayer;
@property (strong, nonatomic) AVCaptureStillImageOutput * stillImageOutput;

@property (weak, nonatomic) IBOutlet UIView *cameraPreview;

@end

@implementation PQCameraPreviewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self turnOnCamera];
}

- (void) turnOnCamera {
    
    [self initializeCameraSession];
}

- (void) startCamera
{
    if (self.previewLayer) {
        [self.previewLayer removeFromSuperlayer];
    }
    
    [self setPreviewLayer:[[AVCaptureVideoPreviewLayer alloc] initWithSession:self.cameraSession]];
    
    CGRect frame = self.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    
    [_previewLayer setFrame:frame];
    [_previewLayer setVideoGravity:AVLayerVideoGravityResize];
    
    [self.layer addSublayer:_previewLayer];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.cameraSession startRunning];
    });
}

- (void) stopCamera
{
    [self.cameraSession stopRunning];
}

- (void) initializeCameraSession
{
    AVCaptureSession * session = [[AVCaptureSession alloc] init];
    AVCaptureDevice *audioCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    session.sessionPreset = AVCaptureSessionPresetPhoto;
    
    AVCaptureDeviceInput *photoInput = [AVCaptureDeviceInput deviceInputWithDevice:audioCaptureDevice error:&error];
    if (photoInput) {
        [session addInput:photoInput];
    }
    else {
        // Handle the failure.
        return;
    }
    
    self.stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [self.stillImageOutput setOutputSettings:outputSettings];
    
    [session addOutput:self.stillImageOutput];
    
    self.cameraSession = session;
}

- (void) takePhoto
{
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in self.stillImageOutput.connections)
    {
        for (AVCaptureInputPort *port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) {
            break;
        }
    }

    // request image capture
    [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection
                                                       completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
         CFDictionaryRef exifAttachments = CMGetAttachment(imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
         if (exifAttachments) {
             PQLog(@"attachements: %@", exifAttachments);
         } else {
             PQLog(@"no attachments");
         }

         NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];

         UIImage *originalImage = [[UIImage alloc] initWithData:imageData];
         
         UIImage *compressedImage = scaleAndRotateImage(originalImage, 1080);
         UIImage *thumbnailImage = scaleAndRotateImage(originalImage, 480);
         
         [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
             [PHAssetChangeRequest creationRequestForAssetFromImage:compressedImage];
         } completionHandler:^(BOOL success, NSError *error) {
             if (success) {
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self.handler hideSpinner];
                     // open pin details view controller on main thread
                     [self.handler openPinDetailsWithImage:compressedImage
                                             withThumbnail:thumbnailImage
                                              withLocation:[PQLocationManager sharedInstance].location];
                 });
             }
             else {
                 PQLog(@"Failed to save image");
             }
         }];
     }];

}

@end
