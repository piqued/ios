//
//  PQMapSelectionHelper.h
//  Piqued
//
//  Created by Kenny M. Liou on 9/3/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Post.h"

typedef enum {
    pq_map_UNKNOWN,
    pq_map_APPLE,
    pq_map_GOOGLE,
    pq_map_WAZE,
    pq_map_HERE,
    pq_map_END
} PQSupportedMaps;

@interface PQMapSelectionHelper : NSObject

+ (PQSupportedMaps) getDefaultMap;
+ (NSString *) getMapURLWithPost:(Post *)post;
+ (NSArray<NSNumber *> *) getCurrentlyInstalledMaps;
+ (NSString *) getMapName:(NSInteger)mapType;
+ (BOOL) hasOtherMaps;

+ (void) setDefaultMap:(PQSupportedMaps)map;


@end
