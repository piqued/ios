//
//  PQiMessageUserDefault.h
//  Piqued
//
//  Created by Kenny M. Liou on 8/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface PQiMessageUserDefault : NSUserDefaults

+ (BOOL) userLoggedIn;
+ (User *) getUser;

@end
