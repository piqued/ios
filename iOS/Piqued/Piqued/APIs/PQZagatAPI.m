//
//  PQZagatAPI.m
//  Piqued
//
//  Created by Kenny M. Liou on 7/10/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQZagatAPI.h"
#import "NSHTTPURLResponse+PIQHelper.h"

#define kSearchURL      @"https://maps.googleapis.com/maps/api/place/radarsearch/json"
#define kLocationParam  @"location=%f,%f"
#define kRadisuParam    @"radius=1000"
#define kNameParam      @"name=%@"
#define kKeyParam       @"key=AIzaSyBKDdC639nZXk--GpU5uRt4-1icsH5rLNE"


@implementation PQZagatAPI

+ (instancetype) sharedInstance
{
    static PQZagatAPI * instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

+ (void) getZagatURL:(Post * _Nonnull)post withCallback:(void(^ _Nonnull) (NSURL * _Nullable zagatLink)) callback
{
    [[PQZagatAPI sharedInstance] performSearch:post withCallback:callback];
}

- (void) appendUrlString:(NSMutableString *) urlString withParameter:(NSString *) param
{
    if ([urlString containsString:@"?"]) {
        [urlString appendFormat:@"&%@", param];
    } else {
        [urlString appendFormat:@"?%@", param];
    }
}

- (void) performSearch:(Post *) post withCallback:(void(^) (NSURL * zagatLink)) callback
{
    // TODO: seems like this doesn't really work. Will kill this file soon
    NSMutableString * searchUrlString = [NSMutableString stringWithString:kSearchURL];
    
    [self appendUrlString:searchUrlString withParameter:[NSString stringWithFormat:kLocationParam, post.lat, post.lng]];
    [self appendUrlString:searchUrlString withParameter:kRadisuParam];
    [self appendUrlString:searchUrlString withParameter:[NSString stringWithFormat:kNameParam, post.title]];
    [self appendUrlString:searchUrlString withParameter:kKeyParam];
    
    // create request
    NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:searchUrlString]];
    // create session
    NSURLSession * session = [NSURLSession sharedSession];
    
    // execute session
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        });
        
        NSError * responseError = error ? error : [NSHTTPURLResponse piq_isValidResponse:(NSHTTPURLResponse *)response];
        if (responseError) {
            // Error in response or making request
            PQLog(@"Network error: %@", responseError);
        } else {
            // Response is OK
            NSError * jsonParseError = NULL;
            NSArray * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonParseError];
            
            if (jsonParseError) {
                PQLog(@"Json parser error: %@", jsonParseError);
                responseError = jsonParseError;
            } else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    callback(NULL);
                });
                
                return;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(NULL);
        });
        
    }] resume];
}

@end
