//
//  PQThreadHelper.m
//  Piqued
//
//  Created by Kenny M. Liou on 9/1/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQThreadHelper.h"

@implementation PQThreadHelper

+ (void) performOnMainThread:(void(^)(void))callback
{
    if ([NSThread isMainThread]) {
        callback();
    } else {
        dispatch_async(dispatch_get_main_queue(), callback);
    }
}
@end
