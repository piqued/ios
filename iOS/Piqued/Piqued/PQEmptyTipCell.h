//
//  PQEmptyTipCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 4/27/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQBasketBaseCell.h"

@interface PQEmptyTipCell : PQBasketBaseCell

+ (int) estimatedHeight;
@end
