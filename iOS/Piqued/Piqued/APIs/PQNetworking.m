//
//  PQNetworking.m
//  Piqued
//
//  Created by Matt Mo on 7/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQNetworking.h"
#import "PQEnvironment.h"
#import "PQUserDefaults.h"
#import "EventModel.h"
#import "PQKeyChain.h"

@implementation PQNetworking

- (instancetype)init
{
    if (self = [super init]) {
        NSURL *url = PQEnvironment.serverBaseURL;
        self.manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
        self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
        self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [self.manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    }
    return self;
}

+ (instancetype)sharedInstance
{
    static PQNetworking *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (BFTask *)postEndPointAsync :(NSString *)url parameters:(id)parameters {
    BFTaskCompletionSource *BFTask = [BFTaskCompletionSource taskCompletionSource];
    AFHTTPResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
    self.manager.responseSerializer = responseSerializer;
    [self.manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) {
        //do something, save response
        [BFTask setResult:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [BFTask setError:error];
    }];
    return BFTask.task;
}

/* GET */
- (BFTask *) executeEndPointAsync :(NSString *)url parameters:(id)parameters {
    BFTaskCompletionSource *BFTask = [BFTaskCompletionSource taskCompletionSource];
    AFHTTPResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
    self.manager.responseSerializer = responseSerializer;
    [self.manager GET:url parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) {
        //do something, save response
        [BFTask setResult:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [BFTask setError:error];
    }];
    return BFTask.task;
}

/* PUT */
- (BFTask *)putEndPointAsync :(NSString *)url parameters:(id)parameters {
    PQLog(@"PUT url: %@, params: %@", url, parameters);
    BFTaskCompletionSource *BFTask = [BFTaskCompletionSource taskCompletionSource];
    AFHTTPResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
    self.manager.responseSerializer = responseSerializer;
    [self.manager PUT:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) {
        //do something, save response
        [BFTask setResult:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        PQLog(@"Request failed with error: %@", error);
        [BFTask setError:error];
    }];
    return BFTask.task;
}

/* PATCH */
- (BFTask *)patchEndPointAsync :(NSString *)url parameters:(id)parameters {
    BFTaskCompletionSource *BFTask = [BFTaskCompletionSource taskCompletionSource];
    AFHTTPResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
    self.manager.responseSerializer = responseSerializer;
    [self.manager PATCH:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) {
        //do something, save response
        [BFTask setResult:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [BFTask setError:error];
    }];
    return BFTask.task;
}

- (BFTask *)fetchEvents
{
    NSString *url = [PQEnvironment.serverBaseURLString stringByAppendingString:@"/events.json"];
    
    User * user = [PQUserDefaults getUser];
    
    NSDictionary *params = @{
                             @"creds": @{
                                     @"token" : user.token ?: @"",
                                     @"secret" : user.secret ?: @"",
                                     },
                             };
    return [[self executeEndPointAsync:url parameters:params] continueWithBlock:^id _Nullable(BFTask * _Nonnull task) {
        BFTaskCompletionSource *BFTask = [BFTaskCompletionSource taskCompletionSource];
        if (task.error != nil) {
            PQLog(@"Error request");
            [BFTask setError:task.error];
        } else {
            NSError *mantleError = nil;
            NSArray<EventModel *> *array = [MTLJSONAdapter modelsOfClass:[EventModel class] fromJSONArray:task.result error:&mantleError];
            if (mantleError) {
                [BFTask setError:task.error];
            } else {
                [BFTask setResult:array];
            }
        }
        return BFTask.task;
    }];
}

- (BFTask *)fetchFollowTypeUsers:(PQFollowType)type ForUser:(User *)user
{
    NSString *followURL = @"";
    switch (type) {
        case PQFollowTypeFollower: {
            followURL = @"followers";
            break;
        }
        case PQFollowTypeFollowing: {
            followURL = @"followings";
            break;
        }
    }
    
    NSString *url = PQEnvironment.serverBaseURLString;
    url = [url stringByAppendingFormat:@"/users/%.f/%@.json", user.userId, followURL];
    
    User * currentUser = [PQUserDefaults getUser];
    
    NSDictionary *params = @{
                             @"creds": @{
                                     @"token" : currentUser.token ?: @"",
                                     @"secret" : currentUser.secret ?: @"",
                                     },
                             };
    
    return [[self executeEndPointAsync:url parameters:params] continueWithBlock:^id _Nullable(BFTask * _Nonnull task) {
        BFTaskCompletionSource *BFTask = [BFTaskCompletionSource taskCompletionSource];
        if (task.error != nil) {
            [BFTask setError:task.error];
        } else {
            NSArray *jsonArray = [task.result objectForKey:followURL];
            
            // Each following/followed user entry is wrapped in a dictionary with key @"user"
            // [{ "user" : userData },{ "user" : userData }] 
            __block NSMutableArray *userArray = [NSMutableArray array];
            [jsonArray enumerateObjectsUsingBlock:^(NSDictionary *entry, NSUInteger idx, BOOL * _Nonnull stop) {
                [userArray addObject:[entry valueForKey:@"user"]];
            }];
            
            NSError *mantleError = nil;
            NSArray<User *> *array = [MTLJSONAdapter modelsOfClass:[User class] fromJSONArray:[userArray copy] error:&mantleError];
            
            if (mantleError) {
                [BFTask setError:task.error];
            } else {
                [BFTask setResult:array];
            }
        }
        return BFTask.task;
    }];
}

- (BFTask *)follow:(BOOL)follow user:(User *)user
{
    NSString *url = PQEnvironment.serverBaseURLString;
    url = [url stringByAppendingFormat:@"/users/%.f/follow.json", user.userId];
    User *currentUser = [PQUserDefaults getUser];
    
    NSDictionary *params = @{
                             @"creds": @{
                                     @"token" : currentUser.token ?: @"",
                                     @"secret" : currentUser.secret ?: @"",
                                     },
                             @"follow": @{
                                     @"followed" : @(follow),
                                     }
                             };
    
    return [self postEndPointAsync:url parameters:params];
}

- (BFTask *)requestPresign
{
    NSString *url = PQEnvironment.serverBaseURLString;
    url = [url stringByAppendingString:@"/photos/cache/presign"];
    User *currentUser = [PQUserDefaults getUser];
    
    NSDictionary *params = @{
                             @"creds": @{
                                     @"token" : currentUser.token ?: @"",
                                     @"secret" : currentUser.secret ?: @"",
                                     }
                             };
    return [self executeEndPointAsync:url parameters:params];
}

- (BFTask *) uploadImageToPresign:(UIImage *)image
{
    BFTask *presignRequestTask = [self requestPresign];
    return [presignRequestTask continueWithBlock:^id _Nullable(BFTask * _Nonnull task) {
        BFTaskCompletionSource *uploadTask = [BFTaskCompletionSource taskCompletionSource];
        if (task.error) {
            PQLog(@"Error getting presign: %@", task.error);
            [uploadTask setError:task.error];
            return uploadTask.task;
        }
        
        if (task.result == nil) {
            PQLog(@"Error getting presign: nil result");
            NSError *error = [NSError errorWithDomain:@"io.piqued.Piqued.error.UploadError"
                                                 code:101
                                             userInfo:@{
                                                        NSLocalizedDescriptionKey : @"Response for getting presign was nil",
                                                        }];
            [uploadTask setError:error];
            return uploadTask.task;
        }
        
        PQLog(@"%s task.result: %@", __PRETTY_FUNCTION__, task.result);
        
        NSDictionary *response = task.result;
        NSString *urlString = [response valueForKey:@"url"];
        NSDictionary *presignParams = [response valueForKey:@"fields"];
        NSString *fileName = @"random";
        NSData *imageData = UIImageJPEGRepresentation(image, 0.75);
        
        // Construct aws upload result
        NSString *awsKey = [presignParams objectForKey:@"key"];
        NSArray *keyComponents = [awsKey componentsSeparatedByString:@"cache/"];
        NSString *imageId = [keyComponents lastObject];
        NSDictionary *awsResults = @{
                                     @"id" : imageId,
                                     @"storage" : @"cache",
                                     @"metadata" : @{
                                             @"size" : @(imageData.length),
                                             @"filename" : fileName,
                                             @"mime_type" : @"image/jpeg"
                                             }
                                     };
        

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            for (NSString *key in [presignParams allKeys]) {
                NSString * value = [presignParams valueForKey:key];
                NSData *data = [value dataUsingEncoding:NSUTF8StringEncoding];
                [formData appendPartWithFormData:data name:key];
            }
            
            [formData appendPartWithFileData:imageData
                                        name:@"file"
                                    fileName:fileName mimeType:@"image/jpeg"];
            
            // etc.
        } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            NSLog(@"%s Response: %@", __PRETTY_FUNCTION__, responseObject);
            
            uploadTask.result = awsResults;
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"Error: %@", error);
            uploadTask.error = error;
        }];
        
        return uploadTask.task;
    }];
}

- (BFTask *)uploadPost:(NSDictionary *)postContents
{
    UIImage *image = [postContents objectForKey:@"image"];
    BFTask *uploadImageTask = [self uploadImageToPresign:image];
    
    return [uploadImageTask continueWithBlock:^id _Nullable(BFTask * _Nonnull task) {
        BFTaskCompletionSource *nextTask = [BFTaskCompletionSource taskCompletionSource];
        if (task.error) {
            PQLog(@"Error uploading image to aws: %@", task.error);
            nextTask.error = task.error;
            return nextTask.task;
        }
        
        if (task.result == nil) {
            NSError *error = [NSError errorWithDomain:@"io.piqued.Piqued.error.UploadError"
                                                 code:101
                                             userInfo:@{
                                                        NSLocalizedDescriptionKey : @"Response for uploading to presign was nil",
                                                        }];
            nextTask.error = error;
            return nextTask.task;
        }
        
        PQLog(@"%s task.result: %@", __PRETTY_FUNCTION__, task.result);
        
        NSDictionary *awsUploadResult = task.result;
        
        // endpoint
        NSMutableString * restUrl = [NSMutableString string];
        [restUrl appendString:[PQEnvironment.serverBaseURLString stringByAppendingString:@"/posts.json"]];
        
        // Post values
        NSString * description = [postContents objectForKey:@"description"];
        NSArray * categories = [postContents objectForKey:@"categories"];
        NSString * title = [postContents objectForKey:@"title"];
        CLLocation * location = [postContents objectForKey:@"location"];
        NSString * locationName = [postContents objectForKey:@"location_name"];
        NSString * address = [postContents objectForKey:@"address"];
        NSString * foursquareID = [postContents objectForKey:@"foursquare_venue_id"];
        NSString * foursquareCC = [postContents objectForKey:@"foursquare_cc"];
        NSString * foursquareState = [postContents objectForKey:@"foursquare_state"];
        NSString * foursquareCity = [postContents objectForKey:@"foursquare_city"];
        NSString * yelpLink = [postContents objectForKey:@"yelp_id"];
        
        NSDictionary * postJSON = @{
                                    @"lat": [NSNumber numberWithDouble:location.coordinate.latitude],
                                    @"lng": [NSNumber numberWithDouble:location.coordinate.longitude],
                                    @"address": address,
                                    @"title": title,
                                    @"description": description,
                                    @"images": @[
                                                @{
                                                    @"lg": awsUploadResult,
                                                    @"sm": awsUploadResult,
                                                },
                                                ],
                                    @"categories": categories,
                                    @"location_name": locationName,
                                    @"foursquare_venue_id": foursquareID,
                                    @"foursquare_cc": foursquareCC,
                                    @"foursquare_state": foursquareState,
                                    @"foursquare_city": foursquareCity,
                                    @"yelp_id": yelpLink,
                                    };
        
        // Credentials
        User *currentUser = [PQUserDefaults getUser];
    
        NSDictionary *credentialsJSON = @{
                                          @"token" : currentUser.token ?: @"",
                                          @"secret" : currentUser.secret ?: @"",
                                          };
        
        // Parameters
        NSDictionary *params = @{
                                 @"creds": credentialsJSON,
                                 @"post": postJSON,
                                 };
        
        PQLog(@"%s post: %@", __PRETTY_FUNCTION__, params);
        return [self postEndPointAsync:restUrl parameters:params];
    }];
}

- (BFTask *)uploadProfileData:(NSDictionary *)profileJSON
{
    NSMutableString * restUrl = [NSMutableString stringWithString:[PQEnvironment.serverBaseURLString stringByAppendingString:@"/profile.json"]];
    
    // setup data
    User *currentUser = [PQUserDefaults getUser];
    
    NSDictionary *credentialsJSON = @{
                                      @"token" : currentUser.token ?: @"",
                                      @"secret" : currentUser.secret ?: @"",
                                      };
    
    NSDictionary * params = @{
                                   @"creds": credentialsJSON,
                                   @"user": profileJSON
                                   };
    
    PQLog(@"%s restUrl: %@\nprofile params:\n%@", __PRETTY_FUNCTION__, restUrl, params);
    return [self patchEndPointAsync:restUrl parameters:params];
}


/**
 Edit Profile

 Sample:
 
 user[email]=testing12345@test.com&
 user[name]=hahahahh%20hahhaha7&
 user[password]=test12&
 user[password_confirmation]=test12&
 user[avatar][id]=5d3cb383967f7c05d83c6b480f5c2352&
 user[avatar][metadata][filename]=dontpanic.jpeg&
 user[avatar][metadata][mime_type]=image%2Fjpeg&
 user[avatar][metadata][size]=17997&
 user[avatar][storage]=cache'
 
 @param profileContents dictionary with profile data to change
 @return task completion source
 */
- (BFTask *)editProfile:(NSDictionary *)profileContents
{
    UIImage *image = [profileContents objectForKey:@"image"];
    
    if (image != nil) {
        BFTask *uploadImageTask = [self uploadImageToPresign:image];
        PQLog(@"%s Uploading image for profile", __PRETTY_FUNCTION__);
        
        return [uploadImageTask continueWithBlock:^id _Nullable(BFTask * _Nonnull task) {
            BFTaskCompletionSource *nextTask = [BFTaskCompletionSource taskCompletionSource];
            if (task.error) {
                PQLog(@"Error uploading image to aws: %@", task.error);
                nextTask.error = task.error;
                return nextTask.task;
            }

            // Grab AWS upload result
            PQLog(@"%s task.result: %@", __PRETTY_FUNCTION__, task.result);
            NSDictionary *awsUploadResult = task.result;
            
            NSMutableDictionary *profileData = [NSMutableDictionary dictionaryWithDictionary:profileContents];
            [profileData removeObjectForKey:@"image"];
            profileData[@"avatar"] = awsUploadResult;
            
            return [self uploadProfileData:profileData];
        }];
    } else {
        return [self uploadProfileData:profileContents];
    }
}

- (BFTask *)registerDeviceToken:(NSString *)deviceToken
{
    // Get endpoint
    NSString *urlString = PQEnvironment.serverBaseURLString;
    User *currentUser = PQUserDefaults.getUser;
    NSString *path = [NSString stringWithFormat:@"/users/%@/devices/%@", @(currentUser.userId), deviceToken];
    urlString = [urlString stringByAppendingString:path];
    
    // Credentials JSON body
    NSDictionary *credentialsJSON = @{
                                      @"token" : currentUser.token ?: @"",
                                      @"secret" : currentUser.secret ?: @"",
                                      };
    
    // Device JSON body
    NSString *uuid = [PQKeyChain getDeviceId];
    NSString *platform = [UIDevice.currentDevice systemName];
    NSString *osVersion = [UIDevice.currentDevice systemVersion];
    
    NSString *environment = @"unknown";
    switch (PQEnvironment.buildConfiguration) {
        case PQBuildConfigurationDebug:
        case PQBuildConfigurationRelease:
            environment = @"sandbox";
            break;
        case PQBuildConfigurationAppStore:
            environment = @"production";
            break;
    }
    
    NSDictionary *deviceJSON = @{
                                 @"udid": uuid,
                                 @"platform": platform,
                                 @"os_version": osVersion,
                                 @"environment": environment,
                                 };
    
    // Parameters
    NSDictionary *params = @{
                             @"creds": credentialsJSON,
                             @"device": deviceJSON,
                             };
    
    return [self putEndPointAsync:urlString parameters:params];
}

#pragma mark - API Check

- (BFTask *)checkAPIVersion
{
    NSString *urlString = PQEnvironment.serverBaseURLString;
    urlString = [urlString stringByAppendingString:@"/client_versions.json"];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];

    // Parameters
    NSDictionary *params = @{
                             @"version": version,
                             @"platform": @"ios",
                             };
    
    PQLog(@"%s url: %@ params: %@", __PRETTY_FUNCTION__, urlString, params);
    return [self executeEndPointAsync:urlString parameters:params];
}

@end
