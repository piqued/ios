//
//  PQImagePickerDelegate.h
//  Piqued
//
//  Created by Kenny M. Liou on 11/9/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#ifndef PQImagePickerDelegate_h
#define PQImagePickerDelegate_h

@import Photos;

@protocol PQImagePickerDelegate

- (PHCachingImageManager *) getManager;
- (void) takePhoto;
- (void) onImageAssetPicked:(PHAsset *) asset;

@end

#endif /* PQImagePickerDelegate_h */
