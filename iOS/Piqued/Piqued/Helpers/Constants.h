//
//  Constants.h
//  Piqued
//
//  Created by Kenny M. Liou on 10/27/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#ifndef Constants_h
#define Constants_h


#define kUserInfo               @"userInfo"
#define kUserToken              @"piqued_user_token"
#define kUserSecret             @"piqued_user_secret"
#define kUserId                 @"user_id"
#define kUserName               @"user_name"
#define kUserEmail              @"email"
#define kProfileImage           @"profile_image"
#define kFollowerCount          @"follower_count"
#define kFollowingCount         @"following_count"
#define kTotalPostsCount        @"total_posts_count"
#define kWelcomeScreenViewed    @"welcome_screen_viewed"

#define kBlankProfileImage      @"blankProfile"
#define kBlankPostImage         @"blankImageBookmark"

#pragma mark - NSUserDefaults
#define kBookmarkListModified   @"post_list_modified"
#define kPQUserDefaultsKeyAPNSDeviceToken   @"kPQUserDefaultsKeyAPNSDeviceToken"
#define PQDefaultsInstagramRequestSeen @"PQDefaultsInstagramRequestSeen"

#pragma mark - NSNotification
#define kSafariViewControllerCloseNotification  @"kSafariViewControllerCloseNotification"

#endif /* Constants_h */
