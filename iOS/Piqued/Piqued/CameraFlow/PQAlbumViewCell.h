//
//  PQAlbumCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 2/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQImageViewCell.h"

@interface PQAlbumViewCell : PQImageViewCell

@end
