//
//  Tip.m
//  Piqued
//
//  Created by Kenny M. Liou on 4/24/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "Tip.h"

static const NSString * tipImageSize = @"40x40";

@interface Tip()

@end

@implementation Tip

- (NSString *) getImageUrl
{
    return [NSString stringWithFormat:@"%@%@%@", self.imagePrefix, tipImageSize, self.imageSuffix];
}

@end
