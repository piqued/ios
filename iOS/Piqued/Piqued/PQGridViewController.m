//
//  PQGridViewController.m
//  Piqued
//
//  Created by Matt Mo on 10/25/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQGridViewController.h"
#import "PQPostCollectionViewCell.h"
#import "PQMainTabBarController.h"
#import "PQAPIs.h"
#import "PQPostManager.h"
#import "PQBasketViewController.h"
#import "PQProfileViewController.h"
#import "PQScrollHandler.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "PQInstagramPermissionController.h"
#import "PQInstagramManager.h"
#import "PQInstagramPost.h"
#import "PQUserDefaults.h"
#import <InstagramKit/InstagramKit.h>
#import "InfiniteScrollActivityView.h"

@interface PQGridViewController() <PQPostManagerDelegate, PQScrollHandlerDelegate, PQPostCollectionViewCellDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *emptyView;

@property (strong, nonatomic) NSArray<NSNumber *> * data; // post ids
@property (strong, nonatomic) CLLocation * location;

@property (strong, nonatomic) UIRefreshControl * refreshControl;
@property (strong, nonatomic) InfiniteScrollActivityView * loadingMoreView;
@property (strong, nonatomic) PQScrollHandler * handler;

@property (weak, nonatomic) Post * selectedPost;

@property (nonatomic) int currentRange;
@property (nonatomic) int currentPage;

@property (atomic) BOOL isEndOfResults;
@property (atomic) BOOL isMoreDataLoading;

@property (nonatomic, strong) NSOperationQueue* instagramQueue;

@end

static PQGridViewController * gridViewController = NULL;

@implementation PQGridViewController

+ (NSString *)segueIdentifier {
    return @"OpenGrid";
}

- (NSOperationQueue *)instagramQueue
{
    if (!_instagramQueue) {
        _instagramQueue = [[NSOperationQueue alloc] init];
        _instagramQueue.name = @"Instagram Queue";
        _instagramQueue.maxConcurrentOperationCount = 1;
    }
    return _instagramQueue;
}

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    gridViewController = self;
    
    // Do any additional setup after loading the view.
    
    self.handler = [[PQScrollHandler alloc] init];
    [self.handler setDelegate:self];
    self.currentRange = 200000;
    self.currentPage = 1;
    
    [self setupCollectionView];
    
    // Get location permissions
    [[PQLocationManager sharedInstance] setDelegate:self];
    CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
    if (authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse) {
//        [self requestInstagram];
        [[PQLocationManager sharedInstance].locationManager startUpdatingLocation];
    } else if (authorizationStatus == kCLAuthorizationStatusNotDetermined) {
        [[PQLocationManager sharedInstance].locationManager requestWhenInUseAuthorization];
    }
}

- (void) viewSwitched
{
    UIEdgeInsets insets = self.collectionView.contentInset;
    
    // TODO: fix me the proper way
    insets.top = 0; // hotfix: fix bug of, click on mini basketview then switch to favorites, switch back, grid view has top padding
    
    [self.collectionView setContentInset:insets];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [[PQLocationManager sharedInstance] setDelegate:self];
    
    [[PQPostManager sharedInstance] addPostMangerDelegate:self];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[PQPostManager sharedInstance] removePostManagerDelegate:self];
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) scrollToTop {
    if ([self.collectionView numberOfSections] > 0 &&
        [self.collectionView numberOfItemsInSection:0] > 0) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]
                                    atScrollPosition:UICollectionViewScrollPositionTop
                                            animated:YES];
    }
}

#pragma mark - Instagram

- (void)requestInstagram
{
    return;
    if (![[InstagramEngine sharedEngine] accessToken]) {
        if (![PQUserDefaults hasInstagramRequestBeenSeen]) {
            __weak PQGridViewController * weakSelf = self;
            
            [PQInstagramPermissionController showPermissionMessage:self withOkayBlock:^(UIAlertAction *action) {
                __block PQGridViewController * strongSelf = weakSelf;
                [[PQInstagramManager sharedInstance] logInFromViewController:strongSelf handler:^(BOOL success) {
                    if (success) {
                        NSBlockOperation * block = [NSBlockOperation blockOperationWithBlock:^{
                            [strongSelf loadInstagramPosts:nil];
                        }];
                        [strongSelf.instagramQueue addOperation:block];
                    }
                }];
            }];
            [PQUserDefaults setInstagramRequestSeen];
        }
    }
}

#pragma mark - Data Source

- (void)loadInitialPosts:(CLLocation *)location {
    if (!_isMoreDataLoading) {
        _isMoreDataLoading = YES;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [PQPostManager getPostsBasedOnCurrentLocation:location isRefresh:NO andGetMore:NO withCallback:^(BOOL success, NSArray<NSNumber *> *postIDs) {
            
            if (success) {
                [self setData:postIDs];
                [self.collectionView reloadData];
                _isMoreDataLoading = NO;
            }
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self checkForEmptyState];
        }];
    }
}

- (void) refreshControlAction:(UIRefreshControl *)refreshControl {
    [self refreshPosts:refreshControl withLocation:[PQLocationManager sharedInstance].location];
}

- (void)refreshPosts:(UIRefreshControl *)refreshControl withLocation:(CLLocation *)location {
    if (!_isMoreDataLoading) {
        _isMoreDataLoading = YES;
        NSArray * currentData = [self.data copy];
        [PQPostManager getPostsBasedOnCurrentLocation:location isRefresh:YES andGetMore:NO withCallback:^(BOOL success, NSArray<NSNumber *> *postIDs) {
            
            // figure out what are the new posts
            NSSet * before = [NSSet setWithArray:currentData];
            NSSet * after = [NSSet setWithArray:postIDs];
            
            PQLog(@"old locations: %@", before);
            PQLog(@"new locations: %@", after);
            
            NSMutableSet *toAdd = [NSMutableSet setWithSet:after];
            [toAdd minusSet:before];
            
            NSMutableArray *toAddArray = [NSMutableArray arrayWithArray:[toAdd allObjects]];
            
            NSMutableArray * indexPaths = [NSMutableArray array];
            for (int i = 0; i < [toAddArray count]; i++) {
                [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
            
            [self setData:postIDs];
            _isMoreDataLoading = NO;
            
            if ([indexPaths count] > 0) {
                [self.collectionView insertItemsAtIndexPaths:indexPaths];
            }
            [refreshControl endRefreshing];
        }];
        
    } else {
        [refreshControl endRefreshing];
    }
}

- (void)loadMorePosts:(CLLocation *)location {
    if (!_isMoreDataLoading) {
        PQLog(@"Load more posts");
        _isMoreDataLoading = YES;
        
        [PQPostManager getPostsBasedOnCurrentLocation:location isRefresh:NO andGetMore:YES withCallback:^(BOOL success, NSArray<NSNumber *> *postIDs) {
            _isMoreDataLoading = NO;
            if (self.isEndOfResults == YES) {
                [self loadMorePosts:self.location];
            } else {
                
                [self setData:postIDs];
                
                [self.loadingMoreView stopAnimating];
                
                [self.collectionView reloadData];
            }
        }];
    }
}

- (void)loadInstagramPosts:(NSString *)nextMaxLikeID
{
    return;
    CLLocation * location = self.location;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 250000, 250000);
    MKCoordinateRegion innerRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 0, 0);
    NSDictionary * options = @{
                               @"next_max_like_id" : nextMaxLikeID ?: [NSNull null]
                               };
    
    __weak PQGridViewController * weakSelf = self;
    [PQAPIs retrieveInstagramPostsForOuterRegion:viewRegion notInnerRegion:innerRegion withOptions:options withCallback:^(NSDictionary *postInfo, NSError *error) {
        __block PQGridViewController *strongSelf = weakSelf;
        if (postInfo) {
            NSArray * rawPosts = postInfo[@"instagram_posts"];
            NSMutableArray * posts = [NSMutableArray array];
            
            if ([rawPosts count]) {
                for (NSDictionary * instagramPost in rawPosts) {
                    InstagramMedia * instagramMedia = [[InstagramMedia alloc] initWithInfo:instagramPost];
                    PQInstagramPost * newPost = [[PQInstagramPost alloc] initWithMedia:instagramMedia];
                    [posts addObject:newPost.postId];
                    [[PQPostManager sharedInstance] addPost:newPost];
                }
                
                NSSet * before = [NSSet setWithArray:strongSelf.data];
                NSSet * after = [NSSet setWithArray:posts];
                
                NSMutableSet *toKeep = [before mutableCopy];
                NSMutableSet *toAdd = [after mutableCopy];
                [toAdd minusSet:toKeep];
                
                NSMutableArray * newPosts = [[toAdd allObjects] mutableCopy];
                [newPosts addObjectsFromArray:strongSelf.data];
                
                strongSelf.data = [newPosts copy];
                PQLog(@"%@", self.data);
                
                NSBlockOperation * block = [NSBlockOperation blockOperationWithBlock:^{
                    [strongSelf loadInstagramPosts:postInfo[kNextMaxLikeId]];
                }];
                
                [strongSelf.instagramQueue addOperation:block];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [strongSelf.collectionView reloadData];
                });
            }
        }
    }];
}

#pragma mark - UICollectionView Methods

- (void)setupCollectionView {
    _isMoreDataLoading = NO;
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    // Configure collection view
    [self.collectionView setPagingEnabled:NO];
    [self.collectionView setAlwaysBounceVertical:YES];
    
    [self.collectionView registerNib:[UINib nibWithNibName:[PQPostCollectionViewCell nibName] bundle:nil] forCellWithReuseIdentifier:[PQPostCollectionViewCell reuseIdentifier]];
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.backgroundView = nil;
    
    // Add refresh control
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshControlAction:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
    
    // Add infinite scroll spinner
    CGRect frame = CGRectMake(0, self.collectionView.contentSize.height, self.collectionView.bounds.size.width, kInfiniteScrollActivityViewDefaultHeight);
    self.loadingMoreView = [[InfiniteScrollActivityView alloc] initWithFrame:frame];
    self.loadingMoreView.hidden = YES;
    [self.collectionView addSubview:self.loadingMoreView];
    
    UIEdgeInsets insets = self.collectionView.contentInset;
    insets.bottom += kInfiniteScrollActivityViewDefaultHeight;
    self.collectionView.contentInset = insets;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize size = self.collectionView.frame.size;
    size.height = size.width / 1.5;
    return size;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.data.count > 0) {
        return self.data.count;
    } else {
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PQPostCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:[PQPostCollectionViewCell reuseIdentifier] forIndexPath:indexPath];
    cell.delegate = self;
    NSNumber * postID = self.data[indexPath.row];
    
    Post * post = [[PQPostManager sharedInstance] getPost:postID];
    
    if (!post) {
        PQLog(@"Post is nil");
    }
    cell.post = post;
    return cell;
}

- (void) checkForEmptyState
{
    BOOL isEmpty = self.data.count < 1;
    [self.collectionView setHidden:isEmpty];
    [self.emptyView setHidden:!isEmpty];
}

#pragma mark PQPostCollectionViewCellDelegate

- (void)openProfile:(double)userId
{
    PQLog(@"Profile opened");
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"TabProfile" bundle:nil];
    PQProfileViewController *profileVC = [storyBoard instantiateViewControllerWithIdentifier:[PQProfileViewController storyboardIdentifier]];
    profileVC.userId = userId;
    
    UINavigationController *navigationVC = [[UINavigationController alloc] initWithRootViewController:profileVC];
    [self presentViewController:navigationVC animated:YES completion:nil];
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!_isMoreDataLoading) {
        // Calculate the position of one screen length before the bottom of the results
        CGFloat scrollViewContentHeight = scrollView.contentSize.height;
        CGFloat scrollOffsetThreshold = scrollViewContentHeight - scrollView.bounds.size.height;
        
        // When the user has scrolled past the threshold, start requesting
        if(scrollView.contentOffset.y > scrollOffsetThreshold && scrollView.dragging) {
            // _isMoreDataLoading = true;
            
            // Update position of loadingMoreView, and start loading indicator
            CGRect frame = CGRectMake(0, scrollView.contentSize.height, scrollView.bounds.size.width, kInfiniteScrollActivityViewDefaultHeight);
            self.loadingMoreView.frame = frame;
            [self.loadingMoreView startAnimating];
            
            // Code to load more results
            [self loadMorePosts:self.location];
        }
    }
    
    [self.handler onViewScrolled:scrollView];
    
    if (self.delegate) {
        [self.delegate gridViewDidScrolled:scrollView];
    }
}

- (void) onScroll:(PQScrollDirection)direction
{
    switch (direction) {
        case UP:
            [PQMainTabBarController setTabBarHidden:YES];
            break;
        case DOWN:
            [PQMainTabBarController setTabBarHidden:NO];
            break;
    }
}

#pragma mark - PQLocationManagerDelegate protocol methods

- (void) locationManagerDidUpdateLocation:(NSArray *)locations {
    self.location = [locations lastObject];
    [self loadInitialPosts:[locations lastObject]];
    
    __weak PQGridViewController * weakSelf = self;
    NSBlockOperation * block = [NSBlockOperation blockOperationWithBlock:^{
        __block PQGridViewController* strongSelf = weakSelf;
        [strongSelf loadInstagramPosts:nil];
    }];
    [self.instagramQueue addOperation:block];
    
    [[PQLocationManager sharedInstance].locationManager stopUpdatingLocation];
}

- (void) locationManagerDidChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    PQLog(@"Location authorization status changed");
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
//        [self requestInstagram];
        [[PQLocationManager sharedInstance].locationManager startUpdatingLocation];
    } else if (status == kCLAuthorizationStatusDenied) {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please enable location permissions.\nSettings->Privacy->LocationServices" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

+ (void) showBasketView:(Post *)post {
    [gridViewController setSelectedPost:post];
    [gridViewController performSegueWithIdentifier:[PQBasketViewController segueIdentifier] sender:gridViewController];
}

#pragma mark Navigation


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController * destinationController = segue.destinationViewController;
    
    if ([destinationController isKindOfClass:[PQBasketViewController class]]) {
        PQBasketViewController * newPostView = (PQBasketViewController *) segue.destinationViewController;
        
        [newPostView setPostID:self.selectedPost.postId];
        
        [PQMainTabBarController setTabBarHidden:NO];
    }
}

#pragma mark PQPostManagerDelegate

- (void) bookmarkUpdated
{
    // notify grid view
    [self.collectionView reloadData];
}

@end
