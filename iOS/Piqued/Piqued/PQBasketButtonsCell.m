//
//  PQBasketButtonsCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 12/10/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQBasketButtonsCell.h"
#import "PQPostManager.h"

@interface PQBasketButtonsCell()

@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@property (weak, nonatomic) IBOutlet UIImageView *ballonsView;


@property (nonatomic) BOOL liked;

@end

@implementation PQBasketButtonsCell

+ (int) estimatedHeight {
    return 32;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self updateImageView];
    
    [self.contentView.superview setClipsToBounds:NO];
}

- (void) reloadData
{
    [self updateLiked];
}

- (IBAction)onComment:(id)sender {
    
    [self.basketDelegate showComment];
}

- (IBAction)onShareButtonPressed:(id)sender
{
    [self.basketDelegate shareButtonPressed];
}

- (IBAction)onLikePressed:(id)sender {
    
    [self setLiked:!self.liked];
    
    [self updateLikeButton];
    
    [self animateLikeButton];
    
    [self updatePostLiked];
    
    [self.basketDelegate setLike:self.liked];
}

#pragma mark Helper methods

- (void) updatePostLiked
{
    Post * post = [self.basketDelegate getPost];
    
    [[PQPostManager sharedInstance] setPostLiked:post isLiked:self.liked];
}

- (void) updateLiked
{
    Post * post = [self.basketDelegate getPost];
    
    [self setLiked:[[PQPostManager sharedInstance] isPostLiked:post]];
 
    [self updateLikeButton];
}

- (void) updateImageView
{
    
    NSMutableArray * images = [NSMutableArray array];
    for (int i = 0; i < 30; i++) {
        [images addObject:[UIImage imageNamed:[NSString stringWithFormat:@"balloons_%05d.png", i]]];
    }
    
    [self.ballonsView setAnimationImages:images];
    [self.ballonsView setAnimationDuration:1.0f];
    
    [self.ballonsView setAnimationRepeatCount:1];
    
}

- (void) animateLikeButton
{
    if (self.liked) {
        [self.ballonsView setHidden:NO];
        [self.ballonsView startAnimating];
    }
}

- (void) updateLikeButton
{
    NSString * imagenName = self.liked ? @"basketBalloonOn" : @"basketBalloonOff";
    
    [self.likeButton setImage:[UIImage imageNamed:imagenName] forState:UIControlStateNormal];
}


@end
