//
//  PQHashTagRecord.h
//  Piqued
//
//  Created by Kenny M. Liou on 6/11/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PQHashTagRecord : NSObject

@property (nonatomic, strong, readonly) NSString * hashTag;
@property (nonatomic, readonly) int usageCount;
@property (nonatomic, readonly) int viewedCount;
@property (nonatomic, readonly) NSTimeInterval lastUsedTime;
@property (nonatomic, readonly) NSTimeInterval lastViewedTime;

- (instancetype) initWithHashTag:(NSString *) hashTag;

- (void) viewed;
- (void) used;

@end
