//
//  PQLocationSearchOptionCell.m
//  Piqued
//
//  Created by Matt Mo on 2/3/18.
//  Copyright © 2018 First Brew. All rights reserved.
//

#import "PQLocationSearchOptionCell.h"

@implementation PQLocationSearchOptionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
