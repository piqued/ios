//
//  UIView+PIQHelper.h
//  Piqued
//
//  Created by Kenny M. Liou on 10/26/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (PIQHelper)

- (void) piq_setBorderColor:(UIColor *)color;
- (void) piq_setGradientBackgroundWithColorStart:(UIColor *) startColor end:(UIColor *) endColor;
- (void) piq_setGradientBackgroundWithColors:(NSArray *)colors start:(CGPoint) startPoint end:(CGPoint) endPoint;

@end
