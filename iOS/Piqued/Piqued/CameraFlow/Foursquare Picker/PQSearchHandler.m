//
//  PQSearchHandler.m
//  Piqued
//
//  Created by Matt Mo on 12/2/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import "PQSearchHandler.h"

@interface PQSearchHandler()

@property (nonatomic, strong) NSString *lastSearchQuery;
@property (nonatomic, assign) NSTimeInterval lastSearchTimeInterval;

@end

@implementation PQSearchHandler

- (void)scheduleSearch:(NSString *)query withMinimumLength:(NSUInteger)minLength
{
    PQLog(@"Query: %@", query);
    self.lastSearchQuery = query;
    
    // Query string is too short
    if (query.length < minLength) {
        return;
    }
    
    PQLog(@"self.lastSearchTimeInterval: %f", self.lastSearchTimeInterval);
    
    NSTimeInterval timeTillNextSearch = MAX(0.5 - ([[NSDate date] timeIntervalSince1970] - self.lastSearchTimeInterval), 0);
    
    PQLog(@"timeTillNextSearch: %f", timeTillNextSearch);
    
    if (timeTillNextSearch > 0) {
        @weakify(self, query);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(timeTillNextSearch * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            @strongify(self, query);
            PQLog(@"Comparing query: %@, with latest query: %@", query, self.lastSearchQuery);
            if (![query isEqualToString: self.lastSearchQuery]) {
                return;
            }
            [self performSearch:query];
        });
    } else {
        [self performSearch:query];
    }
}

- (void)performSearch:(NSString *)query
{
    self.lastSearchTimeInterval = [[NSDate date] timeIntervalSince1970];
    PQLog(@"Send request for query: %@", query);
    if (self.searchCallback != nil) {
        self.searchCallback(query);
    }
}

@end
