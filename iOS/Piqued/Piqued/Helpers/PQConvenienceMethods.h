//
//  PQConvenienceMethods.h
//  Piqued
//
//  Created by Matt Mo on 3/18/17.
//  Copyright © 2017 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+PQConvenience.h"

@interface PQConvenienceMethods : NSObject

BOOL piq_notNilOrNull(id object);
BOOL piq_deviceIsSimulator();

@end
