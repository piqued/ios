//
//  PQExplorerContainerViewController.h
//  Piqued
//
//  Created by Matt Mo on 10/27/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PQGridViewController.h"
#import "PQMapViewController.h"
#import "PQExplorerViewController.h"

@interface PQExplorerContainerViewController : UIViewController

@property (weak, nonatomic) PQExplorerViewController * explorerController;

+ (NSString *)segueIdentifier;
- (void)changeToViewWithSegueIdentifier:(NSString *)segueId;

@end
