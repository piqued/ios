//
//  PQFourSquarePickerViewController.m
//  Piqued
//
//  Created by Matt Mo on 3/5/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import "PQFourSquarePickerViewController.h"
#import "PQFourSquarePlaceTableViewCell.h"
#import "PQFourSquareAPI.h"
#import "PQFourSquareTableDataSource.h"
#import "PQLocationSearchDataSource.h"
#import "PQLocationManager.h"

NSString * const NewPostLocationTypeConstants[] = {
    [NewPostLocationTypeUser] = @"current location",
    [NewPostLocationTypePhoto] = @"photo location",
};

@interface PQFourSquarePickerViewController() <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, PQLocationSearchDataSourceDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *foursquareSearchBar;
@property (weak, nonatomic) IBOutlet UISearchBar *locationSearchBar;
@property (weak, nonatomic) IBOutlet UITableView *foursquarePlaceTableView;
@property (weak, nonatomic) IBOutlet UITableView *locationTableView;

@property (nonatomic) NewPostLocationType locationType;
@property (strong, nonatomic) PQFourSquareTableDataSource *fourSquareDataSource;

@property (strong, nonatomic) PQLocationSearchDataSource *locationSearchDataSource;

@property NSTimeInterval lastSearchTimeInterval;

@property NSString *lastFoursquareSearchTextDidChangeQuery;
@property NSString *lastLocationSearchTextDidChangeQuery;

@end

@implementation PQFourSquarePickerViewController

+ (NSString *)segueIdentifier
{
    return @"PickFourSquarePlace";
}

- (PQFourSquareTableDataSource *)fourSquareDataSource
{
    if (_fourSquareDataSource == nil) {
        _fourSquareDataSource = [[PQFourSquareTableDataSource alloc] init];
    }
    return _fourSquareDataSource;
}

- (PQLocationSearchDataSource *)locationSearchDataSource
{
    if (_locationSearchDataSource == nil) {
        _locationSearchDataSource = [[PQLocationSearchDataSource alloc] init];
        _locationSearchDataSource.delegate = self;
    }
    return _locationSearchDataSource;
}

- (void)viewDidLoad
{
    self.fourSquareDataSource.searchBar = self.foursquareSearchBar;
    self.locationSearchDataSource.searchBar = self.locationSearchBar;
    [self.locationSearchBar setImage:[UIImage imageNamed:@"location"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    
    [self configureSearchBar];
    
    [self.fourSquareDataSource setupTableView:self.foursquarePlaceTableView];
    self.foursquarePlaceTableView.dataSource = self;
    self.foursquarePlaceTableView.delegate = self;
    
    [self.foursquarePlaceTableView setRowHeight:UITableViewAutomaticDimension];
    [self.foursquarePlaceTableView setEstimatedRowHeight:100]; // this is some random number
    
    self.foursquareSearchBar.delegate = self;
    self.locationSearchBar.delegate = self;

    [self.locationSearchDataSource setupTableView:self.locationTableView];
    self.locationTableView.dataSource = self;
    self.locationTableView.delegate = self;
    
    [self.locationTableView setRowHeight:UITableViewAutomaticDimension];
    [self.locationTableView setEstimatedRowHeight:50];
}

- (void)configureSearchBar
{
    NewPostLocationType locationType = self.locationSearchDataSource.currentLocationType;
    switch (locationType) {
        case NewPostLocationTypePhoto:
        {
            self.locationSearchBar.placeholder = NewPostLocationTypeConstants[locationType];
            break;
        }
        default:
        {
            self.locationSearchBar.placeholder = NewPostLocationTypeConstants[locationType];
            break;
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.foursquarePlaceTableView) {
        return [self.fourSquareDataSource numberOfSectionsInTableView:tableView];
    }
    
    if (tableView == self.locationTableView) {
        return [self.locationSearchDataSource numberOfSectionsInTableView:tableView];
    }
    
    PQLog(@"TableView not supported with custom data source");
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.foursquarePlaceTableView) {
        return [self.fourSquareDataSource tableView:tableView numberOfRowsInSection:section];
    }
    
    if (tableView == self.locationTableView) {
        return [self.locationSearchDataSource tableView:tableView numberOfRowsInSection:section];
    }
    
    PQLog(@"TableView not supported with custom data source");
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (tableView == self.foursquarePlaceTableView) {
        return [self.fourSquareDataSource tableView:tableView cellForRowAtIndexPath:indexPath];
    }
    
    if (tableView == self.locationTableView) {
        return [self.locationSearchDataSource tableView:tableView cellForRowAtIndexPath:indexPath];
    }
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] init];
        PQLog(@"TableView not supported with custom data source");
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.foursquarePlaceTableView) {
        [self.delegate handleFourSquarePlacePicked:self.fourSquareDataSource[indexPath.row]];
        [self.navigationController popViewControllerAnimated:YES];
    } else if (tableView == self.locationTableView) {
        [self.locationSearchDataSource tableView:tableView didSelectRowAtIndexPath:indexPath];
    } else {
        PQLog(@"TableView not supported");
    }
}

- (IBAction)cancelButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadWithInitialResults:(NSArray *)data location:(CLLocation *)location locationType:(NewPostLocationType)type
{
    [self.fourSquareDataSource loadWithInitialResults:data location:location];
    
    NSMutableArray *availableLocationTypes = [NSMutableArray array];
    [availableLocationTypes addObject:@(NewPostLocationTypeUser)];
    self.locationSearchDataSource.currentLocationType = type;
    
    switch (self.locationSearchDataSource.currentLocationType) {
        case NewPostLocationTypePhoto: {
            self.photoLocation = location;
            [availableLocationTypes addObject:@(NewPostLocationTypePhoto)];
            break;
        }
        default: {
            self.location = PQLocationManager.sharedInstance.location;
            break;
        }
    }
    
    self.locationSearchDataSource.availableLocationTypes = [availableLocationTypes copy];
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchBar == self.foursquareSearchBar) {
        self.foursquarePlaceTableView.hidden = NO;
        self.locationTableView.hidden = YES;
        [self.fourSquareDataSource searchBar:searchBar textDidChange:searchText];
    } else if (searchBar == self.locationSearchBar) {
        self.foursquarePlaceTableView.hidden = YES;
        self.locationTableView.hidden = NO;
        [self.locationSearchDataSource searchBar:searchBar textDidChange:searchText];
    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if (searchBar == self.locationSearchBar) {
        self.foursquarePlaceTableView.hidden = YES;
        self.locationTableView.hidden = NO;
    } else if (searchBar == self.foursquareSearchBar) {
        self.foursquarePlaceTableView.hidden = NO;
        self.locationTableView.hidden = YES;
    }
    return YES;
}

#pragma mark - PQLocationSearchDataSourceDelegate

- (void)locationDataSource:(PQLocationSearchDataSource *)dataSource didSelectNewLocation:(CLLocation *)location withPlaceName:(NSString *)placeName
{
    self.locationSearchDataSource.currentLocationType = NewPostLocationTypeCustom;
    self.locationSearchBar.text = placeName;
    [self.foursquareSearchBar becomeFirstResponder];
    self.foursquarePlaceTableView.hidden = NO;
    self.locationTableView.hidden = YES;
    [self.fourSquareDataSource updateSearchResultsWithPlaceName:placeName];
}

- (void)locationDataSource:(PQLocationSearchDataSource *)dataSource didSelectLocationType:(NewPostLocationType)locationType
{
    [self configureSearchBar];
    switch (locationType) {
        case NewPostLocationTypePhoto: {
            [self.fourSquareDataSource updateSearchResultsWithLocation:self.photoLocation];
            break;
        }
        default: {
            [self.fourSquareDataSource updateSearchResultsWithLocation:PQLocationManager.sharedInstance.location];
            break;
        }
    }
    self.foursquarePlaceTableView.hidden = NO;
    self.locationTableView.hidden = YES;
    [self.foursquareSearchBar becomeFirstResponder];
}

@end
