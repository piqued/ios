//
//  PQMapViewGestureRecognizer.h
//  Piqued
//
//  Created by Kenny M. Liou on 5/7/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PQMapViewGestureRecognizerDelegate <NSObject>

- (void) onMapViewTouchBegan;

@end

@interface PQMapViewGestureRecognizer : UIGestureRecognizer

@property (weak, nonatomic) id<PQMapViewGestureRecognizerDelegate> pqDelegate;

@end
