//
//  PQActivityBaseCell.h
//  Piqued
//
//  Created by Kenny M. Liou on 5/21/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PQActivityBaseCellDelegate <NSObject>

@required

- (void) openBasketViewWithID:(NSNumber *) postID withComment:(BOOL) openComment;
- (void) openProfile:(double)userId;

@end

@protocol PQActivityBaseCellProtocol

@required

- (void) refreshData;

@end

@interface PQActivityBaseCell : UICollectionViewCell

@property (weak, nonatomic) id<PQActivityBaseCellDelegate> delegate;

@end
