//
//  PQMapViewController.m
//  Piqued
//
//  Created by Matt Mo on 10/27/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQMapViewController.h"
#import "PQPostManager.h"
#import "PQClusterAnnotationView.h"
#import "PQMapAnnotation.h"
#import "PQExplorerViewController.h"
#import "PQClusterPreviewViewController.h"
#import "PQMiniBasketViewController.h"
#import "PQMapViewGestureRecognizer.h"
#import <SDWebImage/SDWebImageManager.h>
#import "PQImageURLHelper.h"
#import "PQInstagramPost.h"
#import <InstagramKit/InstagramKit.h>
#import "PQMainTabBarController.h"
#import "PQImageCacheDelegate.h"

@interface PQMapViewController () <PQMapViewGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *miniBasketContainerView;
@property (weak, nonatomic) IBOutlet UIButton *currentLocationButton;

@property (strong, nonatomic) CLLocation * location;
@property (strong, nonatomic) FBClusteringManager* clusteringManager;
@property (strong, nonatomic) PQMiniBasketViewController *miniBasketViewController;

@property (strong, nonatomic) NSOperationQueue * mapQueue;
@property (strong, nonatomic) NSOperationQueue * instagramQueue;
@property (strong, nonatomic) NSBlockOperation * lastRequestOp;
@property (strong, nonatomic) NSBlockOperation * lastClusteringOp;

@property (strong, atomic) NSSet * addedAnnotationsSet;

@property (nonatomic) BOOL miniBasketIsAnimating;
@property BOOL userLocationUpdated;

@end

@implementation PQMapViewController

+ (NSString *)segueIdentifier {
    return @"OpenMap";
}

#pragma mark Lazy Initializers

- (NSOperationQueue *)mapQueue
{
    return !_mapQueue ? _mapQueue =
    ({
        NSOperationQueue * queue = [[NSOperationQueue alloc] init];
        queue.name = @"Map Queue";
        queue.maxConcurrentOperationCount = 1;
        queue;
    }) : _mapQueue;
}

- (NSOperationQueue *)instagramQueue
{
    return !_instagramQueue ? _instagramQueue =
    ({
        NSOperationQueue * queue = [[NSOperationQueue alloc] init];
        queue.name = @"Instagram Queue";
        queue.maxConcurrentOperationCount = 1;
        queue;
    }) : _instagramQueue;
}

#pragma mark View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.addedAnnotationsSet = [NSSet set];
    // Do any additional setup after loading the view.
    
    self.clusteringManager = [[FBClusteringManager alloc] initWithAnnotations:nil];
    self.clusteringManager.delegate = self;
    
    [self setupMapView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[PQLocationManager sharedInstance] setDelegate:self];
    [[PQLocationManager sharedInstance].locationManager startUpdatingLocation];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[PQLocationManager sharedInstance] setDelegate:self];
    [[PQLocationManager sharedInstance].locationManager stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Interactions

- (IBAction)currentLocationButtonPressed:(id)sender {
    [self.mapView setCenterCoordinate:self.mapView.userLocation.location.coordinate animated:YES];
}

#pragma mark - Helpers

- (void)onMapViewTouchBegan
{
    [self showMiniBasket:NO];
}

- (void)setupMapView {
    self.mapView.delegate = self;
    
    [[PQLocationManager sharedInstance] setDelegate:self];
    [[PQLocationManager sharedInstance].locationManager startUpdatingLocation];
    [self.mapView setShowsUserLocation:YES];
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance([PQLocationManager sharedInstance].location.coordinate, 5000, 5000);
    [self.mapView setRegion:viewRegion animated:YES];
    
    PQMapViewGestureRecognizer * recognizer = [[PQMapViewGestureRecognizer alloc] init];
    [recognizer setPqDelegate:self];
    [self.mapView addGestureRecognizer: recognizer];
}

- (void)updatePins:(MKMapView*) mapView {
    NSBlockOperation* operation = [[NSBlockOperation alloc] init];
    __weak NSBlockOperation * weakOp = operation;
    __weak PQMapViewController *weakSelf = self;
    __block MKMapView *blockMapView = mapView;
    [operation addExecutionBlock:^{
        if ([weakOp isCancelled]) {
            PQLog(@"Download cancelled");
            return;
        }
        
        // Set up parameters for retrieving posts for region
        NSDictionary * options = @{
                                   @"page" : @1,
                                   @"size" : @100
                                   };
        
        // The equivalent of a nil value for MKCoordinateRegion
        MKCoordinateRegion innerRegion = MKCoordinateRegionMake(blockMapView.region.center, MKCoordinateSpanMake(0, 0));
        
        MKCoordinateRegion outerRegion = MKCoordinateRegionMake(blockMapView.region.center, MKCoordinateSpanMake(blockMapView.region.span.latitudeDelta * 3, blockMapView.region.span.longitudeDelta * 3));
        
        __block PQMapViewController * strongSelf = weakSelf;
        [PQAPIs retrievePostsForOuterRegion:outerRegion notInnerRegion:innerRegion withOptions:options withCallback:^(NSDictionary *postInfo, NSError *error) {
            if (postInfo) {
                NSMutableArray * newAnnotations = [NSMutableArray array];
                
                // Results are indexed by location/coordinates
                NSArray * locationPostResults = [postInfo objectForKey:@"posts"];
                
                for (NSDictionary* postResult in locationPostResults) {
                    Post * newPost = [Post modelWithDictionary:postResult];
                    [[PQPostManager sharedInstance] addPost:newPost];
                    
                    NSString *imageUrlString = newPost.imageUrlSmall;
                    if (newPost.imageUrlSmall.length > 0) {
                        imageUrlString = newPost.imageUrlSmall;
                    }
                    CLLocationCoordinate2D coords = CLLocationCoordinate2DMake(newPost.lat, newPost.lng);
                    PQMapAnnotation * newAnnotation = [[PQMapAnnotation alloc] initWithCoordinates:coords postID:newPost.postId imageURL:imageUrlString numNeighbors:0];
                    [newAnnotations addObject:newAnnotation];
                }
                
                // Detect duplicates in retrieved posts
                NSSet * before = strongSelf.addedAnnotationsSet;
                NSSet * after = [NSSet setWithArray:newAnnotations];
                
                NSMutableSet *toKeep = [before mutableCopy];
                NSMutableSet *toAdd = [after mutableCopy];
                [toAdd minusSet:toKeep];
                NSMutableSet *toRemove = [before mutableCopy];
                [toRemove minusSet:after];
                
                [toKeep addObjectsFromArray:newAnnotations];
                strongSelf.addedAnnotationsSet = [toKeep copy];
                
                __block NSArray * annotationsToAdd = [toAdd allObjects];
                
                if ([annotationsToAdd count] > 0) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [strongSelf.clusteringManager addAnnotations:annotationsToAdd];
                        [strongSelf clusterAnnotations:strongSelf.mapView];
                    });
                }
            }
        }];
    }];
    
    [self.lastRequestOp cancel];
    self.lastRequestOp = operation;
    
    // Wait 0.35 seconds to see if the user is still panning
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.35 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        PQLog(@"self.mapQueue: %@", self.mapQueue.operations);
        [self.mapQueue addOperation:operation];
    });
}

- (void)updateInstagram:(MKMapView *)mapView withPagination:(NSString*)nextMaxLikeID
{
    return;
    NSBlockOperation* operation = [[NSBlockOperation alloc] init];
    __weak NSBlockOperation * weakOp = operation;
    __weak PQMapViewController *weakSelf = self;
    __block MKMapView *blockMapView = mapView;
    
    [operation addExecutionBlock:^{
        if ([weakOp isCancelled]) {
            PQLog(@"Download cancelled");
            return;
        }
        
        __block PQMapViewController *strongSelf = weakSelf;
        CLLocationCoordinate2D coordinate = blockMapView.centerCoordinate;
        
        MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 50000, 50000);
        MKCoordinateRegion innerRegion = MKCoordinateRegionMakeWithDistance(coordinate, 0, 0);
        NSDictionary * options = @{
                                   @"next_max_like_id" : nextMaxLikeID ?: [NSNull null]
                                   };
        
        [PQAPIs retrieveInstagramPostsForOuterRegion:viewRegion notInnerRegion:innerRegion withOptions:options withCallback:^(NSDictionary *postInfo, NSError *error) {
            if (postInfo) {
                NSArray * rawPosts = postInfo[@"instagram_posts"];
                NSMutableArray * newAnnotations = [NSMutableArray array];
                
                if ([rawPosts count]) {
                    for (NSDictionary * instagramPost in rawPosts) {
                        InstagramMedia * instagramMedia = [[InstagramMedia alloc] initWithInfo:instagramPost];
                        PQInstagramPost * newPost = [[PQInstagramPost alloc] initWithMedia:instagramMedia];
                        
                        CLLocationCoordinate2D coords = CLLocationCoordinate2DMake(instagramMedia.location.latitude, instagramMedia.location.longitude);
                        
                        PQMapAnnotation * newAnnotation = [[PQMapAnnotation alloc] initWithCoordinates:coords postID:newPost.postId imageURL:newPost.imageUrl numNeighbors:0];
                        [newAnnotations addObject:newAnnotation];
                        
                        [[PQPostManager sharedInstance] addPost:newPost];
                    }
                    
                    // Detect duplicates in retrieved posts
                    NSSet * before = strongSelf.addedAnnotationsSet;
                    NSSet * after = [NSSet setWithArray:newAnnotations];
                    
                    NSMutableSet *toKeep = [before mutableCopy];
                    NSMutableSet *toAdd = [after mutableCopy];
                    [toAdd minusSet:toKeep];
                    NSMutableSet *toRemove = [before mutableCopy];
                    [toRemove minusSet:after];
                    
                    [toKeep addObjectsFromArray:newAnnotations];
                    strongSelf.addedAnnotationsSet = [toKeep copy];
                    
                    __block NSArray * annotationsToAdd = [toAdd allObjects];
                    
                    [strongSelf updateInstagram:blockMapView withPagination:postInfo[kNextMaxLikeId]];
                    
                    if ([annotationsToAdd count] > 0) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [strongSelf.clusteringManager addAnnotations:annotationsToAdd];
                            [strongSelf clusterAnnotations:strongSelf.mapView];
                        });
                    }
                }
            }
        }];
    }];
    
    [self.instagramQueue addOperation:operation];
}

- (void)handleLocationUpdate: (CLLocation *)location {
    if(!self.userLocationUpdated) {
        self.userLocationUpdated = YES;
    }
}

- (UIImage*)getSymbolForImage:(UIImage*)image {
    double maxRadius = (MIN(image.size.width, 70) < 70) ? 70: 70;
    CALayer* imgLayer = [CALayer layer];
    imgLayer.frame = CGRectMake(0,0, maxRadius, maxRadius);
    imgLayer.contents = (id)image.CGImage;
    imgLayer.masksToBounds = YES;
    imgLayer.cornerRadius = maxRadius / 2;
    imgLayer.borderWidth = 2.0f;
    imgLayer.borderColor = [UIColor whiteColor].CGColor;
    CGSize imgSize = CGSizeMake(maxRadius, maxRadius);
    UIGraphicsBeginImageContext(imgSize);
    [imgLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage* newImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(newImg.size, NO, 0.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, newImg.size.width, newImg.size.height);
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    CGContextDrawImage(ctx, area, newImg.CGImage);
    UIImage *pinImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return pinImage;
}

- (void)addBounceAnimationToView:(UIView *)view
{
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    bounceAnimation.values = @[@(0.05), @(1.1), @(0.9), @(1)];
    
    bounceAnimation.duration = 0.6;
    NSMutableArray *timingFunctions = [[NSMutableArray alloc] initWithCapacity:bounceAnimation.values.count];
    for (NSUInteger i = 0; i < bounceAnimation.values.count; i++) {
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    }
    [bounceAnimation setTimingFunctions:timingFunctions.copy];
    bounceAnimation.removedOnCompletion = NO;
    
    [view.layer addAnimation:bounceAnimation forKey:@"bounce"];
}

#pragma mark - FBAnnotationClustering helpers
- (void)clusterAnnotations:(MKMapView *)mapView {
    CGFloat mapViewWidth = self.mapView.bounds.size.width;
    NSBlockOperation * clusterOp = [[NSBlockOperation alloc] init];
    __weak NSBlockOperation * weakOp = clusterOp;
    [clusterOp addExecutionBlock:^{
        if ([weakOp isCancelled]) {
            PQLog(@"Clustering Op is cancelled.");
            return;
        }
        
        double scale = mapViewWidth / self.mapView.visibleMapRect.size.width;
        NSArray *annotations = [self.clusteringManager clusteredAnnotationsWithinMapRect:mapView.visibleMapRect withZoomScale:scale];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.clusteringManager displayAnnotations:annotations onMapView:mapView];
        });
    }];
    [self.lastClusteringOp cancel];
    self.lastClusteringOp = clusterOp;
    [[NSOperationQueue new] addOperation:clusterOp];
}

#pragma mark - MKMapViewDelegate
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    [self updatePins:mapView];
//    [self updateInstagram:mapView withPagination:nil];
    [self clusterAnnotations:self.mapView];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if (annotation == mapView.userLocation)
    {
        return nil;
    } else if ([annotation isKindOfClass:[FBAnnotationCluster class]]) {
        FBAnnotationCluster *cluster = (FBAnnotationCluster *)annotation;
        //PQLog(@"Annotation is cluster. Number of annotations in cluster: %lu", (unsigned long)cluster.annotations.count);
        
        static NSString *const PQClusterAnnotationViewReuseID = @"PQClusterAnnotationViewReuseID";
        
        PQMapAnnotation *pqannotation = cluster.annotations[0];
        
        PQClusterAnnotationView *annotationView = (PQClusterAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:PQClusterAnnotationViewReuseID];
        
        if (!annotationView) {
            annotationView = [[PQClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:PQClusterAnnotationViewReuseID];
        }
        
        annotationView.count = cluster.annotations.count;
        
        NSString * thumbnailURL = [PQImageURLHelper adjustImageURLString:pqannotation.imageURL toSize:PQImageSizeSmall];
        annotationView.imageURL = [NSURL URLWithString:thumbnailURL];
        
        return annotationView;
    } else if ([annotation isKindOfClass:[PQMapAnnotation class]]) {
        PQMapAnnotation * pqannotation = (PQMapAnnotation *)annotation;
        //PQLog(@"Annotation is PQMapAnnotation.");
        
        static NSString *const PQAnnotationViewReuseID = @"PQAnnotationViewReuseID";
        
        MKAnnotationView *annotationView = [self.mapView dequeueReusableAnnotationViewWithIdentifier:PQAnnotationViewReuseID];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:PQAnnotationViewReuseID];
        }
        
        if ([pqannotation.imageURL length] == 0) {
            annotationView.image = [UIImage imageNamed:@"mapBlankImageCircle"];
        } else {
            NSString * thumbnailURL = [PQImageURLHelper adjustImageURLString:pqannotation.imageURL toSize:PQImageSizeMedium];
            
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:[NSURL URLWithString:thumbnailURL]
                                  options:0
                                 progress:nil
                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
             {
                 if (image && finished && [imageURL.absoluteString isEqualToString:thumbnailURL]) {
                     annotationView.image = [self getSymbolForImage:image];
                 } else {
                     annotationView.image = [UIImage imageNamed:@"mapBlankImageCircle"];
                 }
             }];
        }
        
        return annotationView;
    } else {
        return nil;
    }
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    for (UIView *view in views) {
        [self addBounceAnimationToView:view];
    }
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if ([view.annotation isKindOfClass:[PQMapAnnotation class]]) {
        PQMapAnnotation * pqannotation = view.annotation;
        [self.miniBasketViewController setPostId:pqannotation.postID.doubleValue];
        [self showMiniBasket:YES];
    } else if ([view.annotation isKindOfClass:[FBAnnotationCluster class]]) {
        [self performSegueWithIdentifier:[PQClusterPreviewViewController segueIdentifier] sender:view.annotation];
    }
    [mapView deselectAnnotation:view.annotation animated:NO];
}

- (void)showMiniBasket:(BOOL)show {
    if ((![self.miniBasketContainerView isHidden] != show)
        && !self.miniBasketIsAnimating) {
        [self.delegate prepareForMiniBasket:show];
        [self setMiniBasketWithAnimation:show];
    }
}

- (void)setMiniBasketWithAnimation:(BOOL)show {
    if ((![self.miniBasketContainerView isHidden] != show)
         && !self.miniBasketIsAnimating) {
        self.miniBasketIsAnimating = YES;
        
        // only perform animation when basket is not the same status as the argument
        int viewHeight = self.view.frame.size.height;
        CGRect startFrame = self.miniBasketContainerView.frame;
        CGRect endFrame = self.miniBasketContainerView.frame;
        int height = self.miniBasketContainerView.frame.size.height;
        int tabBarHeight = 0;
        if (@available(iOS 11.0, *)) {
            tabBarHeight = self.tabBarController.tabBar.frame.size.height - self.tabBarController.tabBar.safeAreaInsets.bottom;
        } else {
            // Fallback on earlier versions
            tabBarHeight = self.tabBarController.tabBar.frame.size.height;
        }
        
        if (show) {
            // show basket container
            startFrame.origin.y = viewHeight;
            endFrame.origin.y = viewHeight - (height + tabBarHeight);
        } else {
            // hide basket container
            startFrame.origin.y = viewHeight - (height + tabBarHeight);
            endFrame.origin.y = viewHeight;
        }
        
        [self.miniBasketContainerView setFrame:startFrame];
        
        if (show) {
            [self.miniBasketContainerView setHidden:NO];
        }
        
        [UIView animateWithDuration:0.2f animations:^{
            [self.miniBasketContainerView setFrame:endFrame];
            
        } completion:^(BOOL finished) {
            if (finished) {
                if (!show) {
                    [self.miniBasketContainerView setHidden:YES];
                    [self.miniBasketContainerView setFrame:startFrame];
                }
                
                self.miniBasketIsAnimating = NO;
            }
        }];
    }
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:[PQMiniBasketViewController segueIdentifier]]) {
        self.miniBasketViewController = segue.destinationViewController;
    } else if ([segue.identifier isEqualToString:[PQClusterPreviewViewController segueIdentifier]]) {
        if ([segue.destinationViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController * navController = (UINavigationController *)segue.destinationViewController;
            if ([navController.viewControllers.firstObject isKindOfClass:[PQClusterPreviewViewController class]]) {
                PQClusterPreviewViewController * controller = (PQClusterPreviewViewController *)navController.viewControllers.firstObject;
                
                if ([sender isKindOfClass:[FBAnnotationCluster class]]) {
                    FBAnnotationCluster *cluster = (FBAnnotationCluster *)sender;
                    NSMutableArray * postIDs = [NSMutableArray array];
                    for (PQMapAnnotation * pqannotation in cluster.annotations) {
                        [postIDs addObject:pqannotation.postID];
                    }
                    [controller setPosts:postIDs];
                }
            }
        }
    }
}

#pragma mark - PQLocationManager
- (void)locationManagerDidUpdateLocation:(NSArray *)locations {
    self.location = [locations lastObject];
    [self handleLocationUpdate:self.location];
    [[PQLocationManager sharedInstance].locationManager stopUpdatingLocation];
}

- (void)locationManagerDidFailWithError:(NSError *)error {
    
}

- (void)locationManagerDidChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
}

#pragma mark - FBClusterManager delegate - optional

- (CGFloat)cellSizeFactorForCoordinator:(FBClusteringManager *)coordinator
{
    return 1.5;
}

@end
