//
//  PQCameraCollectionViewController.h
//  Piqued
//
//  Created by Kenny M. Liou on 2/27/16.
//  Copyright © 2016 First Brew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PQImageHandler.h"

@interface PQCameraCollectionViewController : NSObject <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) id<PQImageHandler> handler;

+ (NSString *) getCellName;
+ (NSString *) getEmptyCellName;

- (void) start;
- (void) stop;
- (void) takePhoto;

@end
