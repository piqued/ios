//
//  PQBookmarkCell.m
//  Piqued
//
//  Created by Kenny M. Liou on 11/7/15.
//  Copyright © 2015 First Brew. All rights reserved.
//

#import "PQBookmarkCell.h"
#import "PQMath.h"
#import "PQImageURLHelper.h"
#import "PQPostManager.h"
#import <SDWebImage/UIImageView+WebCache.h>

static NSString * const kBookmarkImageName = @"bookmarkOn";
static NSString * const kBookmarkImageNameOff = @"bookmarkOff";
static NSString * const kDistanceUnit = @"%.2lf";
static NSString * const kDistanceLabel = @"%@mi distance";


@interface PQBookmarkCell()

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;

@property (weak, nonatomic) IBOutlet UIView *distanceShadow;
@property (weak, nonatomic) IBOutlet UILabel *distanceView;

@property (weak, nonatomic) IBOutlet UIView *titleShadow;
@property (weak, nonatomic) IBOutlet UILabel *titleView;

@property (weak, nonatomic) IBOutlet UIView *addressShadow;
@property (weak, nonatomic) IBOutlet UILabel *subTitleView;
@property (weak, nonatomic) IBOutlet UIImageView *bookmarkImage;

@end

@implementation PQBookmarkCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setPost:(Post *)post
{
    if (post != nil) {
        NSString * thumbnailURL = [PQImageURLHelper adjustImageURLString:post.imageUrlSmall toSize:PQImageSizeSmall];
        [self updateImage:[NSURL URLWithString:thumbnailURL]];
        [self updateLabel:self.titleView withTitle:post.desc withShadowView:self.titleShadow];
        
        if (post.title != NULL && ![post.title isEqualToString:@""]) {
            [self updateLabel:self.subTitleView withTitle:post.title withShadowView:self.addressShadow];
        } else {
            [self updateLabel:self.subTitleView withTitle:post.address withShadowView:self.addressShadow];
        }
        [self setDistanceLabel:[PQMath getMiles:post]];
        [self.bookmarkImage setBackgroundColor:[UIColor clearColor]];
        
        NSArray * bookmarks = [[PQPostManager sharedInstance] getBookmarkIds];
        
        if ([bookmarks containsObject:post.postId]) {
            [self.bookmarkImage setImage:[UIImage imageNamed:kBookmarkImageName]];
        } else {
            [self.bookmarkImage setImage:[UIImage imageNamed:kBookmarkImageNameOff]];
        }
    }
}

- (void) updateImage:(NSURL *)imageUrl
{
    [self.mainImageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.mainImageView sd_setImageWithURL:imageUrl];
}

- (void) updateLabel:(UILabel *)label withTitle:(NSString *)title withShadowView:(UIView *) shadowView
{
    [shadowView setHidden:YES];
    [label setHidden:NO];
    [label setText:title];
}

- (void) updateLabel:(UILabel *)label withAttributedTitle:(NSAttributedString *)title withShadowView:(UIView *)shadowView
{
    [shadowView setHidden:YES];
    [label setHidden:NO];
    [label setAttributedText:title];
}

- (void) setDistanceLabel:(float) distance
{
    NSString * numberString = @"0";
    if (distance > 0.009f) {
        numberString = [NSString stringWithFormat:kDistanceUnit, distance];
    }
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:13.0];
    NSString * labelString = [NSString stringWithFormat:kDistanceLabel, numberString];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:labelString];
    
    [attrString addAttribute:NSFontAttributeName value:font range:NSMakeRange(labelString.length - 8, 8)];
    
    [self updateLabel:self.distanceView withAttributedTitle:attrString withShadowView:self.distanceShadow];
}

@end
